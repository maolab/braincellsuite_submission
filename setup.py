import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="brainCellSuite",
    version="0.1.1",  # Don't forget to update in braincellsuite/__init__.py
    author="Bart C Jongbloets",
    author_email="b.c.jongbloets@gmail.com",
    description="Package for importing, wrangling, analyzing, and "
                "visualization of brain cell data (electrophysiology, "
                "morphology, imaging data). in development",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/maolab/braincellsuite-dev",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GPL3 License",
        "Operating System :: OS Independent",
    ],
    install_requires=['plotly>= 3.5.0',
                      'nested-lookup>=0.2.18',
                      'xmltodict>=0.11.0',
                      'pandas>=0.23.4',
                      'numpy>=1.15.3',
                      'scipy>=1.2.0',
                      'matplotlib>=3.1.2',
                      'Pillow>=5.4.1',
                      'seaborn>=0.9.0',
                      'dash>=2.5.1',
                      'pyqrcode>=1.2.1',
                      'scikit_learn>=0.20.3',
                      'scikit_image>=0.15.0',
                      'tables>=3.5.2',
                      'pylmeasure>=0.2.0'
                      ]
)
