"""Test whether filenames are correct original tracings of neurons within one
slice .DAT file.

.DAT files: cellID_amplifierID_slideID_animalID_sliceID_status_
tracerInitials.DAT
For which multiple neurons within one slice will be named within cellID
and amplifierID, and consecutive analysis by different observers will be
named in initials. dashes between items mark different items, underscores
different ID/status/initials.
Example:
BJ1554-BJ1555-BJ1555-BJ1556_a2-a1-a2-a1_MSaa00023_Ai9PV-0023_B3_traced_
TJL.dat
This file contains 4 neurons from slice B3, recorded from either
amplifier 1 or 2 (a1 or a2),

individual neurons
.SWC files:
cellID_amplifierID_slideID_animalID_sliceID_status_tracerInitials.SWC
can only include 1 neuron
Example:
BJ1554_a2_MSaa00023_Ai9PV-0023_B3_traced_TJL.swc
This file only contains BJ1554 recorded with amplifier 2

Functions:
    - checkFilename: Check filename based on convention.
    - crossCheckFilenameDB: Function cross checks a given filename with
                            corresponding information in the database.
    - manualRename: Allows user to input newFilename and save the changes in
                    renameLog.csv.
    - renamingLog: Logs renaming of files.
    - renameFile: Rename swc files.
    - executeFileCheck: Batch run swc-file check.
"""
import re
import warnings
import pandas as pd
import os
import datetime
import shutil
from brainCellSuite._io.readDBCSV import readDBCSV
from brainCellSuite import bcs_global

__version__ = '0.1'
__date__ = '2018-07-24'
__author__ = 'Bart C Jongbloets'


def checkFilename(filename, morphDataPath, singleCell):
    """Check filename based on convention.

    Function tests whether at the basal level a given filename
    follows the template

    Args:
        - filename: name of the morphology file (.swc or .dat)
        - morphDataPath: fullpath to folder containing the filename. Or empty
                         if you know for sure the filename is compliant to the
                         convention and just want to distill the parsedName.
        - singleCell: integer input [1] 1 cell expected, [0] multiple cells
                      expected. Is only important for swc files.
    Returns:
        - parsedName: iterable name dict (by cellAmpID) of the filename and
                      format. However if passCheck == 0 parsedName will not
                      contain the identifiers!
        - formatf: format of the file (dat or swc)
        - passCheck: 0 or 1 integer. 0: filename could not be found back in the
                     database, 1: filename relates to a cellID in database.
    """
    passCheck = 1
    parsedName = []
    formatf = filename[-4:]
    if formatf == '.SWC' or formatf == '.swc':
        # Change this line if conventions are being changed. Make sure
        # identifiers can be mapped on columns of the database.
        filenameTemplate = ('cellID_ampID_slideID_animalID_sliceID_status_' +
                            'tracerInitials.SWC')
        formatf = 'swc'
    elif formatf == '.DAT' or formatf == '.dat':
        # Change this line if conventions are being changed. Make sure
        # identifiers can be mapped on columns of the database.
        filenameTemplate = ('cellID_ampID_slideID_animalID_sliceID_status_' +
                            'tracerInitials.DAT')
        formatf = 'dat'

    filenameTemplate = filenameTemplate[:-4]  # remove extension
    namePartsTemplate = re.split('_', filenameTemplate)
    namePartsTemplateD = dict(zip(list(range(len(namePartsTemplate))),
                                  namePartsTemplate))
    namePartsTemplateR = dict(zip(namePartsTemplate,
                                  list(range(len(namePartsTemplate)))))

    filename = filename[:-4]  # remove extension
    namePartsCheck = re.split('_', filename)
    namePartsCheck = dict(zip(list(range(len(namePartsCheck))),
                              namePartsCheck))

    # check the same amount of identifiers are present in the filename
    if len(namePartsCheck) != len(namePartsTemplateD):
        warnings.warn('(Filename has {} identifiers too many(positive #)/ ' +
                      'less(negative #))'.format(str(len(namePartsCheck) -
                                                     len(namePartsTemplateD))),
                      UserWarning)
        parsedName = namePartsCheck
        # Important!! checkFilename will still push a parsedName, without
        # matched identifiers!
        manualRename(filename+'.'+formatf, morphDataPath)
        passCheck = 0

    if passCheck != 0:
        # check that swc files only have one neuron and that within dat files
        # each neurons has a annotated amplifier
        subPartsCheck = dict()
        for idx in namePartsCheck:
            subPartsCheck[idx] = re.split('-', namePartsCheck[idx])
        if formatf == 'swc':
            if singleCell == 1:
                if (len(subPartsCheck[namePartsTemplateR['cellID']]) != 1 or
                        len(subPartsCheck[namePartsTemplateR['ampID']]) != 1):
                    warnings.warn('(Check the filename and file for amount' +
                                  'of neurons present in the file , {}, ' +
                                  '#Neurons | #amplifiers in file)'.format(
                                      str(list([len(subPartsCheck[
                                          namePartsTemplateR['cellID']]),
                                                len(subPartsCheck[
                                                    namePartsTemplateR['ampID']
                                                    ])]))), UserWarning)
                    parsedName = namePartsCheck
                    # Important!! so checkFilename will still push a parsedName
                    # without matched identifiers!
                    passCheck = 0
            if singleCell != 1:
                if (len(subPartsCheck[namePartsTemplateR['cellID']]) !=
                        len(subPartsCheck[namePartsTemplateR['ampID']])):
                    warnings.warn('(Check the filename and file for amount' +
                                  ' of neurons and amplifiers present in the' +
                                  ' file , {}, #Neurons | #amplifiers in' +
                                  ' file)'.format(str(list([
                                      len(subPartsCheck[namePartsTemplateR
                                                        ['cellID']]),
                                      len(subPartsCheck[namePartsTemplateR
                                                        ['ampID']])]))),
                                  UserWarning)
                    parsedName = namePartsCheck
                    # Important!! so checkFilename will still push a parsedName
                    # without matched identifiers!
                    passCheck = 0
        if formatf == 'dat':
            if (len(subPartsCheck[namePartsTemplateR['cellID']]) !=
                    len(subPartsCheck[namePartsTemplateR['ampID']])):
                warnings.warn('(Check the filename and file for amount of' +
                              ' neurons and amplifiers present in the file ,' +
                              ' {}, #Neurons | #amplifiers in file)'.format(
                                str(list([len(subPartsCheck[namePartsTemplateR
                                    ['cellID']]),
                                    len(subPartsCheck[namePartsTemplateR
                                        ['ampID']])]))), UserWarning)
                parsedName = namePartsCheck
                # Important!! so checkFilename will still push a parsedName
                # without matched identifiers!
                passCheck = 0
        if passCheck != 0:
            # At this point filenames contain all identifiers.
            # However in some cases dashes are misused eg. animalID.
            # So for all identifiers other than cellID and ampID, combine
            # subparts of the identifiers (eg. 'Ai9PV' and '0023' becomes
            # 'Ai9PV-0023').
            joinIDs = ['animalID', 'status', 'tracerInitials']
            for idx, vals in enumerate(joinIDs):
                s = '-'
                seq = subPartsCheck[namePartsTemplateR[vals]]
                subPartsCheck[namePartsTemplateR[vals]] = [s.join(seq)]

            parsedName = dict(zip(list(namePartsTemplateR.keys()),
                                  list(subPartsCheck.values())))
    return(parsedName, formatf, passCheck)


def crossCheckFilenameDB(morphDataPath,
                         filename,
                         db_filename=os.path.join(bcs_global.path_database,
                                                  bcs_global.filename_cell_db),
                         singleCell=1):
    """Function cross checks a given filename with corresponding information
    in the database.

    Args:
        - morphDataPath: complete path pointing to the folder where the
        morphology files are stored.
        - filename: filename of the morphology file (.swc or .dat).
        - db_filename: filename of the database, should include path when this
                       script is not in the same folder.
        - singleCell: integer input [1] 1 cell expected, [0] multiple cells
                      expected. Is only important for swc files.
    Returns:
        - passCheck: 0 or 1 integer. 0: filename could not be found back in the
                     database, 1: filename relates to a cellID in database.
        - df: dataframe with updated filenames
        If required, will update filenames of morphology files
    """
    passCheck = 1
    print('Parsing : ' + filename)
    parsedName, formatf, passCheck = checkFilename(filename, morphDataPath,
                                                   singleCell)
    # first run test whether filename is correct

    df = readDBCSV(os.path.dirname(db_filename), os.path.basename(db_filename))
    if (passCheck == 0 and formatf == 'swc' and
            len(re.split('-', parsedName[0])) == 1):
        s = '_'
        parsed_cellAmpID = (parsedName[0] + 'a' + parsedName[1])
        try:
            proposeFilename = s.join([parsedName[0], parsedName[0],
                                     list(df['slideID'][df['cellAmpID'] ==
                                          parsedName[1],
                                      list(df['animalID'][df['cellAmpID'] ==
                                          parsed_cellAmpID])[0],
                                      list(df['sliceID'][df['cellAmpID'] ==
                                          parsed_cellAmpID])[0],
                                      list(df['status'][df['cellAmpID'] ==
                                           parsed_cellAmpID])[0],
                                      list(df['tracerInitials'][df['cellAmpID']
                                           == parsed_cellAmpID])[0]])])
        except (KeyError, ValueError):
            proposeFilename = filename + 'wrongNaming'
            pass

        proposeFilename = proposeFilename+'.swc'
        print('Proposed updated filename: '+proposeFilename)
        sanity_check = 1
        while sanity_check == 1:
            ui = input('Update filename? [1] Yes, [0] No, Do note that' +
                       ' tracerInitials/comments might be lost!')
            if ui == '0':
                print('Please update filename manually!')
                newFilename = manualRename(filename, morphDataPath)
                if formatf == 'swc':
                    (df.loc[
                        df['cellAmpID'] == parsed_cellAmpID,
                        'swcFilename']) = newFilename
                sanity_check = 0
            if ui == '1':
                print('Filename is updated, in progress')
                renameFile(filename, proposeFilename, morphDataPath)
                if formatf == 'swc':
                    (df.loc[
                        df['cellAmpID'] == parsed_cellAmpID,
                        'swcFilename']) = proposeFilename
                sanity_check = 0
    if passCheck != 0:
        # Find data in the dataframe based on the identifiers of the filename
        # and check consistency
        for idx, cellIDs in enumerate(parsedName['cellID']):
            cellAmpID = cellIDs+parsedName['ampID'][idx]
            if idx == 1:
                cellAmpIDPrev = (parsedName['cellID'][idx-1] +
                                 parsedName['ampID'][idx-1])
            if cellAmpID in df['cellAmpID'].array:
                animalIDCheck = (parsedName['animalID'] ==
                                 list(df['animalID'][df['cellAmpID'] ==
                                      cellAmpID]))
                slideIDCheck = (parsedName['slideID'] ==
                                list(df['slideID'][df['cellAmpID'] ==
                                     cellAmpID]))
                sliceIDCheck = (parsedName['sliceID'] ==
                                list(df['sliceID'][df['cellAmpID'] ==
                                     cellAmpID]))
                statusCheck = (parsedName['status'] ==
                               list(df['status'][df['cellAmpID'] ==
                                    cellAmpID]))
                print('Filename: '+str(filename)+', with cellID : ' + cellIDs +
                      ', check status is: animalIDCheck; ' +
                      str(animalIDCheck)+', slideIDCheck; ' +
                      str(slideIDCheck)+', statusCheck; ' +
                      str(statusCheck))
                if not (animalIDCheck & slideIDCheck & statusCheck &
                        sliceIDCheck):
                    if formatf == 'swc' and len(parsedName['cellID']) == 1:
                        s = '_'
                        proposeFilename = s.join([cellIDs,
                                                  parsedName['ampID'][idx],
                                                  list(df['slideID']
                                                       [df['cellAmpID'] ==
                                                       cellAmpID])[0],
                                                  list(df['animalID']
                                                       [df['cellAmpID'] ==
                                                       cellAmpID])[0],
                                                  list(df['sliceID']
                                                       [df['cellAmpID'] ==
                                                       cellAmpID])[0],
                                                  list(df['status']
                                                       [df['cellAmpID'] ==
                                                       cellAmpID])[0],
                                                  parsedName['tracerInitials']
                                                  [0]])
                        proposeFilename = proposeFilename+'.swc'
                        print('Proposed updated filename: '+proposeFilename)
                        sanity_check = 1
                        while sanity_check == 1:
                            ui = input('Update filename? [1] Yes, [0] No')
                            if ui == '0':
                                print('Please update filename manually!')
                                newFilename = manualRename(filename, morphDataPath)
                                if formatf == 'swc':
                                    df.loc[df['cellAmpID'] == cellAmpID,
                                           'swcFilename'] = newFilename
                                sanity_check = 0
                                sanity_check = 0

                            if ui == '1':
                                print('Filename is updated, in progress')
                                renameFile(filename, proposeFilename,
                                           morphDataPath)

                                if formatf == 'swc':
                                    df.loc[df['cellAmpID'] == cellAmpID,
                                           'swcFilename'] = proposeFilename
                                sanity_check = 0
                    if formatf == 'dat' and idx == 0:
                        print('Please update filename manually!')
                        manualRename(filename, morphDataPath)
                        passCheck = 0
                    if (formatf == 'swc' and len(parsedName['cellID']) > 1 and
                            idx == 0):
                        print('Please update filename manually!')
                        passCheck = 0
                    if idx > 0 and cellAmpID in df['cellAmpID'].array:
                        print('Status: ' +
                              str(list(df['status']
                                       [df['cellAmpID'] == cellAmpID])))
                        print('updated filename: ' +
                              str(list(df['swcSliceFilename']
                                       [df['cellAmpID'] == cellAmpIDPrev])))
                        ui = input('Do you want to annotate swcSliceFile of' +
                                   ' cell : {} , with the updated filename?:' +
                                   '[0] no, [1] yes, '.format(cellAmpID))
                        if ui == '1':
                            df.loc[df['cellAmpID'] == cellAmpID,
                                   'swcSliceFilename'] = list(df['swcSlice' +
                                                                 'Filename']
                                                                [df['cell' +
                                                                 'AmpID'] ==
                                                                 cellAmpIDPrev]
                                                              )[0]
                        if ui == '0':
                            df.loc[df['cellAmpID'] == cellAmpID,
                                   'swcSliceFilename'] = 'manualLinkRequired'
                elif (animalIDCheck & slideIDCheck & statusCheck &
                      sliceIDCheck):
                    if formatf == 'swc' and len(parsedName['cellID']) == 1:
                        df.loc[df['cellAmpID'] == cellAmpID,
                               'swcFilename'] = filename
                    elif formatf == 'swc' and len(parsedName['cellID']) > 1:
                        df.loc[df['cellAmpID'] == cellAmpID,
                               'swcSliceFilename'] = filename
                    elif formatf == 'dat':
                        df.loc[df['cellAmpID'] == cellAmpID,
                               'datFilename'] = filename
            else:
                print('\n cellAmpID: '+str(cellAmpID)+', could not be found'
                      'in the InsularCellDatabase. Reconsider filename:' +
                      str(filename)) + '\n'
                passCheck = 0
    df.to_csv(db_filename, sep=',', encoding='utf-8', index=False)
    return df, passCheck


def manualRename(origFilename, morphDataPath):
    """Allows user to input newFilename and save the changes in renameLog.csv.
    Args:
        - origiFilename: filename of the original file, no path needed
        - morphDataPath: path to the folder containing the morphology files to
                         be changed
    Returns:
        - changes filename manually and tracks in log. Given the logic of
        crossCheckFilenameDB it will NOT readily test and update the new
        filename
    """
    print('Change the following file: ' + origFilename)
    newFilename = input('Type new filename, do not add fileformat: ')
    formatf = origFilename[-4:]
    newFilename = newFilename+formatf
    renameFile(origFilename, newFilename, morphDataPath)
    return newFilename

def renamingLog(origFilename, newFilename, morphDataPath):
    """Logs renaming of files.

    Opens renamingLog.csv and appends a new row with the original and new
    filename and the date + time registation.

    Args:
        - origFilename: filename (without path), of the original filename
        - newFilename: filename (without path), of the new filename

    Returns:
        - Diplay confirmation of the appended log action and saved csv file
        named; renameLog.csv in the same folder as the morphology files are
        stored
    """
    datetimeLog = str(datetime.datetime.now())
    renamingLog = pd.read_csv(morphDataPath+'/renameLog.csv', sep=',',
                              encoding='utf-8')
    newLogEntry = {'dateTime': datetimeLog, 'originalFilename': origFilename,
                   'newFilename': newFilename, 'fileFormat': newFilename[-3:]}
    renamingLog = renamingLog.append(newLogEntry, ignore_index=True)
    renamingLog.to_csv(morphDataPath + '/renameLog.csv', sep=',',
                       encoding='utf-8', index=False)

    storedLog = (origFilename + ' converted to: ' + newFilename +
                 ' logged on ' + datetimeLog)
    print('(Stored change in renameLog.csv: {})'.format(storedLog))


def renameFile(origFilename, newFilename, morphDataPath):
    """Rename swc files.

    Copy originalFilename, rename the copied version to newFilename and moves
    the originalFilename to subfolder OLD. Make a annotation in the
    renameLog.csv

    Args:
        - origFilename: original filename without path
        - newFilename: new filename without path
        - morphDataPath: path to folder containing the original file
    Output:
        - renames file and moves original file to subfolder 'OLD', makes log in
        local renameLog.csv
    """
    srcFile = os.path.join(morphDataPath, origFilename)
    dstPath = os.path.join(morphDataPath, 'OLD')
    shutil.copy(srcFile, dstPath)
    renamedFile = os.path.join(morphDataPath, newFilename)
    os.rename(srcFile, renamedFile)
    renamingLog(origFilename, newFilename, morphDataPath)


def executeFileCheck(morphDataPath,
                     db_filename=os.path.join(bcs_global.path_database,
                                              bcs_global.filename_cell_db),
                     singleCell=1):
    """Batch run swc-file check.

    Iterates through filenames in a given folder (morphDataPath) and compares
    the filename with data from database (db_filename).

    Args:
        - db_filename: needs to include path if script is not with the database
                       in the same folder!
        - morphDataPath: complete path pointing to the folder where the
                         morphology files are stored.
        - singleCell: integer input [1] 1 cell expected, [0] multiple cells
                      expected. Is only important for swc files.

    Returns:
        - list of flagged filenames which need manual corrections
    """
    if os.path.exists(morphDataPath):
        fileList = os.listdir(morphDataPath)
    else:
        fullPath = os.getcwd()
        boxPath = '/brainCellSuite'
        appendPath, trsh = re.split(re.escape(boxPath), fullPath)
        if os.path.exists(appendPath+morphDataPath):
            morphDataPath = appendPath+morphDataPath
            fileList = os.listdir(morphDataPath)

        else:
            print('morphDataPath is incorrect: ' + appendPath + morphDataPath)
            sanity_check = 1
            while sanity_check == 1:
                ui = input('Give remainig path after :{}'.format(appendPath))
                ui = appendPath+ui
                if os.path.exists(ui):
                    morphDataPath = ui
                    fileList = os.listdir(morphDataPath)
                    sanity_check = 0
    if not os.path.exists(db_filename):
        boxPath = '/brainCellSuite'
        appendPath, trsh = re.split(re.escape(boxPath), fullPath)
        if os.path.exists(appendPath+db_filename):
            db_filename = appendPath+db_filename
        else:
            print('db_filename is incorrect: ' + appendPath + db_filename)
            sanity_check = 1
            while sanity_check == 1:
                ui = input('Give remainig path after :{}'.format(appendPath))
                ui = appendPath+ui
                if os.path.exists(ui):
                    db_filename = ui
                    sanity_check = 0
    flagFilenames = list()
    for filename in fileList:
        print(filename)
        if filename[-4:] == '.swc' or filename[-4:] == '.dat':
            df, passCheck = crossCheckFilenameDB(morphDataPath, filename,
                                                 db_filename, singleCell)
            if passCheck == 0:
                flagFilenames.append(filename)
    return flagFilenames
