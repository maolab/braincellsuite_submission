""" Following scripts syncronizes data between databases
Databases include:
- insularCellDatabase.csv
- maoLabSlideDatabase.csv

and makes sure the keys and databases are uptodate.

Functions:
    - checkKeys: Check column names of the databases.
    - executeCheckKey: Execute the checkKey fucntion with preset database
                       names.
    - checkSlideLocation(db_path): Compares the location annotation of slides
                                   in slideboxes in the databases.
"""

import pandas as pd
import os
import numpy as np
from brainCellSuite import bcs_global

__version__ = '0.1'
__date__ = '2018-07-26'
__author__ = 'Bart C Jongbloets'


def checkKeys(db_path, db_filename):
    """Check column names of the databases.

    Compares the columnnames of the databases (nameOfDatabase.csv) overlap
    with their keys in a separate file called key_nameOfDatabase.csv

    Args:
        - db_path: path to folder containing the databases
        - db_filename: filename(s) of the databases to be checked
    Returns:
        - flag: list of column names which aren't described in either key or
                database
    """
    flag = dict()
    if isinstance(db_filename, str):
        db_filename = [db_filename]
    for file in db_filename:
        file = str(file)
        db_filepath = os.path.join(db_path, file)
        db_key_filepath = os.path.join(db_path, ('key_' + file))
        if not os.path.exists(db_filepath):
            print('dbFile can not be found')
        if not os.path.exists(db_key_filepath):
            print('keyFile can not be found')
        if os.path.exists(db_filepath) and os.path.exists(db_key_filepath):
            flag[file] = dict()
            df = pd.read_csv(db_filepath)
            dfkeys = pd.read_csv(db_key_filepath)
            if (len(dfkeys.columnName) ==
                    len(set(df.keys()) & set(dfkeys.columnName)
                        ) == len(df.keys())):
                flag[file] = []
            elif (len(dfkeys.columnName) ==
                  len(df.keys()) != len(set(df.keys()) &
                                        set(dfkeys.columnName))):
                # iterate through the db keys and check which of the keys in
                # key_database do not overlap
                for idx, key in enumerate(df.keys()):
                    if key not in dfkeys.columnName:
                        flag[file][idx] = key
            elif len(dfkeys.columnName) < len(df.keys()):
                # iterate through the db keys and check which of the keys in
                # key_database do not overlap
                for idx, key in enumerate(df.keys()):
                    if key not in dfkeys.columnName:
                        flag[file][idx] = key
            elif len(dfkeys.columnName) > len(df.keys()):
                # iterate through the db keys and check which of the keys in
                # key_database do not overlap
                for idx, key in enumerate(dfkeys.columnName):
                    if key not in df.keys():
                        flag[file][idx] = key
    return flag


def executeCheckKey():
    """Execute the checkKey function with preset database names.

    Makes use of paths and filenames defined in the brainCellSuite.ini file.

    Args:
        - none
    Returns:
        - printed statement of problematic columns in the Databases
    """

    # now double check the databases are in sync and keys are correctly
    # annotated
    flag = checkKeys(bcs_global.path_database,
                     [bcs_global.filename_cell_db,
                      bcs_global.filename_slide_db])
    # change the names for different database vs key_database files
    for keys in flag.keys():
        if flag[keys]:
            print('Check identifiers from: ' + keys + ' for: ', flag[keys])
        elif not flag[keys]:
            print('keys and identifiers from: '+keys+' are compliant')


def checkSlideLocation(db_path=bcs_global.path_database):
    """Compares the location annotation of slides in slideboxes in the
    databases.

    Assumes following databases: for cells and slides in brainCellSuite.ini file
    In case the location annotation in the cellDatabase is incorrect
    (slideDatabase is leading), user will be prompted to confirm location
    listed in slideDatabase is correct and will then apply changes to
    location annotation at all cells related to the slideID.

    Args:
        - db_path: path to folder containing the databases
    Output:
        - based on user input either or both databases will be updated
    """
    cellDfFile = bcs_global.filename_cell_db
    slideDfFile = bcs_global.filename_slide_db
    if not os.path.exists(os.path.join(db_path, cellDfFile)):
        print(cellDfFile + 'can not be found')
    if not os.path.exists(os.path.join(db_path, slideDfFile)):
        print(slideDfFile + ' can not be found')
    if (os.path.exists(os.path.join(db_path, cellDfFile)) and
            os.path.exists(os.path.join(db_path, slideDfFile))):

        cellDf = pd.read_csv(os.path.join(db_path, cellDfFile))
        slideDf = pd.read_csv(os.path.join(db_path, slideDfFile))
        slides, slidefields = slideDf.shape
        for slide in np.arange(0, slides):
            slideID = slideDf.loc[slide, 'slideID']
            slideDBBox = str(slideDf.loc[slide, 'storageboxID'])
            print(slideID+'_'+slideDBBox)
            # now find all cells that are found on that slide
            boxIDs = list(cellDf['storageboxID'][cellDf['slideID'] == slideID])
            if boxIDs:
                for boxID in boxIDs:
                    if boxID != slideDBBox:
                        print('Mismatch in storageboxID; please locate slide' +
                              'and confirm location in maoLabSlideDatabase' +
                              'is correct [1] or type new location (will be' +
                              'sync in both databases)')
                        print('In ' + cellDfFile + ' : ' + str(boxIDs))
                        ui = input('[1] confirm, [xxxxx] updated location')
                        if ui == str(1):
                            (cellDf.loc[cellDf['slideID'] == slideID,
                                        ['storageboxID']]) = slideDBBox
                            print('cellDf changed to : ' + slideDBBox)
                            break
                        else:
                            (cellDf.loc[cellDf['slideID'] == slideID,
                                        ['storageboxID']]) = ui
                            (slideDf.loc[slideDf['slideID'] == slideID,
                                         ['storageboxID']]) = ui
                            print('both changed to : ' + ui)
                            break

        cellDf.to_csv(os.path.join(db_path, cellDfFile), index=False)
        slideDf.to_csv(os.path.join(db_path, slideDfFile), index=False)
