"""Initialize object cell

Cells are the elementary objects in the cell database. All data will be
connected to indiviudal cells.

Classes:
    - Cell: Represents a cell from the database.
"""

from brainCellSuite import bcs_global


class Cell:
    """Represents a cell from the database.

    Functions:
        - __init__: initialize cell object.
        - link_db_info: Attach metadata from database.

    """

    __version__ = '0.1'
    __date__ = '2018-07-23'
    __author__ = 'Bart C Jongbloets'

    def __init__(self,
                 cell_id,
                 db_path=bcs_global.path_database,
                 db_filename=bcs_global.filename_cell_db,
                 db_slides_filename=bcs_global.filename_slide_db,
                 df_cell=None,
                 df_slide=None,
                 alternative_morph_data_path=None,
                 verbose=False):
        """Initialize cell_id.

        cellID should be described in the cell database (will be checked
        using link_db_info).

        Args:
            - self: empty
            - cellID: cell ID that belongs to the database.
            - db_path: fullpath to folder containing insularDatabase
                and maoLabSlideDatabase.
            -db_filename: filename containing cellDatabase, located
                          within db_path.
            -db_slides_filename: filename containing slideDatabase, located
                                 within db_path.
            -df_cell: pandas dataframe of the cellDatabase. This optional to
                      avoid loading of the cellDatabase when cellID is
                      initiatlized in a iterative manner. DEFAULT: None.
            -df_slide: pandas dataframe of the cellDatabase. This optional to
                      avoid loading of the cellDatabase when cellID is
                      initiatlized in a iterative manner. DEFAULT: None.
            - alternative_morph_data_path: default None, if path exists use this
                as the location for morph data. This can be helpful to keep
                new morphologies files apart from the already analyzed files
                during QC and modifications.
            - verbose: boolean, default = False. Will plot or print
                calculated values or notifications if true.

        Returns:
            - instance of cell object.

        """
        import os

        from brainCellSuite._io.readSWCplus import readSWCp

        self.cell_id = cell_id
        if verbose:
            print('(Initialized cellID:{})'.format(self.cell_id))
        self.link_db_info(db_path, db_filename, db_slides_filename, df_cell,
                          df_slide)
        self.analysis = {'morphology': dict(),
                         'ephys': dict(),}
        self.path = db_path
        if isinstance(self.meta['swcFilename'], str):
            if alternative_morph_data_path:
                morph_path = alternative_morph_data_path
            else:
                morph_path = bcs_global.path_morph_data
            self.morph_path = morph_path
            self.swc_data, self.swc_meta = readSWCp(
                os.path.join(morph_path, self.meta['swcFilename']))

    def link_db_info(self, db_path, db_filename, db_slides_filename, df_cell,
                     df_slide):
        """ Append meta data from database to cell object.

        Checks whether the cellID is present and appends amplifierID
        (if multiple amplifiers linked to cellID user-input is
        required). Links metadata to the instance and
        Default amplifier = a1.

        Args:
            - self: cell object.
            - db_path: full path to database folder.
            - db_filename: name of insular cell database.
            - db_slides_filename: name of mircoscopeslide database.
            -db_cell: pandas dataframe of the cellDatabase. This optional to
                      avoid loading of the cellDatabase when cellID is
                      initiatlized in a iterative manner. DEFAULT: None.
            -df_slide: pandas dataframe of the cellDatabase. This optional to
                      avoid loading of the cellDatabase when cellID is
                      initiatlized in a iterative manner. DEFAULT: None.

        Returns:
            - addition to cell object:
                - meta data: from cell database.
                - cell_amp_id: combination of cell_id and amplifier_id.

        """
        from brainCellSuite._io.readDBCSV import readDBCSV
        import warnings
        if df_cell is None:
            df_cell = readDBCSV(db_path, db_filename)
        if df_slide is None:
            df_slide = readDBCSV(db_path, db_slides_filename)
        if self.cell_id[-2] == 'a' and self.cell_id in df_cell['cellAmpID'].\
           array:
            self.cell_amp_id = self.cell_id
            self.cell_id = self.cell_id[:-2]
            self.meta = dict(
                zip(list(df_cell.keys()),
                    df_cell[df_cell.cellAmpID == self.cell_amp_id].
                    iloc[0].tolist()))
        elif self.cell_id in df_cell['cellID'].array:
            if sum(df_cell['cellID'].array == self.cell_id) > 1:
                sanity_check = 1
                while sanity_check == 1:
                    select_amplifier = input('(Select amplifier.Found'
                                             'amplifiers:{})'.format
                                             (list((df_cell['ampID']
                                                    [(df_cell['cellID'].
                                                      array ==
                                                      self.cell_id)]))))
                    if int(select_amplifier) in list(df_cell['ampID']
                                                     [(df_cell['cellID'].
                                                       array ==
                                                       self.cell_id)]):
                        sanity_check = 0
            elif sum(df_cell['cellID'].array == self.cell_id) == 1:
                select_amplifier = int(df_cell['ampID']
                                       [(df_cell['cellID'].array ==
                                         self.cell_id)])
            self.cell_amp_id = self.cell_id + 'a' + str(
                select_amplifier)
            self.meta = dict(
                zip(list(df_cell.keys()),
                    df_cell[df_cell.cellAmpID == self.cell_amp_id].
                    iloc[0].tolist()))

        if self.cell_id not in df_cell['cellID'].array:
            warnings.warn('CellID has not been added to the database, '
                          'please add CellID to the database. Addition'
                          ' functionality will be added to the module'
                          ' in the future', UserWarning)
            print('These are the cellIDs included in the database \n' +
                  str(list(df_cell['cellID'])))

        if self.meta:
            slideID = self.meta['slideID']
            if slideID in list(df_slide['slideID']):
                appendDict = dict()
                for key in df_slide.keys():
                    appendDict[key] = df_slide[key][df_slide
                                                    ['slideID'] ==
                                                    slideID].iloc[0]
                self.meta.update(appendDict)
