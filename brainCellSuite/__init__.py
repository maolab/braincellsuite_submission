name = "brainCellSuite"
__version__ = '0.1.1'  # Don't forget to update in setup.py
import matplotlib as mpl
import configparser
import os

mpl.use('TkAgg')

config = configparser.ConfigParser()
ini_file = os.path.join(os.path.dirname(__file__), 'braincellsuite.ini')


def generate_ini_file(ini_file_path):
    """Generate a new initiation file.

    Args:
        ini_file_path: full path to initiation file.

    """
    from tkinter import Tk
    from tkinter.filedialog import askopenfilename
    from tkinter.filedialog import askdirectory
    import pandas as pd

    paths_dict = {'path_database':
                      {'prompt': 'path to main folder containing all databases',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases'},
                  'path_ephys_data':
                      {'prompt': 'path to folder containing ephys data. Should'
                                 ' contain folders for each recorded neuron',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/xsg'},
                  'path_ephys_images':
                      {'prompt': 'path to folder containing ephys images. '
                                 ' Should contain TIFF images for each recorded'
                                 ' neuron',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/cellMorphFiles/'
                                        'ephysTIFF'},
                  'path_ephys_contours':
                      {'prompt': 'path to folder containing ephys contour text'
                                 ' files. Should contain a txt file for each '
                                 'recorded neuron',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/cellMorphFiles/'
                                        'ephysContourTXT'},
                  'path_morph_data':
                      {'prompt': 'path to folder containing swc files ',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/cellMorphFiles/'
                                        'neuronSWC'},
                  'path_input_data':
                      {'prompt': 'path to folder containing input data ',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/LSPS maps by /'
                                        'morphology'},
                  'path_qc_ephys_data':
                      {'prompt': 'path to folder where ephys qc plots are '
                                 'stored',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/QC/xsg'},
                  'path_qc_morph_data':
                      {'prompt': 'path to folder where .csv files are stored'
                                 ' for morph quality control',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/QC'},
                  'path_ephys_analysis':
                      {'prompt': 'path to folder where ephys analysis files'
                                 ' are stored; related to ephys feature'
                                 ' filebase and ephys feature sweeps filebase',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/analysis/ephys',
                       'required_folders': ['sweep_feature_tables',
                                            'fi_tables',]},
                  'path_morph_analysis':
                      {'prompt': 'path to folder where morphology analysis '
                                 'files are stored',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/analysis/morphology'},
                  'path_morph_ephys_analysis':
                      {'prompt': 'path to folder where morphology and electrophysiology '
                                 'combine analysis files are stored',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/analysis/ephys_morphology'},
                  'path_input_analysis':
                      {'prompt': 'path to folder where input analysis '
                                 'files are stored',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/analysis/input'},
                  'path_website':
                      {'prompt': 'path to folder where website data is stored',
                       'ui_path': None,
                       'template_path': '/Users/username/cloudservice/'
                                        'insularDatabases/'
                                        'neuro.muniak.com_source/cells',
                       'required_folders': ['local_input', 'long-range_input',
                                            'meta', 'morph_prints',
                                            'morph_thumbnails',
                                            'passive_properties',
                                            'QR_html_logo', 'spike_ramp',
                                            'spike_train', 'spike_waveform']},
                  }

    files_dict = {
        'filename_cell_db': {'prompt': 'filename of the cell database; '
                                       ' should be stored in '
                                       'path_database!',
                             'template_file':
                                 'template_key_celldb.csv'},
        'filename_slide_db': {'prompt': 'filename of the slide '
                                        'database; should be stored'
                                        ' in path_database!',
                              'template_file':
                                  'template_key_slidedb.csv'}}

    filebases_dict = {'filebase_ephys_feature':
                          {'prompt': 'filebase used to identify the most recent'
                                     ' feature extraction; should be store in '
                                     'path_ephys_analysis!',
                           'suggest_filebase': 'insularIntrinsicFeatures'
                                               'Extract_'},
                      'filebase_ephys_feature_sweeps':
                          {'prompt': 'filebase used to identify the most recent'
                                     ' feature extraction; should be stored in'
                                     ' path_ephys_analysis!',
                           'suggest_filebase': 'insularIntrinsicFeaturesExtract'
                                               'Sweeprefs_'},
                      'filebase_morph_qc':
                          {'prompt': 'filebase used to identify the most recent'
                                     ' qc file for morphologies; should be '
                                     'stored in path_qc_morph_data!',
                           'suggest_filebase': 'QCSWC-'},
                      'filebase_morph_feature':
                          {'prompt': 'filebase used to identify the most recent'
                                     ' feature exctraction file for '
                                     'morphologies; should be stored in '
                                     'path_morph_data!',
                           'suggest_filebase': 'insularMorphFeaturesExtract_'}
                      }
    # create ini file
    cfgfile = open(ini_file, 'w')

    # add settings based on the dicts
    config.add_section('DATABASE_PATHS')
    for key, val_dict in paths_dict.items():
        print('\nPlease select the ' + val_dict['prompt'])
        print('if not present generate the folder and select in gui, '
              'template : ' + val_dict['template_path'])
        Tk().withdraw()
        if key != 'path_database':
            ui_path = askdirectory(initialdir=paths_dict['path_database']
            ['ui_path'])
        else:
            ui_path = askdirectory()
        # store ui in path_dict and initialization file
        paths_dict[key]['ui_path'] = ui_path
        config.set('DATABASE_PATHS', key, ui_path)

        if 'required_folder' in paths_dict[key].keys():
            # test whether all required folder are present otherwise generate
            # that folder
            for req_folder in paths_dict[key]['required_folders']:
                required_folder = os.path.join(paths_dict[key]['ui_path'],
                                               req_folder)
                if not os.path.exists(required_folder):
                    os.mkdir(required_folder, )

    config.add_section('DATABASE_FILENAMES')
    for key, val_dict in files_dict.items():
        existing_db = input('\nIs a ' + key.split('filename_')[-1] + ' present?'
                            + 'Yes: press Return, No: enter any character')
        if existing_db == '':
            print('\nPlease select the ' + val_dict['prompt'])
            Tk().withdraw()
            ui_file_path = askopenfilename(initialdir=paths_dict
            ['path_database']['ui_path'], filetypes=(('csv files', '*.csv'),))
            ui_file = os.path.basename(ui_file_path)
        else:
            print('\nNo database present yet: '
                  'Will create a new database .csv file now')
            db_filename = input('Provide a name for the ' +
                                key.split('filename_')[-1] + 'do not include'
                                                             'extensionname, '
                                                             'eg.: ".csv"')
            print('Please remember to update the descriptors of the key file '
                  'for : ' + db_filename + ' stored in database path : ' +
                  paths_dict['path_database']['ui_path'])
            template_db_path = os.path.join(os.path.dirname(__file__),
                                            val_dict['template_file'])
            template = pd.read_csv(template_db_path)
            db = pd.DataFrame(columns=template.columnName.tolist())
            db_filepath = os.path.join(paths_dict['path_database']['ui_path'],
                                       (db_filename + '.csv'))
            key_db_filepath = os.path.join(
                paths_dict['path_database']['ui_path'],
                ('key_' + db_filename + '.csv'))
            db.to_csv(db_filepath, index=False)
            template.to_csv(key_db_filepath, index=False)
            ui_file = (db_filename + '.csv')
        config.set('DATABASE_FILENAMES', key, ui_file)

    config.add_section('DATABASE_FILEBASES')
    for key, val_dict in filebases_dict.items():
        print('\nDefine ' + val_dict['prompt'])
        print('Suggest to use : ' + val_dict['suggest_filebase'])
        ui = input('Provide filebase or accept {press return} suggestion')
        if ui == '':
            ui = val_dict['suggest_filebase']
        config.set('DATABASE_FILEBASES', key, ui)

    config.write(cfgfile)
    cfgfile.close()
    pass


if not os.path.isfile(ini_file):
    generate_ini_file(ini_file_path=ini_file)

from brainCellSuite.bcs_global import *
from brainCellSuite.ini_cell import *
from brainCellSuite.helpers import *
from brainCellSuite.manageData import *
from brainCellSuite.analysis import *
from brainCellSuite._io import *
from brainCellSuite.qc import *
from brainCellSuite.website import *
