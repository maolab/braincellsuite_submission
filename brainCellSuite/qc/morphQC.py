"""Provide parameters to quality control morpholy_SWCfiles

Module that extract quantifiable parameters to determine the quality of
morphological.
reconstruction. Keep track of the criteria by which SWC files are categorized.

Functions:
    - get_endpoints: gathers coordinate IDs that are endpoints
    - TODO: get_truncatedEnpoints: determine which endpoint are truncated based
            on threshold

"""

import numpy as np

from brainCellSuite._io.readSWCplus import get_dataTree
from brainCellSuite import ini_cell
from brainCellSuite import bcs_global


def get_endpoints(cellAmpID, db_path=bcs_global.path_database):
    """Gathers coordinate IDs that are get_endpoints and which are truncated

    Takes SWC data, will be filtered to be part of neuronal process TYPE >1
    and TYPE < 5. Any coordinate ID which is not referred to in the position
    (P) parameter is considered an endpoint.

    Args:
        - cellAmpID: unique identifier for the recorded cell eg. BJ1832a1

    Return:
        - endPointData: data that resemble endpoint
    """
    cell = ini_cell.Cell(cellAmpID, db_path=db_path)
    markerSlide = int(cell.meta['presenceMarkerSlide'])
    dataTree = get_dataTree(cell.swc_data)
    IDs, P = (list(dataTree['ID']), list(dataTree['P']))
    endpointIDs = set(IDs) - set(P)
    endpointData = cell.swc_data[np.isin(cell.swc_data['ID'],
                                         list(endpointIDs))]
    return endpointData
