"""User-based location of ephysImages and storage in DB.

Reads DB and iterates through the cellIDs, per cellID the user is asked to
select the appropriate file. Only the filename is added to DB.

"""
import os
import glob
import shutil
from brainCellSuite import bcs_global
from brainCellSuite._io.readDBCSV import readDBCSV

ephysImage_path = bcs_global.path_ephys_images
os.chdir(ephysImage_path)
fileList = glob.glob('*.tif')
fileKey = list()
for file in fileList:
    fileKey.append(file[9:15])
filelist = dict(zip(fileKey, fileList))

df = readDBCSV(bcs_global.path_database, bcs_global.filename_cell_db)
for cellID in df['cellID']:
    if cellID in filelist:
        df.loc[df['cellID'] == cellID, 'ephysImageFile'] = filelist[cellID]

cellFileDB = list(df['ephysImageFile'])

pathdestin = os.path.join(ephysImage_path, 'dump')
for cellFile in fileList:
    if cellFile not in cellFileDB:
        srcFile = os.path.join(ephysImage_path, cellFile)
        dstPath = os.path.join(pathdestin)
        shutil.move(srcFile, dstPath)
