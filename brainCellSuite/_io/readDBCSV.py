""" Module to read csv files containing cell-centric or slide-centric'
 metadata. Will also run checks to find inconsistency with'
 key_database. """

import pandas as pd
import os
import re
import numpy as np
from brainCellSuite.manageData.syncDatabase import checkKeys
from brainCellSuite import bcs_global

def readDBCSV(db_path, filename, verbose=False):
    """Opens database saved as .csv

    Args:
        - db_path: full path to the folder containing the databases.
        - filename: filename of the database, including .csv!
        - verbose: boolean, if False print only when error occurs.

    Returns:
        - df: pandas dataframe containing the filename database.

    """
    flag = checkKeys(db_path, filename)
    if flag[filename]:
        print(flag)
        print('revise the keys and database before continuing the ' +
              'analysis')
    elif not flag[filename]:
        if verbose:
            print('database is compliant to keys')
    # BCJ 07-12-2019, changed to joining db_path with filename:
    # df = pd.read_csv(db_path + filename))
    df = pd.read_csv(os.path.join(db_path, filename))
    return df


def read_mostrecent_DBCSV(db_path, filename_base, index_on_cellAmpID=False,
                          verbose=False):
    """"Finds and read the most recent version of a csv file based on filename
    date.

    :param db_path: fullpath to databases/csv
    :param filename_base: base of the filename to parse by
    :param verbose: boolean, if False only show prints when error occur.

    return: dataframe of the csv
    - key_df: pandas dataframe containing the key of the df database.
    """
    key_df = None
    filelist = os.listdir(db_path)
    r = re.compile(str(filename_base) + '\d{8}.csv')
    tables = list(filter(r.match, filelist))
    if len(tables) == 1:
        filename = tables[0]
        df = readDBCSV(db_path=db_path, filename=filename, verbose=verbose)
    elif len(tables) > 1:
        dates = list()
        for i in np.arange(0, len(tables)):
            file = os.path.splitext(tables[i])[0]
            dates.append(file[len(filename_base):])
        dates.sort(reverse=True)
        filename = \
            str(filename_base) + dates[0] + '.csv'
        df = readDBCSV(db_path=db_path, filename=filename, verbose=verbose)
    else:
        print(
            'Warning! No table was found')
        df = None
        return df
    key_df = pd.read_csv(os.path.join(db_path, 'key_' + filename))
    if index_on_cellAmpID:
        df = df.set_index('cellAmpID')

    return df, key_df


def get_databases(verbose=False):
    """Generate a dictionary containing the most used up-to-date databases.

    Args:
        -verbose: bool, if False only print if error
    Returns:
        - databases: dictionary of databases
    """
    cell_df = readDBCSV(bcs_global.path_database,
                                      bcs_global.filename_cell_db,
                        verbose=verbose)
    cell_df = cell_df.set_index('cellAmpID', drop=False)
    slide_df = readDBCSV(bcs_global.path_database,
                        bcs_global.filename_slide_db,
                        verbose=verbose)
    slide_df = slide_df.set_index('slideID', drop=False)
    ephys_df, _ = read_mostrecent_DBCSV(
                     bcs_global.path_ephys_analysis,
                     bcs_global.filebase_ephys_feature,
                     index_on_cellAmpID=True,
                        verbose=verbose)
    ephys_sweep_df, _ = read_mostrecent_DBCSV(
        bcs_global.path_ephys_analysis,
        bcs_global.filebase_ephys_feature_sweeps,
        index_on_cellAmpID=True,
        verbose=verbose)
    morph_df, _ = read_mostrecent_DBCSV(
                     bcs_global.path_morph_analysis,
                     bcs_global.filebase_morph_feature,
                     index_on_cellAmpID=True,
                        verbose=verbose)
    morph_QC_df, _ = read_mostrecent_DBCSV(
                     bcs_global.path_qc_morph_data,
                     bcs_global.filebase_morph_qc,
                     index_on_cellAmpID=True,
                        verbose=verbose)
    databases = {'cell_df': cell_df,
                 'slide_df': slide_df,
                 'ephys_df': ephys_df,
                 'ephys_sweep_df': ephys_sweep_df,
                 'morph_df': morph_df,
                 'morph_QC_df': morph_QC_df}
    return databases