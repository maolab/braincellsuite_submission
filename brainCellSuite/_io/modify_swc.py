"""Modify swcfiles and log changes.

All modifications to swcFiles will be logged in the meta part of the files.
The meta part is labelled by a leading # on each meta line.

Functions:
    - add_modifications_swc: Insert new comments and/or data to the
        SWCplus file.
    - move_z_soma: Moves all z-coordinates of a soma to the targetZ and
        updates swcFile.
    - apply_z_shrinkage: Apply a shrinkage factor in z to all coordinates
        (tree and contours).
    - change_soma_id: Change somaID in the swcFile to 1.
    - correct_soma_id_swc: Executes the correction of soma id in swc files.
    - set_origin_processes: Add origin datapoint for all neuronal processes.
    - apply_xy_shrinkage: Apply a shrinkage factor in X and Y to all X and
    Y-coordinates (tree and contours).
    - executeRotateSWC: Reorientation of the SWCfile according to cortical
        thickness.
    - reduce_swcMeta: Removes all extra SWCplus data that is irrelevant.
    - executeRotateX2YSWC: Rotate SWC data to have Y reflect the depth.
"""

import datetime
import re
import numpy as np
import math
import os

from brainCellSuite._io import readSWCplus
from brainCellSuite.analysis.morphometry.vector import centerOMass
from brainCellSuite.analysis.morphometry.vector import \
    rotate_around_point_highperf


def add_modifications_swc(add_comment_lines, swc_meta, data, original_filename,
                          new_filename):
    """Insert new comments and/or data to the SWCplus file.

    Makes sure that at least one line of comments is associated with the
    modification of the swc file. If data is empty only comments are added.

    Args:
        - add_comment_lines: dictionary with integers as keys eg.:
            add_comment_lines = dict()
            add_comment_lines[1] = 'This comment will be 1 line'
            add_comment_lines[2] = 'This comment is the second line'
        - swc_meta: dictionary containing.
            - 'meta_line': integer, line that needs to be modified
            - 'modified_line': char, literal that will be used to over-write
                the original meta_line.
        - data: when data is modified data should be readSWC_data
            which is a numpy.ndarray. If left empty it will take
            the data from the file itself.
        - original_filename; fullpath to the original swc file.
        - new_filename; fullpath to the new swc file.

    Returns:
        - updated SWCplus file.

    """
    modify_meta = 0
    if not isinstance(add_comment_lines, dict):
        print('Please make sure to at least add one line of comments'
              '. add_comment_lines needs to be a dictionary with keys '
              'starting from 1')
        return
    if isinstance(add_comment_lines, dict):
        modLine = 'Modified on :'+str(datetime.datetime.now())
        add_comment_lines[0] = modLine
        for line in add_comment_lines:
            add_comment_lines[line] = '# '+add_comment_lines[line]+'\n'
        if swc_meta is not None:
            modify_meta = 1
    fileList = dict()
    with open(original_filename, 'rt') as in_file:
        lastHash = 0
        firstDataLine = 0
        fileLine = 1
        for line in in_file:
            pat = re.compile(r'#')
            # by default check if the somaContour ID is set to 1 in
            # the swcfile
            if modify_meta is 1:
                if fileLine is swc_meta['meta_line']:
                    line = swc_meta['modified_line']
            if pat.search(line[0]) is not None:
                lastHash += 1
            if pat.search(line[0]) is None and firstDataLine == 0:
                firstDataLine = fileLine
                # insert the new addCommentLine here
                for addLine in add_comment_lines:
                    fileList[lastHash+1+addLine] = add_comment_lines[addLine]
                    fileLine += 1
                if isinstance(data, np.ndarray):
                    for dataLine in data:
                        dataString = [str(dataLine[0]) + ' '+str(dataLine[1]) +
                                      ' ' + str(dataLine[2]) +
                                      ' ' + str(dataLine[3]) +
                                      ' ' + str(dataLine[4]) +
                                      ' ' + str(dataLine[5]) +
                                      ' ' + str(dataLine[6]) + '\n']

                        fileList[fileLine] = str(dataString[0])
                        fileLine += 1
                    break
            fileList[fileLine] = line
            fileLine += 1
    # write the newFile
    file = open(new_filename, 'wt')
    for iline in range(1, fileLine):
        file.write(fileList[iline])
    file.close()
    print(original_filename + ' , has been modified on ' +
          str(datetime.datetime.now()))


def move_z_soma(swc_filename, targetZ):
    """ Moves all z-coordinates of a soma to the targetZ and updates swcFile.

        If targetZ is empty it will make sure that all coordinates are in
        the same zDepth.

        Args:
            - swc_Data, numpy.ndarray containing all data from swcfile
            - swc_filename, fullpath to swcFile
            - targetZ, new position in um
        Returns:
            - updated swcFile

    """
    data, meta = readSWCplus.readSWCp(swc_filename)
    soma_contour_id = int(meta['swcPlus']['customTypes']['somaContour']['@id'])
    if soma_contour_id is not 1:
        print('Make sure to first correct the soma id in :' + swc_filename)
        return None
    somaData = data[:][data['TYPE'] == 1]
    # BCJ 20190502: double check presence of soma and whether there are more
    # than 1 datapoints. Otherwise exit
    if len(somaData) <= 1:
        return None
    if targetZ is None:
        targetZ = round(np.mean(somaData['Z']), 3)
    cm_original = np.round(centerOMass(x=somaData['X'], y=somaData['Y'],
                                       z=somaData['Z'],
                                       w=np.ones(len(somaData['X']))),
                           3)
    lastXYZ = [data['X'][-1], data['Y'][-1], data['Z'][-1]]
    data['Z'][data['TYPE'] == 1] = round(targetZ, 3)
    add_comment = dict()
    add_comment[1] = 'Moved Z coordinates of soma to: ' + str(targetZ) + 'µm'
    if list(cm_original) == lastXYZ:
        print(swc_filename + ' : has modified origin points! Will also' +
              ' translate these Z coordinates. Please visual check!')
        add_comment[2] = 'and the neuronal processes origin points!'
        for dataLines in range(data['ID'][-1], 0, -1):
            lastXYZ = [data['X'][dataLines-1], data['Y'][dataLines-1],
                       data['Z'][dataLines-1]]
            print(str(dataLines-1))
            if list(cm_original) == lastXYZ:
                data['Z'][dataLines-1] = targetZ  # cm_new[2]
            else:
                break

    add_modifications_swc(add_comment_lines=add_comment, swc_meta=None,
                          data=data, original_filename=swc_filename,
                          new_filename=swc_filename)
    return None


def set_origin_processes(swc_filename):
    """Add origin datapoint for all neuronal processes.

        Adds origin points for all processes (axons, dendrites) to the
        SWC file and links all starting points of the processes to this
        origin point. Origin point coordinates are defined as center of Mass
        of the soma.

        Args:
            - swc_filename, fullpath to swcFile

        Returns:
            - updated swcFile.

    """
    data, meta = readSWCplus.readSWCp(swc_filename)
    soma_contour_id = int(meta['swcPlus']['customTypes']['somaContour']['@id'])
    if soma_contour_id is not 1:
        print('Make sure to first correct the soma id in :' + swc_filename)
        return None
    somaData = data[:][data['TYPE'] == 1]
    # BCJ 20190502: double check presence of soma and whether there are more
    # than 1 datapoints. Otherwise exit
    if len(somaData) <= 1:
        return None
    borderData = data[data['TYPE'] >= 7]
    processesData = data[data['TYPE'] <= 6]
    processesData = processesData[processesData['TYPE'] >= 2]

    cm = centerOMass(x=somaData['X'], y=somaData['Y'], z=somaData['Z'],
                     w=np.ones(len(somaData['X'])))
    cm = np.round(cm, 3)

    tempData = np.zeros(len(somaData)+1, dtype=[('ID', '<i8'),
                                                ('TYPE', '<i8'),
                                                ('X', '<f8'),
                                                ('Y', '<f8'),
                                                ('Z', '<f8'),
                                                ('R', '<f8'),
                                                ('P', '<i8')])
    tempData[1:] = somaData
    tempData[0][0] = int(1)
    tempData[0][1] = int(1)
    tempData[0][2] = cm[0]
    tempData[0][3] = cm[1]
    tempData[0][4] = cm[2]
    tempData[0][5] = somaData[0]['R']
    tempData[0][6] = int(-1)

    dataPos = 2
    for i in range(1, dataPos + len(somaData)-1):
        tempData[i]['ID'] = i+1
        tempData[i]['P'] = tempData[i-1]['ID']
        dataPos += 1
    somaData = tempData
    transDict = dict()
    for i in range(0, len(processesData)):
        transDict[processesData[i]['ID']] = dataPos
        processesData[i]['ID'] = dataPos
        if processesData[i]['P'] == -1:
            processesData[i]['P'] = 1
        elif processesData[i]['P'] != processesData[i-1]['ID']:
            processesData[i]['P'] = transDict[processesData[i]['P']]
        else:
            processesData[i]['P'] = processesData[i-1]['ID']
        dataPos += 1
    for i in range(0, len(borderData)):
        transDict[borderData[i]['ID']] = dataPos
        borderData[i]['ID'] = dataPos
        if borderData[i]['P'] != -1:
            borderData[i]['P'] = borderData[i-1]['ID']
        dataPos += 1
    lenSoma = len(somaData)
    lenProc = len(processesData)
    tempData = np.zeros(lenSoma + lenProc + len(borderData),
                        dtype=[('ID', '<i8'),
                        ('TYPE', '<i8'),
                        ('X', '<f8'),
                        ('Y', '<f8'),
                        ('Z', '<f8'),
                        ('R', '<f8'),
                        ('P', '<i8')])
    tempData[0:lenSoma] = somaData
    tempData[lenSoma:lenSoma+lenProc] = processesData
    tempData[lenSoma+lenProc:] = borderData
    add_comment_lines = dict()
    add_comment_lines[1] = ('Added center of mass to soma as origin points' +
                            'for neuronal processes in data')
    add_comment_lines[2] = 'all ID and P moved, and borders are at end of SWC'
    add_modifications_swc(add_comment_lines=add_comment_lines, swc_meta=None,
                          data=tempData, original_filename=swc_filename,
                          new_filename=swc_filename)
    print(swc_filename + ' : process origin is set to soma center of mass')
    return None


def apply_z_shrinkage(swc_filename, z_shrink_factor):
    """ Apply a shrinkage factor in Z to all Z-coordinates (tree and
        contours).

        Args:
            - swc_filename, fullpath to swcFile
            - z_shrink_factor, factor defined as
                thicknessFresh/thicknessFixed. Coordinates of the SWC
                file will therefore be multiplied by this factor.
        Returns:
            - updated swcFile

    """
    data, meta = readSWCplus.readSWCp(swc_filename)
    data['Z'] = np.round(data['Z'] * z_shrink_factor, 3)
    add_comment = dict()
    add_comment[1] = 'Applied correction for Z-shrinkage by a factor of: '
    add_comment[2] = ('All Z-coordinate were multiplied by ' +
                      str(z_shrink_factor))
    add_modifications_swc(add_comment_lines=add_comment, swc_meta=None,
                          data=data, original_filename=swc_filename,
                          new_filename=swc_filename)
    return None


def change_soma_id(swc_filename):
    """Change somaID in the swcFile to 1.

        Checks whether somaContour has ID == 1 (compliant to SWC
        standard) or different. If different ID it will change the
        metaData and the data

        Args:
            - swc_filename, fullpath to swcFile

        Returns:
            - soma_contour_line: line_number of 'somaContour' in meta.
            - soma_contour_id: original soma_contour_id.
            - data: modified data with soma 'TYPE' changed to 1.

    """

    data, meta = readSWCplus.readSWCp(swc_filename)
    if 'somaContour' not in meta['swcPlus']['customTypes']:
        print('No somaContour annotation found in the metaData')
        soma_contour_line = None
        soma_contour_id = None
        return soma_contour_line, soma_contour_id, data
    soma_contour_id = int(meta['swcPlus']['customTypes']['somaContour']['@id'])
    if soma_contour_id is not 1:
        data['TYPE'][data['TYPE'] == int(soma_contour_id)] = 1
        with open(swc_filename, 'rt') as in_file:
            for line_num, line in enumerate(in_file):
                found_line = re.search('<somaContour', line)
                if found_line is not None:
                    soma_contour_line = line_num + 1
        return soma_contour_line, soma_contour_id, data
    else:
        soma_contour_line = None
    return soma_contour_line, soma_contour_id, data


def correct_soma_id_swc(swc_filename):
    """Executes the correction of soma id in swc files.

    Looks for 'somaContour' id in the SWCplus meta data. If id != 1, the meta
    and data of the swc file are corrected and a comment is added to flag this
    modification.

    Args:
        - swc_filename: fullpath to the original swc file.

    Returns:
        - modified swc file with corrected soma id in the meta and data

    """

    soma_contour_line, soma_contour_id, data = change_soma_id(swc_filename)
    if soma_contour_id is 1:
        print(swc_filename + ' has a correct soma id, no modifications made.')
        return None
    swc_meta = dict()
    swc_meta['meta_line'] = soma_contour_line
    swc_meta['modified_line'] = "# 		<somaContour id='1'/> \n"
    add_comment_lines = dict()
    add_comment_lines[1] = 'Modified soma contour id in SWCplus meta and data'
    add_modifications_swc(add_comment_lines, swc_meta, data, swc_filename,
                          swc_filename)
    print(swc_filename + ' had a different soma id. swc is now updated with' +
          ' the correct soma id.')
    return None


def apply_xy_shrinkage(swc_filename, xy_shrink_factor):
    """ Apply a shrinkage factor in X and Y to all X and Y-coordinates
    (tree and contours).

        Args:
            - swc_filename, fullpath to swcFile
            - xy_shrink_factor, factor defined as cortical thicknessFresh/
                cortical thicknessFixed. Coordinates of the SWC file will
                therefore be multiplied by this factor.
        Returns:
            - updated swcFile

    """
    data, meta = readSWCplus.readSWCp(swc_filename)
    data['X'] = np.round(data['X'] * xy_shrink_factor, 3)
    data['Y'] = np.round(data['Y'] * xy_shrink_factor, 3)
    add_comment = dict()
    add_comment[1] = 'Applied correction for X and Y-shrinkage by a factor of:'
    add_comment[2] = ('All X and Y-coordinate were multiplied by ' +
                      str(xy_shrink_factor))
    add_modifications_swc(add_comment_lines=add_comment, swc_meta=None,
                          data=data, original_filename=swc_filename,
                          new_filename=swc_filename)
    return None


def executeRotateSWC(cell, auto_flip=False):
    """Rotate SWC data.


        Using the slope of the cortical thickness calculation and
        the origin of the cortical thickness line at the corpus callosum the
        data is translated and rotated. This way the origin of the cortical
        thickness line (ie directly underneath the soma) is set at X:0,Y:0.
        And the data is rotated such that pia is mostly perpendicular to the X
        axis. This will allow for further analysis of morphology: Cortical
        layers can then be defined as a fraction over X-axis, column span
        can be expressesd over Y-axis.

        Args:
            - cell: instance of a cell object from class ini_cell
            -auto_flip: boolean, default False, if true will flip X axis by multiplying all X with -1. Flip will
                        only occur if first datapoint of soma has x < 0 .

        Return:
            - updated SWCfile that is rotated and translated
    """
    
    from brainCellSuite.analysis.morphometry.calc_postFixDims import executeCalcPostFixDims
    # BCJ20190110, while importing brainCellSuite it gets hung up on the import
    # of functions from calc_postFixDims. I suspect circular import problem to
    # be the cause.:
    # Above is not the best to do dynamic import, but I want to test if this 
    # problem is a circular import problem
    try:
        swcFilename = cell.meta['swcFilename']
    except KeyError:
        pass
    cell = executeCalcPostFixDims(cell)#BCJ201901
    cell.swc_data['X'] = cell.swc_data['X'] -\
        cell.analysis['swcCCOrigin'][0]
    cell.swc_data['Y'] = cell.swc_data['Y'] -\
        cell.analysis['swcCCOrigin'][1]
    radians = np.arctan(cell.analysis['swcSlope'])
    cell.swc_data['X'],\
        cell.swc_data['Y'] = rotate_around_point_highperf(cell.swc_data['X'],
                                                          cell.swc_data['Y'],
                                                          radians,
                                                          origin=(0, 0))
    cell.swc_data['X'] = np.round(cell.swc_data['X'], 3)
    cell.swc_data['Y'] = np.round(cell.swc_data['Y'], 3)
    swc_filename = os.path.join(cell.morph_path, swcFilename)
    add_comment = dict()
    add_comment[1] = 'Translated and rotated all X and Y data point:'
    add_comment[2] = 'All X and Y-coordinate rotated with ' +\
                     str(radians) + ' radians.'
    add_comment[3] = 'Using the corpus collusum underneath soma as x,y = 0'
    if auto_flip:
        if cell.swc_data['X'][cell.swc_data['TYPE'] == 1][0] < 0:
            cell.swc_data['X'] = np.round(cell.swc_data['X'] * -1, 3)
            # inverse to make pia a positive number
            add_comment[4] = ('Flipped reconstruction over X to make Pia a ' +
                              'positive X')
    add_modifications_swc(add_comment_lines=add_comment, swc_meta=None,
                          data=cell.swc_data, original_filename=swc_filename,
                          new_filename=swc_filename)
    return None


def reduce_swcMeta(original_filename, new_filename):
    """Removes all extra SWCplus data that is irrelevant.

    Removes all data in the SWCplus XML which belongs to <customProperties>.

    Args:
        - original_filename: fullpath to swcFile
        - new_filename: fullpath to new swcFile

    Returns:
        - saved version of the origingal file under new_filename
    """
    fileList = dict()
    fileLine = 1
    with open(original_filename, 'rt') as in_file:
        customPropLineStart = 0
        customPropLineEnd = 0
        for line in in_file:
            pat = re.compile(r'#')
            # by default check if the somaContour ID is set to 1 in
            # the swcfile
            if pat.search(line[0]) is not None:
                if re.search('<customProperties>', line):
                    customPropLineStart = fileLine
                if re.search('</customProperties>', line):
                    customPropLineEnd = fileLine
                    # print(fileLine)
            fileList[fileLine] = line
            fileLine += 1
    # write the newFile
    indexLines = np.arange(1, customPropLineStart)
    indexLines = np.append(indexLines, np.arange(customPropLineEnd + 1,
                                                 fileLine))
    file = open(new_filename, 'wt')
    for iline in indexLines:
        file.write(fileList[iline])
    file.close()
    print(original_filename + ' , has been modified on ' +
          str(datetime.datetime.now()))


def executeRotateX2YSWC(cell, auto_flip=False):
    """Rotate SWC data to have Y reflect the depth.

        Used after executeRotateSWC to have Y reflect cortica thickness and X
        cortical column width.

        Args:
            - cell: instance of a cell object from class ini_cell
            - auto_flip: boolean, if True the sign of the first soma Y-coordinate is checked. When Y-coordinate[0] <0
                         the reconstruction is flipped over the X axis to make the reconstruction towards pia a positive
                         Y.

        Return:
            - updated SWCfile that is rotated
    """
    try:
        swcFilename = cell.meta['swcFilename']
    except KeyError:
        pass
    radians = math.pi/2
    cell.swc_data['X'],\
        cell.swc_data['Y'] = rotate_around_point_highperf(cell.swc_data['X'],
                                                          cell.swc_data['Y'],
                                                          radians,
                                                          origin=(0, 0))
    cell.swc_data['X'] = np.round(cell.swc_data['X'], 3)
    cell.swc_data['Y'] = np.round(cell.swc_data['Y'], 3)
    swc_filename = os.path.join(cell.morph_path, swcFilename)
    add_comment = dict()
    add_comment[1] = 'Translated and rotated all X and Y data point:'
    add_comment[2] = 'All X and Y-coordinate rotated with ' +\
                     str(radians) + ' radians.'
    add_comment[3] = 'Using the corpus collusum underneath soma as x,y = 0'
    if auto_flip == True:
        if cell.swc_data['Y'][cell.swc_data['TYPE'] == 1][0] < 0:
            cell.swc_data['Y'] = np.round(cell.swc_data['Y'] * -1, 3)
            # inverse to make pia a positive number
            add_comment[4] = ('Flipped reconstruction over X to make Pia a' +
                              'positive X')
    add_modifications_swc(add_comment_lines=add_comment, swc_meta=None,
                          data=cell.swc_data, original_filename=swc_filename,
                          new_filename=swc_filename)
    return None


def move_z_alldata(swc_filename, add_to_z):
    """ Moves all z-coordinates of a soma to the targetZ and updates swcFile.

        If targetZ is empty it will make sure that all coordinates are in
        the same zDepth.

        Args:
            - swc_filename, fullpath to swcFile
            - add_to_z, addition of amount in um
        Returns:
            - updated swcFile

    """
    data, meta = readSWCplus.readSWCp(swc_filename)
    if add_to_z is None:
        return None
    data['Z'] = data['Z'] + add_to_z
    add_comment = dict()
    add_comment[1] = 'Moved Z coordinates of all points: ' + str(add_to_z) + 'µm'

    add_modifications_swc(add_comment_lines=add_comment, swc_meta=None,
                          data=data, original_filename=swc_filename,
                          new_filename=swc_filename)
    return None