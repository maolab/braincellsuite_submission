""" Post-fixation calculation in swcFiles and live images.

Module that uses contours and soma data from data, based on description
in metadata to calculate:
1) thickness of the slice
2) cortical thickness (corpus callosum to pia)
3) distance cellbody to pia

Functions:
    - extractContourData: creates a new dataset with contours from SWC.
    - extractsomaData: appends som contour to contourData.
    - extractContourDataEphysImages: extract contour data out of txt files.
    - calc_cortexThickness: calculate the thickness of the cortex over pia to
        corpus callosum, distance soma to pia relative to
        cortical thickness, and orientation of cortexthickness.
    - calc_sliceThickness: calculate the z depth for each point on the
        PiaSurface line relative to PiaSlide.
    - calc_somaCoM: calculate center of mass soma contour.
    - visContourData: visualization of contourData in plotly.
    - executeCalcPostFixDims: executes the extraction of contourData
        to get shrinkage measurements.
"""

import copy
import warnings
import numpy as np
import pandas as pd
from scipy import interpolate
import matplotlib.pyplot as plt
import plotly as py
import plotly.graph_objs as go

from nested_lookup import nested_lookup
from brainCellSuite.analysis.morphometry.vector import centerOMass, \
    intercept2LinFunc, thicknessT, pnt2line


def extractContourData(data, meta):
    """ Creates a new dataset with contours from SWC.

    Check for presence of names CCSurface, PiaSurface, PiaSlide.
    Extract all data for those contours.

    Args:
        - data: from swcFile numpy.ndarray swc data per row organized:
            [ID, TYPE, X, Y, Z, R, P].
        - meta: ordered dictionary containing all .XML data stored
            in the SWCplus file

    Returns:
        - contourData: numpy.ndarray swc data per row organized: ID,
            TYPE, X, Y, Z, R, P of only contours and borders.
    """
    contourData = dict()
    contourNames = (nested_lookup('@name', meta))
    contourNameList = ['CCSurface',
                       'PiaSurface',
                       'PiaSlide']
    for idx, val in enumerate(contourNameList):
        if not contourNameList[idx] in contourNames:
            warnings.showwarning("Couldn't find the contour name" +
                                 contourNameList[
                                     idx] + ", make sure to change meta"
                                            "annotations in SWC before "
                                            "resuming analysis.")
            contourData[contourNameList[idx]] = np.empty(1, float)
            contourData[contourNameList[idx]].fill(np.nan)
        elif not contourNameList[idx] in nested_lookup('@name',
                                                       meta['swcPlus'][
                                                           'customTypes'][
                                                           'border']):
            # in SWCplus: borders are open lines, whereas contours are
            # closed lines. So if the contourNameList items are not in
            # the borders group they should be converted in neurolucida
            # before resuming analysis
            warnings.showwarning("Couldn't find the contour name " +
                                 contourNameList[
                                     idx] + " within borders, make sure to"
                                            "change contour into open line "
                                            "(border) in Neurolucida"
                                            "before resuming analysis",
                                 UserWarning)
        else:
            contourID = []
            for idx2, val2 in \
                    enumerate(meta['swcPlus']['customTypes']['border']):
                # iterate through all borders to find the id
                if meta['swcPlus']['customTypes']['border'][idx2] \
                        ['@name'] == contourNameList[idx]:
                    contourID = int(meta['swcPlus']['customTypes']
                                    ['border'][idx2]['@id'])
                    contourData[contourNameList[idx]] = \
                        data[data['TYPE'] == contourID]
    # there is a list of aditional contour which may not
    # be present in all files. If present add them to the
    # contourData
    contourNameListExtras = ['Outline',
                             'CC',
                             'IC',
                             'fornix',
                             'latVentrical',
                             'ACA']
    for idx, val in enumerate(contourNameListExtras):
        if contourNameListExtras[idx] in contourNames:
            contourID = []
            for idx2, val2 in \
                    enumerate(meta['swcPlus']['customTypes']['border']):
                # iterate through all borders to find the id
                if meta['swcPlus']['customTypes']['border'][idx2] \
                        ['@name'] == contourNameListExtras[idx]:
                    contourID = int(meta['swcPlus']['customTypes']
                                    ['border'][idx2]['@id'])
                    contourData[contourNameListExtras[idx]] = \
                        data[data['TYPE'] == contourID]
            for idx2, val2 in \
                    enumerate(meta['swcPlus']['customTypes']['contour']):
                # iterate through all contours to find the id
                if isinstance(meta['swcPlus']['customTypes']['contour'], list):
                    if meta['swcPlus']['customTypes']['contour'][idx2] \
                            ['@name'] == contourNameListExtras[idx]:
                        contourID = int(meta['swcPlus']['customTypes']
                                        ['contour'][idx2]['@id'])
                        subsetData = data[data['TYPE'] == contourID]
                        if contourNameListExtras[idx] in contourData:
                            subsetData = np.concatenate((
                                contourData[contourNameListExtras[idx]],
                                subsetData), axis=0)
                        contourData[contourNameListExtras[idx]] = subsetData
                else:
                    if meta['swcPlus']['customTypes']['contour'] \
                            ['@name'] == contourNameListExtras[idx]:
                        contourID = int(meta['swcPlus']['customTypes']
                                        ['contour']['@id'])
                        subsetData = data[data['TYPE'] == contourID]
                        if contourNameListExtras[idx] in contourData:
                            subsetData = np.concatenate((
                                contourData[contourNameListExtras[idx]],
                                subsetData), axis=0)
                        contourData[contourNameListExtras[idx]] = subsetData
    return contourData


def extractsomaData(data, meta, contourData):
    """Appends soma contour to contourData.

    Check for presence of name somaContour.
    Extract all data for those contours

    Args:
        - data: from swcFile numpy.ndarray swc data per row organized:
            [ID, TYPE, X, Y, Z, R, P].
        - meta: ordered dictionary containing all .XML data stored
            in the SWCplus file
        - contourData: from swcFile numpy.ndarray swc data per row
            organized: [ID, TYPE, X, Y, Z, R, P] only containing contours
            and borders

    Returns:
        contourData: numpy.ndarray swc data per row organized: ID,
            TYPE, X, Y, Z, R, P of only contours and borders.

    """

    # check whether that the data indeed contains TYPE = 1 soma data
    if not 1 in data['TYPE']:
        warnings.warn("Couldn't find data containing TYPE = 1 = soma."
                      " Please review SWC or run readSWCplus.py with "
                      " somaContour to correct TYPE annotations before "
                      "resuming analysis",
                      UserWarning)
        contourData['soma'] = None
        return contourData
    somaData = data[data['TYPE'] == 1]
    contourData['soma'] = somaData
    # make sure there is only one soma present in the data;
    # in SWCplus each entity have their own starting point: P = -1
    if len(somaData[somaData['P'] == -1]) != 1:
        warnings.warn("There are multiple somata present in this file."
                      " Please review SWC before resuming analysis",
                      UserWarning)
    return contourData


def extractContourDataEphysImages(txtfilename, scalefactor):
    """Extract contour data out of txt files.

    Images of the slice during whole-cell patch-clamp experiments are being
    annotated in FIJI and contour data of the pia, corpus callosum, and soma
    are save in text files. coordinates and the respective contour name are
    extracted and returned as contourData.

    Args:
        - txtfilename: fullpath to the contour txt file
        - scalefactor: factor by which both X and Y are multiplied to correct
            for scaling based on the settings when the contours are drawn:
            usually about 3.322 um per pixel (LSPS rig 2018 4x objective;
            800x600pixel imagescapture)

    Returns:
        - contourDataEphysImage: containing pia, cc, and soma contours
            corrected with scalefactor. nested dictionary with numpy.ndarrays
    """
    if scalefactor is None:
        scalefactor = 1
    print('contourdata is corrected for scaling :' + str(scalefactor))
    contour = pd.read_table(txtfilename,
                            sep=",",
                            header=None,
                            names=['ID', 'X', 'Y', 'contourName'],
                            index_col=0,
                            dtype={'ID': int,
                                   'X': float,
                                   'Y': float,
                                   'contourName': str})
    contour['contourName'] = contour['contourName'].str.strip()
    contour['X'] = contour['X'] * scalefactor
    contour['Y'] = contour['Y'] * scalefactor
    contour.loc[contour['contourName'] == 'ccSurface', 'contourName'] = \
        'CCSurface'
    # Check whether all three contours are present otherwise abort
    if 'PiaSurface' not in list(contour['contourName']):
        print('PiaSurface contour is lacking in :' + txtfilename)
        contourDataEphysImage = None
        return contourDataEphysImage
    if 'soma' not in list(contour['contourName']):
        print('soma contour is lacking in :' + txtfilename)
        contourDataEphysImage = None
        return contourDataEphysImage
    if 'CCSurface' not in list(contour['contourName']):
        print('CCSurface contour is lacking in :' + txtfilename)
        contourDataEphysImage = None
        return contourDataEphysImage
    contourDataEphysImage = dict()
    contourNameList = ['CCSurface', 'PiaSurface', 'soma']
    for idx, val in enumerate(contourNameList):
        contourDataEphysImage[contourNameList[idx]] = dict()
        contourDataEphysImage[contourNameList[idx]]['X'] = copy.deepcopy(
            (contour['X'][contour['contourName'] ==
                          contourNameList[idx]].values))
        contourDataEphysImage[contourNameList[idx]]['Y'] = copy.deepcopy(
            (contour['Y'][contour['contourName'] ==
                          contourNameList[idx]].values))

    return contourDataEphysImage


def calc_cortexThickness(contourData, verbose=True):
    """Calculate the thickness of the cortex over pia to
        corpus callosum, distance soma to pia relative to
        cortical thickness, and orientation of cortexthickness.

    Uses contourData to determine the shortest distance between pia and corpus
    callosum contour, with the criteria that the line should run over the
    center of mass of the soma. Based on this shortest distance the cortical
    thickness is determined as well as relative position of soma in the cortex
    expressed as (distance pia to soma) / (distance pia to corpus callosum).
    The slope and intercept of the shortest distance are stored to annotate
    the orientation for which the shortest distance was found for the soma.

    Args:
        - contourData: numpy.ndarray swc data or txt data per row organized: ID
            TYPE, X, Y, Z, R, P of only contours and borders.
        - verbose: boolean, default = False. Will plot or print
                calculated values or notifications if true.

    Returns:
        - contourData: updated with extra keys:
            - cortex thickness in um (shortest distance between pia and cc
                over soma)
            - relative position soma to cortical thickness (distance pia to
                soma) /  (cortical thickness)
            - slopeCorticalThickness: to generate an orientation line for
                cortical thickness, originating from soma
            - interceptCorticalThickness: to generate an orientation line for
                cortical thickness, originating from soma
    """
    CCSurface = copy.deepcopy([contourData['CCSurface']['X'],
                               contourData['CCSurface']['Y']])
    # check that CCSurface has at least 50 annotated coordinates
    if len(CCSurface[0]) <= 49:
        tempX = None
        tempY = None
        for idx, xvals in enumerate(CCSurface[0][1:]):
            x1 = CCSurface[0][idx]
            x2 = CCSurface[0][idx + 1]
            f = interpolate.interp1d([x1, x2],
                                     [CCSurface[1][idx],
                                      CCSurface[1][idx + 1]],
                                     fill_value="extrapolate")
            if x1 <= x2:
                sign = 1
                steps = (x2 - x1) / 20
            elif x1 >= x2:
                sign = -1
                steps = (x1 - x2) / 20
            if x1 != x2:
                xnew = np.arange(x1, x2 + (steps * sign), steps * sign)
                ynew = f(xnew)
                if idx == 0:
                    tempX = xnew
                    tempY = ynew
                elif idx >= 1:
                    tempX = np.append(tempX, xnew[1:])
                    tempY = np.append(tempY, ynew[1:])
        CCSurface[0] = tempX.astype(float)
        CCSurface[1] = tempY.astype(float)
        CCSurface[0] = CCSurface[0][~np.isnan(CCSurface[0])]
        CCSurface[1] = CCSurface[1][~np.isnan(CCSurface[1])]
    PiaSurface = copy.deepcopy([contourData['PiaSurface']['X'],
                                contourData['PiaSurface']['Y']])
    CCSurfaceNull = np.zeros([2, len(CCSurface[0])])
    PiaSurfaceNull = np.zeros([2, len(PiaSurface[0])])
    cm = centerOMass(contourData['soma']['X'],
                     contourData['soma']['Y'],
                     np.ones(len(contourData['soma']['X'])),
                     np.ones(len(contourData['soma']['X'])))
    cmXY = copy.deepcopy(cm[0:2])
    # transform data so that soma lays on 0,0
    cmXYNull = cmXY - cmXY
    CCSurfaceNull[0] = CCSurface[0] - cmXY[0]
    CCSurfaceNull[1] = CCSurface[1] - cmXY[1]
    PiaSurfaceNull[0] = PiaSurface[0] - cmXY[0]
    PiaSurfaceNull[1] = PiaSurface[1] - cmXY[1]
    if verbose:
        plt.plot(CCSurfaceNull[0], CCSurfaceNull[1], '--')
        plt.plot(PiaSurfaceNull[0], PiaSurfaceNull[1], '--')
        plt.plot(cmXYNull[0], cmXYNull[1], 'o')
        plt.xlim([-1000, 1000])
        plt.ylim([-1000, 1000])
        axes = plt.gca()
        x_vals = np.array(axes.get_xlim())
    thicknessTempSave = dict()
    piaDistanceTempSave = dict()
    thicknessSlopeSave = dict()
    xCC = dict()
    yCC = dict()
    # Iterate through CCSurface annotation points
    for i in np.arange(0, len(CCSurfaceNull[0]) - 1, + 1):
        thickSlope = CCSurfaceNull[1][i] / CCSurfaceNull[0][i]
        for pnt, Xval in enumerate(PiaSurfaceNull[0][:-1]):
            yLeftPia = PiaSurfaceNull[1][pnt]
            yRightPia = PiaSurfaceNull[1][pnt + 1]
            # get the constants for linear regression between left and right
            # point of PiaSurfaceNull
            a = (PiaSurfaceNull[1][pnt] - PiaSurfaceNull[1][pnt + 1]) / \
                (PiaSurfaceNull[0][pnt] - PiaSurfaceNull[0][pnt + 1])
            b = PiaSurfaceNull[1][pnt] - a * PiaSurfaceNull[0][pnt]
            # calculate the intersection point
            xInter, yInter = intercept2LinFunc([thickSlope, 0], [a, b])
            try:
                if xInter != 'NaN':
                    testX = np.logical_or(
                        Xval >= xInter >= PiaSurfaceNull[0][pnt + 1],
                        Xval <= xInter <= PiaSurfaceNull[0][pnt + 1])
                    testY = np.logical_or(
                        yLeftPia >= yInter >= yRightPia,
                        yLeftPia <= yInter <= yRightPia)
                    if testX and testY:
                        thicknessTemp = thicknessT(CCSurfaceNull[0][i],
                                                   CCSurfaceNull[1][i],
                                                   xInter,
                                                   yInter)
                        piaDistanceTemp = thicknessT(0,
                                                     0,
                                                     xInter,
                                                     yInter)
                        piaDistanceTempSave[thicknessTemp] = piaDistanceTemp
                        thicknessTempSave[thickSlope] = thicknessTemp
                        thicknessSlopeSave[thicknessTemp] = thickSlope
                        xCC[thicknessTemp] = CCSurfaceNull[0][i]
                        # point of origin from the CCSurface border
                        yCC[thicknessTemp] = CCSurfaceNull[1][i]
                        break
            except:
                pass

    shortestDistance = min(thicknessTempSave.values())
    shortestDistanceSlope = thicknessSlopeSave[shortestDistance]
    piaDistance = piaDistanceTempSave[shortestDistance]
    xOriginCC = xCC[shortestDistance]
    yOriginCC = yCC[shortestDistance]

    if verbose:
        print('Piadistance: %s µm, Cortical thickness: %s µm'
              %(piaDistance, shortestDistance))
        plt.plot(CCSurfaceNull[0], CCSurfaceNull[1], '--')
        plt.plot(PiaSurfaceNull[0], PiaSurfaceNull[1], '--')
        plt.plot(cmXYNull[0], cmXYNull[1], 'o')
        plt.xlim([-1000, 1000])
        plt.ylim([-1000, 1000])
        axes = plt.gca()
        x_vals = np.array(axes.get_xlim())
        plt.plot(x_vals, x_vals * shortestDistanceSlope, 'k-')
    contourData['cortexThickness'] = shortestDistance
    contourData['somaCortexPosition'] = piaDistance / shortestDistance
    contourData['slopeCorticalThickness'] = shortestDistanceSlope
    contourData['interceptCorticalThickness'] = \
        cmXY[1] - shortestDistanceSlope * cmXY[0]
    contourData['xOriginCC'] = xOriginCC + cmXY[0]  # make sure to re-translate
    # the coordinate system to the original coordinates.
    contourData['yOriginCC'] = yOriginCC + cmXY[1]
    return contourData


def calc_sliceThickness(contourData, markerSlide, showFig=False):
    """ Calculate the z depth for each point on the PiaSurface line
     relative to PiaSlide.

     Do notice the distance is not calculated as
     the minimal distance from point to line, but rather from point to
     point-near-line so that the distance resembles the z-depth
     perpendicular to the slide and coverslip.

    Args:
        - contourData: numpy.ndarray swc data per row organized: ID,
        TYPE, X, Y, Z, R, P of only soma, contour,s and borders.
        Important! requires to have soma contour present!
        - markerSlide: boolean flag for presence of a marker dot on the
        microscopeSlide.

    Returns:
        - contourData: addition to contourData;
            - postFixSliceThickness: median of all zDepths over
                piaSurface.
            - zDepthVector: dictionary of zDepths(values) per position
                (keys) of piaSurface
            - piaSurfaceSlideCoord: piaSurface coordinates as in
                contourData with z-coordinates set to 0
    """

    piaSurfaceSlideCoord = copy.deepcopy(
        [contourData['PiaSurface']['X'],
         contourData['PiaSurface']['Y'],
         contourData['PiaSurface']['Z']])
    zDepthVector = dict()
    for point in np.arange(0, len(contourData['PiaSurface'])):
        # iterate through each annotated point in the PiaSurface
        # contour and find shortest z-depth with nearest line segment
        # of PiaSlide
        pnt = [contourData['PiaSurface']['X'][point],
               contourData['PiaSurface']['Y'][point],
               contourData['PiaSurface']['Z'][point]]
        nearestPoint = dict()
        nearestDistance = dict()

        for segment in np.arange(0, len(contourData['PiaSlide']) - 1):
            start = [contourData['PiaSlide']['X'][segment],
                     contourData['PiaSlide']['Y'][segment],
                     contourData['PiaSlide']['Z'][segment]]
            end = [contourData['PiaSlide']['X'][segment + 1],
                   contourData['PiaSlide']['Y'][segment + 1],
                   contourData['PiaSlide']['Z'][segment + 1]]
            try:
                nearestDistance[segment], nearestPoint[segment] = \
                    pnt2line(pnt, start, end)
            except (KeyError, IndexError):
                pass

        # determine the nearest segment, and calculate the z-depth
        # perpendicularto the coverslip/microscopeslide
        nearestPointKey = min(nearestDistance, key=nearestDistance.get)
        zDepthVector[point] = piaSurfaceSlideCoord[2][point] - \
                              nearestPoint[nearestPointKey][2]
        piaSurfaceSlideCoord[2][point] = nearestPoint[nearestPointKey][2]
        if markerSlide == 1:
            # In this case the marker on the microscope slide is the
            # absolute z = 0. Therefore overwrite all values relative
            # to z = 0 of the coordinate system, not relative to
            # piaSlide
            zDepthVector.update((key, contourData['PiaSurface']['Z'][key])
                                for key in zDepthVector)
            piaSurfaceSlideCoord[2][:] = 0

    contourData['postFixSliceThickness'] = np.median(
        np.fromiter(zDepthVector.values(), dtype=float))
    contourData['zDepthVector'] = zDepthVector
    contourData['piaSurfaceSlideCoord'] = piaSurfaceSlideCoord
    return contourData


def calc_somaCoM(contourData):
    """ Calculate center of mass soma contour.

    Calculates the average coordinate per axis with equal weight.

    Args:
        - contourData: numpy.ndarray swc data per row organized: ID,
        TYPE, X, Y, Z, R, P of only soma, contour,s and borders.
        Important! requires to have soma contour present!

    Returns:
        - contourData: addition to contourData;
            - somaCOM: center of mass of soma.
    """

    if 'soma' in contourData:
        cm = centerOMass(contourData['soma']['X'],
                         contourData['soma']['Y'],
                         contourData['soma']['Z'],
                         np.ones(len(contourData['soma']['X'])))
        # calculate
        contourData['somaCOM'] = np.round(cm, 3)
    return contourData


def visContourData(contourData, savePath):
    """Visualization of contourData in plotly

    Dependent on the content of contourData makes a 3D scatter and line
    plot in plotly

    Args:
        - contourData: numpy.ndarray swc data per row organized: ID,
            TYPE, X, Y, Z, R, P of only soma, contour,s and borders.
        - savePath: fullpath to save the plotly html

    Returns:
        - py.offline.plot(fig, auto_open=True): will produce a link to
            plotly website.
    """
    traces = ['CCSurface', 'PiaSurface', 'PiaSlide', 'soma',
              'piaSurfaceSlideCoord', 'cortexThickness']
    dataPlot = []
    for idx, val in enumerate(traces):
        try:
            dataPlot.append(go.Scatter3d(x=contourData[val]['X'],
                                         y=contourData[val]['Y'],
                                         z=contourData[val]['Z'],
                                         mode='lines',
                                         name=val))
        except KeyError:
            pass
        if val == 'piaSurfaceSlideCoord':
            try:
                for line in np.arange(0, len(
                        contourData['piaSurfaceSlideCoord'][0])):
                    dataPlot.append(
                        go.Scatter3d(x=[contourData['PiaSurface']['X'][line],
                                        contourData[val][0][line]],
                                     y=[contourData['PiaSurface']['Y'][line],
                                        contourData[val][1][line]],
                                     z=[contourData['PiaSurface']['Z'][line],
                                        contourData[val][2][line]],
                                     mode='lines+markers',
                                     name='sliceThickness'))
                dataPlot.append(
                    go.Histogram(x=np.fromiter(contourData['zDepthVector'].
                                               values(), dtype=float),
                                 marker=dict(
                                     color='rgba(45, 65, 80, .75)'),
                                 xaxis='x2',
                                 yaxis='y2',
                                 name='localSliceThicknessHistogram'))
                dataPlot.append(
                    go.Scatter(x=[contourData['postFixSliceThickness'],
                                  contourData['postFixSliceThickness']],
                               y=[0, 3],
                               marker=dict(
                                   size=15,
                                   color='rgba(240,42,0,1)'),
                               xaxis='x2',
                               yaxis='y2',
                               mode='lines+markers',
                               name='median sliceThickness'))
                layout = go.Layout(xaxis=dict(domain=[0, 0.7]),
                                   xaxis2=dict(domain=[0.8, 1],
                                               title='z-depth'),
                                   yaxis2=dict(anchor='x2',
                                               title='Count'))
            except:
                pass
        if val == 'cortexThickness':
            try:
                dataPlot.append(go.Scatter3d(x=[contourData
                                                ['somaCOM'][0]],
                                             y=[contourData
                                                ['somaCOM'][1]],
                                             z=[contourData
                                                ['somaCOM'][2]],
                                             mode='markers',
                                             name='soma center of' +
                                                  'mass'))

            except:
                pass
    if 'layout' in locals():
        fig = go.Figure(data=dataPlot, layout=layout)
        fig['layout'].update(scene=dict(aspectmode="data"))
    else:
        fig = go.Figure(data=dataPlot)
        fig['layout'].update(scene=dict(aspectmode="data"))
        pass
    if savePath == 0 or savePath is None:
        return py.offline.plot(fig, auto_open=True)
    else:
        savePath = savePath + 'plotlyTempVisualizations/' + \
                   'postFixPlot.html'
        return py.offline.plot(fig, auto_open=True,
                               filename=savePath)


def executeCalcPostFixDims(cell, showFig=False):
    """Executes the extraction of contourData to get shrinkage
    measurements

    Args:
        - cell: instance of a cell object from class ini_cell
        - showFig: boolean to plot the contours and soma. Default: False

    Returns:
        - cell: instance of a cell object: addition to instance:
            contourData: duplicate of the instance with soma centerOMass
            and postFixSliceThickness
    """
    try:
        swcFilename = cell.meta['swcFilename']
    except:
        pass
    contourData = extractContourData(cell.swc_data, cell.swc_meta)
    contourData = extractsomaData(cell.swc_data,
                                  cell.swc_meta, contourData)
    contourData = calc_sliceThickness(contourData, 0)
    contourData = calc_somaCoM(contourData)
    contourData = calc_cortexThickness(contourData)
    if hasattr(cell, 'path'):
        savePath = cell.path
    else:
        savePath = 0
    if showFig:
        visContourData(contourData, savePath)
    cell.analysis['somaCOM'] = contourData['somaCOM']
    cell.analysis['postFixSliceThickness'] = \
        contourData['postFixSliceThickness']
    cell.analysis['swcSlope'] = contourData['slopeCorticalThickness']
    cell.analysis['swcCCOrigin'] = [contourData['xOriginCC'],
                                    contourData['yOriginCC']]

    return cell
