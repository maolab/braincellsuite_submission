"""Analysis in python on cluster results

Functions
    generate_average_morphology: Calculate stats on individual cells verus the
                                 averaged cluster morphology.


"""
# import all packages
import numpy as np
import pandas as pd
from pathlib import Path
import seaborn as sns
import copy
import sys
import matplotlib.pyplot as plt

from brainCellSuite import bcs_global
from sklearn.decomposition import PCA
from sklearn.decomposition import SparsePCA


def generate_average_morphology(cellAmpIDs_cluster=None,
                                align_parameter='fraction_overlap_dendrites',
                                morphology_voxel_array_path=
                                Path(bcs_global.path_morph_analysis)/
                                '__voxel_arrays__',
                                morphology_match_summary_file=
                                Path(bcs_global.path_morph_analysis)/
                                    'cell_matching'/
                                    'xyz-normby-cortical_thickness0.05'/
                                    'match_summary.h5',
                                orien_lib_filename=
                                Path(bcs_global.path_morph_analysis)/
                                '__voxel_arrays__' /
                                'lib_xyz-normby-cortical_thickness'
                                '0.05_1104_orientations.csv'
                                ):
    """ Calculate stats on individual cells verus the averaged cluster
     morphology.


    Args:
        cellAmpIDs_cluster: list of cellAmpIDs that belong to the cluster to be
            averaged.
        align_parameter: string, method ued to align the individual cells as
            well as to select the exemplar.
            Must be one amongst the align_parameters;
            'fraction_overlap_dendrites', 'fraction_overlap_basal',
            'fraction_overlap_apical', 'distance_overlap_only_dendrites',
            'distance_overlap_only_apical', 'distance_overlap_only_basal',
            'distance_any_present_dendrites', 'distance_any_present_apical',
            'distance_any_present_basal', 'corrcoef_overlap_only_dendrites',
            'corrcoef_overlap_only_apical', 'corrcoef_overlap_only_basal',
            'corrcoef_any_present_dendrites', 'corrcoef_any_present_apical',
            'corrcoef_any_present_basal'.
        morphology_voxel_array_path: full path where directories are located
            which contain the rotated morphology voxel arrays.
        morphology_match_summary_file: full path to the file holding the
            summary HDF5 file.
        orien_lib_filename: full path to the file holding the library of
            orientations used to transform morphology into voxel arrays.

    Returns:
        fig: plotly figure containing the averaged morphology
        stats_cluster: pandas dataframe containing stats of the morphologies
            relative to the averaged morphology.
    """
    align_parameters = ['fraction_overlap_dendrites',
                        'fraction_overlap_basal',
                        'fraction_overlap_apical',
                        'distance_overlap_only_dendrites',
                        'distance_overlap_only_apical',
                        'distance_overlap_only_basal',
                        'distance_any_present_dendrites',
                        'distance_any_present_apical',
                        'distance_any_present_basal',
                        'corrcoef_overlap_only_dendrites',
                        'corrcoef_overlap_only_apical',
                        'corrcoef_overlap_only_basal',
                        'corrcoef_any_present_dendrites',
                        'corrcoef_any_present_apical',
                        'corrcoef_any_present_basal',]
    assert align_parameter in align_parameters, \
        '%s is not option, choose from : %s' %(align_parameter,
                                               align_parameters)
    if isinstance(cellAmpIDs_cluster, list):
        cellAmpIDs_cluster = np.array(cellAmpIDs_cluster)
    hdf = pd.read_hdf(str(morphology_match_summary_file), 'match_summary/score',
                      columns=[align_parameter],
                      where="template = cellAmpIDs_cluster")
    idx = pd.IndexSlice
    hdf = hdf.loc[idx[:, cellAmpIDs_cluster], idx[:]]
    mask_self = zip(cellAmpIDs_cluster, cellAmpIDs_cluster)
    hdf = hdf.drop(mask_self, axis=0)
    hdf = hdf.sort_values(align_parameter, ascending=False)
    hdf['score'] = np.arange(0, len(hdf))
    grouped = hdf.groupby('template').sum() + hdf.groupby('match').sum()
    grouped = grouped.sort_values('score', ascending=True)
    exemplar = grouped.iloc[0].name
    hdf_flip = pd.read_hdf(str(morphology_match_summary_file),
                           'match_summary/flip',
                           columns=[align_parameter],
                           where="template = exemplar")
    hdf_flip = hdf_flip.loc[idx[:, cellAmpIDs_cluster], idx[:]]
    hdf_axial = pd.read_hdf(str(morphology_match_summary_file),
                            'match_summary/axial',
                            columns=[align_parameter],
                            where="template = exemplar")
    hdf_axial = hdf_axial.loc[idx[:, cellAmpIDs_cluster], idx[:]]
    hdf_azimuth = pd.read_hdf(str(morphology_match_summary_file),
                              'match_summary/azimuth',
                              columns=[align_parameter],
                              where="template = exemplar")
    hdf_azimuth = hdf_azimuth.loc[idx[:, cellAmpIDs_cluster], idx[:]]
    hdf_polar = pd.read_hdf(str(morphology_match_summary_file),
                            'match_summary/polar',
                            columns=[align_parameter],
                            where="template = exemplar")
    hdf_polar = hdf_polar.loc[idx[:, cellAmpIDs_cluster], idx[:]]
    file_dict = {cellAmpID: str(morphology_voxel_array_path / cellAmpID /
        'xyz-normby-cortical_thickness0.05' /
        str('_'.join([cellAmpID,
                      '0.05',
                      str(hdf_axial.loc[
                              idx[:, cellAmpID],
                              idx[align_parameter]].tolist()[0]),
                      str(hdf_azimuth.loc[
                              idx[:, cellAmpID],
                              idx[align_parameter]].tolist()[0]),
                      str(hdf_polar.loc[
                              idx[:, cellAmpID],
                              idx[align_parameter]].tolist()[0]),
                      '.csv'])))
                 for cellAmpID in cellAmpIDs_cluster}
    degree_dict = {
        cellAmpID: [hdf_flip.loc[idx[:, cellAmpID],
                                 idx[align_parameter]].tolist()[0],
                    hdf_axial.loc[idx[:, cellAmpID],
                                  idx[align_parameter]].tolist()[0],
                    hdf_azimuth.loc[idx[:, cellAmpID],
                                    idx[align_parameter]].tolist()[
                        0],
                    hdf_polar.loc[idx[:, cellAmpID],
                                  idx[align_parameter]].tolist()[0]]
        for cellAmpID in cellAmpIDs_cluster}
    # now obtain the exact rads for the degrees
    orien_lib = pd.read_csv(str(orien_lib_filename),
                            index_col=['flip', 'axial', 'azimuth', 'polar'])
    rad_dict = dict()
    for cellAmpID, packed_trans in degree_dict.items():
        flip, ax_deg, az_deg, po_deg = packed_trans
        ax_rad, az_rad, po_rad = orien_lib.loc[
            (flip, ax_deg, az_deg, po_deg), ['axial_rads', 'azimuth_rads',
                                             'polar_rads']]
        rad_dict[cellAmpID] = [packed_trans[0], ax_rad, az_rad, po_rad]

    density_data_dict = {cellAmpID: pd.read_csv(file_path, index_col=['ID']) for
                         cellAmpID, file_path in file_dict.items()}
    # determine extremes
    x_extremes = list()
    y_extremes = list()
    z_extremes = list()
    for cellAmpID, density_data in density_data_dict.items():
        # filter for only voxels with data with apical and basal info
        density_data = density_data[
            np.logical_or(density_data.sum_length_basal > 0,
                          density_data.sum_length_apical > 0)]
        # update the dict
        density_data_dict[cellAmpID] = density_data
        # obtain extremes
        x_extremes.append(np.max(np.abs(density_data.X)))
        y_extremes.append(np.max(np.abs(density_data.Y)))
        z_extremes.append(np.max(np.abs(density_data.Z)))
    x_extreme = np.max(np.array(x_extremes))
    y_extreme = np.max(np.array(y_extremes))
    z_extreme = np.max(np.array(z_extremes))
    x_id_range = 2 * x_extreme + 1
    y_id_range = 2 * y_extreme + 1
    z_id_range = 2 * z_extreme + 1
    # generate a mean array, binary array, sd array for basal, apical and
    # dendrites
    dendrites_mean = np.zeros((x_id_range, y_id_range, z_id_range))
    dendrites_binary = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    dendrites_sd = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    apical_mean = np.zeros((x_id_range, y_id_range, z_id_range))
    apical_binary = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    apical_sd = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    basal_mean = np.zeros((x_id_range, y_id_range, z_id_range))
    basal_binary = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    basal_sd = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    mean_arrays = {'dendrites': dendrites_mean, 'apical': apical_mean,
                   'basal': basal_mean}
    binary_arrays = {'dendrites': dendrites_binary, 'apical': apical_binary,
                     'basal': basal_binary}
    sd_arrays = {'dendrites': dendrites_sd, 'apical': apical_sd,
                 'basal': basal_sd}
    density_dataarray_dict = dict()
    # fill arrays
    c = 0
    for cellAmpID, density_data in density_data_dict.items():
        # translate the voxel coordinates
        density_data.X += x_extreme
        density_data.Y += y_extreme
        density_data.Z += z_extreme
        # make arrays from the dataframes
        dendrites_ = np.zeros((x_id_range, y_id_range, z_id_range))
        apical_ = np.zeros((x_id_range, y_id_range, z_id_range))
        basal_ = np.zeros((x_id_range, y_id_range, z_id_range))
        # fill arrays
        dendrites_[np.array(density_data.X.astype(int)),
                   np.array(density_data.Y.astype(int)),
                   np.array(density_data.Z.astype(
                       int))] = density_data.sum_length_basal + \
                                density_data.sum_length_apical
        basal_[np.array(density_data.X.astype(int)),
               np.array(density_data.Y.astype(int)),
               np.array(density_data.Z.astype(int))] = \
            density_data.sum_length_basal

        apical_[np.array(density_data.X.astype(int)),
                np.array(density_data.Y.astype(int)),
                np.array(
                    density_data.Z.astype(int))] = \
            density_data.sum_length_apical

        # check for flips
        if hdf_flip.loc[idx[:, cellAmpID],
                        idx[align_parameter]].tolist()[0] == 1:
            dendrites_ = np.flip(dendrites_, axis=0)
            basal_ = np.flip(basal_, axis=0)
            apical_ = np.flip(apical_, axis=0)

        # to aid the iterative process:
        local_array_dict = {'dendrites': dendrites_, 'apical': apical_,
                            'basal': basal_}
        density_dataarray_dict[cellAmpID] = local_array_dict
        # place the data into the mean, binary, or SD arrays
        for process in mean_arrays:
            mean_arrays[process] += local_array_dict[process]

        for process in binary_arrays:
            mask_local = local_array_dict[process] > 0
            mask_local = mask_local.astype(int)
            binary_arrays[process][c] = mask_local

        for process in sd_arrays:
            sd_arrays[process][c] += local_array_dict[process]
        c += 1
    for process in sd_arrays:
        sd_arrays[process] = np.std(sd_arrays[process], axis=0)
    for process in mean_arrays:
        mean_arrays[process] = mean_arrays[process] / len(cellAmpIDs_cluster)

    # calculate distribution of apical and basal dendrites over Y
    Y_density_apical = np.sum(
        np.flip(np.rot90(np.sum(mean_arrays['apical'], axis=2), 1), axis=0),
        axis=1)
    Y_density_apical[Y_density_apical == 0] = np.nan
    Y_density_basal = np.sum(
        np.flip(np.rot90(np.sum(mean_arrays['basal'], axis=2), 1), axis=0),
        axis=1)
    Y_density_basal[Y_density_basal == 0] = np.nan

    # calculate distribution of apical and basal dendrites over X
    X_density_apical = np.sum(
        np.flip(np.rot90(np.sum(mean_arrays['apical'], axis=2), 1), axis=0),
        axis=0)
    X_density_apical[X_density_apical == 0] = np.nan
    X_density_basal = np.sum(
        np.flip(np.rot90(np.sum(mean_arrays['basal'], axis=2), 1), axis=0),
        axis=0)
    X_density_basal[X_density_basal == 0] = np.nan
    # calculate the statistics of the morphologies in relation to the averaged
    # morhology.
    stat_cols = ['fraction_voxel_represented_at_75percent_average',
                 'fraction_voxel_represented_at_85percent_average',
                 'corrcoef_column_density_apical',
                 'corrcoef_width_density_apical',
                 'corrcoef_density_basal',
                 'corrcoef_density_dendrite']
    stats_cluster = pd.DataFrame(data=None,
                                 index=cellAmpIDs_cluster,
                                 columns=stat_cols)
    # get representation fractions
    mask_75 = copy.deepcopy(np.sum(binary_arrays['dendrites'], axis=0)/
                            len(cellAmpIDs_cluster))
    mask_85 = copy.deepcopy(np.sum(binary_arrays['dendrites'], axis=0) /
                            len(cellAmpIDs_cluster))
    # make all voxels that have dendrites present in 75% or 85% of all cells 1
    mask_75[mask_75 >= 0.75] = 1
    mask_85[mask_85 >= 0.85] = 1
    # for density set nan to 0
    Y_density_apical[np.isnan(Y_density_apical)] = 0
    X_density_apical[np.isnan(X_density_apical)] = 0
    for i in range(len(cellAmpIDs_cluster)):
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'fraction_voxel_represented_at_75percent_average'] = (
            np.sum(binary_arrays['dendrites'][i, :, :, :][mask_75 == 1])/
            np.sum(binary_arrays['dendrites'][i, :, :, :]))
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'fraction_voxel_represented_at_85percent_average'] = (
            np.sum(binary_arrays['dendrites'][i, :, :, :][mask_85 == 1]) /
            np.sum(binary_arrays['dendrites'][i, :, :, :]))
        # generate distribution along column, width and whole grid
        #for column (Y)
        column_dist_cell = np.zeros((len(Y_density_apical)))
        column_index_cell = (np.sum(
            density_data_dict[cellAmpIDs_cluster[i]].groupby('Y')
            ['sum_length_apical']).index).tolist()
        column_dist_cell[column_index_cell] = (np.sum(
            density_data_dict[cellAmpIDs_cluster[i]].groupby('Y')
            ['sum_length_apical'])).tolist()
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'corrcoef_column_density_apical'] = np.corrcoef(
            Y_density_apical, column_dist_cell)[0, 1]  # correlation coefficient
            # of distribution cell against mean distribution(the 'standard')

        # for width (X)
        width_dist_cell = np.zeros((len(X_density_apical)))
        width_index_cell = (np.sum(
            density_data_dict[cellAmpIDs_cluster[i]].groupby('X')
            ['sum_length_apical']).index).tolist()
        width_dist_cell[width_index_cell] = (np.sum(
            density_data_dict[cellAmpIDs_cluster[i]].groupby('X')
            ['sum_length_apical'])).tolist()
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'corrcoef_width_density_apical'] = np.corrcoef(
            X_density_apical, width_dist_cell)[0, 1]  # correlation coefficient
            # of distribution cell against mean distribution(the 'standard')

        # for 3D all dendrites
        dendrites_dist_cell = (density_dataarray_dict[cellAmpIDs_cluster[i]]
            ['dendrites'][mean_arrays['dendrites'] > 0])
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'corrcoef_density_dendrite'] = np.corrcoef(
            mean_arrays['dendrites'][mean_arrays['dendrites'] > 0],
            dendrites_dist_cell)[0, 1]  # correlation coefficient
            # of distribution cell against mean distribution(the 'standard')

        # for 3D basal
        basal_dist_cell = (density_dataarray_dict[cellAmpIDs_cluster[i]]
            ['basal'][mean_arrays['basal'] > 0])
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'corrcoef_density_basal'] = np.corrcoef(
            mean_arrays['basal'][mean_arrays['basal'] > 0],
            basal_dist_cell)[0, 1]  # correlation coefficient
        # of distribution cell against mean distribution(the 'standard')
    return stats_cluster


def feature_reduction(feature_table=None,
                      cv_threshold=0.25,
                      corr_threshold=0.95,
                      n_threshold=0.01,
                      PCA_threshold=10,
                      sPCA_threshold=0.5,
                      PCA_explained_variance_ratio=0.95,
                      sPCA_n_components=10,
                      check_normalization=True,
                      log_transform=True,
                      report_heatmap=True,
                      show_original=False,
                      verbose=True):
    """Reduce feature dimensions by thresholding on CV, Correlation, Sparseness,
    and PCA.

    Features columns are kept if data within a feature fullfills the
    thresholding criteria for CV, correlation coefficient, and sparseness. In
    addition, principle components are calculated up to cumulative sum of
    explained variance ratio reaches criterium, and n_components with sPCA is
    calculated. Using PCA_threshold and sPCA_threshold, further reduction of
    features is proposed. So in total three feature tables and 2 reduced-
    composed feature tables are returned, with affiliated meta data.

    Args:
        - feature_table: pandas dataframe: n cells x n features. No
            normalization is required! Will test whether the variance is equal
            across all columns. In that case it assumes z-score normalization.
        - cv_threshold: float, default 0.25, minimal coefficient of variation
            within a feature. Default of 0.25 means variation should be more
            than 25%, otherwise that feature is removed.
       - corr_threshold: float, default=0.95, maximal correlation between two
            features. Default of 0.95 means that two features cannot correlate
            more than 0.95 (on a scale of -1 to 1). Note that anticorrelated
            features of -0.95 are still kept.  If two features are above
            the threshold, one of the two will be removed. A decision tree is
            used to decide which is kept: 1) in case only one of the two
            features describes an 'allDendrites' feature, this feature is kept.
            2) in case only one of the two features describes a 'branch'
            feature, this feature is kept. 3) in case only one of the two
            features describes a 'normalized' feature, this feature is kept.
            4) if none of the above rules lead to the removal of a correlated
            features the left feature (the first in the list) will be kept.
        - n_threshold: float default=0.01, minimum fraction of total data
            entries for the feature (== n cells) that should have a different
            value from the remaining entries. The number of cells is rounded up.
            I.e. in a cohort of 350 cells and with the default threshold of 0.01
            gives a threshold of 4 cells. Therefore at least 5 cells need to
            have a different data entry as compared to the other 350 - 5 = 345
            cells.
        - PCA_threshold: float default=0.5, fraction of features proposed based
            on PCA that contribute the most to all principle components.
            After reduction of features using the criteria (CV, Corr,
            Sparseness) PCA is performed the features are sorted based on their
            overall loadings/contributions to the PCA. The top fraction of this
            feature list is used to generate a further reduced feature table.
        - sPCA_threshold: float default=0.5, fraction of features proposed based
            on sparsePCA that contribute the most to all principle components.
            After reduction of features using the criteria (CV, Corr,
            Sparseness) sparsePCA is performed the features are sorted based on
            their overall sum contributions to the PCA. Contribution is measured
            as number of PCs that the feature appeared in plus number of PCs for
            which the feature contributed at least 80% to the PC. The top
            fraction of this feature list is used to generate a further reduced
            feature table.
        - PCA_explained_variance_ratio: float, default=0.95, explained variance
            ratio threshold for the PCA analysis. PCA analysis will continue to
            generate principle components until the threshold of cumulative sum
            of the explained variance ratio is met.
        - sPCA_n_components: integer, default=10, number of components that will
            be generate by the sparsePCA function. Note that the order of
            components is random and will have no relation to their explained
            variance ratio!
        - check_normalization: boolean, default=True, check based on variance
            if data is already z-scored.
        - log_transform: boolean, default=True, natural log transform data
            before performing z-score normalization.
        - report_heatmap: None, string, show the heatmap of the data based on
            either: 'reduced' = feature reduction by CV/Corr/Sparseness alone,
            'PCA_reduced' = feature reduction by CV/Corr/Sparseness and PCA,
            'sPCA_reduced' = feature reduction by CV/Corr/Sparseness and sPCA.
            'PCA' = Principle components after feature reduction by CV/Corr/
            Sparseness.
            'sPCA' = Principle components after feature reduction by CV/Corr/
            Sparseness.
        - show_original, boolean, default=False. Show the heatmap of the
            original feature_table data.
        - verbose, boolean, default=True. Show the reports during processing.
    """
    if 'location_soma' in feature_table.columns.tolist():
        location_soma = feature_table.location_soma
    else:
        location_soma = None
        print('No location data is present! Will calculate fine but meta will'
              ' not contain location data')
    report_variants = ['reduced', 'PCA_reduced', 'sPCA_reduced', 'PCA', 'sPCA']
    assert report_heatmap in report_variants, '%s is not a valid input, pl' \
                                              'ease select one of the follow' \
                                              'ing %s' % (report_heatmap,
                                                          report_variants)

    if np.logical_and(np.nanmax(feature_table.var()) -
                      np.nanmin(feature_table.var()) <= sys.float_info.epsilon *
                      len(feature_table.columns) * 10, check_normalization):
        print('Based on variance of all features the feature table is already'
              'z-score normalized. Will not further perform normalization. '
              '\nPlease rerun function with check_normalization=False, to make'
              'sure that the data is normalized even if the variance is already'
              '1.')
    else:
        if verbose:
            print('Z-score normalizing data')
        # first z-score normalize
        feature_table = (feature_table - np.mean(feature_table, axis=0)) / \
                        np.std(feature_table, axis=0)

    # First step is reduce features by low variance thresholding
    # calculate the Coefficient of variation within each column,
    # remove any with a variation lower than cv_threshold.
    cv_table = copy.deepcopy(np.std(feature_table) / np.mean(feature_table))
    cv_table[cv_table.isna()] = 0  # make sure no na are present
    # (if SD=0 and mean=0)
    low_variance_features = cv_table[cv_table<=cv_threshold].index.tolist()
    if verbose:
        print('With a variance threshold of %s, the following features will be '
              'removed: %s' % (str(cv_threshold), low_variance_features))

    # check for features with very sparse data i.e. less than 1% of the data or
    # less than 1 cell has data different from the remainder of the dataset.
    n_threshold = np.ceil(len(feature_table) * n_threshold)
    sparse_columns = list()
    for col in feature_table.columns:
        val_unique = feature_table[col].unique()
        if len(val_unique) <= n_threshold:
            # in this case very few cells have a value for the feature
            # make sure that the number of observations per unique values are
            # low too
            n_unique = np.zeros(len(val_unique))
            for i, val in enumerate(val_unique):
                n_unique[i] = np.sum(feature_table[col]==val)
            majority = np.max(n_unique)
            if majority >= len(feature_table) - n_threshold:
                # in this case the majority of cells has the same value so
                # discard
                sparse_columns.append(col)
    if verbose:
        print('\nWith a sparseness threshold of %s cell(s) with data, '
              'the following features will be removed: %s' % (str(n_threshold),
                                                              sparse_columns))
    remove_cols = np.unique(np.hstack((low_variance_features, sparse_columns)))
    keep_index = np.ones(len(feature_table.columns)).astype(bool)
    for feature in remove_cols:
        keep_index[np.where(np.array(
            feature_table.columns.tolist()) == feature)[0][0]] = False
    reduced_cols = np.array(feature_table.columns.tolist())[keep_index]
    # generate a new dataframe without the low variance and sparse features.
    feature_table_reduced = copy.deepcopy(feature_table[reduced_cols])

    # Second step is reduce by high correlational features
    corr = feature_table_reduced.corr()
    columns = np.full((corr.shape[0],), True, dtype=bool)
    column_names = corr.columns.tolist()
    removed_features = list()
    remained_features = list()
    for i in range(corr.shape[0]):
        for j in range(i+1, corr.shape[0]):
            if corr.iloc[i, j] >= corr_threshold:
                if columns[j]:
                    # impose rules which column to remove
                    col = np.array([i, j])
                    # first rule: prioritize 'allDendrites' if competing with
                    # 'basal' or 'apical'
                    i_all = column_names[i].split('_')[-1] == 'allDendrites'
                    j_all = column_names[j].split('_')[-1] == 'allDendrites'
                    select_col = np.invert(np.array([i_all, j_all]))
                    if np.logical_xor(i_all, j_all):
                        columns[col[select_col][0]] = False
                        removed_features.append(column_names[col[
                            select_col][0]])
                        remained_features.append(column_names[col[
                            np.invert(select_col)][0]])
                        continue
                    # second rule: prioritize 'N_branch'
                    i_node = column_names[i].split('_')[1] == 'branch'
                    j_node = column_names[j].split('_')[1] == 'branch'
                    select_col = np.array([i_node, j_node])
                    if np.logical_xor(i_node, j_node):
                        columns[col[select_col][0]] = False
                        removed_features.append(column_names[col[
                            select_col][0]])
                        remained_features.append(column_names[col[
                            np.invert(select_col)][0]])
                        continue
                    # third rule: prioritize normalized
                    i_norm = column_names[i].split('_')[1] == 'norm'
                    j_norm = column_names[j].split('_')[1] == 'norm'
                    select_col = np.array([i_norm, j_norm])
                    if np.logical_xor(i_norm, j_norm):
                        columns[col[select_col][0]] = False
                        removed_features.append(column_names[col[
                            select_col][0]])
                        remained_features.append(column_names[col[
                            np.invert(select_col)][0]])
                        continue
                    # otherwise it is 'first come first serve', so the first
                    # column [i] will remain the second column [j] will be
                    # removed
                    else:
                        columns[j] = False
                        removed_features.append(column_names[j])
                        remained_features.append(column_names[i])
                        continue

    selected_columns = feature_table_reduced.columns[columns]
    feature_table_reduced = feature_table_reduced[selected_columns]

    # perform PCA on the feature_table
    # make sure no NAs in the data
    feature_table_reduced_PCA = feature_table_reduced.dropna(axis=1, how='any')
    component_table_PCA = copy.deepcopy(feature_table_reduced_PCA)
    transformer_PCA = PCA(n_components=PCA_explained_variance_ratio, copy=True,
                          whiten=False, svd_solver='auto',
                          tol=sys.float_info.epsilon, iterated_power='auto',
                          random_state=10) # random state makes sure to reliably
    # generate the PCA output.
    transformer_PCA.fit(component_table_PCA)
    PCA_data = transformer_PCA.transform(component_table_PCA)
    _, PCs = np.shape(PCA_data)
    component_table_PCA = pd.DataFrame(data=PCA_data,
                                     columns=['PC_%s' % pca
                                              for pca in range(PCs)],
                                     index=feature_table.index)
    feature_table_PCA_reduced = None

    feature_reduction_meta = dict()
    feature_reduction_meta['location_soma'] = location_soma
    feature_reduction_meta['normalized_original_table'] = feature_table
    feature_reduction_meta['removed_features'] = list(set(meta['normalized_original_table'].columns.tolist()).difference(feature_table_reduced.columns.tolist()))


    feature_tables = {'reduced': feature_table_reduced,
                      'PCA_reduced': feature_table_PCA_reduced,
                      'sPCA_reduced': feature_table_sPCA_reduced,
                      'PCA': component_table_PCA,
                      'sPCA': component_table_sPCA}
    if report_heatmap:
        if show_original:
            # report stats
            if verbose:
                print('\nTotal features in original data: %s features' %
                      len(feature_table.columns.tolist()))
                print('\nReduced to: %s features' %
                      len(feature_tables[report_heatmap].columns.tolist()))
                print('\nTotal cells in data: %s cells' % len(feature_table))
            # report heatmaps
            plt.figure(figsize=(30, 10))
            plt.subplot(1, 2, 1)
            plt.title('Full dataset n_features = %s' %
                      len(feature_table.columns.tolist()))
            sns.heatmap(feature_table.T, cmap='PRGn', vmin=-3, vmax=3, )
            plt.subplot(1, 2, 2)
            plt.title('Feature reduced dataset n_features = %s'
                      % len(feature_tables[report_heatmap].columns.tolist()))
            # after removal of the correlated features
            sns.heatmap(feature_tables[report_heatmap].T, cmap='PRGn', vmin=-3,
                        vmax=3, )
        else:
            # report stats
            if verbose:
                print('\nTotal features in original data: %s features' %
                      len(feature_table.columns.tolist()))
                print('\nReduced to: %s features' %
                      len(feature_tables[report_heatmap].columns.tolist()))
                print('\nTotal cells in data: %s cells' %
                      len(feature_table))
            # report heatmaps
            plt.figure(figsize=(30, 10))
            plt.title('Feature reduced dataset n_features = %s' %
                      len(feature_tables[report_heatmap].columns.tolist()))
            sns.heatmap(feature_tables[report_heatmap].T, cmap='PRGn', vmin=-3,
                        vmax=3, )
    return feature_tables, feature_reduction_meta


