"""Visualization of SWC data

Functions:
    - get_endpoints: Gathers coordinate IDs that are endpoints.
    - visNeuronSWC: Use SWC data to get data of the dendritic
                    and axonal arborization and contours.
    - imageSWC2D: Use SWC data to plot normalized 2D profiles of cell with
                  layer borders.
    - screenImageSWC2D: Use SWC data to plot normalized 2D profiles of cell
                        with layer borders at screen resolution.
    - mo: Generate a gallery overview of cell morphologies.
"""

import plotly as py
import plotly.graph_objs as go
import numpy as np
import os
from itertools import compress
import matplotlib.pyplot as plt
import numpy.matlib as nm
from pathlib import Path
from brainCellSuite import bcs_global
from brainCellSuite.analysis.morphometry.calc_postFixDims import \
    extractContourData, extractsomaData, calc_cortexThickness
from brainCellSuite import ini_cell
from brainCellSuite._io.readSWCplus import get_dataTree
from brainCellSuite.analysis.morphometry.extractFeatures import \
    findCorticalThickness



def get_endpoints(swc_data):
    """Gathers coordinate IDs that are endpoints

    Takes SWC data, will be filtered to be part of neuronal process TYPE >1
    and TYPE < 5. Any coordinate ID which is not referred to in the position
    (P) parameter is considered an endpoint.

    Args:
        - swc_data: numpy.ndarray swc data per row organized: ID,
        TYPE, X, Y, Z, R, P of only soma, contour,s and borders.

    Return:
        - endPointData: data that resemble endpoint
    """
    dataTree = get_dataTree(swc_data)
    IDs, P = (list(dataTree['ID']), list(dataTree['P']))
    endpointIDs = set(IDs) - set(P)
    endpointData = swc_data[np.isin(swc_data['ID'], list(endpointIDs))]
    return endpointData


def visNeuronSWC(cellAmpID,
                 db_path,
                 viewEndpoints=False,
                 viewContours=False,
                 viewLocalContours=False,
                 savePath=None,
                 auto_open=True,
                 show_grid=False,
                 line_thickness_factor=2):
    """ Use SWC data to get data of the dendritic and axonal
        arborization and contours.

    SWC data contains ID of coordinate and P (link to
    previous coordinate).Segments are drawn based on coordinate for
    which ID == P-1. Linking nodes are found by searching for the ID
    defined by P of a segments first coordinate.

    Args:
        - cellAmpID: unique identifier for the recorded cell eg. BJ1832a1
        - db_path: fullpath to folder containing insularDatabase
            and maoLabSlideDatabase.
        - viewEndpoints: default is None; so by default will not show the
            endpoints. Any assignment will trigger visualization of
            endpoints eg. viewEndpoints=False will show endpoints neuronal
            processes.
        - viewContours: default is False; so by default will not show all the
            contours. viewContours=True will show contours.
        - viewLocalContours: default is False; so by default will not show the
            local contours ie pia and corpus callosum contours.
        - savePath: defulat is None; so by default plot is saved in current
            directory. When path is given a subfolder is created: neuronPlots.
            the plot is saved as the current 'cell_amp_id'.html
        - auto_open: default is True, so by default plot is shown. auto_open=
            False supresses opening the plot.
        - show_grid: default is False, so no grids,labels, and zerolines shown.
        - line_thickness_factor: default is 2, thickness as defined by tracing,
                                 multiplied with factor line_thickness_factor.

    Returns:
        - plotly based interactive 3D scatterplot with lines
    """
    cell = ini_cell.Cell(cellAmpID, db_path=db_path)
    dataTree = get_dataTree(cell.swc_data)
    # # Append centerOfMass; soma center
    # com = cell.swc_data[:][cell.swc_data['TYPE'] == 1] 
    # com =com[:][com['P']==-1]
    # dataTree = np.append(com,dataTree)

    processNameDict = {1: 'soma',
                       2: 'axon',
                       3: 'dendrite',
                       4: 'apical dendrite'}
    processColorDict = {1: 'rgb(64, 64, 64)',
                        2: 'rgb(43, 106, 108)',
                        3: 'rgb(242, 151, 36)',
                        4: 'rgb(184, 13, 73)'}
    # In order to only draw only once a segment in the legend make dict
    visible_legend_entry_dict = {2: True,
                                 3: True,
                                 4: True}
    # temp color change to match plotting of 2D
    processColorDict[1] = 'rgb(0, 0, 0)'
    processColorDict[3] = 'rgb(254, 0, 0)'
    processColorDict[4] = 'rgb(22, 0, 249)'
    #
    processDiameterDict = {1: 6, 2: 3, 3: 4, 4: 5}
    contourNameList = ['CCSurface',
                       'PiaSurface',
                       'PiaSlide',
                       'Outline',
                       'CC',
                       'IC',
                       'fornix',
                       'latVentrical',
                       'ACA']
    localContourNameList = ['CCSurface',
                       'PiaSurface',
                       'PiaSlide']
    contourColorDict = {'CCSurface': 'rgba(57, 158, 14, 1)',
                        'PiaSurface': 'rgba(10, 77, 100, 1)',
                        'PiaSlide': 'rgba(42, 77, 154, 1)',
                        'Outline': 'rgba(39, 109, 255, 0.7)',
                        'CC': 'rgba(39, 109, 63, 0.7)',
                        'IC': 'rgba(255, 30, 40, 0.7)',
                        'fornix': 'rgba(196, 11, 40, 0.7)',
                        'latVentrical': 'rgba(5, 45, 58, 0.7)',
                        'ACA': 'rgba(38, 145, 133, 0.7)'}
    contourGroup = {'CCSurface': 'thicknessBorders',
                    'PiaSurface': 'thicknessBorders',
                    'PiaSlide': 'thicknessBorders',
                    'Outline': 'AnatomicalBorders',
                    'CC': 'AnatomicalBorders',
                    'IC': 'AnatomicalBorders',
                    'fornix': 'AnatomicalBorders',
                    'latVentrical': 'AnatomicalBorders',
                    'ACA': 'AnatomicalBorders'}

    dataPlot = []
    # print contours
    if viewContours is False and viewLocalContours is False:
        viewContours = 'legendonly'
    elif viewContours is False and viewLocalContours is True:
        viewContours = False
    contourData = extractContourData(cell.swc_data, cell.swc_meta)
    for contourName in contourNameList:
        if contourName in contourData:
            # In some cases there are multiple startingpoints for borders
            # eg lateral ventricles. Make sure to treat the as seperate
            P = list(contourData[contourName]['P'] == -1)
            startPoints = list(compress(range(len(P)), P))
            startPoints.append(len(contourData[contourName]))
            for idx, start in enumerate(startPoints[:-1]):
                dataPlot.append(
                    go.Scatter3d(
                        x=contourData[contourName]['X'][start:
                                                        startPoints[idx +
                                                                    1]],
                        y=contourData[contourName]['Y'][start:
                                                        startPoints[idx +
                                                                    1]],
                        z=contourData[contourName]['Z'][start:
                                                        startPoints[idx +
                                                                    1]],
                        mode='lines',
                        name=contourName,
                        visible=viewContours,
                        legendgroup=contourGroup[contourName],
                        line={'width': 4 * line_thickness_factor,
                              'reversescale': False,
                              'color': contourColorDict[contourName]}
                    )
                )
    # print only local contours
    if viewLocalContours is True:
        contourData = extractContourData(cell.swc_data, cell.swc_meta)
        for contourName in localContourNameList:
            if contourName in contourData:
                # In some cases there are multiple startingpoints for borders
                # eg lateral ventricles. Make sure to treat the as seperate
                P = list(contourData[contourName]['P'] == -1)
                startPoints = list(compress(range(len(P)), P))
                startPoints.append(len(contourData[contourName]))
                for idx, start in enumerate(startPoints[:-1]):
                    dataPlot.append(
                        go.Scatter3d(
                            x=contourData[contourName]['X'][start:
                                                            startPoints[idx +
                                                                        1]],
                            y=contourData[contourName]['Y'][start:
                                                            startPoints[idx +
                                                                        1]],
                            z=contourData[contourName]['Z'][start:
                                                            startPoints[idx +
                                                                        1]],
                            mode='lines',
                            name=contourName,
                            legendgroup=contourGroup[contourName],
                            line={'width': 4 * line_thickness_factor,
                                  'reversescale': False,
                                  'color': contourColorDict[contourName]}
                        )
                    )
    # print all endpoint
    if viewEndpoints is True:
        endpointData = get_endpoints(cell.swc_data)
        dataPlot.append(
            go.Scatter3d(
                x=endpointData['X'],
                y=endpointData['Y'],
                z=endpointData['Z'],
                mode='markers',
                name='endpoint',
                legendgroup='endpoints',
                marker={'size': 5 * line_thickness_factor,
                        'color': 'rgba(79,93,105,0.8)'}))

    # also print soma
    dataPlot.append(
        go.Scatter3d(
            x=cell.swc_data['X'][cell.swc_data['TYPE'] == 1],
            y=cell.swc_data['Y'][cell.swc_data['TYPE'] == 1],
            z=cell.swc_data['Z'][cell.swc_data['TYPE'] == 1],
            mode='lines',
            name='soma',
            line={'width': processDiameterDict[1] *
                               line_thickness_factor,
                  'color': processColorDict[1],
                  'reversescale': False}))

    # print neuronal processes
    startSegment = list()
    stopSegment = list()
    # the Soma center of Mass can act as the startpoint for all neuronal
    # processes. In that case there is no P=-1 for TYPE:2,3,4 and a extra
    # initial segment need to be added from center of mass to startSegment
    stemIDs = list()
    stemIDs = dataTree['ID'][dataTree['P'] == -1]
    comON = False
    if stemIDs.size < 1:
        comON = True
        somaData = cell.swc_data[cell.swc_data['TYPE'] == 1]
    for ID, P in list(zip(dataTree['ID'], dataTree['P'])):
        if int(ID) - (int(P)) != 1:
            startSegment.append(int(ID))
    np.array(startSegment)
    stopSegment = np.array(startSegment[1:])
    stopSegment = np.append(stopSegment, dataTree['ID'][len(dataTree)-1])
    stopSegment = stopSegment

    for start, stop in list(zip(startSegment, stopSegment)):
        data2Plot = ()
        if (comON and dataTree[int(np.where(dataTree['ID'] == start)[0])]
                ['P'] == 1):
            data2Plot = somaData[somaData['P'] == -1]
        try:
            connectedNodeP = dataTree['P'][int(np.where(dataTree['ID'] ==
                                                        start)[0])]
            data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                      connectedNodeP)[0])]
            if data2Plot:
                data2Plot = np.append(data2Plot, data2PlotAp)
            else:
                data2Plot = data2PlotAp

        except (KeyError, ValueError, TypeError):
            pass
        if data2Plot:
            data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                   start)[0]):
                                      int(np.where(dataTree['ID'] ==
                                                   stop)[0])]
            data2Plot = np.append(data2Plot, data2PlotAp)
        else:
            data2Plot = dataTree[:][int(np.where(dataTree['ID'] ==
                                                 start)[0]):
                                    int(np.where(dataTree['ID'] ==
                                                 stop)[0])]
        process = dataTree['TYPE'][int(
                          np.where(dataTree['ID'] == start)[0])]
        dataPlot.append(
            go.Scatter3d(
                x=data2Plot['X'],
                y=data2Plot['Y'],
                z=data2Plot['Z'],
                mode='lines',
                name=processNameDict[dataTree['TYPE'][int(
                    np.where(dataTree['ID'] == start)[0])]],
                legendgroup=str(dataTree['TYPE'][int(
                    np.where(dataTree['ID'] == start)[0])]),
                showlegend=visible_legend_entry_dict[process],
                line={'width': processDiameterDict[dataTree['TYPE'][int(
                    np.where(dataTree['ID'] == start)[0])]] *
                               line_thickness_factor,
                      'reversescale': False,
                      'color': processColorDict[process]}))
        # Shut down the visiblity for this particular process
        visible_legend_entry_dict[process] = False
    if show_grid:
        ticks = 'outside'
        ticks_name = ('y', 'x', 'z')
    else:
        ticks = ''
        ticks_name = ('', '', '')
    layout = go.Layout({'scene': {
                            'yaxis': {'title': ticks_name[0],
                                      'showgrid': show_grid,
                                      'showline': show_grid,
                                      'zeroline': show_grid,
                                      'ticks': ticks,
                                      'showticklabels': show_grid},
                            'xaxis': {'title': ticks_name[1],
                                      'showgrid': show_grid,
                                      'showline': show_grid,
                                      'zeroline': show_grid,
                                      'ticks': ticks,
                                      'showticklabels': show_grid},
                            'zaxis': {'title': ticks_name[2],
                                      'showgrid': show_grid,
                                      'showline': show_grid,
                                      'zeroline': show_grid,
                                      'ticks': ticks,
                                      'showticklabels': show_grid}}})

    fig = go.Figure(data=dataPlot, layout=layout)
    camera = dict(
        up=dict(x=0, y=1, z=1),
        center=dict(x=0, y=0, z=0),
        eye=dict(x=0.1, y=0.1, z=2.5))
    fig['layout'].update(scene=dict(aspectmode="data"))
    fig['layout'].update(scene=dict(camera=camera))
    fig['layout'].update(title=go.layout.Title(text=str(cellAmpID),
                                               xref='paper', x=0))
    if savePath == 0 or savePath is None:
        return py.offline.plot(fig, auto_open=auto_open)
    savePath = savePath + 'cellMorphFiles/neuronPlots/' +\
        str(cell.cell_amp_id) + '.html'
    return py.offline.plot(fig, auto_open=auto_open, filename=savePath)


def imageSWC2D(cellAmpID,
               db_path=bcs_global.path_database,
               save_path=os.path.join(
                   os.path.dirname(bcs_global.path_morph_data), '2DProjectEPS'),
               layerBins=[0, 0.1, 0.15, 0.3, 0.5, 0.7, 0.9, 1],
               layerColors=[(203, 203, 203), (235, 127, 79), (178, 34, 35),
                            (126, 255, 211), (0, 190, 255), (0, 1, 253),
                            (1, 1, 1)],
               sideView=True,
               visLayerBorders=True,
               defWidth=0.5,
               fileformat='eps',
               dpi=1000,
               auto_open=True):
    """ Use SWC data to plot normalized 2D profiles of cell with layer borders.

    SWC data contains ID of coordinate and P (link to
    previous coordinate).Segments are drawn based on coordinate for
    which ID == P-1. Linking nodes are found by searching for the ID
    defined by P of a segments first coordinate.

    Args:
        - cellAmpID: unique identifier for the recorded cell eg. BJ1832a1
        - db_path: fullpath to folder containing insularDatabase
                   and maoLabSlideDatabase.
        - savePath: default is '/Users/jongbloe/Box Sync/insularDatabases/cell
                    MorphFiles/2DProjectEPS/' ; so by default plot is saved in
                    2DProjectEPS. The plot is saved as the current
                    'cell_amp_id'.eps
        - layerBins: default is [0,0.1,0.15,0.3,0.5,0.7,0.9,1]; Used to
                     determine location of the soma within the local cortical
                     thickness.
        - layerColors: default is RGB style; Colors selected to represent
                       different layers.
        - sideView: default is True; show YZ projection of cell.
        - visLayerBorders: default is True; show layer borders based on
                           layerBins
        - defWidth: default is 0.5, linewidth of all lines in plot.
        - auto_open: default is True, so by default plot is shown. auto_open=
                     False supresses opening the plot.

    Returns:
        - eps file of cell
    """
    cell = ini_cell.Cell(cellAmpID, db_path)
    layerBins = [0, 0.1, 0.15, 0.3, 0.5, 0.7, 0.9, 1]
    dataTree = get_dataTree(cell.swc_data)
    dataTree = dataTree[dataTree['TYPE'] >= 3]
    # # Append centerOfMass; soma center
    # com = cell.swc_data[:][cell.swc_data['TYPE'] == 1] 
    # com =com[:][com['P']==-1]
    # dataTree = np.append(com,dataTree)
    
    visLayerBorders = True
    cortThick = findCorticalThickness(cell)
    layerApical = np.array(layerColors)/255
    layerBasal = layerApical*0.5

    # normalize dataTree to cortical thickness:
    dataTree['X'] = dataTree['X']/cortThick
    dataTree['Y'] = dataTree['Y']/cortThick
    dataTree['Z'] = dataTree['Z']/cortThick
    xlim = ([round(round(min(dataTree['X']), 2), 1), max(dataTree['X']) +
             0.05])
    zlim = ([round(round(min(dataTree['Z']), 2), 1),
             round(round(max(dataTree['Z']), 2), 1)])
    zlim = np.array(zlim) - xlim[0]
    layerBins = 1 - np.array(layerBins)

    for left, right, iC in zip(layerBins[0:-1], layerBins[1:],
                               np.arange(len(layerBins))):
        if left > (1-cell.meta['somaCortexPosition']) >= right:
            colorDendrite = layerBasal[:][iC]
            colorApical = layerApical[:][iC]
    colorSelect = {2: np.array((101, 0, 223)) / 255, 3: colorDendrite,
                   4: colorApical}
    fig, axes = plt.subplots()
    # print borders
    if visLayerBorders is True:
        xBorders = nm.repmat(np.array(xlim).reshape(2, 1), 1,
                                    len(layerBins))
        yBorders = nm.repmat(layerBins, 2, 1)
        axes.plot(xBorders, yBorders, '--', color=(0.8, 0.8, 0.8),
                 linewidth=defWidth)
        axes.plot([(xlim[0], xlim[0]), (xlim[1], xlim[1])], [(0, 1), (0, 1)],
                 '-', color=(0.7, 0.7, 0.7), linewidth=defWidth)

    # print neuronal processes
    startSegment = list()
    stopSegment = list()
    # the Soma center of Mass can act as the startpoint for all neuronal
    # processes. In that case there is no P=-1 for TYPE:2,3,4 and a extra
    # initial segment need to be added from center of mass to startSegment
    stemIDs = list()
    stemIDs = dataTree['ID'][dataTree['P'] == -1]
    for ID, P in list(zip(dataTree['ID'], dataTree['P'])):
        if int(ID) - (int(P)) != 1:
            startSegment.append(int(ID))
    np.array(startSegment)
    stopSegment = np.array(startSegment[1:])
    stopSegment = np.append(stopSegment, dataTree['ID'][len(dataTree)-1])
    stopSegment = stopSegment
    for start, stop in list(zip(startSegment, stopSegment)):
        data2Plot = ()
        try:
            connectedNodeP = dataTree['P'][int(np.where(dataTree['ID'] ==
                                                        start)[0])]
            data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                   connectedNodeP)[0])]
            if data2Plot:
                data2Plot = np.append(data2Plot, data2PlotAp)
            else:
                data2Plot = data2PlotAp

        except (KeyError, ValueError,TypeError):
            pass
        if data2Plot:
            data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                   start)[0]):
                                      int(np.where(dataTree['ID'] ==
                                                   stop)[0])]
            data2Plot = np.append(data2Plot, data2PlotAp)
        else:
            data2Plot = dataTree[:][int(np.where(dataTree['ID'] ==
                                                 start)[0]):
                                    int(np.where(dataTree['ID'] ==
                                                 stop)[0])]
        cS = colorSelect[data2Plot['TYPE'][len(data2Plot)-1]]
        axes.plot(data2Plot['X'], data2Plot['Y'], '-', color=cS,
                 linewidth=defWidth)

    # print soma
    soma = cell.swc_data[cell.swc_data['TYPE'] == 1]
    axes.plot(soma['X'][1:]/cortThick, soma['Y'][1:]/cortThick, '-',
             color=(0, 0, 0), linewidth=defWidth)
    axes.fill(soma['X']/cortThick, soma['Y']/cortThick, color=(0, 0, 0),
             linewidth=defWidth)

    if sideView:
        # print borders
        if visLayerBorders is True:
            zBorders = nm.repmat(np.array(zlim).reshape(2, 1), 1,
                                        len(layerBins)) * -1
            zBorder = zBorders-(zBorders[1, 0]-xBorders[1, 0])
            yBorders = nm.repmat(layerBins, 2, 1)
            zarray = (np.array([(zlim[0] * -1, zlim[0] * -1), (zlim[1]*-1,
                                                               zlim[1]*-1)]) -
                      (zBorders[1, 0] - xBorders[1, 0]))
            axes.plot(zBorder, yBorders, '--', color=(0.8, 0.8, 0.8),
                     linewidth=defWidth)
            axes.plot(zarray, [(0, 1), (0, 1)], '-', color=(0.7, 0.7, 0.7),
                     linewidth=defWidth)

        # print neuronal processes
        startSegment = list()
        stopSegment = list()
        # the Soma center of Mass can act as the startpoint for all neuronal
        # processes. In that case there is no P=-1 for TYPE:2,3,4 and a extra
        # initial segment need to be added from center of mass to startSegment
        stemIDs = list()
        stemIDs = dataTree['ID'][dataTree['P'] == -1]
        for ID, P in list(zip(dataTree['ID'], dataTree['P'])):
            if int(ID) - (int(P)) != 1:
                startSegment.append(int(ID))
        np.array(startSegment)
        stopSegment = np.array(startSegment[1:])
        stopSegment = np.append(stopSegment, dataTree['ID'][len(dataTree)-1])
        stopSegment = stopSegment
        zstart = 1
        for start, stop in list(zip(startSegment, stopSegment)):
            data2Plot = ()
            try:
                connectedNodeP = dataTree['P'][int(np.where(dataTree['ID'] ==
                                                            start)[0])]
                data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                       connectedNodeP)[0])]
                if data2Plot:
                    data2Plot = np.append(data2Plot, data2PlotAp)
                else:
                    data2Plot = data2PlotAp

            except (KeyError, ValueError, TypeError):
                pass
            if data2Plot:
                data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                       start)[0]):
                                          int(np.where(dataTree['ID'] ==
                                                       stop)[0])]
                data2Plot = np.append(data2Plot, data2PlotAp)
            else:
                data2Plot = dataTree[:][int(np.where(dataTree['ID'] ==
                                                     start)[0]):
                                        int(np.where(dataTree['ID'] ==
                                                     stop)[0])]
            cS = colorSelect[data2Plot['TYPE'][len(data2Plot)-1]]
            axes.plot(((data2Plot['Z'] - xlim[0]) * -1) -
                     (zBorders[1, 0] - xBorders[1, 0]),
                     data2Plot['Y'], '-', color=cS, linewidth=defWidth)
            if zstart > (min(((data2Plot['Z'] - xlim[0]) * -1) -
                             (zBorders[1, 0] - xBorders[1, 0]))):
                zstart = min(((data2Plot['Z'] - xlim[0]) * -1) -
                             (zBorders[1, 0]-xBorders[1, 0]))
        # print soma
        soma = cell.swc_data[cell.swc_data['TYPE'] == 1]
        axes.plot((((soma['Z'][1:] / cortThick) - xlim[0]) * -1) -
                 (zBorders[1, 0] - xBorders[1, 0]), soma['Y'][1:]/cortThick,
                 '-', color=(0, 0, 0), linewidth=defWidth)
        axes.fill((((soma['Z'] / cortThick) - xlim[0]) * -1) -
                 (zBorders[1, 0]-xBorders[1, 0]), soma['Y'] / cortThick,
                 color=(0, 0, 0), linewidth=defWidth)
        axes.plot((zstart, zstart), (0.98, -0.05), '-', color=(0.2, 0.2, 0.2),
                 linewidth=defWidth)
        axes.arrow(zstart, -0.05, 0.1, 0, color=(0.2, 0.2, 0.2), width=0.0001,
                  head_width=0.03, linewidth=defWidth)
    axes.axis('off')
    axes.set_title(str(cell.cell_amp_id), fontdict={'fontsize': 7})
    # fig = axes.gcf()
    fig.set_size_inches(0.7, 1.4)
    if save_path == 0 or save_path is None:
        plt.close('all')
        return fig, axes
    else:
        save_path = save_path + str(cell.cell_amp_id) + '.' + str(fileformat)
        plt.savefig(save_path, format=fileformat, dpi=dpi, transparant=True)
    plt.close('all')
    return fig, axes


def screenImageSWC2D(cellAmpID,
                     db_path,
                     save_path=os.path.join(
                   os.path.dirname(bcs_global.path_morph_data), '2DProjectEPS'),
                     layerBins=[0, 0.1, 0.15, 0.3, 0.5, 0.7, 0.9, 1],
                     layerColors=[(203, 203, 203), (235, 127, 79),
                                  (178, 34, 35), (126, 255, 211),
                                  (0, 190, 255), (0, 1, 253), (1, 1, 1)],
                     sideView=True,
                     visLayerBorders=True,
                     defWidth=0.5,
                     fileformat='eps',
                     dpi=1000,
                     auto_open=True):
    """ Use SWC data to plot normalized 2D profiles of cell with layer borders
    at screen resolution.

    SWC data contains ID of coordinate and P (link to
    previous coordinate).Segments are drawn based on coordinate for
    which ID == P-1. Linking nodes are found by searching for the ID
    defined by P of a segments first coordinate.

    Args:
        - cellAmpID: unique identifier for the recorded cell eg. BJ1832a1
        - db_path: fullpath to folder containing insularDatabase
            and maoLabSlideDatabase.
        - savePath: default is '/Users/jongbloe/Box Sync/insularDatabases/
                    cellMorphFiles/2DProjectEPS/' ; so by default plot is
                    saved in 2DProjectEPS. The plot is saved as the current
                    'cell_amp_id'.eps
        - layerBins: default is [0,0.1,0.15,0.3,0.5,0.7,0.9,1]; Used to
                     determine location of the soma within the local cortical
                     thickness.
        - layerColors: default is RGB style; Colors selected to represent
                       different layers.
        - sideView: default is True; show YZ projection of cell.
        - visLayerBorders: default is True; show layer borders based on
                           layerBins
        - defWidth: default is 0.5, linewidth of all lines in plot.
        - auto_open: default is True, so by default plot is shown. auto_open=
                     False supresses opening the plot.

    Returns:
        - eps file of cell
    """
    # plt.figure()
    cell = ini_cell.Cell(cellAmpID, db_path)
    layerBins = [0, 0.1, 0.15, 0.3, 0.5, 0.7, 0.9, 1]
    dataTree = get_dataTree(cell.swc_data)
    dataTree = dataTree[dataTree['TYPE'] >= 2]
    # # Append centerOfMass; soma center
    # com = cell.swc_data[:][cell.swc_data['TYPE'] == 1] 
    # com =com[:][com['P']==-1]
    # dataTree = np.append(com,dataTree)
    
    visLayerBorders = True
    cortThick = findCorticalThickness(cell)
    layerApical = np.array(layerColors)/255
    layerBasal = layerApical*0.5

    # normalize dataTree to cortical thickness:
    dataTree['X'] = dataTree['X']/cortThick
    dataTree['Y'] = dataTree['Y']/cortThick
    dataTree['Z'] = dataTree['Z']/cortThick
    xlim = ([round(round(min(dataTree['X']), 2), 1), max(dataTree['X']) +
             0.05])
    zlim = ([round(round(min(dataTree['Z']), 2), 1), round(round(
        max(dataTree['Z']), 2), 1)])
    zlim = np.array(zlim) - xlim[0]
    layerBins = 1 - np.array(layerBins)

    for left, right, iC in zip(layerBins[0:-1],
                               layerBins[1:],
                               np.arange(len(layerBins))):
        if left > (1-cell.meta['somaCortexPosition']) >= right:
            colorDendrite = layerBasal[:][iC]
            colorApical = layerApical[:][iC]
    colorSelect = {2: np.array((101, 0, 223))/255,
                   3: colorDendrite,
                   4: colorApical}

    # print borders
    if visLayerBorders is True:
        xBorders = nm.repmat(np.array(xlim).reshape(2, 1), 1,
                                    len(layerBins))
        yBorders = nm.repmat(layerBins, 2, 1)
        plt.plot(xBorders, yBorders, '--', color=(0.8, 0.8, 0.8),
                 linewidth=defWidth)
        plt.plot([(xlim[0], xlim[0]), (xlim[1], xlim[1])], [(0, 1), (0, 1)],
                 '-', color=(0.7, 0.7, 0.7), linewidth=defWidth)

    # print neuronal processes
    startSegment = list()
    stopSegment = list()
    # the Soma center of Mass can act as the startpoint for all neuronal
    # processes. In that case there is no P=-1 for TYPE:2,3,4 and a extra
    # initial segment need to be added from center of mass to startSegment
    stemIDs = list()
    stemIDs = dataTree['ID'][dataTree['P'] == -1]
    for ID, P in list(zip(dataTree['ID'], dataTree['P'])):
        if int(ID) - (int(P)) != 1:
            startSegment.append(int(ID))
    np.array(startSegment)
    stopSegment = np.array(startSegment[1:])
    stopSegment = np.append(stopSegment, dataTree['ID'][len(dataTree)-1])
    stopSegment = stopSegment
    for start, stop in list(zip(startSegment, stopSegment)):
        data2Plot = ()
        try:
            connectedNodeP = dataTree['P'][int(np.where(dataTree['ID'] ==
                                                        start)[0])]
            data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                   connectedNodeP)[0])]
            if data2Plot:
                data2Plot = np.append(data2Plot, data2PlotAp)
            else:
                data2Plot = data2PlotAp

        except (KeyError, ValueError, TypeError):
            pass
        if data2Plot:
            data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                   start)[0]):
                                      int(np.where(dataTree['ID'] ==
                                                   stop)[0])]
            data2Plot = np.append(data2Plot, data2PlotAp)
        else:
            data2Plot = dataTree[:][int(np.where(dataTree['ID'] ==
                                                 start)[0]):
                                    int(np.where(dataTree['ID'] ==
                                                 stop)[0])]
        cS = colorSelect[data2Plot['TYPE'][len(data2Plot)-1]]
        plt.plot(data2Plot['X'], data2Plot['Y'], '-', color=cS,
                 linewidth=defWidth)

    # print soma
    soma = cell.swc_data[cell.swc_data['TYPE'] == 1]
    plt.plot(soma['X'][1:] / cortThick, soma['Y'][1:] / cortThick,
             '-', color=(0, 0, 0), linewidth=defWidth)
    plt.fill(soma['X'] / cortThick, soma['Y'] / cortThick, color=(0, 0, 0),
             linewidth=defWidth)

    if sideView:
        # print borders
        if visLayerBorders is True:
            zBorders = nm.repmat(np.array(zlim).reshape(2, 1),
                                        1, len(layerBins)) * -1
            zBorder = zBorders-(zBorders[1, 0]-xBorders[1, 0])
            yBorders = nm.repmat(layerBins, 2, 1)
            zarray = (np.array([(zlim[0] * -1, zlim[0] * -1),
                                (zlim[1] * -1, zlim[1] * -1)]) -
                      (zBorders[1, 0]-xBorders[1, 0]))
            plt.plot(zBorder, yBorders, '--', color=(0.8, 0.8, 0.8),
                     linewidth=defWidth)
            plt.plot(zarray, [(0, 1), (0, 1)], '-', color=(0.7, 0.7, 0.7),
                     linewidth=defWidth)

        # print neuronal processes
        startSegment = list()
        stopSegment = list()
        # the Soma center of Mass can act as the startpoint for all neuronal
        # processes. In that case there is no P=-1 for TYPE:2,3,4 and a extra
        # initial segment need to be added from center of mass to startSegment
        stemIDs = list()
        stemIDs = dataTree['ID'][dataTree['P'] == -1]
        for ID, P in list(zip(dataTree['ID'], dataTree['P'])):
            if int(ID) - (int(P)) != 1:
                startSegment.append(int(ID))
        np.array(startSegment)
        stopSegment = np.array(startSegment[1:])
        stopSegment = np.append(stopSegment, dataTree['ID'][len(dataTree)-1])
        stopSegment = stopSegment
        zstart = 1
        for start, stop in list(zip(startSegment, stopSegment)):
            data2Plot = ()
            try:
                connectedNodeP = dataTree['P'][int(np.where(dataTree['ID'] ==
                                                            start)[0])]
                data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                       connectedNodeP)[0])]
                if data2Plot:
                    data2Plot = np.append(data2Plot, data2PlotAp)
                else:
                    data2Plot = data2PlotAp

            except (KeyError, ValueError, TypeError):
                pass
            if data2Plot:
                data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                       start)[0]):
                                          int(np.where(dataTree['ID'] ==
                                                       stop)[0])]
                data2Plot = np.append(data2Plot, data2PlotAp)
            else:
                data2Plot = dataTree[:][int(np.where(dataTree['ID'] ==
                                                     start)[0]):
                                        int(np.where(dataTree['ID'] ==
                                                     stop)[0])]
            cS = colorSelect[data2Plot['TYPE'][len(data2Plot)-1]]
            plt.plot(((data2Plot['Z'] - xlim[0]) * -1) -
                     (zBorders[1, 0]-xBorders[1, 0]),
                     data2Plot['Y'], '-', color=cS, linewidth=defWidth)
            if zstart > min(((data2Plot['Z'] - xlim[0]) * -1) -
                            (zBorders[1, 0]-xBorders[1, 0])):
                zstart = min(((data2Plot['Z'] - xlim[0]) * -1) -
                             (zBorders[1, 0] - xBorders[1, 0]))
        # print soma
        soma = cell.swc_data[cell.swc_data['TYPE'] == 1]
        plt.plot((((soma['Z'][1:]/cortThick) - xlim[0]) * -1)-(zBorders[1, 0] -
                                                               xBorders[1, 0]),
                 soma['Y'][1:]/cortThick, '-', color=(0, 0, 0),
                 linewidth=defWidth)
        plt.fill((((soma['Z']/cortThick) - xlim[0]) * -1) -
                 (zBorders[1, 0]-xBorders[1, 0]),
                 soma['Y']/cortThick, color=(0, 0, 0), linewidth=defWidth)
        plt.plot((zstart, zstart), (0.98, -0.05), '-', color=(0.2, 0.2, 0.2),
                 linewidth=defWidth)
        plt.arrow(zstart, -0.05, 0.1, 0, color=(0.2, 0.2, 0.2), width=0.0001,
                  head_width=0.03, linewidth=defWidth)
    plt.axis('off')
    left, right = plt.xlim()
    if right - left < 1.6:
        padding = 1.6 - (right - left)
        plt.xlim([left - padding / 2, right + padding / 2])
        rightN = right + padding / 2
    else:
        rightN = right
    plt.text(rightN-0.15, -0.1, str(cell.cell_amp_id),
             fontdict={'fontsize': 12}, color=colorApical)
    fig = plt.gcf()
    fig.set_size_inches([2560/227, 1600/227])
    fig.set_dpi(227)
    plt.rcParams['axes.facecolor'] = np.array([33, 33, 34])/255
    plt.rcParams['savefig.facecolor'] = np.array([33, 33, 34])/255
    if save_path == 0 or save_path is None:
        plt.close('all')
        return fig
    else:
        save_path = save_path + str(cell.cell_amp_id) + '.' + str(fileformat)
        plt.savefig(save_path, format=fileformat, dpi=dpi,
                    transparant=True, bbox_inches='tight', pad_inches=0)
    plt.close('all')
    return fig


def plot_gallery_view_cells(cellAmpIDs_cluster,
                                    folder_name,
                                    plot_color='#FF0000',
                                    group_name='groupname',
                                    df_cell=None,
                                    df_slide=None,
                                    sideView=True,
                                    visLayerBorders=True,
                                    defWidth=0.5,
                                    dpi=300,
                                    save_fig=True,
                                    save_format='png'):
    """ Visualize all morphologies of the cluster

    Args:
        - cellAmpIDs_cluster: list of cellAmpIDs
        - plot_color: string, HEX color code of the cluster based on the HCA R cluster analysis
        - group_name: string, concatenation of the present group_name and its parental group_names
        - df_cell: pandas dataframe, cell database.
        - df_slide: pandas dataframe, brain slide database.
        - folder_name: string, e.g. name of the HCA cutoff.
        - savefig: boolean, if true will save as eps and png
        - save_format: string, format name, e.g. 'png', 'eps'.
    Returns:
        - stats
        - fig
    """
    # initialize variables
    layerBins = np.array([0, 0.1, 0.21, 0.3, 0.41, 0.5, 0.7, 0.8, 1])
    if len(cellAmpIDs_cluster) > 8:
        end_ind = np.arange(8, len(cellAmpIDs_cluster), 8)
        if end_ind[-1] <= len(cellAmpIDs_cluster):
            end_ind = np.append(end_ind, len(cellAmpIDs_cluster))
        sets = {i: cellAmpIDs_cluster[start: stop] for i, start, stop in zip(
            np.arange(0, int(np.ceil(len(cellAmpIDs_cluster) / 8)), 1),
            np.arange(0, len(cellAmpIDs_cluster), 8),
            end_ind)}
    else:
        sets = {0: cellAmpIDs_cluster}

    x_spacer = 0.5
    x_start = 0
    for i, set_ in sets.items():
        fig, axes = plt.subplots()
        for cellAmpID in set_:
            cell = ini_cell.Cell(cellAmpID, df_cell=df_cell, df_slide=df_slide)
            dataTree = get_dataTree(cell.swc_data)
            dataTree = dataTree[dataTree['TYPE'] >= 3]

            contourData = extractContourData(cell.swc_data, cell.swc_meta)
            contourData = extractsomaData(cell.swc_data, cell.swc_meta, contourData)
            contourData = calc_cortexThickness(contourData, verbose=False)
            cortThick = contourData['cortexThickness']

            # normalize dataTree to cortical thickness:
            dataTree['X'] = dataTree['X'] / cortThick
            dataTree['Y'] = dataTree['Y'] / cortThick
            dataTree['Z'] = dataTree['Z'] / cortThick
            xlim = ([round(round(min(dataTree['X']), 2), 1), max(dataTree['X']) +
                     0.05])
            zlim = ([round(round(min(dataTree['Z']), 2), 1),
                     round(round(max(dataTree['Z']), 2), 1)])
            zlim = np.array(zlim) - xlim[0]
            layerBins = 1 - np.array(layerBins)

            # define borders
            xBorders = nm.repmat(np.array(xlim).reshape(2, 1), 1,
                                 len(layerBins))
            yBorders = nm.repmat(layerBins, 2, 1)
            zBorders = nm.repmat(np.array(zlim).reshape(2, 1), 1,
                                 len(layerBins)) * -1
            zBorder = zBorders - (zBorders[1, 0] - xBorders[1, 0])

            colorSelect = {2: np.array((101, 0, 223)) / 255, 3: np.array([58, 58, 58]) / 255,
                           4: plot_color}

            # print neuronal processes
            startSegment = list()
            stopSegment = list()
            # the Soma center of Mass can act as the startpoint for all neuronal
            # processes. In that case there is no P=-1 for TYPE:2,3,4 and a extra
            # initial segment need to be added from center of mass to startSegment
            stemIDs = list()
            stemIDs = dataTree['ID'][dataTree['P'] == -1]
            for ID, P in list(zip(dataTree['ID'], dataTree['P'])):
                if int(ID) - (int(P)) != 1:
                    startSegment.append(int(ID))
            np.array(startSegment)
            stopSegment = np.array(startSegment[1:])
            stopSegment = np.append(stopSegment, dataTree['ID'][len(dataTree) - 1])
            stopSegment = stopSegment
            for start, stop in list(zip(startSegment, stopSegment)):
                data2Plot = ()
                try:
                    connectedNodeP = dataTree['P'][int(np.where(dataTree['ID'] ==
                                                                start)[0])]
                    data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                           connectedNodeP)[0])]
                    if data2Plot:
                        data2Plot = np.append(data2Plot, data2PlotAp)
                    else:
                        data2Plot = data2PlotAp

                except (KeyError, ValueError, TypeError):
                    pass
                if data2Plot:
                    data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                           start)[0]):
                                              int(np.where(dataTree['ID'] ==
                                                           stop)[0])]
                    data2Plot = np.append(data2Plot, data2PlotAp)
                else:
                    data2Plot = dataTree[:][int(np.where(dataTree['ID'] ==
                                                         start)[0]):
                                            int(np.where(dataTree['ID'] ==
                                                         stop)[0])]
                cS = colorSelect[data2Plot['TYPE'][len(data2Plot) - 1]]
                axes.plot(x_start + data2Plot['X'],
                          data2Plot['Y'],
                          '-',
                          color=cS,
                          linewidth=defWidth)

            # print soma
            soma = cell.swc_data[cell.swc_data['TYPE'] == 1]
            axes.plot(x_start + soma['X'][1:] / cortThick,
                      soma['Y'][1:] / cortThick,
                      '-',
                      color=(0, 0, 0),
                      linewidth=defWidth)
            axes.fill(x_start + soma['X'] / cortThick,
                      soma['Y'] / cortThick,
                      color=(0, 0, 0),
                      linewidth=defWidth)

            if sideView:
                x_right = 0
                # print neuronal processes
                startSegment = list()
                stopSegment = list()
                # the Soma center of Mass can act as the startpoint for all neuronal
                # processes. In that case there is no P=-1 for TYPE:2,3,4 and a extra
                # initial segment need to be added from center of mass to startSegment
                stemIDs = list()
                stemIDs = dataTree['ID'][dataTree['P'] == -1]
                for ID, P in list(zip(dataTree['ID'], dataTree['P'])):
                    if int(ID) - (int(P)) != 1:
                        startSegment.append(int(ID))
                np.array(startSegment)
                stopSegment = np.array(startSegment[1:])
                stopSegment = np.append(stopSegment, dataTree['ID'][len(dataTree) - 1])
                stopSegment = stopSegment
                zstart = 1
                for start, stop in list(zip(startSegment, stopSegment)):
                    data2Plot = ()
                    try:
                        connectedNodeP = dataTree['P'][int(np.where(dataTree['ID'] ==
                                                                    start)[0])]
                        data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                               connectedNodeP)[0])]
                        if data2Plot:
                            data2Plot = np.append(data2Plot, data2PlotAp)
                        else:
                            data2Plot = data2PlotAp

                    except (KeyError, ValueError, TypeError):
                        pass
                    if data2Plot:
                        data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                               start)[0]):
                                                  int(np.where(dataTree['ID'] ==
                                                               stop)[0])]
                        data2Plot = np.append(data2Plot, data2PlotAp)
                    else:
                        data2Plot = dataTree[:][int(np.where(dataTree['ID'] ==
                                                             start)[0]):
                                                int(np.where(dataTree['ID'] ==
                                                             stop)[0])]
                    cS = colorSelect[data2Plot['TYPE'][len(data2Plot) - 1]]
                    axes.plot(x_start + ((data2Plot['Z'] - xlim[0]) * -1) -
                              (zBorders[1, 0] - xBorders[1, 0]),
                              data2Plot['Y'],
                              '-',
                              color=cS,
                              linewidth=defWidth)
                    max_x_right_local = np.max(x_start + ((data2Plot['Z'] - xlim[0]) * -1) -
                                               (zBorders[1, 0] - xBorders[1, 0]))
                    if x_right < max_x_right_local:
                        x_right = max_x_right_local
                    if zstart > (min(((data2Plot['Z'] - xlim[0]) * -1) -
                                     (zBorders[1, 0] - xBorders[1, 0]))):
                        zstart = min(((data2Plot['Z'] - xlim[0]) * -1) -
                                     (zBorders[1, 0] - xBorders[1, 0]))
                # print soma
                soma = cell.swc_data[cell.swc_data['TYPE'] == 1]
                axes.plot(x_start + (((soma['Z'][1:] / cortThick) - xlim[0]) * -1) -
                          (zBorders[1, 0] - xBorders[1, 0]),
                          soma['Y'][1:] / cortThick,
                          '-',
                          color=(0, 0, 0),
                          linewidth=defWidth)
                axes.fill(x_start + (((soma['Z'] / cortThick) - xlim[0]) * -1) -
                          (zBorders[1, 0] - xBorders[1, 0]),
                          soma['Y'] / cortThick,
                          color=(0, 0, 0),
                          linewidth=defWidth)
                max_x_right_local = np.max(x_start + (((soma['Z'][1:] / cortThick) - xlim[0]) * -1) -
                                           (zBorders[1, 0] - xBorders[1, 0]))
                if x_right < max_x_right_local:
                    x_right = max_x_right_local

                axes.plot((x_start + zstart, x_start + zstart),
                          (0.98, -0.05),
                          '-',
                          color=(0.2, 0.2, 0.2),
                          linewidth=defWidth)
                axes.arrow(x_start + zstart,
                           -0.05,
                           0.1,
                           0,
                           color=(0.2, 0.2, 0.2),
                           width=0.0001,
                           head_width=0.03,
                           linewidth=defWidth)
            axes.axis('off')
            axes.set_aspect(1)
            x_start = x_right + x_spacer

        # print borders
        if visLayerBorders is True:
            xlim = axes.get_xlim()
            xBorders = nm.repmat(np.array(xlim).reshape(2, 1), 1,
                                 len(layerBins))
            yBorders = nm.repmat(layerBins, 2, 1)
            axes.plot(xBorders, yBorders, '--', color=(0.8, 0.8, 0.8),
                      linewidth=defWidth, zorder=0)
            axes.plot([(xlim[0], xlim[0]), (xlim[1], xlim[1])], [(0, 1), (0, 1)],
                      '-', color=(0.7, 0.7, 0.7), linewidth=defWidth, zorder=1)
        if save_fig:
            # make sure to have the folder_name already there
            folder_name = Path(folder_name)
            if not folder_name.is_dir():
                folder_name.mkdir()
            save_path = folder_name / (str(group_name) + '_pt' + str(i + 1) + '.' + save_format)
            plt.savefig(str(save_path), format=save_format, dpi=dpi, transparant=True)
    return (fig, axes)