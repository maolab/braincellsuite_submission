""" Helper functions for calculating distances in the slice.
Copied from http://www.fundza.com/vectors/vectors.html, see also:
http://www.fundza.com/vectors/point2line/index.html

Functions:
    - pnt2line: Given a line with coordinates 'start' and 'end' and the
                coordinates of a point 'pnt' the proc returns the shortest
                distance from pnt to the line and the coordinates of the
                nearest point on the line.
    - dot: Dot product of two vectors.

"""
import math
import numpy as np


def pnt2line(pnt, start, end):
    """ Given a line with coordinates 'start' and 'end' and the
    coordinates of a point 'pnt' the proc returns the shortest
    distance from pnt to the line and the coordinates of the
    nearest point on the line.

    1  Convert the line segment to a vector ('line_vec').
    2  Create a vector connecting start to pnt ('pnt_vec').
    3  Find the length of the line vector ('line_len').
    4  Convert line_vec to a unit vector ('line_unitvec').
    5  Scale pnt_vec by line_len ('pnt_vec_scaled').
    6  Get the dot product of line_unitvec and pnt_vec_scaled ('t').
    7  Ensure t is in the range 0 to 1.
    8  Use t to get the nearest location on the line to the end
    of vector pnt_vec_scaled ('nearest').
    9  Calculate the distance from nearest to pnt_vec_scaled.
    10 Translate nearest back to the start/end line.
    Malcolm Kesson 16 Dec 2012

    Args:
        - pnt: vector with X, Y, Z elements.
        - start: vector with X, Y, Z elements.
        - end: vector with X, Y, Z elements.

    Returns:
        - distance: value of distance between point and line.
        - nearest: value of point on line used to calculate distance.
    """

    line_vec = vector(start, end)
    pnt_vec = vector(start, pnt)
    line_len = length(line_vec)
    line_unitvec = unit(line_vec)
    pnt_vec_scaled = scale(pnt_vec, 1.0/line_len)
    t = dot(line_unitvec, pnt_vec_scaled)
    if t < 0.0:
        t = 0.0
    elif t > 1.0:
        t = 1.0
    nearest = scale(line_vec, t)
    dist = distance(nearest, pnt_vec)
    nearest = add(nearest, start)
    return (dist, nearest)


def dot(v, w):
    """Dot product of two vectors.

    Args:
        - v: vector 1 with X, Y, Z.
        - w: vector 2 with X, Y, Z.

    Returns:
        - dot product value.
    """
    x, y, z = v
    X, Y, Z = w
    return x*X + y*Y + z*Z


def length(v):
    x, y, z = v
    return math.sqrt(x*x + y*y + z*z)


def vector(b, e):
    x, y, z = b
    X, Y, Z = e
    return (X-x, Y-y, Z-z)


def unit(v):
    x, y, z = v
    mag = length(v)
    return (x/mag, y/mag, z/mag)


def distance(p0, p1):
    return length(vector(p0, p1))


def scale(v, sc):
    x, y, z = v
    return (x * sc, y * sc, z * sc)


def add(v, w):
    x, y, z = v
    X, Y, Z = w
    return (x+X, y+Y, z+Z)


def centerOMass(x, y, z, w):
    """ w: weight"""
    cm = np.average([x, y, z], axis=1, weights=w)
    return cm


def intercept2LinFunc(fun1, fun2):
    """Calculate the intercept of two linear functions, given the formulation:
    y = ax+b. Both fun's comprise of [a, b] for y = ax+b.

    Returns:
        - intercept = [xCoordinate, yCoortdinate]

    """
    # mx + b = nx + c
    # mx - nx = c - b
    # (m-n)x = c - b
    # x = (c - b) / (m-n)
    old_settings = np.seterr(all='print')
    if fun1[0]-fun2[0] == float('inf'):
        x = 'NaN'
        y = 'NaN'
    elif float(fun1[0] - fun2[0]) == float(0):
        x = 'NaN'
        y = 'NaN'
    elif float(fun1[0] - fun2[0]) != float(0):
        x = (fun2[1] - fun1[1])/(fun1[0]-fun2[0])
        y = fun1[0]*x+fun1[1]
    return [x, y]


def thicknessT(x1, y1, x2, y2):
    '''Calculate the length between two points'''
    thicknessT = math.sqrt((x1-x2)**2 + (y1-y2)**2)
    return thicknessT


def findLin(points):
    '''points = [(x1,y1),(x2,y2)]'''
    x_coords, y_coords = zip(*points)
    A = np.vstack([x_coords, np.ones(len(x_coords))]).T
    a, b = np.linalg.lstsq(A, y_coords, rcond=-1)[0] #rcond=None didn't work
    return a, b


def rotate_around_point_highperf(x, y, radians, origin=(0, 0)):
    """Rotate a point around a given point.

    from: https://sciencing.com/calculate-radians-slope-8033052.html
    I call this the "high performance" version since we're caching some
    values that are needed >1 time. It's less readable than the previous
    function but it's faster.
    """

    offset_x, offset_y = origin
    adjusted_x = (x - offset_x)
    adjusted_y = (y - offset_y)
    cos_rad = math.cos(radians)
    sin_rad = math.sin(radians)
    qx = offset_x + cos_rad * adjusted_x + sin_rad * adjusted_y
    qy = offset_y + -sin_rad * adjusted_x + cos_rad * adjusted_y

    return qx, qy
