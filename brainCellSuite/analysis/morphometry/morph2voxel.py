"""Transformation of SWC files to voxel array.

Functions:
    - segmentplaneint, Calculate the point of intesrection between line segments
                       and a plane. Contributed by Michael Muniak 2019
    - pointinpoly3, Determine if a point is contained within a poly face defined
                    in 3D space. Contributed by Michael Muniak 2019
    - segmentfaceint, Calculate the point of intersection between line segments
                      and a set of faces. Contributed by Michael Muniak 2019
    - multiface, Return index array of segments that intersect more than one
                 face simultaneously. Contributed by Michael Muniak 2019
    - get_surrounding_voxels, Get the adjecent voxel IDs based on ID of voxel.
    - view_voxel_array_2D, Visualize in 2D the voxel_array.
    - get_point_on_line_3D, Give coordinates of a point at x distance from p0 on
                            line defined by p0 and p1.
    - fill_points_on_line_3D, Give coordinates of points at x distance from p0
                              on line defined by p0 and p1.
    - morph2voxel, Transforms datatree from SWC to voxel_array.
    - gen_visdata_voxel_array_3D: Visualize the voxel array in 3D with plotly.


"""
from builtins import bool, int, str, list, float
from builtins import len
import numpy as np
import pandas as pd
import sys
import re
from plotly import graph_objs as go
from sklearn.decomposition import PCA
from brainCellSuite._io.readSWCplus import get_dataTree
from brainCellSuite.analysis.morphometry.vector import distance
from brainCellSuite.analysis.morphometry.calc_postFixDims import \
    calc_cortexThickness, extractContourData, extractsomaData
import matplotlib.pyplot as plt
from pathlib import Path
from brainCellSuite import bcs_global
from brainCellSuite import Cell
import multiprocessing
import time
from functools import partial
from itertools import combinations

# Floating point tolerance for determining equality.
tol = sys.float_info.epsilon * 1e10

def segmentplaneint(p, s):
    """ Calculate the point of intesrection between line segments and a plane.

        INPUT--
        p: (m,3) numpy.array of points defining plane (m>2).
        s: (2,n,3) numpy.array of points defining n line segments.  First and second
                   arrays are paired start- and end-points of each line segment.

        OUTPUT--
        c: (n,3) numpy.array of intersection coordinates (NaNs if no intersection).
        i: (n,3) logical numpy.array indicating segments that intersected plane.
        par_in: (n,3) logical numpy.array indicating segments that are parallel to and within plane.
        par_out: (n,3) logical numpy.array indicating segments that are parallel to but outside of plane.

        Adapted from pseudocode:
        http://geomalgorithms.com/a05-_intersect-1.html
    """
    # Check inputs.
    if p.shape[1] != 3:
        raise Exception('p must be a (n,3) numpy.array!')
    elif p.shape[0] < 3:
        raise Exception('Not enough points in p to define a plane!')
    elif s.shape[0] != 2 or s.shape[2] != 3 or s[0].size != s[1].size:
        raise Exception('s must be a (2,n,3) numpy.array!')

        # Make sure we're not using integers.
    p = np.double(p)
    s = np.double(s)

    # Normal vector to plane.
    n = np.cross(p[0] - p[1], p[0] - p[2])

    # Check if all points in p are on same plane.
    if not all(np.dot(p[0] - p, n) == 0):
        raise Exception('Not all points in p lie on the same plane!')

    # Get other vectors for math.
    u = s[1] - s[0]  # Segments.
    w = s[0] - p[0]  # Plane to segment.

    # Check for segments parallel to plane.
    par = np.dot(u, n) == 0  # All segments that are parallel.
    u0 = np.dot(p[0] - s[0],
                n) == 0  # All segments with one point on plane.
    par_in = par & u0  # All parallel segments on plane.
    par_out = par & ~u0  # All parallel segments not on plane.
    u[par] = np.nan  # Disregard these points from now on.

    # Find intersections.
    d = np.dot(w, -n) / np.dot(u,
                               n)  # Relative point of intersection along line.
    c = s[0] + u * d.reshape((-1, 1))  # Coordinate of intersection.
    i = np.logical_and(d >= 0,
                       d <= 1)  # Filter segments that do not intersect plane.
    c[~i] = np.nan  # Kill non-intersecting points.

    return c, i, par_in, par_out


def pointinpoly3(p, poly, check_coplanar=False):
    """ Determine if a point is contained within a poly face defined in 3D space.

        Assumes each poly face is non-intersecting and convex.  Results are
        unreliable if this is not the case (not adding extra overhead to check).

        INPUT--
        p: (m,3) numpy.array of points to check.
        poly: (n,3) numpy.array of ordered points defining defining polygon.
                    Note assumptions above.

        OUTPUT--
        tf: (n,1) logical numpy.array indicating True for each point in poly.

        Adapted from pseudocode:
        http://www.eecs.umich.edu/courses/eecs380/HANDOUTS/PROJ2/InsidePoly.html
    """

    # Check inputs.
    if p.shape[1] != 3:
        raise Exception('p must be a (n,3) numpy.array!')
    elif poly.shape[1] != 3:
        raise Exception('poly must be a (n,3) numpy.array!')
    elif poly.shape[0] < 3:
        raise Exception('Not enough points in poly to define a face!')

    # Make sure we're not using integers.
    p = np.double(p)
    poly = np.double(poly)

    # Check if poly points are coplanar (if requested).
    if check_coplanar:
        if not all(np.dot(poly[0] - poly, np.cross(poly[0] - poly[1],
                                                   poly[0] - poly[
                                                       2])) == 0):
            raise Exception('Not all points in poly lie on the same plane!')

    # Initialize vars.
    n_p = len(p)
    n_poly = len(poly)
    a = np.zeros(n_p)
    tf = np.zeros(n_p, dtype=bool)

    # Loop thru poly edges.
    for i in range(n_poly):
        # Math.
        p1 = poly[i] - p
        p2 = poly[(i + 1) % n_poly] - p
        m1 = np.linalg.norm(p1, axis=1)
        m2 = np.linalg.norm(p2, axis=1)

        # If point is on a node, consider it in poly.
        tf[m1 * m2 <= tol] = True

        # Get costheta.
        th = np.sum(p1 * p2, axis=1) / (m1 * m2)
        # Fix costheta vals _just_ beyond COS domain of [-1, 1]
        # due to floating point errors.
        th[abs(th - 1) < tol] = 1.0
        th[abs(th + 1) < tol] = -1.0
        # Add angle to running total.
        a = a + np.arccos(th)

    # If summed winding angle equals 2*pi, point is inside poly.
    tf[abs(a - (np.pi * 2)) < tol] = True
    return tf


def segmentfaceint(segments, faces):
    """ Calculate the point of intersection between line segments and a set of faces.

        INPUT--
        segments: (2,m,3) numpy.array of points defining m line segments.  First and
                          second arrays are paired start- and end-points of each line segment.

        faces: (n,>2,3) array of numpy.arrays defining n faces.  Each face must be defined
                        by at least 3 points but each may be defined by a different
                        number of points.

        OUTPUT--
        results: (n,1) array of results comparing all m segments to each of n faces.
                       Each sub-array is similar to that returned by segmentplaneint()
                       except that all points that do not intersect the poly-face are set
                       to nan.
    """
    # If a single face is provided, need to restructure for program to work.
    if isinstance(faces, np.ndarray) and faces.ndim == 2:
        faces = np.expand_dims(faces, axis=0)
    # Check inputs.
    if segments.shape[0] != 2 or segments.shape[2] != 3 or segments[
        0].size != segments[1].size:
        raise Exception('segments must be a (2, n, 3) numpy.array!')
    elif any([face.shape[0] < 3 or face.shape[1] != 3 for face in faces]):
        raise Exception('faces must be a (n, >2, 3) numpy.array!')

    # Array of intersection results for segments with planes defined by each face.
    results = [segmentplaneint(face, segments) for face in faces]

    # Filter out points that are not inside each face.
    for i in range(len(faces)):
        # b == indices of points that intersect plane defined by face.
        # No sense in testing points that have already been ruled out.
        b = np.flatnonzero(results[i][1])
        # tf == True for any point contained within face.
        tf = pointinpoly3(results[i][0][b], faces[i])
        # Set any points not on face to nans.
        results[i][0][b[~tf]] = np.nan
        results[i][1][b[~tf]] = False
    return results


def multiface(results):
    """ Return index array of segments that intersect more than one face
    simultaneously.

    Args:
        - results:

    Returns:
        - idx_array:
    """
    n_segments = len(results[0][0])
    n_faces = len(results)
    idx_array = [[] for _ in range(n_segments)]
    combos = combinations(range(n_faces), 2)
    for c in combos:
        tf = results[c[0]][1] * results[c[1]][1]
        for idx in np.flatnonzero(tf):
            idx_array[idx].append(c)
    return idx_array


def get_surrounding_voxels(ID, Xdim, Ydim, Zdim, voxel_array,
                           include_all=False):
    """Get the adjecent voxel IDs based on ID of voxel.

    Args:
        - ID, int: location ID in voxel array
        - Xdim/Ydim/Zdim: dimensions of the 3D representation of the voxel_array
        - voxel_array: numpy array
        - include_all: include also the diagonal adjecent voxels
            (this adds to 27)

    Returns:
        - adjecent_voxel_IDs: vector containing integers of voxel IDs direct
            adjecent to the given ID voxel
    """
    frame = Xdim * Ydim
    row = Xdim
    front_1 = ID - frame - row - 1
    back_1 = ID + frame - row - 1
    all_voxel_IDs = np.array([
        # plane in front of ID
        front_1, front_1 + 1, front_1 + 2,
                 front_1 + row, ID - frame, ID - frame + 1,
                 front_1 + (2 * row), front_1 + (2 * row) + 1,
                 front_1 + (2 * row) + 2,
        # plane witht the ID
                 ID - row - 1, ID - row, ID - row + 1,
                 ID - 1, ID + 1,
                 ID + row - 1, ID + row, ID + row + 1,
        # plane behind the ID
        back_1, back_1 + 1, back_1 + 2,
                 back_1 + row, ID + frame, ID + frame + 1,
                 back_1 + (2 * row), back_1 + (2 * row) + 1,
                 back_1 + (2 * row) + 2]).astype(float)
    adjecent_indexes = [4, 10, 12, 13, 15, 21]
    non_adjecent_ind = [0, 1, 2, 3, 5, 6, 7, 8, 9, 11, 14, 16, 17, 18, 19,
                        20, 22, 23,
                        24, 25]
    # replace all voxels that are not in the voxel_array with np.nan
    for loc, voxel in enumerate(all_voxel_IDs):
        if voxel not in voxel_array['ID']:
            all_voxel_IDs[loc] = np.nan

    adjecent_voxel_IDs = all_voxel_IDs[adjecent_indexes]
    adjecent_voxel_IDs = adjecent_voxel_IDs[
        np.logical_not(np.isnan(adjecent_voxel_IDs))]
    if not include_all:
        return adjecent_voxel_IDs.astype(int)
    else:
        #         (all_voxel_IDs[non_adjecent_ind])
        adjecent_voxel_IDs = np.hstack((all_voxel_IDs[adjecent_indexes],
                                        all_voxel_IDs[non_adjecent_ind]))
    return adjecent_voxel_IDs.astype(int)


def view_voxel_array_2D(voxel_array):
    """Visualize in 2D the voxel_array

    :param voxel_array: numpy nd_array containing data on length, nodes, ends
    for each process type per voxel

    :return voxel_space: numpy nd_array rearrange in 3D
    """
    bins_x = np.max(voxel_array['X']) - np.min(voxel_array['X']) + 1
    bins_y = np.max(voxel_array['Y']) - np.min(voxel_array['Y']) + 1
    bins_z = np.max(voxel_array['Z']) - np.min(voxel_array['Z']) + 1

    voxel_space = voxel_array.reshape((bins_z, bins_y, bins_x), order='C')

    plt.figure(figsize=(12, 12))
    plt.subplot(231)
    plt.imshow(np.rot90(np.rot90(np.sum(voxel_space['sum_length_apical'], 0))))
    plt.colorbar()
    plt.grid(False)

    plt.subplot(232)
    plt.imshow(np.rot90(np.rot90(np.sum(voxel_space['sum_length_basal'], 0))))
    plt.colorbar()
    ax1 = plt.gca()
    ax1.set_yticklabels([])
    ax1.grid(False)

    ax2 = plt.subplot(233)
    ax2.plot(np.sum(np.sum(voxel_space['sum_length_apical'], 0), 1),
             np.arange(0, bins_y), 'green', linewidth=2)
    ax2.plot(np.sum(np.sum(voxel_space['sum_length_basal'], 0), 1),
             np.arange(0, bins_y), 'purple', linewidth=2)
    ax2.grid(False)
    ax2.set_ylim((-0.5, bins_y + .5))
    ax2.set_yticklabels([])

    plt.subplot(234)
    plt.imshow(np.rot90(np.rot90(np.sum(voxel_space['sum_length_apical'], 2))))
    plt.colorbar()
    plt.grid(False)

    plt.subplot(235)
    plt.imshow(np.rot90(np.rot90(np.sum(voxel_space['sum_length_basal'], 2))))
    plt.colorbar()
    ax3 = plt.gca()
    ax3.set_yticklabels([])
    ax3.grid(False)

    ax4 = plt.subplot(236)
    ax4.plot(np.sum(np.sum(voxel_space['sum_length_apical'], 2), 1),
             np.arange(0, bins_z), 'green', linewidth=2)
    ax4.plot(np.sum(np.sum(voxel_space['sum_length_basal'], 2), 1),
             np.arange(0, bins_z), 'purple', linewidth=2)
    ax4.grid(False)
    ax4.set_ylim((-0.5, bins_y + .5))
    ax4.set_yticklabels([])
    plt.show()

    return voxel_space


def get_point_on_line_3D(p0, p1, distance_point):
    """Give coordinates of a point at x distance from p0 on line defined by p0
    and p1

    Args:
        - p0 and p1, np.arrays of n x 3, containing x,y,z
        - distance, distance relative to p0 towards p1 along line between p0 and
        p1.

    Return:
        - new_point, np.arrays of n x 3, containing x,y,z at distance_point from
        p0 along the line p0-p1.
    """
    vector = p0 - p1
    distance_segment = np.sqrt(
        (vector[:, 0]) ** 2 + (vector[:, 1]) ** 2 + (vector[:, 2]) ** 2)
    u = distance_point / distance_segment
    new_point = np.array(((1 - u) * p0[:, 0] + u * p1[:, 0],
                          (1 - u) * p0[:, 1] + u * p1[:, 1],
                          (1 - u) * p0[:, 2] + u * p1[:, 2])).T
    return new_point


def fill_points_on_line_3D(p0, p1, distance_var):
    """Give coordinates of points at x distance from p0 on line defined by p0
    and p1.

    Args:
        - p0 and p1, np.arrays of 1 x 3, containing x,y,z
        - distance_var, distance by which a segment is cut into.

    Return:
        - new_point, np.arrays of n x 3, containing x,y,z at distance_point from
        p0 along the line p0-p1.
    """
    vector = p0 - p1
    distance_segment = np.sqrt(
        (vector[:, 0]) ** 2 + (vector[:, 1]) ** 2 + (vector[:, 2]) ** 2)
    # divide up the segment length such that there is 1um segment length left
    distance_points = np.arange(distance_var, distance_segment, distance_var)
    u = distance_points / distance_segment
    # u became an array with point, therefore
    p0 = np.reshape(np.tile(p0, len(u)), (len(u), 3))
    p1 = np.reshape(np.tile(p1, len(u)), (len(u), 3))
    new_point = np.array(((1 - u) * p0[:, 0] + u * p1[:, 0],
                          (1 - u) * p0[:, 1] + u * p1[:, 1],
                          (1 - u) * p0[:, 2] + u * p1[:, 2])).T
    return new_point


def morph2voxel(cell,
                voxel_size,
                ignore_axon=False,
                calc_total_length=False,
                save_voxel_array=False,
                datatree_init=None,
                normalized_axes=None,
                normalizer_axis=None,
                parallel_on=False,
                axial_angle=0,
                azimuth_angle=0,
                polar_angle=0,
                suppress_output=False):
    """Transforms datatree from SWC to voxel_array

    Args:
        - cell, instance of a cell object from class ini_cell
        - voxel_size, size of voxel in um
        - ignore_axon,  boolean. True axon data is not used for calculation.
        - calc_total_length, for checking purposes calculate the total length
                             for each process (apical/basal dendrite)
        - save_voxel_array, False default. Whether to save the voxel_array.
        - datatree_init, Default None. Transformed datatree in numpy array with
            tuples annotating the specific axes. If None use cell.
            Important: first row needs to be the origin point.
        - normalized_axes, Default None. Selected axes along which the datatree_
            init was normalized. Important: the datatree_init should already be
            normalized, this is for the purpose of composing the resulting
            filename.
        - axial_angle, azimuth_angle, and polar_angle, rotational angles used to
            change the morphology orientation.
        - suppress_output, Default False. If True the return will be empty.

    Returns:
        - voxel_array, numpy ndarray containing the lenght, nodes, ends
                       information per process per voxel.
    """
    # Morphology data is build up as follows:
    # [I] Coordinate index, [T]Morphology TYPE, [X], [Y], [Z], [D]diameter,
    # [P]coordinate index connected to (-1 if coordinate is the starting point)
    # read morphology of test cell
    if not isinstance(datatree_init, np.ndarray):
        datatree = get_dataTree(data=cell.swc_data)
        com = cell.swc_data[np.logical_and(cell.swc_data['P'] == -1,
                                           cell.swc_data['TYPE'] == 1)]
        # here we assume that the origin of all processes are
        # coming from the center of mass of the cellbody contour

        # Transform morhology coordinate system to align with a specific feature
        # to become 0,0,0 (x,y,z)
        datatree['X'] = datatree['X'] - com['X']
        datatree['Y'] = datatree['Y'] - com['Y']
        datatree['Z'] = datatree['Z'] - com['Z']
        origin_tree = com
        origin_tree['X'] = origin_tree['X'] - com['X']
        origin_tree['Y'] = origin_tree['Y'] - com['Y']
        origin_tree['Z'] = origin_tree['Z'] - com['Z']
        datatree = np.hstack((com[0], datatree))
    if isinstance(datatree_init, np.ndarray):
        # the first row needs to be the com
        assert np.sum(
            np.array(datatree_init[['X', 'Y', 'Z']][0].
                     tolist()).astype(int)) == 0, \
            'first row of datatree_init is not set on 0,0,0'
        datatree = datatree_init
    # remove axon data if axon_ignore
    if ignore_axon:
        datatree = datatree[datatree['TYPE']!=2]
    # generate voxel array based on voxel size and binning of the data
    voxel_padding = 2
    voxel_demi = voxel_size / 2
    voxel_padding = voxel_size * voxel_padding

    #Preprocess data such that coordinate that are located on the voxel
    # boundaries are moved away from it
    distance_point = 2 * tol
    for i in np.arange(1, len(datatree)):
        p0 = np.array([datatree[i]['X'], datatree[i]['Y'], datatree[i]['Z']])
        p0_boundary = (np.abs(p0) - (voxel_demi * np.abs(
            np.floor(np.abs(p0) / voxel_demi)))) <= tol
        if np.any(p0_boundary):
            p1 = np.array([datatree[datatree['ID'] == datatree[i]['P']]['X'][0],
                           datatree[datatree['ID'] == datatree[i]['P']]['Y'][0],
                           datatree[datatree['ID'] == datatree[i]['P']]['Z'][
                               0]])
            p0 = np.reshape(p0, (1, 3))
            p1 = np.reshape(p1, (1, 3))
            new_point = get_point_on_line_3D(p0, p1, distance_point)
            datatree[i]['X'] = new_point[0][0]
            datatree[i]['Y'] = new_point[0][1]
            datatree[i]['Z'] = new_point[0][2]
            # print(datatree[i]['P'])

    datatree_df = pd.DataFrame(datatree[['TYPE', 'X', 'Y', 'Z', 'R', 'P']],
                               index=datatree['ID'],
                               columns=['TYPE', 'X', 'Y', 'Z', 'R', 'P'])
    # coordinates of the voxel array are as follows (X, Y, Z)
    #       -1,1,-1-------------------1,1,-1
    #          /|                     /|
    #         / |                    / |
    #        /  |                   /  |
    #       /   |                  /   |
    #      /    |                 /    |
    # -1,1,1----+--------------1,1,1   |
    #     |     |                |     |
    #     |     |                |     |
    #     |     |_ _ _ _ _ _ _ _ |_ _ _|
    #     |    /                 |    /
    #     |   /                  |   /
    #     |  /                   |  /
    #     | /                    | /
    #     |/                     |/
    # -1,-1,1-----------------1,-1,1

    # determine bins and indexes of the coordinates where 0,0,0 coordinate is
    # the center bin in the voxel array
    x_limits = np.array([(np.floor(np.min(datatree['X']) / (
                voxel_size / 2)) * voxel_size * 0.5) - voxel_padding,
                         (np.floor(np.max(datatree['X']) / (
                                     voxel_size / 2)) * voxel_size * 0.5) +
                         voxel_padding])  # to make sure that the grid is big
    # enough append voxel_size to min and max
    x_limits = x_limits - ((x_limits % voxel_size) - (
                voxel_size / 2))  # ensure that center bin centers around 0
    bins_x = np.arange(x_limits[0], x_limits[1], voxel_size)
    center_bin_x = int(np.digitize(0, bins_x, right=True))
    inds_x = np.digitize(datatree['X'], bins_x,
                         right=True) - center_bin_x  # correct indexes for
    # center bin

    y_limits = np.array([(np.floor(np.min(datatree['Y']) / (
                voxel_size / 2)) * voxel_size * 0.5) - voxel_padding,
                         (np.floor(np.max(datatree['Y']) / (
                                     voxel_size / 2)) * voxel_size * 0.5) +
                         voxel_padding])  # to make sure that the grid is big
    # enough append voxel_size to min and max
    y_limits = y_limits - ((y_limits % voxel_size) - (
                voxel_size / 2))  # ensure that center bin centers around 0
    bins_y = np.arange(y_limits[0], y_limits[1], voxel_size)
    center_bin_y = int(np.digitize(0, bins_y, right=True))
    inds_y = np.digitize(datatree['Y'], bins_y,
                         right=True) - center_bin_y  # correct indexes for
    # center bin

    z_limits = np.array([(np.floor(np.min(datatree['Z']) / (
                voxel_size / 2)) * voxel_size * 0.5) - voxel_padding,
                         (np.floor(np.max(datatree['Z']) / (
                                     voxel_size / 2)) * voxel_size * 0.5) +
                         voxel_padding])  # to make sure that the grid is big
    # enough append voxel_size to min and max
    z_limits = z_limits - ((z_limits % voxel_size) - (
                voxel_size / 2))  # ensure that center bin centers around 0
    bins_z = np.arange(z_limits[0], z_limits[1], voxel_size)
    center_bin_z = int(np.digitize(0, bins_z, right=True))
    inds_z = np.digitize(datatree['Z'], bins_z,
                         right=True) - center_bin_z
    # correct indexes for center bin
    num_bins_x = len(bins_x)+1
    num_bins_y = len(bins_y)+1
    num_bins_z = len(bins_z)+1
    voxel_array = np.zeros((num_bins_x * num_bins_y * num_bins_z,),
                           dtype=[('ID', int), ('X', int), ('Y', int),
                                  ('Z', int),
                                  ('sum_length_basal', float),
                                  ('sum_length_apical', float),
                                  ('sum_length_axon', float),
                                  ('sum_node_basal', float),
                                  ('sum_node_apical', float),
                                  ('sum_node_axon', float),
                                  ('sum_end_basal', float),
                                  ('sum_end_apical', float),
                                  ('sum_end_axon', float)])

    # fill the known parameters
    voxel_array['ID'] = np.arange(0, len(voxel_array))
    voxel_array['X'] = np.tile(np.tile(np.arange(-center_bin_x,
                                                 num_bins_x - center_bin_x,
                                                 dtype=int)+1, num_bins_y),
                               num_bins_z).astype(int)
    voxel_array['Y'] = np.tile(np.repeat(np.arange(-center_bin_y,
                                                   num_bins_y - center_bin_y,
                                                   dtype=int)+1, num_bins_x),
                               num_bins_z)
    voxel_array['Z'] = np.repeat(np.arange(-center_bin_z,
                                           num_bins_z - center_bin_z,
                                           dtype=int)+1,
                                 num_bins_x * num_bins_y)

    coordinate_voxel_loc = (
                ((inds_z + center_bin_z - 1) * (num_bins_x * num_bins_y)) +
                ((inds_y + center_bin_y - 1) * (num_bins_x)) +
                inds_x + center_bin_x - 1).astype(int)
    center_x = bins_x[inds_x + center_bin_x - 1] + voxel_demi
    center_y = bins_y[inds_y + center_bin_y - 1] + voxel_demi
    center_z = bins_z[inds_z + center_bin_z - 1] + voxel_demi
    coordinate_voxel_loc = np.vstack(
        (coordinate_voxel_loc, center_x, center_y, center_z)).T
    coordinate_voxel_loc = pd.DataFrame(coordinate_voxel_loc,
                                        index=datatree['ID'],
                                        columns=['voxel_ID', 'center_x',
                                                 'center_y', 'center_z'])

    # 1. Start at second coordinate (ID); this will make sure to calculate
    # lengths based on connection with previous point (P). The coordinate we
    # will work from we call seed coordinate, and the coordinate the segments
    # are made with and  calculated on are called the target coordinate
    # make dicts for keeping track of:
    # voxel location
    process = {2: 'axon',
               3: 'basal',
               4: 'apical'}

    voxel_ID_P = coordinate_voxel_loc.loc[datatree['P'][1:]]['voxel_ID'].astype(
        int)
    tracker = np.rec.fromarrays((datatree['ID'][1:],
                                 np.zeros(len(datatree[1:]), dtype=bool),
                                 np.zeros(len(datatree[1:])),
                                 np.zeros(len(datatree[1:]), dtype=bool),
                                 datatree['TYPE'][1:],
                                 coordinate_voxel_loc.iloc[1:]['voxel_ID'],
                                 datatree['P'][1:],
                                 voxel_ID_P),
                                dtype=[('ID', int),
                                       ('calculated', bool),
                                       ('plane', int),
                                       ('adjecent_target', bool),
                                       ('process', int),
                                       ('voxel_id', int),
                                       ('P', int),
                                       ('P_voxel_id', int)]
                                )  # to annotate whether the target_coor is
    # directly adjecent to seed_coor
    planes_bound_dict = {'[0. 0. 0.]': -1,
                         '[ 0.  0. -1.]': 0,
                         '[ 0. -1.  0.]': 1,
                         '[-1.  0.  0.]': 2,
                         '[1. 0. 0.]': 3,
                         '[0. 1. 0.]': 4,
                         '[0. 0. 1.]': 5,
                         '[-1. -1. -1.]': 6,
                         '[ 0. -1. -1.]': 7,
                         '[ 1. -1. -1.]': 8,
                         '[-1.  0. -1.]': 9,
                         '[ 1.  0. -1.]': 10,
                         '[-1.  1. -1.]': 11,
                         '[ 0.  1. -1.]': 12,
                         '[ 1.  1. -1.]': 13,
                         '[-1. -1.  0.]': 14,
                         '[ 1. -1.  0.]': 15,
                         '[-1.  1.  0.]': 16,
                         '[1. 1. 0.]': 17,
                         '[-1. -1.  1.]': 18,
                         '[ 0. -1.  1.]': 19,
                         '[ 1. -1.  1.]': 20,
                         '[-1.  0.  1.]': 21,
                         '[1. 0. 1.]': 22,
                         '[-1.  1.  1.]': 23,
                         '[0. 1. 1.]': 24,
                         '[1. 1. 1.]': 25}
    planes_dict = {0: np.array([(-1, 1, -1),  # surface/front
                                (1, 1, -1),
                                (1, -1, -1),
                                (-1, -1, -1)]),
                   1: np.array([(-1, -1, 1),  # top
                                (1, -1, 1),
                                (1, -1, -1),
                                (-1, -1, -1)]),
                   2: np.array([(-1, 1, 1),  # left
                                (-1, -1, 1),
                                (-1, -1, -1),
                                (-1, 1, -1)]),
                   3: np.array([(1, 1, 1),  # right
                                (1, -1, 1),
                                (1, -1, -1),
                                (1, 1, -1)]),
                   4: np.array([(-1, 1, 1),  # bottom
                                (1, 1, 1),
                                (1, 1, -1),
                                (-1, 1, -1)]),
                   5: np.array([(-1, 1, 1),  # deep/back
                                (1, 1, 1),
                                (1, -1, 1),
                                (-1, -1, 1)])}

    planes_realign_dict = {-1: np.array([0, 0, 0]),
                           0: np.array([0, 0, 1]),
                           1: np.array([0, 1, 0]),
                           2: np.array([1, 0, 0]),
                           3: np.array([-1, 0, 0]),
                           4: np.array([0, -1, 0]),
                           5: np.array([0, 0, -1]),
                           6: np.array([1, 1, 1]),
                           7: np.array([0, 1, 1]),
                           8: np.array([-1, 1, 1]),
                           9: np.array([1, 0, 1]),
                           10: np.array([-1, 0, 1]),
                           11: np.array([1, -1, 1]),
                           12: np.array([0, -1, 1]),
                           13: np.array([-1, -1, 1]),
                           14: np.array([1, 1, 0]),
                           15: np.array([-1, 1, 0]),
                           16: np.array([1, -1, 0]),
                           17: np.array([-1, -1, 0]),
                           18: np.array([1, 1, -1]),
                           19: np.array([0, 1, -1]),
                           20: np.array([-1, 1, -1]),
                           21: np.array([1, 0, -1]),
                           22: np.array([-1, 0, -1]),
                           23: np.array([1, -1, -1]),
                           24: np.array([0, -1, -1]),
                           25: np.array([-1, -1, -1])}
    faces = np.array(
        list(planes_dict[i] * voxel_demi for i in planes_dict.keys()))
    # Since the adjecent voxels are sorted the index position will
    # 0:  back adjecent, 1: top adjecent, 2: left adjecent, 3: right adjecent,
    # 4: bottom adjecent, 5:  front adjecent.
    # 2. Iterate through each coordinate
    for seed_coor in datatree[1:]:
        #     1. Determine voxel ID of seed_coor:
        seed_voxel = int(coordinate_voxel_loc.loc[seed_coor['ID']]['voxel_ID'])
        #     2. IF P of the next coordinate from the seed coordinate is not
        #     the ID of the seed coordinate then you have an endpoint
        #         1. Add 1 to sum ends of this seed coordinate voxel
        if seed_coor == datatree[
            -1]:  # this means that we have the second to last coordinate and
            # thus the last_coor is a ending
            voxel_array[seed_voxel]['sum_end_' + process[
                seed_coor['TYPE']]] += 1  # add 1 to the sum_end_process-type
        else:
            seed1_coor = datatree[datatree['ID'] == seed_coor['ID'] + 1]
            if seed1_coor['P'] != seed_coor['ID']:
                voxel_array[seed_voxel]['sum_end_' + process[seed_coor[
                    'TYPE']]] += 1  # add 1 to the sum_end_process-type
        #     3. IF ID of seed coordinate is present > 1 in the P list then
        #     you have an branchpoint.
        #         1. Add 1 to the sum nodes of this seed coordinate voxel
        if np.sum(datatree['P'] == seed_coor['ID']) > 1:
            voxel_array[seed_voxel]['sum_node_' + process[
                seed_coor['TYPE']]] += int(
                    np.sum(datatree['P'] == seed_coor['ID']))-1
            # add sum of occcurance of ID in P
            # to the sum_node_process-type
        #     4. Lookup connecting target coordinate (P). If P == 1 provide com
        target_coor = datatree_df.loc[seed_coor['P']]
        target_voxel = int(coordinate_voxel_loc.loc[seed_coor['P']]['voxel_ID'])
        #     6. Since the lines between coordinates are straight lines the
        #     voxel containing the line should be between the two voxels.
        #     7. If IDs of the two voxels are the same:
        #         1. Determine the length of the segment and add to the voxel
        #         sum length
        #         2. Continue to next seed coordinate in the morphology file.
        if target_voxel == seed_voxel:
            p0 = [seed_coor['X'], seed_coor['Y'], seed_coor['Z']]
            p1 = [target_coor['X'], target_coor['Y'], target_coor['Z']]
            segment_length = distance(p0, p1)
            voxel_array[seed_voxel]['sum_length_' + process[seed_coor[
                'TYPE']]] += segment_length  # add segment length to voxel
            # keep track of the seed_coor's for which segment lengths are
            # calculated
            tracker['calculated'][tracker['ID'] == seed_coor['ID']] = True
        #     8. If not IDs of the two voxels are the same:
        else:
            #       1. Determine whether target coordinate is in a direct
            #       adjecent voxel
            adjecent_coors = get_surrounding_voxels(seed_voxel, num_bins_x,
                                                    num_bins_y, num_bins_z,
                                                    voxel_array)
            if target_voxel in adjecent_coors:
                tracker['adjecent_target'][
                    tracker['ID'] == seed_coor['ID']] = True
                # annotate the adjecent plane based on location in the
                # adjecent_coors
                tracker['plane'][tracker['ID'] == seed_coor['ID']] = \
                np.where(target_voxel == adjecent_coors)[0][0]

    # at this point we have calculated segment lengths of all coordinate pairs
    # (seed-target) that are within 1 voxel and have annotated all coordinate
    # pairs that lay directly adjecent to each other. The remaining coordinate
    # pairs are not directly adjecent and the segment lengths need to calculated
    # as the segment travels through the voxels first lets calculate the
    # intersection coordinate for each seed coordinate there are 6 planes
    # through which a segment can intersect, therefore we need to calculate the
    # potential intersection point for each of the 6 planes. However for those
    # we already established the direct adjecent voxel of the target coordinate
    # we don't need to run all 6 planes and used the target_plane dictionary to
    # assess the right plane keep track of intersections of segments that do not
    # have s2 in a directly adjecent voxel
    tracker_added = tracker[np.logical_and(
        tracker['calculated'] == False,
        tracker['adjecent_target'] == False)]
    diag_len = len(tracker_added)
    diag_ID = datatree
    diag_seg = np.rec.fromarrays((
        np.arange(0, diag_len),
        tracker_added['process'],
        tracker_added['ID'],
        np.zeros(diag_len),
        np.zeros(diag_len),
        np.zeros(diag_len),
        np.zeros(diag_len),
        np.zeros(diag_len),
        np.zeros(diag_len),
        np.zeros(diag_len),
        np.zeros(diag_len),
        np.zeros(diag_len),
        np.zeros(diag_len),
        tracker_added['voxel_id'],
        tracker_added['P_voxel_id'],
        np.zeros(diag_len),
        np.zeros(diag_len),
        np.zeros(diag_len),
        np.zeros(diag_len).astype(bool),
        tracker_added['voxel_id'] == tracker_added['P_voxel_id']
    ),
        dtype=[('Index', int),
               ('process', int),
               ('ID', int),
               ('c_x', float),
               ('c_y', float),
               ('c_z', float),
               ('s2_x', float),
               ('s2_y', float),
               ('s2_z', float),
               ('s1_x', float),
               ('s1_y', float),
               ('s1_z', float),
               ('c_plane', int),
               ('s1_voxelID', int),
               ('s2_voxelID', int),
               ('c_voxelID', int),
               ('c2_voxelID', int),
               ('c_s2_adjecent', bool),
               ('c_s2_adjecent_plane', int),
               ('c_s2_samevoxel', bool),
               ])
    faces = np.array(
        list(planes_dict[i] * voxel_demi for i in planes_dict.keys()))
    tracker_sub = tracker[np.logical_and(
        tracker['calculated'] == False,
        tracker['adjecent_target'] == True)]
    start_add = len(
        tracker_sub)  # this is the start index where tracker_added is added.
    tracker_sub = np.hstack((tracker_sub, tracker_added))
    # this way the order of tracker_sub is such that adjecent_target comes first
    # followed by non-adjecent targets get IDs of the P
    Ps = datatree_df.loc[tracker_sub['ID']]['P']
    # Subset the coordinates
    s1 = datatree_df.loc[tracker_sub['ID']][
        ['X', 'Y', 'Z']]  # nx3 numpy array of end points for n line segments
    s1_centers = np.array(coordinate_voxel_loc.loc[tracker_sub['ID']][
                              ['center_x', 'center_y',
                               'center_z']])  # nx3 numpy array of end points
    # for n line segments
    s2 = datatree_df.loc[Ps][
        ['X', 'Y', 'Z']]  # nx3 numpy array of end points for n line segments
    s1_trans_x = s1['X'] - s1_centers[:, 0]
    s1_trans_y = s1['Y'] - s1_centers[:, 1]
    s1_trans_z = s1['Z'] - s1_centers[:, 2]
    s2_trans_x = s2['X'] - s1_centers[:, 0]
    s2_trans_y = s2['Y'] - s1_centers[:, 1]
    s2_trans_z = s2['Z'] - s1_centers[:, 2]
    s1_trans = np.vstack((s1_trans_x, s1_trans_y, s1_trans_z)).T
    s2_trans = np.vstack((s2_trans_x, s2_trans_y, s2_trans_z)).T
    segments = np.array([s1_trans, s2_trans])
    intersect_results = segmentfaceint(segments, faces)
    intersect_seg = np.full((len(s1_trans), 3), np.nan)
    intersect_plane = np.full(len(s1_trans), np.nan)
    intersect_voxelID = np.full(len(s1_trans), np.nan)
    for j in np.arange(0, len(s1_trans)):
        intersect_planes = np.full((6, 3), np.nan)
        for i in np.arange(0, 6):
            intersect_planes[i, :] = intersect_results[i][0][j]
        if np.sum(np.isnan(intersect_planes[:, 0])) == 5:
            intersect_seg[j, :] = intersect_planes[
                                  ~np.isnan(intersect_planes[:, 0]), :]
            intersect_plane[j] = np.where(~np.isnan(intersect_planes[:, 0]))[0][
                0]
            # identify the voxel ID of voxel where the intersecting point is
            # adjecent to
            s1_voxelID = tracker_sub[j]['voxel_id']
            adjecent_voxels = get_surrounding_voxels(s1_voxelID, num_bins_x,
                                                     num_bins_y, num_bins_z,
                                                     voxel_array)
            intersect_voxelID[j] = adjecent_voxels[int(intersect_plane[j])]

        elif np.sum(np.isnan(intersect_planes[:, 0])) < 5:
            print('Warning multiple intersecting planes are identified for'
                  ' s1_trans loc : ' + str(s1.iloc[j]) + ' first segment')

        elif np.sum(np.isnan(intersect_planes[:, 0])) == 6:
            print('Warning no intersecting plane was identified for s1_trans'
                  ' loc : ' + str(s1.iloc[j]) + ' first segment')

            #######
            # The problem here is with segments that within tolerance intersect
            # at the boundaries of two or more
            # planes. So here we need a way to deduct which boundary/boundaries
            # the segment intersects and provide an inferred intersection
            # coordinate as well as the correct voxel ID determine for which
            # axes the segment is crossing a boundary (this only works for
            # cardinal coordinates)

            p0_in = np.abs(s1_trans[j]) - voxel_demi < tol
            p1_in = np.abs(s2_trans[j]) - voxel_demi < tol
            in_between = np.logical_or(np.invert(p0_in),
                                       np.invert(p1_in)).astype(int)
            c_new = np.full(3, np.nan)

            # determine the polarity of the coordinates
            polarity = s2_trans[j] / np.abs(s2_trans[j])

            # fill out the new intersection coordinates, where when s1 and s2
            # are on opposite side of a boundary they will get the boundary
            # coordinate and otherwise the center of the two coordinates
            c_new[in_between == 1] = np.repeat(voxel_demi,
                                               np.sum(in_between == 1)) * \
                                     polarity[in_between == 1]
            c_new[in_between != 1] = (s1_trans[j][in_between != 1] +
                                      s2_trans[j][in_between != 1]) / 2
            s1_voxelID = tracker_sub[j]['voxel_id']
            # determine the planeID of the new c
            c_bounds = (np.floor(np.abs(c_new) / voxel_demi) * (
                        c_new / np.abs(c_new))) + 0.0
            intersect_plane[j] = planes_bound_dict[str(c_bounds)]
            adjecent_voxels = get_surrounding_voxels(s1_voxelID, num_bins_x,
                                                     num_bins_y, num_bins_z,
                                                     voxel_array,
                                                     include_all=True)
            if intersect_plane[j] == -1:
                intersect_voxelID[j] = tracker_sub[j]['P_voxel_id']
                intersect_seg[j, :] = s2_trans[j]
            else:
                intersect_voxelID[j] = adjecent_voxels[int(intersect_plane[j])]
                intersect_seg[j, :] = c_new
        if np.sum(np.isnan(intersect_seg[j])) > 0:
            # escape instance where no intersection could be calculated
            # in this case place the intersecting coordinate to s1 coordinates
            # and let the voxel be to the left of the s2 voxel. This way
            # length will still be incorporated with a slight misannotation
            # of the segment length per voxel. The occurance of this is however
            # very low.
            print('Warning, please note that during the calculation '
                  'an erroneous calculation of intersection was made. '
                  'To avoid crashing, the length was calculated but not'
                  'split/annotated between multiple voxels, but instead '
                  'placed in the same voxelID as target voxelID')
            intersect_seg[j, :] = s1_trans[j]
            intersect_voxelID[j] = tracker_sub[j]['P_voxel_id'] - 1
            intersect_plane[j] = 2

    segment_length_s1c = np.sqrt((s1_trans - intersect_seg)[:, 0] ** 2 +
                                 (s1_trans - intersect_seg)[:, 1] ** 2 +
                                 (s1_trans - intersect_seg)[:, 2] ** 2)
    segment_length_s2c = np.sqrt((s2_trans - intersect_seg)[:, 0] ** 2 +
                                 (s2_trans - intersect_seg)[:, 1] ** 2 +
                                 (s2_trans - intersect_seg)[:, 2] ** 2)
    # omit any segments below a certain tolerance, these cause problematic
    # inclusions of found intersections that actually are incorrect
    s1c_omit = segment_length_s1c < tol
    s2c_omit = segment_length_s2c < tol
    segment_length_s1c[s1c_omit] = 0
    segment_length_s2c[s2c_omit] = 0

    for i, val in process.items():
        proc_sub = tracker_sub[tracker_sub['process'] == i]
        segment_sub = segment_length_s1c[tracker_sub['process'] == i]
        # to avoid potential slip ins of nan's
        notnan = ~np.isnan(segment_sub)
        proc_sub = proc_sub[notnan]
        segment_sub = segment_sub[notnan]
        for entry in np.arange(0, len(proc_sub)):
            voxel_array[proc_sub[entry]['voxel_id']]['sum_length_' + val] += \
            segment_sub[entry]
    # populate the segment length to the correct process type and voxel indexes
    # between s2 till c for only those with adjecent voxels
    for i, val in process.items():
        subset_criteria = np.logical_and(tracker_sub['process'] == i,
                                         tracker_sub[
                                             'P_voxel_id'] == intersect_voxelID)
        voxel_sub = tracker_sub['P_voxel_id'][subset_criteria]
        segment_sub = segment_length_s2c[subset_criteria]
        # to avoid potential slip ins of nan's
        notnan = ~np.isnan(segment_sub)
        segment_sub = segment_sub[notnan]
        voxel_sub = voxel_sub[notnan]
        for entry in np.arange(0, len(voxel_sub)):
            voxel_array[voxel_sub[entry]]['sum_length_' + val] += segment_sub[
                entry]

    # second calculate for all the coordinate pairs with direct adjecent voxels
    # the remaining segment lengths.
    # third iteratively go through the coordinate pairs which are not adjecent
    # and register the calculated segment lenght per voxel as the segment
    # travels through the voxels.
    # check there are no nan's in intersect_seg
    # fill out the diag_seg
    diag_seg['c_x'] = intersect_seg[start_add:, 0]
    diag_seg['c_y'] = intersect_seg[start_add:, 1]
    diag_seg['c_z'] = intersect_seg[start_add:, 2]
    diag_seg['s1_x'] = s1_trans[start_add:, 0]
    diag_seg['s1_y'] = s1_trans[start_add:, 1]
    diag_seg['s1_z'] = s1_trans[start_add:, 2]
    diag_seg['s2_x'] = s2_trans[start_add:, 0]
    diag_seg['s2_y'] = s2_trans[start_add:, 1]
    diag_seg['s2_z'] = s2_trans[start_add:, 2]
    diag_seg['c_plane'] = intersect_plane[start_add:]
    diag_seg['c_voxelID'] = intersect_voxelID[start_add:]
    # determine voxel ID for c and affiliated info
    for i in np.arange(0, len(diag_seg)):
        adjecent_voxels_c = get_surrounding_voxels(diag_seg[i]['c_voxelID'],
                                                   num_bins_x, num_bins_y,
                                                   num_bins_z,
                                                   voxel_array)
        if diag_seg[i]['s2_voxelID'] in adjecent_voxels_c:
            diag_seg[i]['c_s2_adjecent'] = True
            diag_seg[i]['c_s2_adjecent_plane'] = \
            np.where(diag_seg[i]['s2_voxelID'] == adjecent_voxels_c)[0][0]

    count = 0
    compute_limit = 20
    while count < compute_limit:
        # calculate segment lengths for those with same voxelID
        diag_seg['c_s2_samevoxel'] = diag_seg['c_voxelID'] == diag_seg[
            's2_voxelID']
        same_voxel_bool = diag_seg['c_s2_samevoxel']
        diag_seg_same = diag_seg[diag_seg['c_s2_samevoxel']]
        if np.logical_and(len(diag_seg_same) >= 1, count > 0):
            for i in np.arange(0, len(diag_seg_same)):
                p0 = diag_seg_same[i][['c_x', 'c_y', 'c_z']]
                p1 = diag_seg_same[i][['s2_x', 's2_y', 's2_z']]
                segment_length = distance(p0, p1)
                voxel_array[diag_seg_same[i]['c_voxelID']][
                    'sum_length_' + process[
                        diag_seg_same[i]['process']]] += segment_length
        # remove these coordinates
        diag_seg = np.delete(diag_seg, np.where(diag_seg['c_s2_samevoxel'])[0],
                             axis=0)
        # correct coordinates based on planes
        error_index = []
        for i in np.arange(0, len(diag_seg)):
            try:
                diag_seg[i]['c_x'] = diag_seg[i]['c_x'] + (
                            planes_realign_dict[diag_seg[i]['c_plane']][
                                0] * voxel_size)
                diag_seg[i]['c_y'] = diag_seg[i]['c_y'] + (
                            planes_realign_dict[diag_seg[i]['c_plane']][
                                1] * voxel_size)
                diag_seg[i]['c_z'] = diag_seg[i]['c_z'] + (
                            planes_realign_dict[diag_seg[i]['c_plane']][
                                2] * voxel_size)
                diag_seg[i]['s2_x'] = diag_seg[i]['s2_x'] + (
                            planes_realign_dict[diag_seg[i]['c_plane']][
                                0] * voxel_size)
                diag_seg[i]['s2_y'] = diag_seg[i]['s2_y'] + (
                            planes_realign_dict[diag_seg[i]['c_plane']][
                                1] * voxel_size)
                diag_seg[i]['s2_z'] = diag_seg[i]['s2_z'] + (
                            planes_realign_dict[diag_seg[i]['c_plane']][
                                2] * voxel_size)
            except KeyError:
                print('Warning diag_seg :' +  str(i) + ' gave nan. Will be '
                                                       'removed')
                error_index.append(i)
            # remove these coordinates
        diag_seg = np.delete(diag_seg,
                             error_index,
                             axis=0)

            # compute intersections
        c_trans = np.vstack(
            (diag_seg['c_x'], diag_seg['c_y'], diag_seg['c_z'])).T
        s2_trans = np.vstack(
            (diag_seg['s2_x'], diag_seg['s2_y'], diag_seg['s2_z'])).T
        segments = np.array([c_trans, s2_trans])
        intersect_results = segmentfaceint(segments, faces)
        intersect_seg = np.full((len(c_trans), 3), np.nan)
        intersect_plane = np.full(len(c_trans), np.nan)
        intersect_voxelID = np.full(len(c_trans), np.nan)

        for j in np.arange(0, len(c_trans)):
            intersect_planes = np.full((6, 3), np.nan)
            for i in np.arange(0, 6):
                intersect_planes[i, :] = intersect_results[i][0][j]
            if np.sum(np.isnan(intersect_planes[:, 0])) == 5:
                intersect_seg[j, :] = intersect_planes[
                                      ~np.isnan(intersect_planes[:, 0]), :]
                intersect_plane[j] = \
                np.where(~np.isnan(intersect_planes[:, 0]))[0][0]
                # identify the voxel ID of voxel where the intersecting point
                # is adjecent to
                c_voxelID = diag_seg[j]['c_voxelID']
                adjecent_voxels = get_surrounding_voxels(c_voxelID, num_bins_x,
                                                         num_bins_y,
                                                         num_bins_z,
                                                         voxel_array)
                intersect_voxelID[j] = adjecent_voxels[int(intersect_plane[j])]
            elif np.sum(np.isnan(intersect_planes[:, 0])) < 5:
                # in this case most likely there is the c_coordinate that was
                # picked up as intersection.
                # thus remove that coordinate
                pickup = [np.all(intersect_planes[n] == segments[0][j]) for n in
                          np.arange(0, 6)]
                intersect_planes[pickup, :] = np.nan
                if np.sum(np.isnan(intersect_planes[:, 0])) == 5:
                    intersect_seg[j, :] = intersect_planes[
                                          ~np.isnan(intersect_planes[:, 0]), :]
                    intersect_plane[j] = \
                    np.where(~np.isnan(intersect_planes[:, 0]))[0][0]
                    # identify the voxel ID of voxel where the intersecting
                    # point is adjecent to
                    c_voxelID = diag_seg[j]['c_voxelID']
                    adjecent_voxels = get_surrounding_voxels(c_voxelID,
                                                             num_bins_x,
                                                             num_bins_y,
                                                             num_bins_z,
                                                             voxel_array)
                    intersect_voxelID[j] = adjecent_voxels[
                        int(intersect_plane[j])]
                else:
                    pickup = [np.all(intersect_planes[n] - segments[0][j] < tol)
                              for n in np.arange(0, 6)]
                    intersect_planes[pickup, :] = np.nan
                    if np.sum(np.isnan(intersect_planes[:, 0])) == 5:
                        print('Removing c_origin solved the problem')
                        intersect_seg[j, :] = intersect_planes[
                                              ~np.isnan(intersect_planes[:, 0]),
                                              :]
                        intersect_plane[j] = \
                        np.where(~np.isnan(intersect_planes[:, 0]))[0][0]
                        # identify the voxel ID of voxel where the intersecting
                        # point is adjecent to
                        c_voxelID = diag_seg[j]['c_voxelID']
                        adjecent_voxels = get_surrounding_voxels(c_voxelID,
                                                                 num_bins_x,
                                                                 num_bins_y,
                                                                 num_bins_z,
                                                                 voxel_array)
                        intersect_voxelID[j] = adjecent_voxels[
                            int(intersect_plane[j])]
            if np.sum(np.isnan(intersect_planes[:, 0])) == 6:
                print('Warning no intersecting plane was identified for c_trans'
                      ' loc : ' + str(j) + 'second segment')
                #######
                # The problem here is with segments that within tolerance
                # intersect at the boundaries of two or more planes. So here we
                # need a way to deduct which boundary/boundaries the segment
                # intersects and provide an inferred intersection coordinate as
                # well as the correct voxel ID determine for which axes the
                # segment is crossing a boundary (this only works for cardinal
                # coordinates)
                p0_in = np.abs(c_trans[j]) - voxel_demi < tol
                p1_in = np.abs(s2_trans[j]) - voxel_demi < tol
                in_between = np.logical_or(np.invert(p0_in),
                                           np.invert(p1_in)).astype(int)
                c_new = np.full(3, np.nan)

                # determine the polarity of the coordinates
                polarity = s2_trans[j] / np.abs(s2_trans[j])

                # fill out the new intersection coordinates, where when s1 and
                # s2 are on opposite side of a boundary they will get the
                # boundary coordinate and otherwise the center of the two
                # coordinates
                c_new[in_between == 1] = np.repeat(voxel_demi,
                                                   np.sum(in_between == 1)) * \
                                         polarity[in_between == 1]
                c_new[in_between != 1] = (c_trans[j][in_between != 1] +
                                          s2_trans[j][in_between != 1]) / 2
                c_voxelID = diag_seg[j]['c_voxelID']
                # determine the planeID of the new c
                c_bounds = (np.floor(np.abs(c_new) / voxel_demi) * (
                            c_new / np.abs(c_new))) + 0.0
                if str(c_bounds) == '[0. 0. 0.]':
                    # in this case the intersection is within voxel_space
                    intersect_voxelID[j] = c_voxelID
                    intersect_seg[j, :] = s2_trans[j]
                else:
                    intersect_plane[j] = planes_bound_dict[str(c_bounds)]
                    adjecent_voxels = get_surrounding_voxels(c_voxelID,
                                                             num_bins_x,
                                                             num_bins_y,
                                                             num_bins_z,
                                                             voxel_array,
                                                             include_all=True)
                    intersect_voxelID[j] = adjecent_voxels[int(
                        intersect_plane[j])]
                    intersect_seg[j, :] = c_new
                if np.sum(np.isnan(intersect_seg[j])) > 0:
                    # escape instance where no intersection could be calculated
                    # in this case place the intersecting coordinate to s1 coordinates
                    # and let the voxel be to the left of the s2 voxel. This way
                    # length will still be incorporated with a slight misannotation
                    # of the segment length per voxel. The occurance of this is however
                    # very low.
                    print('Warning, please note that during the calculation '
                          'an erroneous calculation of intersection was made. '
                          'To avoid crashing, the length was calculated but not'
                          'split/annotated between multiple voxels, but instead '
                          'placed in the same voxelID as target voxelID')
                    intersect_seg[j, :] = c_trans[j]
                    intersect_voxelID[j] = diag_seg[j]['s2_voxelID'] - 1
                    intersect_plane[j] = 2

        segment_length_cc2 = np.sqrt((c_trans - intersect_seg)[:, 0] ** 2 +
                                     (c_trans - intersect_seg)[:, 1] ** 2 +
                                     (c_trans - intersect_seg)[:, 2] ** 2)
        segment_length_s2c2 = np.sqrt((s2_trans - intersect_seg)[:, 0] ** 2 +
                                      (s2_trans - intersect_seg)[:, 1] ** 2 +
                                      (s2_trans - intersect_seg)[:, 2] ** 2)
        cc2_omit = segment_length_cc2 < tol
        s2c2_omit = segment_length_s2c2 < tol
        segment_length_cc2[cc2_omit] = np.nan
        segment_length_s2c2[s2c2_omit] = np.nan

        # populate the segment length to the correct process type and voxel
        # indexes between c till c2
        for i, val in process.items():
            proc_sub = diag_seg[diag_seg['process'] == i]
            segment_sub = segment_length_cc2[diag_seg['process'] == i]
            # to avoid potential slip ins of nan's
            notnan = ~np.isnan(segment_sub)
            proc_sub = proc_sub[notnan]
            segment_sub = segment_sub[notnan]
            for entry in np.arange(0, len(proc_sub)):
                voxel_array[proc_sub[entry]['c_voxelID']][
                    'sum_length_' + val] += segment_sub[entry]
                # populate the segment length to the correct process type and
                # voxel indexes between s2 till c2 for only those with adjecent
                # voxels
        for i, val in process.items():
            subset_criteria = np.logical_and(diag_seg['process'] == i,
                                             diag_seg['c_s2_adjecent'])
            if np.sum(subset_criteria) >= 1:
                proc_sub = diag_seg[subset_criteria]
                voxel_sub = diag_seg[subset_criteria]['s2_voxelID']
                segment_sub = segment_length_s2c2[subset_criteria]
                # to avoid potential slip ins of nan's
                notnan = ~np.isnan(segment_sub)
                segment_sub = segment_sub[notnan]
                voxel_sub = voxel_sub[notnan]
                for entry in np.arange(0, len(voxel_sub)):
                    voxel_array[voxel_sub[entry]]['sum_length_' + val] += \
                    segment_sub[entry]

        # fill out c coordinates based on the new intersections (c2)
        diag_seg['c_x'] = intersect_seg[:, 0]
        diag_seg['c_y'] = intersect_seg[:, 1]
        diag_seg['c_z'] = intersect_seg[:, 2]
        # remove all coordinates with adjecent voxels
        plane_register = np.delete(intersect_plane,
                                   np.where(diag_seg['c_s2_adjecent'])[0])
        intersect_voxelID = np.delete(intersect_voxelID,
                                      np.where(diag_seg['c_s2_adjecent'])[0])
        diag_seg = np.delete(diag_seg, np.where(diag_seg['c_s2_adjecent'])[0],
                             axis=0)
        diag_seg['c_plane'] = plane_register
        diag_seg['c_voxelID'] = intersect_voxelID
        # determine voxel ID for c and affiliated info
        for i in np.arange(0, len(diag_seg)):
            adjecent_voxels_c = get_surrounding_voxels(diag_seg[i]['c_voxelID'],
                                                       num_bins_x, num_bins_y,
                                                       num_bins_z,
                                                       voxel_array)
            if diag_seg[i]['s2_voxelID'] in adjecent_voxels_c:
                diag_seg[i]['c_s2_adjecent'] = True
                diag_seg[i]['c_s2_adjecent_plane'] = \
                np.where(diag_seg[i]['s2_voxelID'] == adjecent_voxels_c)[0][0]
        diag_seg['c_s2_samevoxel'] = diag_seg['c_voxelID'] == diag_seg[
            's2_voxelID']
        if len(diag_seg) == 0:
            count = compute_limit
        else:
            count += 1
    if calc_total_length is True:
        # to double check whether my script made errorous additions or
        # subtractions relative to simple point-point segment length calculation
        com_zero = datatree_df[datatree_df['P'] == -1]
        apical_df = pd.concat([com_zero, datatree_df[datatree_df['TYPE'] == 4]])
        apical_len = 0
        for i in np.arange(1, len(apical_df)):
            p1 = apical_df.loc[apical_df.iloc[i]['P']][['X', 'Y', 'Z']]
            p0 = apical_df.iloc[i][['X', 'Y', 'Z']]
            segment_length = distance(p0, p1)
            apical_len += segment_length
        basal_df = pd.concat([com_zero, datatree_df[datatree_df['TYPE'] == 3]])
        basal_len = 0
        for i in np.arange(1, len(basal_df)):
            p1 = basal_df.loc[basal_df.iloc[i]['P']][['X', 'Y', 'Z']]
            p0 = basal_df.iloc[i][['X', 'Y', 'Z']]
            segment_length = distance(p0, p1)
            basal_len += segment_length
    else:
        apical_len = None
        basal_len = None

    # append voxel_array to the cell instance
    if not parallel_on:
        cell.analysis['morphology'] = {'voxel_array': voxel_array,
                                       'voxel_array_bins': {'x': bins_x,
                                                            'y': bins_y,
                                                            'z': bins_z},
                                       'voxel_array_bin_centers':
                                           {'x': center_bin_x,
                                            'y': center_bin_y,
                                            'z': center_bin_z},
                                       'voxel_size': voxel_size}
    if save_voxel_array:
        # convert voxel_array numpy array to a dataframe:
        voxel_df = pd.DataFrame(voxel_array)
        # reduce the array based on presence of information
        voxel_df_reduc = voxel_df[np.nansum(voxel_df[['sum_length_basal',
                                                      'sum_length_apical',
                                                      'sum_length_axon',
                                                      'sum_node_basal',
                                                      'sum_node_apical',
                                                      'sum_node_axon',
                                                      'sum_end_basal',
                                                      'sum_end_apical',
                                                      'sum_end_axon']],
                                            axis=1) > 0]
        save_file_root = Path(bcs_global.path_morph_analysis) / '__voxel_arrays__'
        cell_path = save_file_root / str(cell.cell_amp_id)
        if not cell_path.exists():
            cell_path.mkdir(parents=True)
        cell_path_voxel = cell_path / (str(voxel_size) + 'um')
        if normalizer_axis:
            cell_path_voxel = cell_path / (str(normalized_axes) + '-normby-' +
                                           str(normalizer_axis) +
                                           str(voxel_size))
        if not cell_path_voxel.exists():
            cell_path_voxel.mkdir()

        save_filename = cell_path_voxel / ('_'.join([str(cell.cell_amp_id),
                                                     str(voxel_size),
                                                     str(axial_angle),
                                                     str(azimuth_angle),
                                                     str(polar_angle),
                                                     '.csv']))
        voxel_df_reduc.to_csv(save_filename, index=False)
    if not suppress_output:
        return cell, voxel_array, apical_len, basal_len
    else:
        return


def gen_visdata_voxel_array_3D(cell, select, voxel_size=50, show_PCA=False,
                               distance_var=1):
    """Visualize the voxel array in 3D with plotly.

    Args:
        - cell: instance of object Cell representing all information of this
                particular cell.
        - voxel_size: in µm size of the voxel used to calculate density maps.
        - select: numpy array of strings describing processes and datatypes to
                  select;
                      processes: dendrite (apical + basal), apical, basal, axon
                      datatyptes: length, node, end
        - show_PCA: default False, if True will display the PCA axes for length
                    on whole dendrite.

    Returns:
        - fig_data: list of plotly traces.
        - cell: updated with PCA and highres

    """
    if 'voxel_array' not in cell.analysis['morphology'].keys():
        cell, voxel_array, _, _ = morph2voxel(cell, voxel_size)
    else:
        voxel_array = cell.analysis['morphology']['voxel_array']
    # subset in processes, datatype (length/nodes/ends) and by presence of data 
    # in voxel
    processes = np.array(['dendrite', 'apical', 'basal', 'axon'])
    datatypes = np.array(['length', 'node', 'end'])
    markers = {'length': 'square',
               'node': 'circle',
               'end': 'diamond'}
    colorscales = {'dendrite': 'Portland',
                   'apical': 'Reds',
                   'basal': 'Blues',
                   'axon': 'Greys'}

    fig_data = list()

    mask = np.ones((len(processes))).astype(bool)
    for ind, process in enumerate(processes):
        if not process in select:
            mask[ind] = False
    processes = processes[mask]

    mask = np.ones((len(datatypes))).astype(bool)
    for ind, datatype in enumerate(datatypes):
        if not datatype in select:
            mask[ind] = False
    datatypes = datatypes[mask]

    for process in processes:
        for datatype in datatypes:
            if process == 'dendrite':
                data_string = str('sum_' + datatype + '_dendrite')
                data_string_1 = str('sum_' + datatype + '_apical')
                data_string_2 = str('sum_' + datatype + '_basal')

                array = voxel_array[
                    np.logical_or(voxel_array[data_string_1] > 0,
                                  voxel_array[data_string_2] > 0)]
                density = array[data_string_1] + array[data_string_2]
            else:
                data_string = str('sum_' + datatype + '_' + process)
                array = voxel_array[voxel_array[data_string] > 0]
                density = array[data_string]
            fig_data.append(
                go.Scatter3d(
                    x=array['X'],
                    y=array['Y'],
                    z=array['Z'],
                    name=data_string[4:],
                    mode='markers',
                    marker=dict(
                        size=8,
                        sizemin=8,
                        sizeref=20,
                        color=density,
                        colorscale=colorscales[process],
                        symbol=markers[datatype],
                        opacity=0.6)))

    # add cardinal line pia to cc
    fig_data.append(
        go.Scatter3d(
            x=[0, 0],
            y=[np.min(array['Y']), np.max(array['Y'])],
            z=[0, 0],
            name='cardinal',
            mode='lines',
            line=dict(
                width=3,
                color='royalblue')))
    if show_PCA:
        datatree = get_dataTree(data=cell.swc_data)
        # remove axon!
        datatree = datatree[datatree['TYPE']!=2]
        com = cell.swc_data[
            np.logical_and(cell.swc_data['P'] == -1, cell.swc_data[
                'TYPE'] == 1)]  # here we assume that the origin of all
        # processes are coming from the center of mass of the cellbody contour

        # Transform morhology coordinate system to align with a specific feature
        # to become 0,0,0 (x,y,z)
        datatree['X'] = datatree['X'] - com['X']
        datatree['Y'] = datatree['Y'] - com['Y']
        datatree['Z'] = datatree['Z'] - com['Z']
        origin_tree = com
        origin_tree['X'] = origin_tree['X'] - com['X']
        origin_tree['Y'] = origin_tree['Y'] - com['Y']
        origin_tree['Z'] = origin_tree['Z'] - com['Z']
        datatree = np.hstack((com[0], datatree))
        datatree_df = pd.DataFrame(
            data=datatree[['TYPE', 'X', 'Y', 'Z', 'R', 'P']],
            index=datatree['ID'])
        # make highres version of datatree
        datatree_highres = np.array([[0, 0, 0]])
        for i in datatree_df.index[1:]:
            p = datatree_df.loc[i, 'P']
            p0 = np.array([datatree_df.loc[i][['X', 'Y', 'Z']]])
            p1 = np.array([datatree_df.loc[p][['X', 'Y', 'Z']]])
            datatree_highres = np.vstack((datatree_highres,
                                          p0,
                                          fill_points_on_line_3D(p0, p1,
                                                                 distance_var),
                                          p1))
        # add the highres to the plot
        fig_data.append(
            go.Scatter3d(
                x=datatree_highres[:, 0] / voxel_size,
                y=datatree_highres[:, 1] / voxel_size,
                z=datatree_highres[:, 2] / voxel_size,
                name='morphology points',
                mode='markers',
                marker=dict(
                    size=3,
                    color='black')))
        pca = PCA(n_components=3)
        pca.fit(datatree_highres)
        eigen_vectors = pca.components_
        eigen_values = np.round(pca.explained_variance_ratio_, decimals=3)
        pca_results = {'eigen_vectors': eigen_vectors,
                       'eigen_values': eigen_values}
        max_distance = np.max([np.max(array['X']),
                               np.max(array['Y']),
                               np.max(array['Z'])])
        PCA1 = ((0, (max_distance * eigen_values[0]) * eigen_vectors[0, 0]),
                (0, (max_distance * eigen_values[0]) * eigen_vectors[0, 1]),
                (0, (max_distance * eigen_values[0]) * eigen_vectors[0, 2]))
        PCA2 = ((0, (max_distance * eigen_values[1]) * eigen_vectors[1, 0]),
                (0, (max_distance * eigen_values[1]) * eigen_vectors[1, 1]),
                (0, (max_distance * eigen_values[1]) * eigen_vectors[1, 2]))
        PCA3 = ((0, (max_distance * eigen_values[2]) * eigen_vectors[2, 0]),
                (0, (max_distance * eigen_values[2]) * eigen_vectors[2, 1]),
                (0, (max_distance * eigen_values[2]) * eigen_vectors[2, 2]))
        fig_data.append(
            go.Scatter3d(
                x=PCA1[0],
                y=PCA1[1],
                z=PCA1[2],
                name='PCA1, explained variance(%): ' + str(
                    np.round(eigen_values[0] * 100, 2)),
                mode='lines',
                line=dict(
                    width=6,
                    color='firebrick')))
        fig_data.append(
            go.Scatter3d(
                x=PCA2[0],
                y=PCA2[1],
                z=PCA2[2],
                name='PCA2, explained variance(%): ' + str(
                    np.round(eigen_values[1] * 100, 2)),
                mode='lines',
                line=dict(
                    width=6,
                    color='fuchsia')))
        fig_data.append(
            go.Scatter3d(
                x=PCA3[0],
                y=PCA3[1],
                z=PCA3[2],
                name='PCA3, explained variance(%): ' + str(
                    np.round(eigen_values[2] * 100, 2)),
                mode='lines',
                line=dict(
                    width=6,
                    color='gold')))
        cell.analysis['morphology']['PCA'] = pca_results
        cell.analysis['morphology']['highres'] = datatree_highres
    fig = go.Figure(data=fig_data)
    fig.show()
    return cell, fig_data


def transform_tree(original_tree, tform):
    """Transform morphology using a transformation matrix for all angle combos.

    Args:
        original_tree: numpy array of nx3
        tform: numpy array of 3x3xangle_combinations.

    Returns: numpy array of nx3xangle_combinations

    """
    # the transformation is along Z,Y,Z. In the data the y reflects z.
    # so we need to switch the columns and then
    # do the transformation, then switch back the columns
    total_coordinates, _ = np.shape(original_tree)
    _, _, total_rotations = np.shape(tform)
    switch_tree = np.vstack((original_tree[:, 0],
                             original_tree[:, 2],
                             original_tree[:, 1])).T
    transformed_trees_switched = (np.reshape(switch_tree[:, 0],
                                             (total_coordinates, 1, 1)) *
                                  tform[0, :, :] +
                                  np.reshape(switch_tree[:, 1],
                                             (total_coordinates, 1, 1)) *
                                  tform[1, :, :] +
                                  np.reshape(switch_tree[:, 2],
                                             (total_coordinates, 1, 1)) *
                                  tform[2, :, :])
    transformed_trees = np.hstack((
        np.reshape(transformed_trees_switched[:, 0, :],
                   (total_coordinates, 1, total_rotations)),
        np.reshape(transformed_trees_switched[:, 2, :],
                   (total_coordinates, 1, total_rotations)),
        np.reshape(transformed_trees_switched[:, 1, :],
                   (total_coordinates, 1, total_rotations))))
    return transformed_trees


def transform_morphology2voxel(cellAmpID,
                               samples_sphere=500,
                               polar_limit=np.pi/3,  # equivalent to 60º
                               axial_rotation_intervals=np.pi/4,  # equivalent to 45º
                               ignore_axon=False,
                               normalized_axes='xyz',
                               normalizer_axis='cortical_thickness',
                               voxel_size=0.05,):
    """

    Args:
        cellAmpID:
        samples_sphere:
        polar_limit:
        normalized_axes:
        normalizer_axis: Default is None. Option is cortical_thickness

    Returns:

    """
    # first generate vectors for azimuth, polar, and axial angles based on a
    # equidistant distribution of samples_sphere points.
    n_count = 0
    r = 1
    a = 4 * np.pi * r ** 2 / samples_sphere
    d = np.sqrt(a)
    m_polar = np.round(np.pi / d)
    d_polar = np.pi / m_polar
    d_azimu = a / d_polar
    coord = np.array([[0, 0, 0], ])
    polar_vec = np.array([0])
    azimu_vec = np.array([0])
    for m in np.arange(0, m_polar, 1):
        polar = np.pi * (m + 0.5) / m_polar
        m_azimu = np.round(2 * np.pi * np.sin(polar) / d_azimu)
        if polar <= polar_limit:
            # only use the locations that are inside the cone; defined by polar_
            # limit
            for n in np.arange(0, m_azimu, 1):
                azimu = 2 * np.pi * n / m_azimu
                coord_ap = np.dot(r, [[np.sin(polar) * np.cos(azimu)],
                                      [np.sin(polar) * np.sin(azimu)],
                                      [np.cos(polar)]])
                if coord_ap[2][0] > 0:
                    # select one half of the sphere.
                    coord = np.vstack((coord, np.array(
                        [[coord_ap[0][0], coord_ap[1][0], coord_ap[2][0]]])))
                    polar_vec = np.hstack((polar_vec, polar))
                    azimu_vec = np.hstack((azimu_vec, azimu))
                n_count += 1
    # modify the angle vector such that it accomodates for the rotations along
    # the axis of the cortical thickness
    axial_vec = np.arange(0, np.pi * 2, axial_rotation_intervals)
    # this produces x steps in a full circle
    dome_locations = len(polar_vec)
    polar_vec = np.repeat(polar_vec, len(axial_vec))
    azimu_vec = np.repeat(azimu_vec, len(axial_vec))
    axial_vec = np.tile(axial_vec, dome_locations)
    total_orientations = len(polar_vec)
    print('Total orientations used: ' + str(total_orientations) +
          '. \nTotal dome locations used: ' + str(dome_locations))

    # generate a transformation matrix to transform the data along all the
    # combinations of angles in polar, azimuth and axial rotation.
    # transform matrix for data
    # Contributed by Cesar Celis and Michael Muniak 2019
    tform = np.zeros((3, 3, total_orientations))
    tform[0, 0, :] = (np.cos(azimu_vec) * np.cos(polar_vec) * np.cos(axial_vec)
                      - np.sin(azimu_vec) * np.sin(axial_vec))
    tform[0, 1, :] = (-np.sin(azimu_vec) * np.cos(polar_vec) * np.cos(axial_vec)
                      - np.cos(azimu_vec) * np.sin(axial_vec))
    tform[0, 2, :] = (np.sin(polar_vec) * np.cos(axial_vec))
    tform[1, 0, :] = (np.cos(azimu_vec) * np.cos(polar_vec) * np.sin(axial_vec)
                      + np.sin(azimu_vec) * np.cos(axial_vec))
    tform[1, 1, :] = (-np.sin(azimu_vec) * np.cos(polar_vec) * np.sin(axial_vec)
                      + np.cos(azimu_vec) * np.cos(axial_vec))
    tform[1, 2, :] = (np.sin(polar_vec) * np.sin(axial_vec))
    tform[2, 0, :] = (-np.cos(azimu_vec) * np.sin(polar_vec))
    tform[2, 1, :] = (np.sin(azimu_vec) * np.sin(polar_vec))
    tform[2, 2, :] = (np.cos(polar_vec))

    # load data tree
    cell = Cell(cellAmpID)
    data_tree = get_dataTree(cell.swc_data)
    # set coordinates such that center of mass of soma is at 0,0,0
    com = cell.swc_data[np.logical_and(cell.swc_data['P'] == -1, cell.swc_data[
        'TYPE'] == 1)]  # here we assume that the origin of all processes are
    # coming from the center of mass of the cellbody contour

    # Transform morphology coordinate system to align with a specific feature to
    # become 0,0,0 (x,y,z)
    data_tree['X'] = data_tree['X'] - com['X']
    data_tree['Y'] = data_tree['Y'] - com['Y']
    data_tree['Z'] = data_tree['Z'] - com['Z']
    origin_tree = com
    origin_tree['X'] = origin_tree['X'] - com['X']
    origin_tree['Y'] = origin_tree['Y'] - com['Y']
    origin_tree['Z'] = origin_tree['Z'] - com['Z']

    # if noramlizer_axis is given then normalize the morphology
    if normalizer_axis:
        # TODO may want to add more options how to normalize the 3D space
        if normalizer_axis == 'cortical_thickness':
            contourData = extractContourData(cell.swc_data, cell.swc_meta)
            contourData = extractsomaData(cell.swc_data,
                                          cell.swc_meta,
                                          contourData)
            contourData = calc_cortexThickness(contourData)
            print('Using ' + str(contourData['cortexThickness']) + ' µm to '
                                                                   'normalize '
                                                                   'morphology')
            if normalized_axes == 'xyz':
                data_tree['X'] = data_tree['X'] / contourData['cortexThickness']
                data_tree['Y'] = data_tree['Y'] / contourData['cortexThickness']
                data_tree['Z'] = data_tree['Z'] / contourData['cortexThickness']
            else:
                print('Warning, given normalized axes are not understood, '
                      'given: ' + str(normalized_axes))
        else:
            print('Warning, given normalizer axis are not understood, '
                  'given: ' + str(normalizer_axis))
    # add COM to the data_tree
    data_tree = np.hstack((origin_tree[0], data_tree))
    # place data_tree in a dataframe > normal numpy array
    # TODO to reduce processing time this should be fixed to one step.
    #  But it works now
    matrix_tree = pd.DataFrame(data_tree[['X', 'Y', 'Z']])
    matrix_tree = np.array(matrix_tree)
    transformed_trees = transform_tree(matrix_tree, tform)
    # Iterate through the transformed trees and generate voxel_arrays and save
    # them.
    datatree_init = data_tree
    # for i in np.arange(0, total_orientations):
        # # place the transformed_tree into the original data_tree structure
        # # this required for the morph2voxel function.
        # datatree_init['X'] = transformed_trees[:, 0, i]
        # datatree_init['Y'] = transformed_trees[:, 1, i]
        # datatree_init['Z'] = transformed_trees[:, 2, i]
        # # print(datatree_init[['X','Y','Z']][:10])
        # morph2voxel(cell,
        #             voxel_size,
        #             calc_total_length=False,
        #             save_voxel_array=True,
        #             datatree_init=datatree_init,
        #             normalized_axes=normalized_axes,
        #             normalizer_axis=normalizer_axis,
        #             axial_angle=round(axial_vec[i] * (180 / np.pi)),
        #             azimuth_angle=round(azimu_vec[i] * (180 / np.pi)),
        #             polar_angle=round(polar_vec[i] * (180 / np.pi)),
        #             suppress_output=True)
        # make this to process in parallel
    starttime = time.time()
    pool = multiprocessing.Pool()
    i_orient = np.arange(0, total_orientations).tolist()
    pool.map(partial(multiprocess_morph2voxel,
                     datatree_init=datatree_init,
                     transformed_trees=transformed_trees,
                     cell=cell,
                     voxel_size=voxel_size,
                     ignore_axon=ignore_axon,
                     normalized_axes=normalized_axes,
                     normalizer_axis=normalizer_axis,
                     axial_vec=axial_vec,
                     azimu_vec=azimu_vec,
                     polar_vec=polar_vec,
                     ), i_orient)
    pool.close()
    print('That took {} seconds'.format(time.time() - starttime))


def multiprocess_morph2voxel(i_orient,
                             datatree_init,
                             transformed_trees,
                             cell,
                             voxel_size,
                             ignore_axon,
                             normalized_axes,
                             normalizer_axis,
                             axial_vec,
                             azimu_vec,
                             polar_vec):
    # place the transformed_tree into the original data_tree structure
    # this required for the morph2voxel function.
    datatree_init['X'] = transformed_trees[:, 0, i_orient]
    datatree_init['Y'] = transformed_trees[:, 1, i_orient]
    datatree_init['Z'] = transformed_trees[:, 2, i_orient]
    # print(datatree_init[['X','Y','Z']][:10])
    morph2voxel(cell,
                voxel_size,
                ignore_axon=ignore_axon,
                calc_total_length=False,
                save_voxel_array=True,
                datatree_init=datatree_init,
                normalized_axes=normalized_axes,
                normalizer_axis=normalizer_axis,
                parallel_on=True,
                axial_angle=round(axial_vec[i_orient] * (180 / np.pi)),
                azimuth_angle=round(azimu_vec[i_orient] * (180 / np.pi)),
                polar_angle=round(polar_vec[i_orient] * (180 / np.pi)),
                suppress_output=True)


# generate two function for dissimilarity
# copied from https://matthew-brett.github.io/teaching/optimizing_space.html
def correl_mismatch(vector_1, vector_2):
    """Correlation coefficient between two vectors"""
    if len(vector_1) > 1:
        corrcoef = np.corrcoef(vector_1, vector_2)[
            0, 1]  # this index takes vector_1 as the template
    else:
        corrcoef = 0 # in this case we don't want to pick up the score.
        # if length of the vectors <= 1 it means that there is only 1 voxel used
        # A correlation coefficient cannot be computed!
    return corrcoef


def mean_abs_mismatch(vector_1, vector_2, normalize=False):
    """Mean absolute difference between vectors"""
    if normalize:
        vector_1 = vector_1 / np.nansum(vector_1)
        vector_2 = vector_2 / np.nansum(vector_2)
    return np.mean(np.abs(vector_1 - vector_2))


def calc_dissimilarity(cellAmpID_tuple,
                       voxel_array_extraction=
                       'xyz-normby-cortical_thickness0.05',
                       expected_file_number=1104,
                       save_file=True,
                       check_already_processed=True,
                       update_summary=True,
                       return_df=False):
    """Calculates dissimilarity between two cells.

    Requires the selection of a feature, process, and voxel_array extraction
    set.

    Warning needs adjustements to enable axon calculations. For now any thing
    above 20 voxels from origin is removed

    Args:
        - cellAmpID_tupple: tuple containing strings:
            cellAmpID_1 and cellAmpID_2: string, must be in the folder
            path_morph_analysis/__voxel_arrays__.
            The first cellAmpID will be used as templates; the non-transformed
            voxel_array will be used (axial:0, azimuth:0, polar:0).
        - voxel_array_extraction: string, exact match to the subdirectory in
            each cellAmpID. This is where the voxel_arrays are stored. The name
            describes if the morphology is normalized and what the voxel_size
            was.
        - expected_file_number: integer, used to check whether all files are
            present.
        - remove_empty_voxels: boolean, remove voxels that in both arrays are
            empty.
        - save_file: boolean, save the results in a .csv file.
        - check_already_processed: boolean, only process if the resulting file
            is not yet created.
        - update_summary: boolean, will gather best matching orientations per
            metric and update the summary.h5.
        - return_df: boolean, whether a master_df is provided back.
        """
    # unpack tuple
    cellAmpID_1 = cellAmpID_tuple[0]
    cellAmpID_2 = cellAmpID_tuple[1]
    # Save files in subfolders by the template cellAmpID. This is to avoid
    # too high of file number per directory.
    save_folder = Path(
        bcs_global.path_morph_analysis) / 'cell_matching' / \
                    voxel_array_extraction / str(cellAmpID_1)
    if not save_folder.is_dir():
        save_folder.mkdir()
    # do the same for summary_data
    save_folder_summary = Path(
        bcs_global.path_morph_analysis) / 'cell_matching' / \
                  voxel_array_extraction / 'summary_data' / str(cellAmpID_1)
    if not save_folder_summary.is_dir():
        save_folder_summary.mkdir()
    # make sure the combination has not made and save before
    save_filename = save_folder /\
                    (str(cellAmpID_1) + '_' + str(cellAmpID_2) + '.csv')
    if check_already_processed and save_file:
        # Commented below because better to check whether an summary was
        # calculated
        # if save_filename.is_file():
        #     print('Warning! ' + str(cellAmpID_1) + ' vs ' + str(
        #         cellAmpID_2) + 'is already calculated' +
        #           '\nNo further calculations were made for this couple')
        #     return
        summary_save_filename = save_folder_summary / \
                                (str(cellAmpID_1) + '_' + str(cellAmpID_2) +
                                 '.csv')
        if summary_save_filename.is_file():
            print('Warning! ' + str(cellAmpID_1) + ' vs ' + str(
                cellAmpID_2) + 'is already calculated' +
                  '\nNo further calculations were made for this couple')
            return
    # save paths and check for existence
    c1_path = Path(
        bcs_global.path_morph_analysis) / '__voxel_arrays__' / cellAmpID_1 / \
              voxel_array_extraction
    c2_path = Path(
        bcs_global.path_morph_analysis) / '__voxel_arrays__' / cellAmpID_2 / \
              voxel_array_extraction
    if not c1_path.is_dir():
        print('Warning! ' + str(c1_path) + ' cannot be found')
        return
    if not c2_path.is_dir():
        print('Warning! ' + str(c2_path) + ' cannot be found')
        return

    # check that there are the expected amount of .csv present
    if len(list(c1_path.glob('*.csv'))) != expected_file_number:
        print('Warning! Found ' + str(
            len(list(c1_path.glob('*.csv')))) + ' files, instead of ' +
              str(expected_file_number))
        return
    if len(list(c2_path.glob('*.csv'))) != expected_file_number:
        print('Warning! Found ' + str(
            len(list(c2_path.glob('*.csv')))) + ' files, instead of ' +
              str(expected_file_number))
        return
    print('Matching:... template: %s to match: %s' % (cellAmpID_1, cellAmpID_2))
    # get lib for orientations
    orientation_lib = pd.read_csv(
        str(Path(bcs_global.path_morph_analysis) / '__voxel_arrays__' /
            ('lib_' + voxel_array_extraction + '_' + str(expected_file_number)
             + '_orientations.csv')), index_col=[0])

    # voxel_size
    voxel_size_str = re.findall("\d+\.\d+", voxel_array_extraction)[0]

    # instantiate result dataframes
    indexes = pd.MultiIndex.from_frame(orientation_lib)
    metrics = ['corrcoef', 'distance']
    filters = ['_any_present', '_overlap_only']
    processes = ['_dendrites', '_basal', '_apical']
    colnames = [metric + filter_ + process for metric in metrics for filter_ in
                filters for process in processes]
    colnames.insert(0, 'fraction_overlap_basal')
    colnames.insert(0, 'fraction_overlap_apical')
    colnames.insert(0, 'fraction_overlap_dendrites')
    master_df = pd.DataFrame(data=None, index=indexes, columns=colnames)

    # initialize variables:
    extremes_voxel_space = 20
    id_range = 2 * extremes_voxel_space + 1
    total_orientations = expected_file_number

    # load reference voxel_arrays for first cellAmpID
    tempc1 = pd.read_csv(str(c1_path / (
                str(cellAmpID_1) + '_' + voxel_size_str + '_0.0_0.0_0.0_.csv')))

    # remove any voxel that exceeds the extreme_voxel_space boundaries
    tempc1 = tempc1.drop(np.array(tempc1.index)[np.any(
        np.abs(tempc1[['X', 'Y', 'Z']]) > extremes_voxel_space,
        axis=1)], axis=0)

    # generate arrays for length data
    dendrites_tempc1 = np.zeros((id_range, id_range, id_range))
    basal_tempc1 = np.zeros((id_range, id_range, id_range))
    apical_tempc1 = np.zeros((id_range, id_range, id_range))

    # convert the indexes for X Y Z such that they match with the length array
    tempc1[['X', 'Y', 'Z']] += extremes_voxel_space
    # fill out the arrays for length
    dendrites_tempc1[np.array(tempc1['X']).astype(int),
                     np.array(tempc1['Y']).astype(int),
                     np.array(tempc1['Z']).astype(int)] = \
        tempc1['sum_length_basal'] +  tempc1['sum_length_apical']

    basal_tempc1[np.array(tempc1['X']).astype(int),
                 np.array(tempc1['Y']).astype(int),
                 np.array(tempc1['Z']).astype(int)] = tempc1['sum_length_basal']

    apical_tempc1[np.array(tempc1['X']).astype(int),
                  np.array(tempc1['Y']).astype(int),
                  np.array(tempc1['Z']).astype(int)] = tempc1[
        'sum_length_apical']

    # reshape the arrays into vectors
    dendrites_tempc1 = np.reshape(dendrites_tempc1, (1, id_range ** 3))
    basal_tempc1 = np.reshape(basal_tempc1, (1, id_range ** 3))
    apical_tempc1 = np.reshape(apical_tempc1, (1, id_range ** 3))
    # generate masks
    dendrites_mask_tempc1 = dendrites_tempc1.astype(bool)
    basal_mask_tempc1 = basal_tempc1.astype(bool)
    apical_mask_tempc1 = apical_tempc1.astype(bool)

    # iterate through all orientations for  cellAmpID_2 and run comparisons with
    # cellAmpID_1 as template
    for i in np.arange(int(total_orientations), int(2 * total_orientations)):
        i_flip = int(2 * total_orientations - i - 1)
        # load reference voxel_arrays for first cellAmpID
        c2_orient_str = ('_'.join([str(cellAmpID_2),
                                   voxel_size_str,
                                   str(orientation_lib.axial.iloc[i]),
                                   str(orientation_lib.azimuth.iloc[i]),
                                   str(orientation_lib.polar.iloc[i]),
                                   '.csv']))

        # because the orientations are tested earlier we are sure visit all
        # orientations in both cells
        c2 = pd.read_csv(str(c2_path / c2_orient_str))

        # remove any voxel that exceeds the extreme_voxel_space boundaries
        c2 = c2.drop(np.array(c2.index)[np.any(
            np.abs(c2[['X', 'Y', 'Z']]) > extremes_voxel_space,
            axis=1)], axis=0)

        # generate arrays for length data
        dendrites_c2 = np.zeros((id_range, id_range, id_range))
        basal_c2 = np.zeros((id_range, id_range, id_range))
        apical_c2 = np.zeros((id_range, id_range, id_range))

        # convert the indexes for X Y Z such that they match with the length
        # array
        c2[['X', 'Y', 'Z']] += extremes_voxel_space
        # fill out the arrays for length
        dendrites_c2[np.array(c2['X']).astype(int),
                     np.array(c2['Y']).astype(int),
                     np.array(c2['Z']).astype(int)] = c2['sum_length_basal'] + \
                                                      c2['sum_length_apical']
        dendrites_c2_flip = np.flip(dendrites_c2, axis=0)
        basal_c2[np.array(c2['X']).astype(int),
                 np.array(c2['Y']).astype(int),
                 np.array(c2['Z']).astype(int)] = c2['sum_length_basal']
        basal_c2_flip = np.flip(basal_c2, axis=0)
        apical_c2[np.array(c2['X']).astype(int),
                  np.array(c2['Y']).astype(int),
                  np.array(c2['Z']).astype(int)] = c2['sum_length_apical']
        apical_c2_flip = np.flip(apical_c2, axis=0)

        # reshape the arrays into vectors
        dendrites_c2 = np.reshape(dendrites_c2, (1, id_range ** 3))
        dendrites_c2_flip = np.reshape(dendrites_c2_flip, (1, id_range ** 3))
        basal_c2 = np.reshape(basal_c2, (1, id_range ** 3))
        basal_c2_flip = np.reshape(basal_c2_flip, (1, id_range ** 3))
        apical_c2 = np.reshape(apical_c2, (1, id_range ** 3))
        apical_c2_flip = np.reshape(apical_c2_flip, (1, id_range ** 3))

        # for the dendrite (total)
        any_present_dendrites_mask = np.logical_or(dendrites_mask_tempc1,
                                                   dendrites_c2.astype(bool))
        any_present_dendrites_flip_mask = np.logical_or(dendrites_mask_tempc1,
                                                        dendrites_c2_flip.
                                                        astype(bool))
        overlap_only_dendrites_mask = np.logical_and(dendrites_mask_tempc1,
                                                     dendrites_c2.astype(bool))
        overlap_only_dendrites_flip_mask = np.logical_and(dendrites_mask_tempc1,
                                                          dendrites_c2_flip.
                                                          astype(bool))

        # for the apical dendrites only
        any_present_apical_mask = np.logical_or(apical_mask_tempc1,
                                                apical_c2.astype(bool))
        any_present_apical_flip_mask = np.logical_or(apical_mask_tempc1,
                                                     apical_c2_flip.
                                                     astype(bool))
        overlap_only_apical_mask = np.logical_and(apical_mask_tempc1,
                                                  apical_c2.astype(bool))
        overlap_only_apical_flip_mask = np.logical_and(apical_mask_tempc1,
                                                       apical_c2_flip.
                                                       astype(bool))

        # for the basal dendrites only
        any_present_basal_mask = np.logical_or(basal_mask_tempc1,
                                               basal_c2.astype(bool))
        any_present_basal_flip_mask = np.logical_or(basal_mask_tempc1,
                                                    basal_c2_flip.astype(bool))
        overlap_only_basal_mask = np.logical_and(basal_mask_tempc1,
                                                 basal_c2.astype(bool))
        overlap_only_basal_flip_mask = np.logical_and(basal_mask_tempc1,
                                                      basal_c2_flip.
                                                      astype(bool))

        # calculate fraction_overlap and put in the master dataframe
        master_df.fraction_overlap_dendrites.iat[i] = \
            np.sum(overlap_only_dendrites_mask) / np.sum(
                any_present_dendrites_mask)
        master_df.fraction_overlap_dendrites.iat[i_flip] = \
            np.sum(overlap_only_dendrites_flip_mask) / np.sum(
                any_present_dendrites_flip_mask)
        master_df.fraction_overlap_apical.iat[i] = \
            np.sum(overlap_only_apical_mask) / np.sum(any_present_apical_mask)
        master_df.fraction_overlap_apical.iat[i_flip] = \
            np.sum(overlap_only_apical_flip_mask) / np.sum(
                any_present_apical_flip_mask)
        master_df.fraction_overlap_basal.iat[i] = \
            np.sum(overlap_only_basal_mask) / np.sum(any_present_basal_mask)
        master_df.fraction_overlap_basal.iat[i_flip] = \
            np.sum(overlap_only_basal_flip_mask) / np.sum(
                any_present_basal_flip_mask)

        # calculate and populate the metrics
        # dendrites -> corrcoef -> any_present
        master_df.corrcoef_any_present_dendrites.iat[i] = \
            correl_mismatch(dendrites_tempc1[any_present_dendrites_mask],
                            dendrites_c2[any_present_dendrites_mask])

        # dendrites_flipped -> corrcoef -> any_present
        master_df.corrcoef_any_present_dendrites.iat[i_flip] = \
            correl_mismatch(dendrites_tempc1[any_present_dendrites_flip_mask],
                            dendrites_c2_flip[any_present_dendrites_flip_mask])

        # dendrites -> corrcoef -> overlap_only
        master_df.corrcoef_overlap_only_dendrites.iat[i] = \
            correl_mismatch(dendrites_tempc1[overlap_only_dendrites_mask],
                            dendrites_c2[overlap_only_dendrites_mask])

        # dendrites_flipped -> corrcoef -> overlap_only
        master_df.corrcoef_overlap_only_dendrites.iat[i_flip] = \
            correl_mismatch(dendrites_tempc1[overlap_only_dendrites_flip_mask],
                            dendrites_c2_flip[overlap_only_dendrites_flip_mask])

        # dendrites -> distance -> any_present
        master_df.distance_any_present_dendrites.iat[i] = \
            mean_abs_mismatch(dendrites_tempc1[any_present_dendrites_mask],
                              dendrites_c2[any_present_dendrites_mask])

        # dendrites_flipped -> distance -> any_present
        master_df.distance_any_present_dendrites.iat[i_flip] = \
            mean_abs_mismatch(dendrites_tempc1[any_present_dendrites_flip_mask],
                              dendrites_c2_flip[
                                  any_present_dendrites_flip_mask])

        # dendrites -> distance -> overlap_only
        master_df.distance_overlap_only_dendrites.iat[i] = \
            mean_abs_mismatch(dendrites_tempc1[overlap_only_dendrites_mask],
                              dendrites_c2[overlap_only_dendrites_mask])

        # dendrites_flipped -> distance -> overlap_only
        master_df.distance_overlap_only_dendrites.iat[i_flip] = \
            mean_abs_mismatch(
                dendrites_tempc1[overlap_only_dendrites_flip_mask],
                dendrites_c2_flip[overlap_only_dendrites_flip_mask])

        # apical -> corrcoef -> any_present
        master_df.corrcoef_any_present_apical.iat[i] = \
            correl_mismatch(apical_tempc1[any_present_apical_mask],
                            apical_c2[any_present_apical_mask])

        # apical_flipped -> corrcoef -> any_present
        master_df.corrcoef_any_present_apical.iat[i_flip] = \
            correl_mismatch(apical_tempc1[any_present_apical_flip_mask],
                            apical_c2_flip[any_present_apical_flip_mask])

        # apical -> corrcoef -> overlap_only
        master_df.corrcoef_overlap_only_apical.iat[i] = \
            correl_mismatch(apical_tempc1[overlap_only_apical_mask],
                            apical_c2[overlap_only_apical_mask])

        # apical_flipped -> corrcoef -> overlap_only
        master_df.corrcoef_overlap_only_apical.iat[i_flip] = \
            correl_mismatch(apical_tempc1[overlap_only_apical_flip_mask],
                            apical_c2_flip[overlap_only_apical_flip_mask])

        # apical -> distance -> any_present
        master_df.distance_any_present_apical.iat[i] = \
            mean_abs_mismatch(apical_tempc1[any_present_apical_mask],
                              apical_c2[any_present_apical_mask])

        # apical_flipped -> distance -> any_present
        master_df.distance_any_present_apical.iat[i_flip] = \
            mean_abs_mismatch(apical_tempc1[any_present_apical_flip_mask],
                              apical_c2_flip[any_present_apical_flip_mask])

        # apical -> distance -> overlap_only
        master_df.distance_overlap_only_apical.iat[i] = \
            mean_abs_mismatch(apical_tempc1[overlap_only_apical_mask],
                              apical_c2[overlap_only_apical_mask])

        # apical_flipped -> distance -> overlap_only
        master_df.distance_overlap_only_apical.iat[i_flip] = \
            mean_abs_mismatch(apical_tempc1[overlap_only_apical_flip_mask],
                              apical_c2_flip[overlap_only_apical_flip_mask])

        # basal -> corrcoef -> any_present
        master_df.corrcoef_any_present_basal.iat[i] = \
            correl_mismatch(basal_tempc1[any_present_basal_mask],
                            basal_c2[any_present_basal_mask])

        # basal_flipped -> corrcoef -> any_present
        master_df.corrcoef_any_present_basal.iat[i_flip] = \
            correl_mismatch(basal_tempc1[any_present_basal_flip_mask],
                            basal_c2_flip[any_present_basal_flip_mask])

        # basal -> corrcoef -> overlap_only
        master_df.corrcoef_overlap_only_basal.iat[i] = \
            correl_mismatch(basal_tempc1[overlap_only_basal_mask],
                            basal_c2[overlap_only_basal_mask])

        # basal_flipped -> corrcoef -> overlap_only
        master_df.corrcoef_overlap_only_basal.iat[i_flip] = \
            correl_mismatch(basal_tempc1[overlap_only_basal_flip_mask],
                            basal_c2_flip[overlap_only_basal_flip_mask])

        # basal -> distance -> any_present
        master_df.distance_any_present_basal.iat[i] = \
            mean_abs_mismatch(basal_tempc1[any_present_basal_mask],
                              basal_c2[any_present_basal_mask])

        # basal_flipped -> distance -> any_present
        master_df.distance_any_present_basal.iat[i_flip] = \
            mean_abs_mismatch(basal_tempc1[any_present_basal_flip_mask],
                              basal_c2_flip[any_present_basal_flip_mask])

        # basal -> distance -> overlap_only
        master_df.distance_overlap_only_basal.iat[i] = \
            mean_abs_mismatch(basal_tempc1[overlap_only_basal_mask],
                              basal_c2[overlap_only_basal_mask])

        # basal_flipped -> distance -> overlap_only
        master_df.distance_overlap_only_basal.iat[i_flip] = \
            mean_abs_mismatch(basal_tempc1[overlap_only_basal_flip_mask],
                              basal_c2_flip[overlap_only_basal_flip_mask])
    if save_file:
        master_df.to_csv(save_filename)

    summary_df = None
    if update_summary:
        overlap_col = {'dendrites': 'fraction_overlap_dendrites',
                       'apical': 'fraction_overlap_apical',
                       'basal': 'fraction_overlap_basal'}
        summary_save_filename = save_folder_summary / \
                                (str(cellAmpID_1) + '_' + str(cellAmpID_2) +
                                 '.csv')
        summary_df = pd.DataFrame(data=None,
                                  index=['score', 'flip', 'axial', 'azimuth',
                                         'polar'], columns=colnames)
        # correct all the overlap_only columns by the corresponding overlap_
        # fraction
        for col in colnames:
            # make a copy of the column to allow for manipulations
            master_series = master_df[col]
            if len(re.findall('overlap_only', col)) >= 1:
                # in case of overlap_only we want to correct for fraction of
                # overlap
                select_process = col.split('_')[-1]
                if len(re.findall('distance', col)) >= 1:
                    # is distance metric is used, then use division by fraction_
                    # overlap
                    master_series = master_series / master_df[
                        overlap_col[select_process]]
                else:
                    # is distance metric is used, then use multiply by fraction_
                    # overlap
                    master_series = master_series * master_df[
                        overlap_col[select_process]]
            if len(re.findall('distance', col)) >= 1:
                summary_df.at['score', col] = master_series.min()
            else:
                summary_df.at['score', col] = master_series.max()
            # identify the indexes with the best score
            ind_scores = \
                np.array(
                    np.where(master_series == summary_df.loc['score', col]))[0]
            # unpack into the summary_df
            if 'flip' in master_df.columns:
                index = pd.MultiIndex.from_tuples(list(
                    zip(master_df['flip'], master_df['axial'], master_df['azimuth'],
                        master_df['polar'])),
                    names=['flip', 'axial', 'azimuth', 'polar'])
            else:
                index = master_df.index
                # there are multiple levels present, however only the following
                # should be used
                index = pd.MultiIndex.from_tuples(list(
                    zip(index.get_level_values(level='flip'),
                        index.get_level_values(level='axial'),
                        index.get_level_values(level='azimuth'),
                        index.get_level_values(level='polar'))),
                    names=['flip', 'axial', 'azimuth', 'polar'])
            master_series.index = index
            try:
                (summary_df.at['flip', col],
                 summary_df.at['axial', col],
                 summary_df.at['azimuth', col],
                 summary_df.at['polar', col]) = \
                    master_series.index[ind_scores].sortlevel(
                        ['polar', 'azimuth', 'axial', 'flip'])[0][0]
            except (IndexError, ValueError):
                print('Index error %s  vs %s please check the output data'
                      % (cellAmpID_1, cellAmpID_2))
                summary_df.to_csv(str(summary_save_filename))
        summary_df.to_csv(str(summary_save_filename))

    else:
        if return_df and update_summary:
            return master_df, summary_df
        else:
            return master_df

def parallel_process_cell_matching(cellAmpIDs,
                                   temp_id=None,
                                   match_id=None,
                                   check_already_processed=True):
    """Match cells in parallel mode

    """
    # make list of tuples to iterate over
    if not isinstance(temp_id, np.ndarray):
        temp_id = np.repeat(cellAmpIDs, len(cellAmpIDs))
    if not isinstance(match_id, np.ndarray):
        match_id = np.tile(cellAmpIDs, len(cellAmpIDs))
    indexes = list(zip(temp_id, match_id))
    pool = multiprocessing.Pool()
    pool.map(partial(calc_dissimilarity,
                     voxel_array_extraction=
                     'xyz-normby-cortical_thickness0.05',
                     expected_file_number=1104,
                     save_file=True,
                     check_already_processed=check_already_processed,
                     update_summary=True,
                     return_df=False
                     ), indexes)
    pool.close()


def generate_summary_cell_matching(template_cellAmpIDs,
                                   match_cellAmpIDs,
                                   voxel_array_extraction=\
                                           'xyz-normby-cortical_thickness0.05'):
    """Generate summary hdf5 table for the cell matching data.

    Args:
        - template_cellAmpIDs: numpy array of cellAmpIDs that was used as
            template.
        - match_cellAmpIDs: numpy array of cellAmpIDs that was used as match
        - voxel_array_extraction: string describing the voxel_array extraction
        procedure
    """
    start_time = time.time()
    assert isinstance(template_cellAmpIDs, np.ndarray), \
        'cellAmpIDs is required to be a numpy array'
    assert isinstance(match_cellAmpIDs, np.ndarray), \
        'cellAmpIDs is required to be a numpy array'
    parent_directory = (Path(bcs_global.path_morph_analysis) /
                        'cell_matching' / voxel_array_extraction)

    # make sure that there are no duplicates of combos
    # make sure that there are no duplicates of combos
    combined = [a + '_' + b for a, b in zip(template_cellAmpIDs,
                                            match_cellAmpIDs)]
    combined_unique = np.unique(combined)
    temp_id = np.ones(len(combined_unique)).astype(str)
    match_id = np.ones(len(combined_unique)).astype(str)
    for i in range(len(combined_unique)):
        splits = combined_unique[i].split('_')
        temp_id[i] = splits[0]
        match_id[i] = splits[1]

    indexes = pd.MultiIndex.from_tuples(list(zip(temp_id, match_id)),
                                        names=['template', 'match'])
    files = [temp + '_' + match + '.csv' for temp, match in zip(temp_id,
                                                                match_id)]
    variables = ['fraction_overlap_dendrites',
                 'fraction_overlap_apical',
                 'fraction_overlap_basal',
                 'corrcoef_any_present_dendrites',
                 'corrcoef_any_present_basal',
                 'corrcoef_any_present_apical',
                 'corrcoef_overlap_only_dendrites',
                 'corrcoef_overlap_only_basal',
                 'corrcoef_overlap_only_apical',
                 'distance_any_present_dendrites',
                 'distance_any_present_basal',
                 'distance_any_present_apical',
                 'distance_overlap_only_dendrites',
                 'distance_overlap_only_basal',
                 'distance_overlap_only_apical']

    df_score = pd.DataFrame(data=np.zeros((len(indexes), len(variables))),
                            index=indexes, columns=variables)
    df_flip = pd.DataFrame(data=np.zeros((len(indexes), len(variables))),
                           index=indexes, columns=variables)
    df_axial = pd.DataFrame(data=np.zeros((len(indexes), len(variables))),
                            index=indexes, columns=variables)
    df_azimuth = pd.DataFrame(data=np.zeros((len(indexes), len(variables))),
                              index=indexes, columns=variables)
    df_polar = pd.DataFrame(data=np.zeros((len(indexes), len(variables))),
                            index=indexes, columns=variables)

    total = len(indexes)

    count = 0
    step_func = 0
    report_percentages = np.arange(0.1, 1.1, 0.05)
    for file, index in zip(files, indexes):
        data_ = pd.read_csv(str(parent_directory /
                                'summary_data' /
                                index[0] / file),
                            index_col=[0])
        if len(data_.columns) == 15:
            df_score.loc[index] = data_.loc['score']
            df_flip.loc[index] = data_.loc['flip']
            df_axial.loc[index] = data_.loc['axial']
            df_azimuth.loc[index] = data_.loc['azimuth']
            df_polar.loc[index] = data_.loc['polar']
            if count/total > report_percentages[step_func]:
                fraction_done = count/total
                elapsed_time = time.time() - start_time
                anticipated_time = ((elapsed_time / fraction_done) -
                                    elapsed_time) / 60
                elapsed_time = elapsed_time / 60
                print('Progress: %f percent of list. \n'
                      'Elapsed time: %f minutes \n'
                      'Anticipated time remaining: %f minutes.' %(
                    (fraction_done*100),
                      elapsed_time, anticipated_time))
                step_func += 1
        else:
            print('Warning %s file needs to be inspected, '
                  'not enough columns present' %(file))
        count += 1

    hdf = pd.HDFStore(str(parent_directory / 'match_summary.h5'),
                     complevel=9, complib='zlib')
    hdf.append('match_summary/score', df_score, format='Table',
               data_columns=variables,
               min_itemsize={'template': 50, 'match': 50})
    hdf.append('match_summary/flip', df_flip, format='Table',
               data_columns=variables,
               min_itemsize={'template': 50, 'match': 50})
    hdf.append('match_summary/axial', df_axial, format='Table',
               data_columns=variables,
               min_itemsize={'template': 50, 'match': 50})
    hdf.append('match_summary/azimuth', df_azimuth, format='Table',
               data_columns=variables,
               min_itemsize={'template': 50, 'match': 50})
    hdf.append('match_summary/polar', df_polar, format='Table',
               data_columns=variables,
               min_itemsize={'template': 50, 'match':  50})
    hdf.close()