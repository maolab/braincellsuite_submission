"""Extract features from SWC files.

Functions:
    - get_nodes: Get dataTree data of all nodes in dataTree.
    - findCorticalThickness: Get the Ycoordinate at X=0.
    - count_nodes_near_soma: Count nodes within radius from soma.
    - count_nodes_cortical_layers: Count nodes within cortical layers.
    - fieldspan_process: Measure fieldspan of processes parallel to pia.
    - sum_length_cortical_layers: Sum length dendrites within cortical layers.
    - calculate_overlap_basal_apical_dendrites: Calculates overlap of basal and
        apical dendrites within voxel and lamina.
TODO

"""

import numpy as np
import pandas as pd
import scipy
from brainCellSuite.analysis.morphometry.vector import findLin
from brainCellSuite.analysis.morphometry.calc_postFixDims import \
        extractContourData
from brainCellSuite.analysis.morphometry.calc_postFixDims import \
        extractsomaData
from brainCellSuite.analysis.morphometry.calc_postFixDims import \
        calc_somaCoM
from brainCellSuite._io.readSWCplus import get_dataTree
from brainCellSuite.analysis.morphometry.vector import distance
from brainCellSuite.analysis.morphometry.morph2voxel import \
    fill_points_on_line_3D, segmentplaneint
from brainCellSuite import Cell
from brainCellSuite._io.readDBCSV import get_databases
from brainCellSuite.helpers import fitEllipse
from brainCellSuite.helpers import ellipse_center
from brainCellSuite.helpers import ellipse_angle_of_rotation
from brainCellSuite.helpers import ellipse_axis_length
from brainCellSuite.analysis.morphometry.morph2voxel import morph2voxel
from pathlib import Path
from brainCellSuite import bcs_global

def get_nodes(swc_data):
    """Get dataTree data of all nodes in dataTree.

        All nodes (excluding start point) are coordinates for which
        P(ID) != ID(ID-1), and P(ID) != -1 (start point, in SWCp raw data this
        will be every start of the process, in processed data -1 will only be
        present in soma) and P(ID) != 1 (startpoint in SWC processed data this
        is the actual starting point, note that if using SWCp raw chances are
        that bifurcations right at thimport brainCellSuite as bcse first data point are not included in
        this count.)

        Args:
            - swc_data: numpy.ndarray swc data per row organized: ID,
            TYPE, X, Y, Z, R, P of only soma, contour,s and borders.

        Returns:
            - nodesData: coordinate data of the nodes

    """
    dataTree = get_dataTree(swc_data)
    IDs, Ps = (list(dataTree['ID']), list(dataTree['P']))
    nodes = list()
    for x in range(0, len(IDs)):
        if Ps[x] != IDs[x-1] and Ps[x] != -1 and Ps[x] != 1:
            nodes.append(IDs[x])
    nodesData = dataTree[np.isin(dataTree['ID'], nodes)]
    return nodesData


def findCorticalThickness(cell):
    """Get the Ycoordinate at X=0.

    The Ycoordinate at X=0 represents the cortical thickness.

    Args:
        - contourData: must contain PiaSurface key with data.

    Returns:
        - corticalThick: distance between corpus Callosum Surface
            (assuming this is set at X,Y = 0,0) and piaSurface (at intersect
            with X = 0).
    """
    contourData = extractContourData(cell.swc_data, cell.swc_meta)
    for x, y, i in zip(list(contourData['PiaSurface']['X']),
                       list(contourData['PiaSurface']['Y']),
                       list(np.arange(0,
                                      len(list(contourData['PiaSurface']['X'])))
                            )):
        if i != len(list(contourData['PiaSurface']['X'])) - 1 and \
                0 == np.sign(x) + np.sign(
            contourData['PiaSurface']['X'][i - 1]):
            if i != 0:
                x1 = contourData['PiaSurface']['X'][i - 1]
                y1 = contourData['PiaSurface']['Y'][i - 1]
                x2 = x
                y2 = y
                a, corticalThick = findLin([(x2, y2), (x1, y1)])
                return corticalThick
    return None


def count_nodes_near_soma(cell, process_TYPE, norm_distance):
    """Count nodes within radius from soma.

    Extract nodes from SWC of particular neuronal process type.
    Measure distance node to soma. Count nodes that fall within range from
    soma based on norm_distance boundaries.

    Args:
        - cell: instance of a cell object from class ini_cell
        - process_TYPE: list of neuronal process TYPE included in analysis
        - norm_distance: list of normalized distance(s) used to set boundary

    Returns:
        - node_count_near_soma: list of node counts based on norm_distance
            boundary
    """
    allNodesData = get_nodes(cell.swc_data)
    count = 1
    for i in process_TYPE:
        if count == 1:
            nodesData = allNodesData[allNodesData['TYPE'] == i]
        else:
            nodesData = np.append(nodesData,
                                  allNodesData[allNodesData['TYPE'] == i])
        count += 1
    contourData = extractContourData(cell.swc_data, cell.swc_meta)
    contourData = extractsomaData(cell.swc_data, cell.swc_meta, contourData)
    contourData = calc_somaCoM(contourData)
    corticalThickness = findCorticalThickness(cell)

    distanceNode2Soma = list()
    for x, y, z in zip(list(nodesData['X']),
                       list(nodesData['Y']),
                       list(nodesData['Z'])):
        p0 = [x, y, z]
        p1 = [contourData['somaCOM'][0],
              contourData['somaCOM'][1],
              contourData['somaCOM'][2]]
        distanceNode2Soma.append(distance(p0, p1))
    somaBinsRaw = np.array(norm_distance) * corticalThickness
    node_count_near_soma = list()
    for i in somaBinsRaw:
        node_count_near_soma.append(sum(distanceNode2Soma <= i))
    return node_count_near_soma


def count_nodes_cortical_layers(cell, process_TYPE, norm_cortical_bins):
    """Count nodes within cortical layers.

    Extract nodes from SWC_data and count number of nodes per cortical layer
    bin. Bins are calculated over cortical thickness (Y-axis). All nodes with
    XZ coordinates within Y range are included (so layers are defined by
    borders in Y dimension neglecting XZ).

    Args:
        - cell: instance of a cell object from class ini_cell
        - process_TYPE: list of neuronal process TYPE included in analysis
        - norm_cortical_bins: list of bins as fraction of cortical thickness.
            binborders are defined in list. Pia = 0, corpus callosum = 1self.
            eg. [0.15 0.3] means: taking any node within 0.15 and 0.3 fraction
            cortical thickness away from Pia. Please note: if 0 or 1 is
            included as borders the nodes lower then 0 and above 1 are also
            included since pia and corpus callosum are not straight.
            Nodes are included if they are exact Y as the lower border of the
            bin, but not upper border.
    Returns:
        - node_count_cortical_layers: list of node counts based on
            norm_cortical_bins.
    """
    # Note: in SWC files the corpus callosum is set at X,Y = 0,0, therefore
    # 1-norm_cortical_bins
    norm_cortical_bins = 1 - np.array(norm_cortical_bins)
    allNodesData = get_nodes(cell.swc_data)
    count = 1
    for i in process_TYPE:
        if count == 1:
            nodesData = allNodesData[allNodesData['TYPE'] == i]
        else:
            nodesData = np.append(nodesData,
                                  allNodesData[allNodesData['TYPE'] == i])
        count += 1
    corticalThickness = findCorticalThickness(cell)
    exact_cortical_bins = list(np.array(norm_cortical_bins) *
                               corticalThickness)
    exact_cortical_bins.sort(reverse=True)
    nodeYs = nodesData['Y']
    node_count_cortical_layers = list()
    for highBorder, lowBorder in zip(exact_cortical_bins[:-1],
                                     exact_cortical_bins[1:]):
        if highBorder == 1 and lowBorder == 0:
            node_count_cortical_layers.append(len(nodeYs))
        if highBorder == 1:
            node_count_cortical_layers.append(sum(nodeYs >= lowBorder))
        if lowBorder == 0:
            node_count_cortical_layers.append(sum(nodeYs < highBorder))
        else:
            node_count_cortical_layers.append(
                sum((nodeYs >= lowBorder) * (nodeYs < highBorder)))
    total_nodes = len(nodesData)
    return node_count_cortical_layers


def fieldspan_process(cell, process_TYPE, norm_cortical_bins):
    """Measure fieldspan of processes parallel to pia.

    Assumes the orientation of the SWC file is such that the X-axis is
    parallel to pia. X and Z coordinates of all tracing points are used after
    selection of the processes TYPE and range along the Y-axis in which the
    points are laying in. Uses euclidean distance between all points to find
    the max distance.

    Args:
        - cell: instance of a cell object from class ini_cell
        - process_TYPE: list of neuronal process TYPE included in analysis
        - norm_cortical_bins: list of bins as fraction of cortical thickness.
            binborders are defined in list. Pia = 0, corpus callosum = 1.
            eg. [0.15 0.3] means: taking any tracingpoint within 0.15 and 0.3
            fraction cortical thickness away from Pia. Please note: if 0 or 1
            is included as borders the tracing point lower then 0 and above 1
            are also included since pia and corpus callosum are not straight.
            Tracing points are included if they are exact Y as the lower
            border of the bin, but not upper border.

    Returns:
        - fieldspan: list of fieldspans based on norm_cortical_bins.
    """
    # Note: in SWC files the corpus callosum is set at X,Y = 0,0, therefore
    # 1-norm_cortical_bins
    norm_cortical_bins = 1 - np.array(norm_cortical_bins)
    corticalThickness = findCorticalThickness(cell)
    exact_cortical_bins = list(np.array(norm_cortical_bins) *
                               corticalThickness)
    exact_cortical_bins.sort(reverse=True)
    dataTrees = get_dataTree(cell.swc_data)

    count = 1
    for i in process_TYPE:
        if count == 1:
            dataTree = dataTrees[dataTrees['TYPE'] == i]
        else:
            dataTree = np.append(dataTree,
                                 dataTrees[dataTrees['TYPE'] == i])
        count += 1
    coordsAll = np.zeros((len(dataTree), 3))
    for i, x, z, y in zip(range(0, len(dataTree)),
                          dataTree['X'],
                          dataTree['Z'],
                          dataTree['Y']):
        coordsAll[i][0] = x
        coordsAll[i][1] = z
        coordsAll[i][2] = y
    fieldspan = list()
    for highBorder, lowBorder in zip(exact_cortical_bins[:-1],
                                     exact_cortical_bins[1:]):
        if highBorder == 1 and lowBorder == 0:
            fieldspan.append(scipy.spatial.distance.cdist(coordsAll[:, 0:2],
                                            coordsAll[:, 0:2],
                                            metric='euclidean').max())
        if highBorder == 1:
            coords = coordsAll[coordsAll[:, 2] >= lowBorder, 0:2]
            if len(coords) > 1:
                fieldspan.append(scipy.spatial.distance.cdist(coords,
                                                coords,
                                                metric='euclidean').max())
            else:
                fieldspan.append(0)
        if lowBorder == 0:
            coords = coordsAll[coordsAll[:, 2] < highBorder, 0:2]
            if len(coords) > 1:
                fieldspan.append(scipy.spatial.distance.cdist(coords,
                                                coords,
                                                metric='euclidean').max())
            else:
                fieldspan.append(0)
        else:
            coords = coordsAll[(coordsAll[:, 2] >= lowBorder) * \
                               (coordsAll[:, 2] < highBorder), 0:2]
            if len(coords) > 1:
                fieldspan.append(scipy.spatial.distance.cdist(coords,
                                                coords,
                                                metric='euclidean').max())
            else:
                fieldspan.append(0)
    return fieldspan


def sum_length_cortical_layers(cell, process_TYPE, norm_cortical_bins, calculate_cortical_thick=True):
    """Sum length dendrites within cortical layers.

    Args:
        - cell: instance of a cell object from class ini_cell
        - process_TYPE: list of neuronal process TYPE included in analysis
        - norm_cortical_bins: list of bins as fraction of cortical thickness.
            binborders are defined in list. Pia = 0, corpus callosum = 1.
            eg. [0.15 0.3] means: taking any tracingpoint within 0.15 and 0.3
            fraction cortical thickness away from Pia. Please note: if 0 or 1
            is included as borders the tracing point lower then 0 and above 1
            are also included since pia and corpus callosum are not straight.
            Tracing points are included if they are exact Y as the lower
            border of the bin, but not upper border.
        - calculate_cortical_thick: transforms the normalized cortical bins to
            the local cortical_thickness. Do not use if already the swc data
            has been normalized!

    Returns:
        -dendritic_length
    """
    if calculate_cortical_thick == True:
        # Note: in SWC files the corpus callosum is set at X,Y = 0,0, therefore
        # 1-norm_cortical_bins
        norm_cortical_bins = 1 - np.array(norm_cortical_bins)
        corticalThickness = findCorticalThickness(cell)
        exact_cortical_bins = list(np.array(norm_cortical_bins) *
                                   corticalThickness)
        exact_cortical_bins.sort(reverse=True)
    else:
        exact_cortical_bins = list(np.array(norm_cortical_bins))
        exact_cortical_bins.sort(reverse=True)

    dataTreesAll = get_dataTree(cell.swc_data)

    count = 1
    for i in process_TYPE:
        if count == 1:
            dataTrees = dataTreesAll[dataTreesAll['TYPE'] == i]
        else:
            dataTrees = np.append(dataTrees,
                                  dataTreesAll[dataTreesAll['TYPE'] == i])
        count += 1
    sum_length = list()
    for highBorder, lowBorder in zip(exact_cortical_bins[:-1],
                                     exact_cortical_bins[1:]):
        summed = 0
        if highBorder == 1 and lowBorder == 0 and \
                len(exact_cortical_bins == 2):
            dataTree = dataTrees
            for i in range(1, len(dataTree)):
                if 1 < dataTree['P'][i] == dataTree['ID'][i - 1]:
                    p0 = [dataTree['X'][i - 1],
                          dataTree['Y'][i - 1],
                          dataTree['Z'][i - 1]]
                    p1 = [dataTree['X'][i],
                          dataTree['Y'][i],
                          dataTree['Z'][i]]
                    summed += distance(p0, p1)
                if 1 < dataTree['P'][i] != dataTree['ID'][i - 1]:
                    try:
                        x = dataTree['ID'][dataTree['P'][i] == dataTree['ID']]
                        p0 = [dataTree['X'][dataTree['ID'] == x],
                              dataTree['Y'][dataTree['ID'] == x],
                              dataTree['Z'][dataTree['ID'] == x]]
                        p1 = [dataTree['X'][i],
                              dataTree['Y'][i],
                              dataTree['Z'][i]]
                        summed += distance(p0, p1)
                    except:
                        pass
            sum_length.append(summed)
        if highBorder == 1 and not lowBorder == 0:
            dataTree = dataTrees[dataTrees['Y'] >= lowBorder]
            if len(dataTree) > 1:
                for i in range(1, len(dataTree)):
                    if 1 < dataTree['P'][i] == dataTree['ID'][i - 1]:
                        p0 = [dataTree['X'][i - 1],
                              dataTree['Y'][i - 1],
                              dataTree['Z'][i - 1]]
                        p1 = [dataTree['X'][i],
                              dataTree['Y'][i],
                              dataTree['Z'][i]]
                        summed += distance(p0, p1)
                    if 1 < dataTree['P'][i] != dataTree['ID'][i - 1]:
                        try:
                            x = (dataTree['ID'][dataTree['P'][i] ==
                                                dataTree['ID']])
                            p0 = [dataTree['X'][dataTree['ID'] == x],
                                  dataTree['Y'][dataTree['ID'] == x],
                                  dataTree['Z'][dataTree['ID'] == x]]
                            p1 = [dataTree['X'][i],
                                  dataTree['Y'][i],
                                  dataTree['Z'][i]]
                            summed += distance(p0, p1)
                        except:
                            pass
                sum_length.append(summed)
            else:
                sum_length.append(0)
        if lowBorder == 0 and not highBorder == 1:
            dataTree = dataTrees[dataTrees['Y'] < highBorder]
            if len(dataTree) > 1:
                for i in range(1, len(dataTree)):
                    if 1 < dataTree['P'][i] == dataTree['ID'][i - 1]:
                        p0 = [dataTree['X'][i - 1],
                              dataTree['Y'][i - 1],
                              dataTree['Z'][i - 1]]
                        p1 = [dataTree['X'][i],
                              dataTree['Y'][i],
                              dataTree['Z'][i]]
                        summed += distance(p0, p1)
                    if 1 < dataTree['P'][i] != dataTree['ID'][i - 1]:
                        try:
                            x = (dataTree['ID'][dataTree['P'][i] ==
                                                dataTree['ID']])
                            p0 = [dataTree['X'][dataTree['ID'] == x],
                                  dataTree['Y'][dataTree['ID'] == x],
                                  dataTree['Z'][dataTree['ID'] == x]]
                            p1 = [dataTree['X'][i],
                                  dataTree['Y'][i],
                                  dataTree['Z'][i]]
                            summed += distance(p0, p1)
                        except:
                            pass
                sum_length.append(summed)
            else:
                sum_length.append(0)
        if not lowBorder == 0 and not highBorder == 1:
            dataTree = dataTrees[(dataTrees['Y'] >= lowBorder) *
                                 (dataTrees['Y'] < highBorder)]
            if len(dataTree) > 1:
                for i in range(1, len(dataTree)):
                    if 1 < dataTree['P'][i] == dataTree['ID'][i - 1]:
                        p0 = [dataTree['X'][i - 1],
                              dataTree['Y'][i - 1],
                              dataTree['Z'][i - 1]]
                        p1 = [dataTree['X'][i],
                              dataTree['Y'][i],
                              dataTree['Z'][i]]
                        summed += distance(p0, p1)
                    if 1 < dataTree['P'][i] != dataTree['ID'][i - 1]:
                        try:
                            x = (dataTree['ID'][dataTree['P'][i] ==
                                                dataTree['ID']])
                            p0 = [dataTree['X'][dataTree['ID'] == x],
                                  dataTree['Y'][dataTree['ID'] == x],
                                  dataTree['Y'][dataTree['ID'] == x],
                                  dataTree['Z'][dataTree['ID'] == x]]
                            p1 = [dataTree['X'][i],
                                  dataTree['Y'][i],
                                  dataTree['Z'][i]]
                            summed += distance(p0, p1)
                        except:
                            pass
                sum_length.append(summed)
            else:
                sum_length.append(0)
    return sum_length


def calculate_soma_diameter(cell, fill_distance=1):
    """Calculates the diameter of the soma.

    Args:
        cell: instance of object Cell
        fill_distance: minimum distance to use for filling the soma contour
            annotation.
    """
    # first extract soma data
    contourData = extractContourData(cell.swc_data, cell.swc_meta)
    contourData = extractsomaData(cell.swc_data, cell.swc_meta, contourData)
    contourData = calc_somaCoM(contourData)
    soma_data = cell.swc_data[np.logical_and(cell.swc_data['TYPE'] == 1,
                                             cell.swc_data['P'] >= 1)]
    # insert points such that there is equal spacing between each point
    soma_df = pd.DataFrame(
        data=soma_data[['TYPE', 'X', 'Y', 'Z', 'R', 'P']],
        index=soma_data['ID'])
    soma_highres = soma_df[['X', 'Y', 'Z']].iloc[0]
    # connect first point with the last point
    first_ID = soma_df.iloc[0].name
    soma_df.P.at[first_ID] = soma_df.iloc[-1].name
    for i in soma_df.index:
        p = int(soma_df.loc[i, 'P'])
        p0 = np.array([soma_df.loc[i][['X', 'Y', 'Z']]])
        p1 = np.array([soma_df.loc[p][['X', 'Y', 'Z']]])
        fill_points = fill_points_on_line_3D(p0, p1, fill_distance)
        if len(fill_points) >= 1:
            soma_highres = np.vstack((soma_highres,
                                          fill_points,
                                          p1))
    # calculate distance ie radius between center of mass and each annotated
    # spot
    radius_soma = list()
    for x, y, z in zip(list(soma_highres[:, 0]),
                       list(soma_highres[:, 1]),
                       list(soma_highres[:, 2])):
        p0 = [x, y, z]
        p1 = [contourData['somaCOM'][0],
              contourData['somaCOM'][1],
              contourData['somaCOM'][2]]
        radius_soma.append(distance(p0, p1))
    # calculate the soma radius
    radius = np.nanmean(radius_soma)
    soma_diameter = 2 * radius
    # calculate the somatic length in Y direction and width in X direction
    # since the morphologies are oriented with pia > corpus callosum over Y
    # fit
    height = max(soma_highres[:, 1]) - min(soma_highres[:, 1])
    width = max(soma_highres[:, 0]) - min(soma_highres[:, 0])
    form_factor_simple = np.log2(height/width)
    # fit ellipisis to the soma to get a finer calculation of form_factor,
    # including an estimation of the soma angle.
    a = fitEllipse(soma_highres[:, 0], soma_highres[:, 1])
    center = ellipse_center(a)
    phi = ellipse_angle_of_rotation(a)
    width_el, height_el = ellipse_axis_length(a)
    form_factor = np.log2(height_el/width_el)
    angle = phi * 180 / np.pi - 90

    return (soma_diameter, angle, form_factor, form_factor_simple, center,
    width, height, width_el, height_el)


def calculate_somata(cellAmpIDs=None):
    """
    Args:
        cellAmpIDs: array of strings reflecting the cellAmpID.

    Returns:
        soma_df: dataframe containing features of the soma
    """
    # read databases
    dbs = get_databases()
    if not cellAmpIDs:
        cellAmpIDs = dbs['cell_df'][
            np.invert(dbs['cell_df'].swcFilename.isna())].index.tolist()
    soma_df = pd.DataFrame(data=None, columns=['soma_diameter',
                                               'soma_angle',
                                               'soma_form_factor',
                                               'soma_form_factor_simple',
                                               'soma_ellipse_center'],
                               index=cellAmpIDs)
    for cellAmpID in cellAmpIDs:
        cell = Cell(cellAmpID)
        (diameter, angle,
         form_factor,
         form_factor_simple,
         center) = calculate_soma_diameter(cell)
        soma_df.soma_diameter.loc[cellAmpID] = diameter
        soma_df.soma_angle.loc[cellAmpID] = angle
        soma_df.soma_form_factor.loc[cellAmpID] = form_factor
        soma_df.soma_form_factor_simple.loc[cellAmpID] = form_factor_simple
        soma_df.soma_ellipse_center.loc[cellAmpID] = center

    soma_df = soma_df.merge(dbs['cell_df'][['somaCortexPosition', 'cellType']],
                            left_index=True,
                            right_index=True)
    return soma_df


def calculate_overlap_basal_apical_dendrites(cellAmpIDs, voxel_size):
    """Calculates overlap of basal and apical dendrites within voxel and lamina.

    Requires a voxel_size to first calculate 3D dendrite density voxel space.
    The voxel_size will also be used as the height of the lamina to calculate
    overlap in laminar orientation.

    Args:
        cellAmpIDs: string, unique identifier of experiment.
        voxel_size: float in µm, determines size of voxel for 3D dendrite
            density voxel space.

    Returns:
        overlap_df: pandas dataframe containing all cellAmpIDs with their
            overlap in terms of voxel and lamina.
    """
    if not cellAmpIDs:
        dbs = get_databases()
        cellAmpIDs = dbs['cell_df'][
            np.invert(dbs['cell_df'].swcFilename.isna())].index.tolist()
    overlap_df = pd.DataFrame(None, index=cellAmpIDs,
                              columns=['overlap_Y', 'overlap_voxel'])
    save_file_root = Path(bcs_global.path_morph_analysis) / '__voxel_arrays__'
    rot_angle_1 = 0
    rot_angle_2 = 0
    for cellAmpID in cellAmpIDs:
        # first see if the voxel_array has already been calculated. This will
        # reduce the calculation time.
        cell_path = save_file_root / str(cellAmpID)
        if not cell_path.exists():
            cell_path.mkdir(parents=True)
        cell_path_voxel = cell_path / (str(voxel_size) + 'um')
        if not cell_path_voxel.exists():
            cell_path_voxel.mkdir()
        save_filename = cell_path_voxel / ('_'.join([str(cellAmpID),
                                                     str(voxel_size),
                                                     str(rot_angle_1),
                                                     str(rot_angle_2),
                                                     '.csv']))
        if not save_filename.exists():
            cell = Cell(cellAmpID)
            _, voxel_array, _, _ = morph2voxel(cell,
                                               voxel_size=voxel_size,
                                               save_voxel_array=True)
            voxel_array = pd.DataFrame(voxel_array)
        else:
            voxel_array = pd.read_csv(save_filename)

        basal_Y = voxel_array['Y'][voxel_array['sum_length_basal'] > 0]
        apical_Y = voxel_array['Y'][voxel_array['sum_length_apical'] > 0]
        Y_same = list(
            set(basal_Y.unique()).intersection(set(apical_Y.unique())))
        total_Y = np.unique(
            np.hstack([apical_Y.unique(), basal_Y.unique()]))
        overlap_df.overlap_Y.at[cellAmpID] = len(Y_same) / len(total_Y)

        voxel_same = np.sum(
            np.logical_and(voxel_array.sum_length_apical > 0,
                           voxel_array.sum_length_basal > 0))
        total_voxel = np.sum(
            np.logical_or(voxel_array.sum_length_apical > 0,
                          voxel_array.sum_length_basal > 0))
        overlap_df.overlap_voxel.at[cellAmpID] = voxel_same / total_voxel
    return overlap_df


def sum_length_width(cell, process_TYPE, x_coordinate_division=0):
    """Sum length dendrites within cortical layers.

    Args:
        - cell: instance of a cell object from class ini_cell
        - process_TYPE: list of neuronal process TYPE included in analysis
        - x_coordinate_division: float, border in X-axis that divides the
        morphology in a left/right part, in each part the summed length of
        the selected process is calculated.

    Returns:
        -dendritic_length
    """
    # Note: in SWC files the corpus callosum is set at X,Y = 0,0, therefore
    # 1-norm_cortical_bins
    dataTreesAll = get_dataTree(cell.swc_data)

    count = 1
    for i in process_TYPE:
        if count == 1:
            dataTrees = dataTreesAll[dataTreesAll['TYPE'] == i]
        else:
            dataTrees = np.append(dataTrees,
                                  dataTreesAll[dataTreesAll['TYPE'] == i])
        count += 1
    sum_length = dict()
    dataTree_ = dict()
    dataTree_['posterior'] = dataTrees[dataTrees['X'] > x_coordinate_division]
    dataTree_['anterior'] = dataTrees[dataTrees['X'] <= x_coordinate_division]
    for loc, dataTree in dataTree_.items():
        if len(dataTree) > 1:
            summed = 0
            for i in range(1, len(dataTree)):
                if 1 < dataTree['P'][i] == dataTree['ID'][i - 1]:
                    p0 = [dataTree['X'][i - 1],
                          dataTree['Y'][i - 1],
                          dataTree['Z'][i - 1]]
                    p1 = [dataTree['X'][i],
                          dataTree['Y'][i],
                          dataTree['Z'][i]]
                    distance_ = distance(p0, p1)
                    if not np.isnan(distance_):
                        summed += distance_
                if np.logical_and(1 < dataTree['P'][i],  dataTree['P'][i] not in dataTree['ID']):
                    # the datapoint is on the otherside, so the p0 needs to be inferred
                    p0_real = [dataTrees['X'][dataTrees['ID'] == dataTree['P'][i]],
                          dataTrees['Y'][dataTrees['ID'] == dataTree['P'][i]],
                          dataTrees['Z'][dataTrees['ID'] == dataTree['P'][i]]]
                    p1 = [dataTree['X'][i],
                          dataTree['Y'][i],
                          dataTree['Z'][i]]
                    plane_ = np.array([[0, p0_real[1], p0_real[2]],
                                        [0, p0_real[1], p1[2]],
                                       [0, p1[1], p0_real[2]],
                                       [0, p1[1], p1[2]]],dtype=object)
                    p0_intersect, _, _, _ = segmentplaneint(plane_, np.array([[p0_real],[p1]],dtype=object))
                    if np.any(np.isnan(p0_intersect)):
                        plane_ = np.array([[0, p0_real[1], p0_real[2]+10],
                                        [0, p0_real[1]+10, p1[2]],
                                       [0, p1[1]+10, p0_real[2]],
                                       [0, p1[1], p1[2]+10]],dtype=object)
                    p0_intersect, _, _, _ = segmentplaneint(plane_, np.array([[p0_real],[p1]]))
                    distance_ = distance(p0_intersect[0], p1)
                    if not np.isnan(distance_):
                        summed += distance_
                if 1 < dataTree['P'][i] != dataTree['ID'][i - 1]:
                    try:
                        x = (dataTree['ID'][dataTree['P'][i] ==
                                            dataTree['ID']])
                        p0 = [dataTree['X'][dataTree['ID'] == x],
                              dataTree['Y'][dataTree['ID'] == x],
                              dataTree['Z'][dataTree['ID'] == x]]
                        p1 = [dataTree['X'][i],
                              dataTree['Y'][i],
                              dataTree['Z'][i]]
                        distance_ = distance(p0, p1)
                        if not np.isnan(distance_):
                            summed += distance_
                    except:
                        pass
            sum_length[loc] = summed
        else:
            sum_length[loc] = 0
    return sum_length


def sum_length_height(cell, process_TYPE, y_coordinate_division=0):
    """Sum length dendrites within cortical layers.

    Args:
        - cell: instance of a cell object from class ini_cell
        - process_TYPE: list of neuronal process TYPE included in analysis
        - y_coordinate_division: float, border in X-axis that divides the
        morphology in a left/right part, in each part the summed length of
        the selected process is calculated. The soma is located at  0
    Returns:
        -dendritic_length
    """
    # Note: in SWC files the corpus callosum is set at X,Y = 0,0, therefore
    # 1-norm_cortical_bins
    dataTreesAll = get_dataTree(cell.swc_data)

    # translocate the soma to Y=0
    soma_origin = cell.swc_data['Y'][np.logical_and(cell.swc_data['TYPE'] == 1, cell.swc_data['P'] == -1)]
    dataTreesAll['Y'] = dataTreesAll['Y'] - soma_origin

    count = 1
    for i in process_TYPE:
        if count == 1:
            dataTrees = dataTreesAll[dataTreesAll['TYPE'] == i]
        else:
            dataTrees = np.append(dataTrees,
                                  dataTreesAll[dataTreesAll['TYPE'] == i])
        count += 1
    sum_length = dict()
    dataTree_ = dict()
    dataTree_['above'] = dataTrees[dataTrees['Y'] > y_coordinate_division]
    dataTree_['below'] = dataTrees[dataTrees['Y'] <= y_coordinate_division]
    for loc, dataTree in dataTree_.items():
        if len(dataTree) > 1:
            summed = 0
            for i in range(1, len(dataTree)):
                if 1 < dataTree['P'][i] == dataTree['ID'][i - 1]:
                    p0 = [dataTree['X'][i - 1],
                          dataTree['Y'][i - 1],
                          dataTree['Z'][i - 1]]
                    p1 = [dataTree['X'][i],
                          dataTree['Y'][i],
                          dataTree['Z'][i]]
                    distance_ = distance(p0, p1)
                    if not np.isnan(distance_):
                        summed += distance_
                if np.logical_and(1 < dataTree['P'][i],  dataTree['P'][i] not in dataTree['ID']):
                    # the datapoint is on the otherside, so the p0 needs to be inferred
                    p0_real = [dataTrees['X'][dataTrees['ID'] == dataTree['P'][i]],
                          dataTrees['Y'][dataTrees['ID'] == dataTree['P'][i]],
                          dataTrees['Z'][dataTrees['ID'] == dataTree['P'][i]]]
                    p1 = [dataTree['X'][i],
                          dataTree['Y'][i],
                          dataTree['Z'][i]]
                    plane_ = np.array([[0, p0_real[1], p0_real[2]],
                                        [0, p0_real[1], p1[2]],
                                       [0, p1[1], p0_real[2]],
                                       [0, p1[1], p1[2]]],dtype=object)
                    p0_intersect, _, _, _ = segmentplaneint(plane_, np.array([[p0_real],[p1]],dtype=object))
                    if np.any(np.isnan(p0_intersect)):
                        plane_ = np.array([[0, p0_real[1], p0_real[2]+10],
                                        [0, p0_real[1]+10, p1[2]],
                                       [0, p1[1]+10, p0_real[2]],
                                       [0, p1[1], p1[2]+10]],dtype=object)
                    p0_intersect, _, _, _ = segmentplaneint(plane_, np.array([[p0_real],[p1]]))
                    distance_ = distance(p0_intersect[0], p1)
                    if not np.isnan(distance_):
                        summed += distance_
                if 1 < dataTree['P'][i] != dataTree['ID'][i - 1]:
                    try:
                        x = (dataTree['ID'][dataTree['P'][i] ==
                                            dataTree['ID']])
                        p0 = [dataTree['X'][dataTree['ID'] == x],
                              dataTree['Y'][dataTree['ID'] == x],
                              dataTree['Z'][dataTree['ID'] == x]]
                        p1 = [dataTree['X'][i],
                              dataTree['Y'][i],
                              dataTree['Z'][i]]
                        distance_ = distance(p0, p1)
                        if not np.isnan(distance_):
                            summed += distance_
                    except:
                        pass
            sum_length[loc] = summed
        else:
            sum_length[loc] = 0
    return sum_length