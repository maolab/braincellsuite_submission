"""Calculate electric features.

All functions related to calculating electrical feature of electrophysiology
traces are defined here. Apart from execution functions, functions take
numpy arrays of acquired and stimulation data.

Functions:
- Helper functions:
    - get_rms: Calculates root-mean-squared of trace.
    - get_rs: Calculate series resistance from V-clamp trace.
    - get_rm: Calculate membrane resistance from V-clamp trace.
    - get_vm: Calculates membrane voltage of trace.
    - get_vsag_fraction: Calculates relative sag voltage to steady-state.
    - get_mts: Calculates membrane time constant.
    - get_mts_ini: Calculates membrane time constant during rising phase.
    - filter_bessel: filters signal based on Bessel.
    - get_spike_location: Determines spike locations in trace.
    - get_spike_features: Extracts features for the spikes in the trace.
    - get_spike_train_features: Calculates features related to spike trains.
    - get_vsag_fraction: Calculates relative sag voltage to steady-state.
    - filter_by_criteria: Filters sweep table by patch and noise rms thresholds
                          for specific parameters.
    - fill_appending_table: Calculates median and fill both median and sweep
                            ref to dataframes.
    - find_sweep_bool: Finds the sweep name that is representing >= 50% of the
                       column.
    - find_sweep_extreme: Finds the sweep name that is associated with the
                          median of the column.
    - find_sweep_median: Finds the sweep name that is associated with the
                         median of the column.
    - add_qc_features_sweep_table: Adds calculated features to sweep_table.
    - isolate_action_potential: Isolates action potential trace from sweep.
    - select_sweeps: Selection of sweeps based on stimulus criteria.
    - gen_drug_conditions: Names and subdivided sweeps in drug conditions.
    - remove_depol_block: Removes sweeps that show depolarization block.
    - measure_bursts: Measures the ratio of cv long and short ISI and initial
    bursting.


- Execute functions:
    - calculate_features: sweep-wise calculation of features.
    - generate_cellAmpID_feature_table: Generates a feature_table based on all
                                        sweep_feature_tables of cells.

Calculation of features:
    -- subthreshold --
    For each sweep
    - resting membrane potential (mV)
    - membrane/input resistance (MOhm)
    - series resistance (MOhm)
    - membrane time constant (ms)
    - vsag fraction (% sag voltage to steady-state.
    - vsag time point peak (ms)
    - vsag minimum membrane potential (mV)
    - ramp rate till ap (mV/s, linear fit to ramp untill reaching ap threshold)
    - ramp tau till ap (ms, exponential fit to ramp untill reaching ap
    threshold)
    -- spike --
    For each sweep above 0pA, calculate AP kinetics for ap_1, ap_2, ap_3
    - ap height (mV; delta mV treshold to peak)
    - ap width (ms; at full-width at half-max peak of spike)
    - ap threshold (mV, at 5% of max derivative rise)
    - ap through (mV, minimum Vm between action potentials)
    - rheobase (pA, minimum pA required for eliciting AP
    - ap relative time spend through (ratio timing through vs isi ap)
    - ap fast through (mV, fast through Vm - ap threshold)
    - ap slow through (mV, slow through Vm - ap threshold)
    - ap max upstroke rate (mv/ms)
    - ap max downstroke rate (mV/ms)
    - ap upstroke2downstroke ratio (upstroke / downstroke)
    -- spike train --
    for each sweep above 0pA
    - first ap ISI: interspike interval between first two action potentials
                    (ms).
    - average ap ISI: mean interspike interval of all spikes (ms).
    - coefficient of variation ap ISI: coefficient of variation of the
                                    interspike interval (fraction sd/mean).
    - average ap firing rate: mean firing rate of action potentials, trace
                           length /number of action potentials.
    - ap adaptation index: rate at which action potentials firing speeds up or
                           slows down.
    - ap fire current rate: rate at which action potentional firing increases
                    per increasein injected current (ap/pA).
                    Linear fit to F/I plot.
    - ap latency: time between start of trace and the first action potential.
    - delay: true/false, true if latency is longer than of average
             isi. Or fraction of sweeps that have delay.
    - burst: true/false, true if first two ISIs are less or equal to
             5 ms. Or fraction of sweeps that have burst.
    - pause: True/False, True if median isi is more than 3 times the duration of
             the ISIs immediately prior and after. Or fraction of sweeps that
             have pause in the spiking
    - ap desensitization height rate: ratio between ap height first and last ap.
    - ap desensitization width rate: ratio between ap width first and last ap.
    -ramp_burst: calculated from ramp true/false, true if first two ISIs are
             less or equal to5 ms. Or fraction of sweeps that have burst.
    -ramp_ap_threshold: calculated from the ramp (mV, at 5% of max derivative
                        rise)
"""

import numpy as np
import pandas as pd
import os
import re
import datetime
from scipy.signal import iirfilter, filtfilt, find_peaks
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from brainCellSuite._io.xsg2NWB import check_columns_dataframe
from brainCellSuite._io.readDBCSV import readDBCSV
from brainCellSuite import bcs_global


def fit_2lin(x, a, b):
    return a * x + b


def fit_2lin_fixb_null(x, a):
    return a * x


def fit_2expPos(x, a, b, c):
    return a * np.exp(x / b) + c


def fit_2exp(x, a, b, c):
    return a * np.exp(-x / b) + c


def get_rms(trace, start=0, end=-1, correct_for_vm=True):
    """Calculates root-mean-squared of trace.

    Compute RMS during a given period.

    Args:
        - trace: numpy array
        - start: datapoint startpoint to be included in the calculation
        - end: first datapoint endpoint not to included in calculation
        - correct_for_vm: True: corrects trace for mean membrane potential

    Returns:
        - rms: calculated root-mean-squared
    """
    trace_chop = trace[int(start):int(end)]
    if correct_for_vm:
        trace_chop = trace_chop - np.mean(trace_chop)
    rms = np.sqrt(np.mean(trace_chop ** 2))
    return rms


def get_rs(trace, start=4000, end=4500, voltage_step=-2.5):
    """Calculate series resistance from V-clamp trace.

    Calculates the series resistance from V-clamp trace with a voltage step
    commando. Rs = Vstep/Iin, where Rs (series resistance), Vstep (voltage step
    commando), and Iin (initial transient current upon voltage step).

    Args:
        - trace: numpy array
        - start: datapoint startpoint of the voltage step.
        - end: first datapoint endpoint of the voltage step.
        - voltage_step: amplitude of the given voltage step (in mV).

    Returns:
        - series resistance: float, MOhm.
    """
    trace_chop = trace[int(start):int(end)]
    trace_prechop = trace[int(start) - 50:int(start)]
    if np.isnan(voltage_step):
        voltage_step = -2.5
    if voltage_step != 0:
        if voltage_step < 0:
            Iin = abs(np.min(trace_chop) - np.mean(trace_prechop))
        elif voltage_step > 0:
            Iin = abs(np.max(trace_chop) - np.mean(trace_prechop))
        rs = ((abs(voltage_step) * 10 ** -3) / (
                    Iin * 10 ** -12)) * 10 ** -6  # in MOhm
        if np.std(trace_prechop) > Iin:
            rs = np.nan
    else:
        rs = np.nan
    return rs


def get_rm(trace, start=4000, end=4500, voltage_step=-2.5):
    """Calculate membrane resistance from V-clamp trace.

    Calculates the series resistance from V-clamp trace with a voltage step
    commando. Rm = Vstep/Iss, where Rm (membrane resistance), Vstep (voltage
    step commando), and Iss (steady-state current during voltage step).

    Args:
        - trace: numpy array
        - start: datapoint startpoint of the voltage step.
        - end: first datapoint endpoint of the voltage step.
        - voltage_step: amplitude of the given voltage step (in mV).
    """
    trace_chop = trace[int(start):int(end)]
    trace_steadychop = trace[int(start) - 50:int(start)]
    Iss = 0
    if np.isnan(voltage_step):
        voltage_step = -2.5
    if voltage_step != 0:
        if voltage_step < 0:
            Iss = abs(np.mean(trace_chop) - np.mean(trace_steadychop))
        elif voltage_step > 0:
            Iss = abs(np.max(trace_chop) - np.mean(trace_steadychop))
        rm = abs(
            (voltage_step * 10 ** -3) / (Iss * 10 ** -12)) * 10 ** -6  # in MOhm
        if np.std(trace_steadychop) > Iss:
            rm = np.nan
    else:
        rm = np.nan
    return rm


def get_vm(trace, start=0, end=-1):
    """Calculates membrane voltage of trace.

    Computate between start and end points in trace

    Args:
        - trace: numpy array
        - start: datapoint startpoint to be included in the calculation
        - end: first datapoint endpoint not to included in calculation

    Returns:
        - vm: calculated membrane voltage
    """
    vm = np.mean(trace[int(start):int(end)])
    return vm


def get_mts(trace, rmp=None, start=None, samplerate=10000):
    """Calculates membrane time constant.

    Fits a single exponential to the decaying phase of a hyperpolarizing
    currentstep to calculate membrane time constant.

    Args:
        - trace: numpy array
        - rmp:  resting membrane potential
        - start: datapoint startpoint to be included in the calculation
        - samplerate: samplerate of data acquisition for trace

    Returns:
        - mts: membrane time constant in ms
    """
    trace_to_fit = trace[int(start): -1]
    loc_rmp = np.where(trace_to_fit >= rmp)[0]
    if len(loc_rmp) > 0:
        # in case the trace comes back to rmp cut of the trace there
        trace_to_fit = trace_to_fit[:loc_rmp[0]]
        if len(trace_to_fit) == 0:
            # make sure that empty traces or non returning locations are avoided
            mts = np.nan
            return mts
    # make sure that membrane potential is returning back to rmp
    loc_decay = np.where(trace_to_fit > trace_to_fit[0])[0]
    if len(trace_to_fit) == 0 or len(loc_decay) == 0:
        # make sure that empty traces or non returning locations are avoided
        mts = np.nan
        return mts
    trace_to_fit = trace_to_fit[loc_decay[0]:]
    time_to_fit = np.arange(0, len(trace_to_fit)) / (samplerate / 1000)
    try:
        popt, pcov = curve_fit(fit_2exp,
                               time_to_fit,
                               trace_to_fit,
                               p0=(trace_to_fit[0],10,trace_to_fit[-1]))
        mts = popt[1]
    except (TypeError, RuntimeError):
        mts = np.nan
    return mts


def get_mts_ini(trace, rmp=None, start=None, end=None, samplerate=10000):
    """Calculates membrane time constant on a hyperpolarizing decent.

    Fits a single exponential to the rising phase (towards hyperpolarized
    voltage) of a hyperpolarizing currentstep to calculate membrane time
    constant. Fits against 10% of minimum voltage till maximum minimum voltage.

    Args:
        - trace: numpy array
        - rmp:  resting membrane potential
        - start: datapoint startpoint to be included in the calculation
        - end: datapoint endpoint to be excluded in the calculation.
        - samplerate: samplerate of data acquisition for trace

    Returns:
        - mts: membrane time constant in ms
    """
    trace_to_fit = trace[int(start): int(end)] - rmp
    # detect maximum hyperpolarizing deflect
    hyp = np.min(trace_to_fit)
    loc_hyp = np.where(trace_to_fit == hyp)[0]
    loc_10hyp = np.where(trace_to_fit <= hyp * 0.1)[0]

    if len(loc_hyp) > 0:
        # crop till most hyperpolarized state
        trace_to_fit = trace_to_fit[:loc_hyp[0]]
        if len(trace_to_fit) == 0:
            # make sure that empty traces or non returning locations are avoided
            mts = np.nan
            return mts

    # crop start till 10% of the most hyperpolarized state
    if len(loc_10hyp) > 0:
        # crop till most hyperpolarized state
        trace_to_fit = trace_to_fit[loc_10hyp[0]:]
        if len(trace_to_fit) == 0:
            # make sure that empty traces or non returning locations are avoided
            mts = np.nan
            return mts
    time_to_fit = np.arange(0, len(trace_to_fit)) / (samplerate / 1000)
    try:
        popt, pcov = curve_fit(fit_2exp,
                               time_to_fit,
                               trace_to_fit,
                               p0=(trace_to_fit[0], 10, trace_to_fit[-1]))
        if np.isinf(pcov[0][0]):
            popt, pcov = curve_fit(fit_2exp,
                                   time_to_fit,
                                   trace_to_fit,
                                   p0=(trace_to_fit[0], 1, trace_to_fit[-1]))
        if 0 < popt[1] < 200 and not np.isinf(pcov[0][0]):
            # only use tau that is realistic. So no negative nor tau > 200 ms
            mts = popt[1]
        else:
            mts = np.nan
    except (TypeError, RuntimeError):
        mts = np.nan
    return mts


def filter_bessel(trace, order=4, samplerate=100000, cutfreq=10000,
                  ftype='bessel', btype='low'):
    """Filters signal based on Bessel

    Uses Bessel filter to reduce noise in trace.

    Args:
        - trace: numpy array
        - order: number of poles
        - samplerate: samplerate of data acquisition for trace
        - cutfreq: cut-off frequency
        - ftype: filter types, can also be 'butter'
        - btype: passing in either, 'bandpass','high','low','bandstop'

    Returns:
        - filtered_trace: filtered version of the input trace
    """
    Wn = cutfreq / (samplerate / 2)
    b, a = iirfilter(N=order, Wn=Wn, btype=btype, ftype=ftype)
    filtered_trace = filtfilt(b, a, trace)
    return filtered_trace


def get_spike_location(trace, start=0, end=-1, samplerate=10000,
                       dt_threshold=20, dt_return=0, spike_threshold=5,
                       latency_threshold=2, peak_threshold=2, peak_minimum=-30,
                       peak_max=100, suppress_messages=True):
    """Determines spike locations in trace

    After a four-pole 1kHz Bessel filter, putative spikes are localized.
    Based on spike kinetics criteria false-positives are discarded.
    Args:
        - trace: numpy array.
        - start: datapoint startpoint to be included in the calculation.
        - end: first datapoint endpoint not to included in calculation.
        - samplerate : rate of acquisition.
        - dt_threshold: spikes with equal or faster rate (mV/ms) are included.
        - dt_return: time derivative needs to go return below the rate (mV/ms)
                     to be included.
        - spike_threshold: percentage of max upstroke rate that is used as spike
                           threshold.
        - latency_threshold: maximum latency (ms) between peak and threshold.
        - peak_threshold: minimum delta (mV) between threshold mV and peak mV.
        - peak_minimum: minimum absolute peak mV.
        - peak_max: maximum absolute peak mV.
        - suppress_messages: show messages and graphs of identified spikes.

    Returns:
        - time_peak: numpy array with datapoints of spike/action potential
                      peak.
        - vm_peak: numpy array with membrane potentials of spike/action
                    potential peak.
        - time_thresh: numpy array with datapoints of spike/action potential
                       threshold.
        - vm_thresh: numpy array with membrane potentials of spike/action
                     potential threshold.
    """
    trace = trace[int(start):int(end)]
    dt_threshold = dt_threshold / (samplerate / 1000)
    deriv_trace = np.gradient(trace)
    put_spike_location, put_spike_rate = find_peaks(
        np.gradient(filter_bessel(trace=trace,
                                  samplerate=samplerate,
                                  order=4, cutfreq=1000,
                                  ftype='bessel',
                                  btype='low')),
        height=dt_threshold)
    put_spike_rate = put_spike_rate['peak_heights']
    if len(put_spike_location) >= 1:
        # start_spikes = np.append(0, put_spike_location)
        start_spikes = put_spike_location - int(
            (latency_threshold * (samplerate /
                                  1000) + 5))
        if start_spikes[0] < 0:
            start_spikes[0] = 0
        till_spikes = np.append(put_spike_location, len(trace))
        till_spikes = till_spikes[1:]
        time_thresh = np.zeros(len(put_spike_location))
        vm_thresh = np.zeros(len(put_spike_location))
        time_peak = np.zeros(len(put_spike_location))
        vm_peak = np.zeros(len(put_spike_location))
        for n in np.arange(0, len(put_spike_location)):
            if np.min(deriv_trace[
                      put_spike_location[n]:till_spikes[n]]) < dt_return:
                time_peak[n] = put_spike_location[n] + np.argmax(
                    trace[put_spike_location[n]:till_spikes[n]])
                vm_peak[n] = np.max(
                    trace[put_spike_location[n]:till_spikes[n]])
                # define spiking threshold as 5% of max upstroke rate
                # prespike_trace = deriv_trace[
                #                  start_spikes[n]:put_spike_location[n]]
                prespike_trace = deriv_trace[
                                 start_spikes[n]:int(time_peak[n])]
                put_spike_rate[n] = np.max(prespike_trace)
                # make sure to cap the prespike_trace by the put_spike_rate,
                # to avoid throwing away spikes that may have a slower deriv at
                # the peak.
                if np.argmax(prespike_trace) >= 1:
                    prespike_trace = prespike_trace[:np.argmax(prespike_trace)]
                # in some cases the ap arises during the rising phase of the
                # current step. In that case the derivative will be often
                # higher then the spike_threshold. In that case take the lowest
                # derivative value as the threshold
                deriv_thresh = (put_spike_rate[n] * (spike_threshold / 100))
                if not np.any(prespike_trace <= deriv_thresh):
                    deriv_thresh = prespike_trace.min()
                time_thresh[n] = int(
                    start_spikes[n] + len(prespike_trace) - np.argmax(
                        np.flip(prespike_trace) <= deriv_thresh))
                vm_thresh[n] = trace[int(time_thresh[n])]
                if ((time_peak[n] - time_thresh[
                    n]) / samplerate) * 1000 > latency_threshold or \
                        abs(vm_peak[n] - vm_thresh[n]) < peak_threshold or \
                        vm_peak[n] < peak_minimum or \
                        vm_peak[n] > peak_max:
                    time_peak[n] = np.nan
                    vm_peak[n] = np.nan
                    time_thresh[n] = np.nan
                    vm_thresh[n] = np.nan
        vm_peak = np.delete(vm_peak, np.where(np.isnan(time_peak)))
        time_thresh = np.delete(time_thresh, np.where(np.isnan(time_peak)))
        vm_thresh = np.delete(vm_thresh, np.where(np.isnan(time_peak)))
        time_peak = np.delete(time_peak, np.where(np.isnan(time_peak)))
        vm_test = vm_peak == vm_thresh
        vm_peak = np.delete(vm_peak, np.where(vm_test))
        time_thresh = np.delete(time_thresh, np.where(vm_test))
        vm_thresh = np.delete(vm_thresh, np.where(vm_test))
        time_peak = np.delete(time_peak, np.where(vm_test))
        if not suppress_messages:
            plt.plot(trace)
            plt.plot(time_thresh, vm_thresh, 'x')
            plt.plot(time_peak, vm_peak, 'o')
        if len(time_peak) == sum(np.isnan(time_peak)):
            time_peak = np.nan
            vm_peak = np.nan
            time_thresh = np.nan
            vm_thresh = np.nan
            # plt.plot('No spikes were identified')
    else:
        time_peak = np.nan
        vm_peak = np.nan
        time_thresh = np.nan
        vm_thresh = np.nan
        if not suppress_messages:
            plt.title('No spikes were identified')
            plt.plot(trace)
    return time_peak, vm_peak, time_thresh, vm_thresh


def get_spike_features(trace, start=0, end=-1, samplerate=10000,
                       dt_threshold=20, dt_return=0, spike_threshold=5,
                       latency_threshold=2, peak_threshold=2, peak_minimum=-30,
                       peak_max=100, through_border=5, suppress_messages=True):
    """Extracts features for the spikes in the trace.

    Will run get_spike_location to determining the spike locations. Followed by
    extraction of features. All data is out put in a numpy array.
    * Spike = action potential

    Args:
        - trace: numpy array.
        - start: datapoint startpoint to be included in the calculation.
        - end: first datapoint endpoint not to included in calculation.
        - samplerate : rate of acquisition.
        - dt_threshold: spikes with equal or faster rate (mV/ms) are included.
        - dt_return: time derivative needs to go return below the rate (mV/ms)
                     to be included.
        - spike_threshold: percentage of max upstroke rate that is used as spike
                           threshold.
        - latency_threshold: maximum latency (ms) between peak and threshold.
        - peak_threshold: minimum delta (mV) between threshold mV and peak mV.
        - peak_minimum: minimum absolute peak mV.
        - peak_max: maximum absolute peak mV.
        - through_border: latency threshold relative to action potential
                             peak. Throughs found left of the border are cond-
                             sidered fast, right are considered slow.
        - suppress_messages: show messages and graphs of identified spikes.

    Returns:
        - feature_dp: features calculated based on datapoints
        - feature_time: features calculated based on time (datapoints
                            corrected for samplerate)
        - features of both spike_arrays:
            - time_peak: timing action potential peak (s).
            - vm_peak: membrane potentials of action potential peak.
            - time_threshold: timing action potential threshold relative to
                              action potential peak (ms).
            - vm_threshold: membrane potentials of action potential threshold.
            - time_through: timing of minimum Vm between action potential peak
                            and consecutive spike.
            - vm_through: minimum Vm between action potential and consecutive
                          spike.
            - time_fast_through: timing of minimum Vm between action potential
                                 peak and 5 ms after.
            - vm_fast_through: minimum Vm between action potential and 5 ms
                                 after.
            - time_slow_through: timing of minimum Vm between 5 ms after action
                                 potential peak and next action potential.
            - vm_slow_through: minimum Vm between 5 ms after action potential
                               and next spike.
            - rel_time_spend_through: ratio timing through vs. inter action
                                      potential interval.
            - v_peak: amplitude of the action potential between threshold and
                      peak action potential peak.
            - time_upstroke_half_peak: timing of Vm half peak in upstroke
                                       relative to action potential peak.
            - time_downstroke_half_peak: timing of Vm half peak in downstroke
                                         relative to action potential peak.
            - width_spike: full-width at half-max peak of spike.
            - max_upstroke_rate: maximum dV/dt in upstroke.
            - max_downstroke_rate: maximum dV/dt in downstroke.
            - upstroke_downstroke_ratio: ratio between up- and downstroke
                                         velocity.
            - isi: time till next action potential.


    """
    (time_peak,
     vm_peak,
     time_thresh,
     vm_thresh) = get_spike_location(trace, start=start, end=end,
                                     samplerate=samplerate,
                                     dt_threshold=dt_threshold,
                                     dt_return=dt_return,
                                     spike_threshold=spike_threshold,
                                     latency_threshold=latency_threshold,
                                     peak_threshold=peak_threshold,
                                     peak_minimum=peak_minimum,
                                     peak_max=peak_max,
                                     suppress_messages=True)
    trace_copy = trace
    trace = trace[int(start):int(end)]
    if not isinstance(time_peak, float):
        feature_dp = np.ones((len(time_peak)), dtype=[
            ('time_peak', int), ('vm_peak', float), ('time_threshold', int),
            ('vm_threshold', float), ('time_through', int),
            ('vm_through', float),
            ('time_fast_through', int), ('vm_fast_through', float),
            ('time_slow_through', int), ('vm_slow_through', float),
            ('rel_time_spend_through', float), ('v_peak', float),
            ('time_upstroke_half_peak', int),
            ('time_downstroke_half_peak', int),
            ('width_spike', int), ('max_upstroke_rate', float),
            ('max_downstroke_rate', float),
            ('upstroke_downstroke_ratio', float), ('isi', float)])
        feature_dp['time_peak'] = time_peak
        feature_dp['vm_peak'] = vm_peak
        trace_stop = np.append(time_peak, len(trace))
        trace_stop = trace_stop[1:]
        if len(time_peak) == 1:
            feature_dp['isi'][0] = np.nan
        else:
            feature_dp['isi'] = trace_stop - time_peak
            feature_dp['isi'][-1] = np.nan  # last one has no isi, because
            # trace_stop[-1] resembles end of trace.
        pad_time = latency_threshold * (samplerate / 1000)
        through_border = int(through_border * (samplerate / 1000))
        for n in np.arange(0, len(time_peak)):
            if time_peak[n] - pad_time < 0:
                corr_time = 0
                pad_peak = 0
            else:
                pad_peak = time_peak[n] - pad_time
                corr_time = pad_time
            spike_trace = trace[int(pad_peak):
                                int(trace_stop[n])]
            if np.size(spike_trace) == 1:
                spike_trace = np.append(spike_trace, spike_trace)
            if np.size(trace[int(pad_peak):int(time_peak[n])]) == 0 or \
                    np.size(spike_trace) == 0:
                feature_dp['time_through'][n] = 0
                feature_dp['vm_through'][n] = np.nan
                feature_dp['time_fast_through'][n] = 0
                feature_dp['vm_fast_through'][n] = np.nan
                feature_dp['time_slow_through'][n] = 0
                feature_dp['vm_slow_through'][n] = np.nan
                feature_dp['max_upstroke_rate'][n] = np.nan
                feature_dp['max_downstroke_rate'][n] = np.nan
                feature_dp['upstroke_downstroke_ratio'] = np.nan
            else:
                deriv_trace = np.gradient(spike_trace)
                deriv_trace_pad = np.gradient(trace[int(pad_peak):
                                                    int(time_peak[n])])
                feature_dp['time_through'][n] = np.argmin(spike_trace) - \
                                                corr_time
                feature_dp['vm_through'][n] = np.min(spike_trace)

                if len(spike_trace) <= through_border:
                    through_border_c = len(spike_trace)
                    feature_dp['time_fast_through'][n] = np.argmin(
                        spike_trace[0:through_border_c]) - corr_time
                    feature_dp['vm_fast_through'][n] = np.min(
                        spike_trace[0:through_border_c])
                    feature_dp['time_slow_through'][n] = \
                        feature_dp['time_fast_through'][n]
                    feature_dp['vm_slow_through'][n] = \
                        feature_dp['vm_fast_through'][n]
                if len(spike_trace) > through_border:
                    through_border_c = len(spike_trace)
                    feature_dp['time_fast_through'][n] = np.argmin(
                        spike_trace[0:through_border]) - corr_time
                    feature_dp['vm_fast_through'][n] = np.min(
                        spike_trace[0:through_border])
                    if feature_dp['time_fast_through'][n] <= 0:
                        feature_dp['time_fast_through'][n] = 0
                        feature_dp['vm_fast_through'][n] = np.nan
                    feature_dp['time_slow_through'][n] = np.argmin(
                        spike_trace[through_border::]) + through_border - \
                                                         corr_time
                    feature_dp['vm_slow_through'][n] = np.min(
                        spike_trace[through_border::])
                rel_time_through = feature_dp['time_through'][n] / \
                                   feature_dp['isi'][n]
                if rel_time_through < 0:
                    # avoid negative fractions.
                    rel_time_through = 0
                feature_dp['rel_time_spend_through'][n] = rel_time_through
                feature_dp['max_upstroke_rate'][n] = np.max(deriv_trace_pad)
                feature_dp['max_downstroke_rate'][n] = np.min(deriv_trace)
                if feature_dp['max_upstroke_rate'][n] != 0 and \
                        feature_dp['max_downstroke_rate'][n] != 0:
                    feature_dp['upstroke_downstroke_ratio'] = \
                        abs(feature_dp['max_upstroke_rate'][n] /
                            feature_dp['max_downstroke_rate'][n])
        # calculate the average max_upstroke and use a percentage
        # (spike_threshold) for the calculation of the action potential
        # threshold.
        threshold_calc = np.mean(feature_dp['max_upstroke_rate']) * \
                         (spike_threshold / 100)
        for n in np.arange(0, len(time_peak)):
            spike_trace = trace[int(time_peak[n]): int(trace_stop[n])]
            if time_peak[n] - pad_time < 0:
                pad_peak = 0
                corr_time = 0
            else:
                pad_peak = time_peak[n] - pad_time
                corr_time = pad_time
            spike_trace_pad = trace[int(pad_peak):
                                    int(time_peak[n]) + 1]
            if np.size(spike_trace_pad) == 1:
                spike_trace_pad = np.append(spike_trace_pad, spike_trace_pad)
            if np.size(spike_trace_pad) == 0 or np.size(spike_trace) == 0:
                feature_dp['time_threshold'][n] = 0
                feature_dp['vm_threshold'][n] = np.nan
                feature_dp['v_peak'][n] = np.nan
                feature_dp['time_upstroke_half_peak'][n] = 0
                feature_dp['time_downstroke_half_peak'][n] = 0
                feature_dp['width_spike'][n] = 0
            else:
                deriv_trace_pad = np.gradient(spike_trace_pad)
                feature_dp['time_threshold'][n] = np.argmax(
                    np.flip(deriv_trace_pad) <= threshold_calc)
                feature_dp['vm_threshold'][n] = spike_trace_pad[-feature_dp
                ['time_threshold'][n]]
                feature_dp['v_peak'][n] = feature_dp['vm_peak'][n] - \
                                          feature_dp['vm_threshold'][n]
                half_peak = (feature_dp['v_peak'][n] / 2) + \
                            feature_dp['vm_threshold'][n]
                feature_dp['time_upstroke_half_peak'][n] = pad_time - np.argmax(
                    spike_trace_pad >= half_peak)
                feature_dp['time_downstroke_half_peak'][n] = np.argmin(
                    spike_trace >= half_peak)
                feature_dp['width_spike'][n] = \
                    (feature_dp['time_upstroke_half_peak'][n] +
                     feature_dp['time_downstroke_half_peak'][n])
        feature_time = np.ones((len(time_peak)), dtype=[
            ('time_peak', float), ('vm_peak', float), ('time_threshold', float),
            ('vm_threshold', float), ('time_through', float),
            ('vm_through', float),
            ('time_fast_through', float), ('vm_fast_through', float),
            ('time_slow_through', float), ('vm_slow_through', float),
            ('rel_time_spend_through', float), ('v_peak', float),
            ('time_upstroke_half_peak', float),
            ('time_downstroke_half_peak', float),
            ('width_spike', float), ('max_upstroke_rate', float),
            ('max_downstroke_rate', float),
            ('upstroke_downstroke_ratio', float), ('isi', float)])
        dp_factor = int(samplerate / 1000)
        feature_time['time_peak'] = np.true_divide(feature_dp['time_peak'],
                                                   samplerate)
        feature_time['vm_peak'] = feature_dp['vm_peak']
        feature_time['time_threshold'] = np.true_divide(
            feature_dp['time_threshold'], dp_factor)
        feature_time['vm_threshold'] = feature_dp['vm_threshold']
        feature_time['time_through'] = np.true_divide(
            feature_dp['time_through'],
            dp_factor)
        feature_time['vm_through'] = feature_dp['vm_through']
        feature_time['time_fast_through'] = np.true_divide(
            feature_dp['time_fast_through'], dp_factor)
        feature_time['vm_fast_through'] = feature_dp['vm_fast_through']
        feature_time['time_slow_through'] = np.true_divide(
            feature_dp['time_slow_through'], dp_factor)
        feature_time['vm_slow_through'] = feature_dp['vm_slow_through']
        feature_time['rel_time_spend_through'] = feature_dp[
            'rel_time_spend_through']
        feature_time['v_peak'] = feature_dp['v_peak']
        feature_time['time_upstroke_half_peak'] = np.true_divide(
            feature_dp['time_upstroke_half_peak'], dp_factor)
        feature_time['time_downstroke_half_peak'] = np.true_divide(
            feature_dp['time_downstroke_half_peak'], dp_factor)
        feature_time['width_spike'] = np.true_divide(feature_dp['width_spike'],
                                                     dp_factor)
        feature_time['max_downstroke_rate'] = feature_dp[
                                                  'max_downstroke_rate'] * \
                                              dp_factor
        feature_time['max_upstroke_rate'] = feature_dp[
                                                'max_upstroke_rate'] * dp_factor
        feature_time['upstroke_downstroke_ratio'] = feature_dp[
            'upstroke_downstroke_ratio']
        feature_time['isi'] = np.true_divide(feature_dp['isi'], samplerate)
    else:
        feature_dp = np.empty(1, dtype=[
            ('time_peak', float), ('vm_peak', float), ('time_threshold', float),
            ('vm_threshold', float), ('time_through', float),
            ('vm_through', float),
            ('time_fast_through', float), ('vm_fast_through', float),
            ('time_slow_through', float), ('vm_slow_through', float),
            ('rel_time_spend_through', float), ('v_peak', float),
            ('time_upstroke_half_peak', float),
            ('time_downstroke_half_peak', float),
            ('width_spike', float), ('max_upstroke_rate', float),
            ('max_downstroke_rate', float),
            ('upstroke_downstroke_ratio', float), ('isi', float)])
        feature_dp[:] = np.nan
        feature_time = feature_dp
    return feature_dp, feature_time


def measure_bursts(isi_train, short_ISI_thres=10):
    """Measures the ratio of cv long and short ISI and initial bursting.

    Attempt to put a number to the 'bursty'-ness of spike patterns.


    Args:
        - isi_train: np.array with inter-spike-intervals.
        - short_ISI_thres: integer, cut-off value of ISI. Any value below is
                           considered a short_ISI.
    Returns:
        - potential_burst: bool, True: has spikes with short_ISI and long_ISI
        - r_cv: ratio of cv_long_ISI / cv_short_ISI
        - ini_burst: bool, True: initial short ISI action potentials followed
                     by slower ISIs
    """
    isi_train = isi_train[~np.isnan(isi_train)]
    spike_short = isi_train[isi_train < short_ISI_thres]
    spike_long = isi_train[isi_train >= short_ISI_thres]
    # calculate longest length of bursting
    sum_isi = list()
    bool_isi = isi_train < short_ISI_thres
    sum_check = 0
    for i in np.arange(0, len(bool_isi)):
        if bool_isi[i]:
            sum_check += isi_train[i]
        elif not bool_isi[i] and sum_check > 0 or i == len(bool_isi):
            sum_isi.append(sum_check)
            sum_check = 0
    longest_burst = np.nan
    if len(sum_isi) >= 1:
        longest_burst = np.max(sum_isi)
    # this would be a regular spiking neuron
    potential_burst = False
    r_cv = np.nan
    ini_burst = False
    fr_short = len(spike_short) / len(isi_train)
    fr_long = len(spike_long) / len(isi_train)
    if len(spike_long) <= 1 or len(spike_short) == 0:
        # fast or slow spiking regular neuron
        return potential_burst, r_cv, fr_long, fr_short, ini_burst, \
               longest_burst
    if len(spike_short) >= 1:
        # potential bursting neuron
        potential_burst = True
        if len(spike_short) == 1:
            r_cv = np.nan
        else:
            cv_short = np.std(spike_short) / np.mean(spike_short)
            cv_long = np.std(spike_long) / np.mean(spike_long)
            r_cv = cv_long / cv_short
    if isi_train[0] < short_ISI_thres:
        # initial bursting neuron
        ini_burst = True
    return potential_burst, r_cv, fr_long, fr_short, ini_burst, longest_burst


def get_spike_train_features(trace, start=0, end=-1, samplerate=10000,
                             dt_threshold=20, dt_return=0, spike_threshold=5,
                             latency_threshold=2, peak_threshold=2,
                             peak_minimum=-30, peak_max=100,
                             suppress_messages=True):
    """Calculates features related to spike trains.

    Calculations based on action potential timing from get_spike_location.

    Args:
        - trace: numpy array.
        - start: datapoint startpoint to be included in the calculation.
        - end: first datapoint endpoint not to included in calculation.
        - samplerate : rate of acquisition.
        - dt_threshold: spikes with equal or faster rate (mV/ms) are included.
        - dt_return: time derivative needs to go return below the rate (mV/ms)
                     to be included.
        - spike_threshold: percentage of max upstroke rate that is used as spike
                           threshold.
        - latency_threshold: maximum latency (ms) between peak and threshold.
        - peak_threshold: minimum delta (mV) between threshold mV and peak mV.
        - peak_minimum: minimum absolute peak mV.
        - peak_max: maximum absolute peak mV.
        - suppress_messages: show messages and graphs of identified spikes.

    Returns:
        - isi: numpy array of interspike intervals of all action
                   potentials.
        -feature_time: features calculated based on time (datapoints
                            corrected for samplerate). Containing:
            - first_isi: interspike interval between first two action
                         potentials.
            - average_isi: average interspike interval in a trace.
            - isi_cv: coefficient of variation (sd/mean) of interspike
                      intervals.
            - average_firing_rate: number of action potentials divided by
                                   length of trace.
            - adaptation_index: rate at which action potential firing speeds
                                slow down or speeds up.
            - latency: time between start of trace and the first action
                       potential. (trace should be cropped to start of stimulus)
            - delay: True/False, True: if latency is longer than average_isi.
            - burst: True/False, True: if first two ISIs are less or equal
                         to 5 ms.
            - pause: True/False, True: if isi is more than 3 times the duration
                     of the ISIs immediately prior and after.
    """
    (time_peak,
     vm_peak,
     time_thresh,
     vm_thresh) = get_spike_location(trace, start=start, end=end,
                                     samplerate=samplerate,
                                     dt_threshold=dt_threshold,
                                     dt_return=dt_return,
                                     spike_threshold=spike_threshold,
                                     latency_threshold=latency_threshold,
                                     peak_threshold=peak_threshold,
                                     peak_minimum=peak_minimum,
                                     peak_max=peak_max,
                                     suppress_messages=True)
    trace = trace[int(start):int(end)]
    time_peak *= (1000 / samplerate)
    if isinstance(time_peak, float):
        feature_time = np.nan
        return feature_time, 0
    elif len(time_peak) == 1:
        feature_time = np.ones(1, dtype=[
            ('first_isi', float), ('average_isi', float),
            ('isi_cv', float), ('average_firing_rate', float),
            ('adaptation_index', float), ('latency', float), ('delay', bool),
            ('burst', bool), ('pause', bool), ('potential_burst', bool),
            ('r_cv', float), ('fr_long', float), ('fr_short', float),
            ('ini_burst', bool), ('longest_burst', float)])
        feature_time['latency'] = time_peak[0]
        return feature_time, 0
    else:
        feature_time = np.ones(1, dtype=[
            ('first_isi', float), ('average_isi', float),
            ('isi_cv', float), ('average_firing_rate', float),
            ('adaptation_index', float), ('latency', float), ('delay', bool),
            ('burst', bool), ('pause', bool), ('potential_burst', bool),
            ('r_cv', float), ('fr_long', float), ('fr_short', float),
            ('ini_burst', bool), ('longest_burst', float)])
        trace_stop = np.append(time_peak, len(trace))
        trace_stop = trace_stop[1:]
        isi = np.array(trace_stop - time_peak)
        isi[-1] = np.nan  # last one has no isi, because
        # trace_stop[-1] resembles end of trace.
        feature_time['first_isi'] = isi[0]
        feature_time['average_isi'] = np.nanmean(isi)
        feature_time['isi_cv'] = np.nanstd(isi) / np.nanmean(isi)
        feature_time['average_firing_rate'] = len(time_peak) / (len(trace) /
                                                                samplerate)
        feature_time['adaptation_index'] = (1 / (
                    len(time_peak) - 1)) * np.nansum(
            (time_peak[1::] - time_peak[0:-1]) /
            (time_peak[1::] + time_peak[0:-1]))
        feature_time['latency'] = time_peak[0]
        if feature_time['latency'] > feature_time['average_isi']:
            feature_time['delay'] = True
        else:
            feature_time['delay'] = False
        if isi[0] <= 5 and isi[1] <= 5:
            feature_time['burst'] = True
        else:
            feature_time['burst'] = False
        if len(isi) >= 4:
            rate_prior = isi[1:-2] / isi[0:-3]
            rate_after = isi[1:-2] / isi[2:-1]
            rates_combo = np.logical_and([rate_prior > 3], [rate_after > 3])
            # sanity checked, identifies pausing ISI, with trailing shorter isi
            feature_time['pause'] = np.any(rates_combo)
        else:
            feature_time['pause'] = False
        if len(isi) >= 10:
            (feature_time['potential_burst'],
                feature_time['r_cv'],
                feature_time['fr_long'],
                feature_time['fr_short'],
                feature_time['ini_burst'],
                feature_time['longest_burst']) = measure_bursts(isi)
        else:
            feature_time['potential_burst'] = None
            feature_time['r_cv'] = np.nan
            feature_time['fr_long'] = np.nan
            feature_time['fr_short'] = np.nan
            feature_time['ini_burst'] = None
            feature_time['longest_burst'] = np.nan
    return feature_time, isi


def get_vsag_fraction(trace, vm, start=0, end=-1, samplerate=10000,
                      suppress_messages=True):
    """Calculates relative sag voltage to steady-state.

    Works only on trace sections that have hyperpolarized direction of voltage.

    Args:
        - trace: numpy array.
        - Vm: membrane potential typically determined prior stimulation
        - start: datapoint startpoint to be included in the calculation. Should
                 be the trace during the hyperpolarizing stimulation step only
        - end: first datapoint endpoint not to included in calculation. Should
               be the trace during the hyperpolarizing stimulation step only
        - samplerate : rate of acquisition.
        - suppress_messages: show graph of fitting.

    Returns:
        - feature_time: features calculated based on time (datapoints
                            corrected for samplerate). Containing:
            - vsag_fraction: relative sag voltage to steady-state.
            - vm_max_hyperpolarization: minimum membrane potential (i.e., sag) (mV).
            - time_vsag: time point of sag, relative to start trace (ms).
            - tau_membrane_time_constant: tau calculate by single-exponential
                                          fit of the hyperpolarizing part of the
                                          trace (ms).
    """
    trace = trace[int(start):int(end)]
    deriv_trace = np.gradient(trace)
    feature_time = np.nan
    if np.mean(deriv_trace[0:5]) <= 0:
        # this trace contains a voltage trace that is declining towards more
        # hyperpolarized voltage.
        feature_time = np.ones(1, dtype=[('vsag_fraction', float),
                                         ('vm_max_hyperpolarization', float),
                                         ('time_vsag', float),
                                         ('tau_membrane_time_constant', float)])
        steady_dp = int(50 * (samplerate / 1000))
        if len(trace) <= steady_dp:
            steady_dp = 1  # avoid that the steady_dp is longer than the trace
        vm_max_hyperpolarization = np.min(trace[:-steady_dp])  # find the most hyperpolarized vm
        # before reaching steady state (defined here as the last 50 ms of the
        # pulse
        feature_time['vm_max_hyperpolarization'] = vm_max_hyperpolarization
        size_vsag = vm_max_hyperpolarization - vm
        time_vsag = np.argmin(trace)
        vm_steady = get_vm(trace, -steady_dp, -1)
        feature_time['vsag_fraction'] = -(vm_steady - vm_max_hyperpolarization) / size_vsag
        if feature_time['vsag_fraction'] < 0:
            feature_time['vsag_fraction'] = 0
        feature_time['time_vsag'] = time_vsag / (samplerate / 1000)
        time_10_percent = np.argmin(trace >= ((0.1 * size_vsag) + vm))
        trace_to_fit = trace[time_10_percent:time_vsag]
        time_to_fit = np.arange(0, len(trace_to_fit)) / (samplerate / 1000)

        if len(trace_to_fit) > 2:
            try:
                popt, pcov = curve_fit(fit_2exp,
                                       time_to_fit,
                                       trace_to_fit,
                                       p0=(vm, 1, 0))
                if not suppress_messages:
                    plt.subplot(121)
                    plt.plot(time_to_fit, trace_to_fit)
                    xx = np.linspace(0, time_vsag, 1000)
                    yy = fit_2exp(xx, popt[0], popt[1] * (samplerate / 1000),
                                  popt[2])
                    plt.subplot(122)
                    plt.plot(xx, yy, linewidth=10)
                    plt.plot(trace)
                tau = popt[1]
            except (TypeError, RuntimeError):
                tau = np.nan
        else:
            tau = np.nan
        feature_time['tau_membrane_time_constant'] = tau
    return feature_time


def add_qc_features_sweep_table(experiment, noise_rms_time=1.5,
                                patch_rms_time=50, membrane_time=50,
                                dt_threshold=20, dt_return=0,
                                spike_threshold=5, latency_threshold=2,
                                peak_threshold=2, peak_minimum=-30,
                                peak_max=100, through_border=5,
                                suppress_messages=True):
    """Adds calculated features to sweep_table.

    Uses sweep_table embedded in experiment (requires 'read_ephys_experiment'-
    function)
    Iterates through all sweeps and extracts features:
        - noise_rms: noise level.
        - patch_rms: stability of the patch-clamp.
        - membrane_potential: stability of Vm.
        - first_ap_peak_v: absolute mV of first action potential peak.
        - first_ap_size_v: height of first action potential (threshold-peak).
        - delta_Vm_trace: delta mV between start and end of each trace.

    Args:
        - experiment: nested dictionary.
        - noise_rms_time: time epoch used to calculate noise, relative to start
                          of trace.
        - patch_rms_time: time epoch used to calculate patch instability,
                          relative to start of trace.
        - membrane_time: time epoch used to calculate membrane potential,
                         relative to start of trace.
        - dt_threshold: spikes with equal or faster rate (mV/ms) are included.
        - dt_return: time derivative needs to go return below the rate (mV/ms)
                     to be included.
        - spike_threshold: percentage of max upstroke rate that is used as spike
                           threshold.
        - latency_threshold: maximum latency (ms) between peak and threshold.
        - peak_threshold: minimum delta (mV) between threshold mV and peak mV.
        - peak_minimum: minimum absolute peak mV.
        - peak_max: maximum absolute peak mV.
        - through_border: latency threshold relative to action potential
                             peak. Throughs found left of the border are cond-
                             sidered fast, right are considered slow.
        - suppress_messages: show messages and graphs of identified spikes.

    Returns:
        - experiment: features calculated based on datapoints


    """
    experiment['sweep_table'] = check_columns_dataframe(
        experiment['sweep_table'], 'ephys_stim_pulseAmplitude')
    experiment['sweep_table'] = check_columns_dataframe(
        experiment['sweep_table'], 'noise_rms')
    experiment['sweep_table'] = check_columns_dataframe(
        experiment['sweep_table'], 'patch_rms')
    experiment['sweep_table'] = check_columns_dataframe(
        experiment['sweep_table'], 'membrane_potential')
    experiment['sweep_table'] = check_columns_dataframe(
        experiment['sweep_table'], 'first_ap_peak_v')
    experiment['sweep_table'] = check_columns_dataframe(
        experiment['sweep_table'], 'first_ap_size_v')
    experiment['sweep_table'] = check_columns_dataframe(
        experiment['sweep_table'], 'delta_Vm_trace')
    for ind, sweep_info in experiment['sweep_table'].iterrows():
        trace = (experiment['acquired_data'][sweep_info['sweep']]['ephys']
        [sweep_info['amplifier']]['data'])
        samplerate = sweep_info['samplerate']
        patch_rms_time_sw = patch_rms_time * (samplerate / 1000)
        noise_rms_time_sw = noise_rms_time * (samplerate / 1000)
        membrane_time_sw = membrane_time * (samplerate / 1000)
        noise_rms = get_rms(trace=trace, start=0, end=noise_rms_time_sw)
        if sweep_info['ephys_stim_pulseDelay'] < patch_rms_time:
            patch_rms = np.nan
            vm = np.nan
            delta_vm = np.nan
        else:
            patch_rms = get_rms(trace=trace, start=0, end=patch_rms_time_sw)
            if sweep_info['amp_mode'] == 'I-Clamp':
                vm = get_vm(trace=trace, start=0, end=membrane_time_sw)
                post_vm = get_vm(trace=trace, start=-membrane_time_sw - 1,
                                 end=-1)
                delta_vm = post_vm - vm
            else:
                vm = np.nan
                delta_vm = np.nan
        if sweep_info['amp_mode'] == 'I-Clamp' and \
                sweep_info['ephys_stim_enabled'] and \
                np.isnan(sweep_info['ephys_stim_pulseAmplitude']):
            sweep = sweep_info['sweep']
            amp = sweep_info['amplifier']
            try:
                pulse_amplitude = (experiment['stim_data'][sweep]['ephys'][amp]
                ['pulse_parameters']['pulseAmplitude'])
                sweep_info['ephys_stim_pulseAmplitude'] = pulse_amplitude
            except KeyError:
                sweep_info['ephys_stim_pulseAmplitude'] = np.nan
                pulse_amplitude = np.nan
        elif sweep_info['amp_mode'] == 'I-Clamp' and \
                sweep_info['ephys_stim_enabled']:
            sweep = sweep_info['sweep']
            amp = sweep_info['amplifier']
            try:
                pulse_amplitude = (experiment['stim_data'][sweep]['ephys'][amp]
                ['pulse_parameters']['pulseAmplitude'])
                sweep_info['ephys_stim_pulseAmplitude'] = pulse_amplitude
            except KeyError:
                sweep_info['ephys_stim_pulseAmplitude'] = np.nan
                pulse_amplitude = np.nan
        if sweep_info['amp_mode'] == 'I-Clamp' and \
                sweep_info['ephys_stim_enabled'] and \
                sweep_info['ephys_stim_pulseAmplitude'] > 0:
            stim_start = sweep_info['ephys_stim_pulseDelay']
            stim_end = (sweep_info['ephys_stim_pulseDelay'] +
                        sweep_info['ephys_stim_pulseWidth'])
            _, spike_features = get_spike_features(trace,
                                                   start=stim_start,
                                                   end=stim_end,
                                                   samplerate=samplerate,
                                                   dt_threshold=dt_threshold,
                                                   dt_return=dt_return,
                                                   spike_threshold=
                                                   spike_threshold,
                                                   latency_threshold=
                                                   latency_threshold,
                                                   peak_threshold=
                                                   peak_threshold,
                                                   peak_minimum=peak_minimum,
                                                   peak_max=peak_max,
                                                   through_border=
                                                   through_border,
                                                   suppress_messages=
                                                   suppress_messages)
            if not np.isnan(spike_features['time_peak'][0]):
                first_ap_peak_v = spike_features['vm_peak'][0]
                first_ap_size_v = spike_features['v_peak'][0]
            else:
                first_ap_peak_v = np.nan
                first_ap_size_v = np.nan
        else:
            first_ap_peak_v = np.nan
            first_ap_size_v = np.nan
            sweep = sweep_info['sweep']
            amp = sweep_info['amplifier']
            try:
                pulse_amplitude = (experiment['stim_data'][sweep]['ephys'][amp]
                ['pulse_parameters']['pulseAmplitude'])
                sweep_info['ephys_stim_pulseAmplitude'] = pulse_amplitude
            except (KeyError, TypeError):
                sweep_info['ephys_stim_pulseAmplitude'] = np.nan
                pulse_amplitude = np.nan
        experiment['sweep_table'].loc[ind, 'ephys_stim_pulseAmplitude'] = \
            pulse_amplitude
        experiment['sweep_table'].loc[ind, 'noise_rms'] = noise_rms
        experiment['sweep_table'].loc[ind, 'patch_rms'] = patch_rms
        experiment['sweep_table'].loc[ind, 'membrane_potential'] = vm
        experiment['sweep_table'].loc[ind, 'first_ap_peak_v'] = first_ap_peak_v
        experiment['sweep_table'].loc[ind, 'first_ap_size_v'] = first_ap_size_v
        experiment['sweep_table'].loc[ind, 'delta_Vm_trace'] = delta_vm

    return experiment


def select_sweeps(experiment, preset_name=None, amp_select=None,
                  mode_select=None, hide_discarded=False, show_mapper=False,
                  ephys_stim_enabled=True, ephys_stim_pulseType=None,
                  ephys_stim_pulseNumber=None, ephys_stim_pulseWidth=None,
                  ephys_stim_pulseAmplitude=None, noise_rms=None,
                  patch_rms=None, membrane_potential=None):
    """Selection of sweeps based on stimulus criteria

    Takes criteria to select groups of stimuli from sweep_table in experiments.

    Args:
        - experiment: nested dictionary.
        - preset_name: string, exact match to name in preset_pulses.csv.
        - amp_select: string, exact match to name of amplifier in experiment.
        - mode_select: string, exact match to amplifier mode.
        - hide_discarded: bool, True remove all discarded traces.
        - show_mapper: bool, True includes all mapper traces. Be aware these
                       traces are very long!
        - ephys_stim_enabled: bool, True only include sweeps for which ephys
                              stim was enabled.
        - ephys_stim_pulseType: string, select specific pulse type.
                                exact match with pulsetypes: 'ramp',
                                'squarePulseTrain'.
        - ephys_stim_pulseNumber: integer, select specific stimuli with #
                                  pulses.
        - ephys_stim_pulseWidth: float, time (seconds). if list of two floats
                                 all pulses equal and between the two values
                                 are selected.
        - ephys_stim_pulseAmplitude: float, amplitude(mV or pA). if list of two
                                     floats all pulses equal and between the two
                                     values are selected.
        - noise_rms: float, rms below this threshold are selected.
        - patch_rms: float, rms below this threshold are selected.
        - membrane_potential: float, mV below this threshold are selected.

    Returns:
        - sweeps: list containing names of the sweeps
        - cellAmpIDs: list containing names of the cellAmpIDs in same order as
        sweeps
    """
    sweep_table = experiment['sweep_table']
    amps = sweep_table['amplifier'].unique()
    modes = sweep_table['amp_mode'].unique()
    if preset_name:
        preset_file = os.path.join(os.path.dirname(__file__),
                                   'preset_pulses.csv')
        pulse_presets = pd.read_csv(preset_file)
        if preset_name in list(pulse_presets['preset_name']):
            pulse_presets = pulse_presets.where(pd.notnull(pulse_presets), None)
            ind = pulse_presets[pulse_presets['preset_name'] ==
                                preset_name].index[0]
            amp_select = pulse_presets.loc[ind, 'amp_select']
            mode_select = pulse_presets.loc[ind, 'mode_select']
            hide_discarded = pulse_presets.loc[ind, 'hide_discarded']
            show_mapper = pulse_presets.loc[ind, 'show_mapper']
            ephys_stim_enabled = pulse_presets.loc[ind, 'ephys_stim_enabled']
            ephys_stim_pulseType = pulse_presets.loc[
                ind, 'ephys_stim_pulseType']
            ephys_stim_pulseNumber = pulse_presets.loc[
                ind, 'ephys_stim_pulseNumber']
            if isinstance(ephys_stim_pulseNumber, str):
                if ephys_stim_pulseNumber[0] == '[':
                    ephys_stim_pulseNumber = ephys_stim_pulseNumber[1:-1]
                    ephys_stim_pulseNumber = list(
                        ephys_stim_pulseNumber.split(','))
                    for ind, i in enumerate(ephys_stim_pulseNumber):
                        ephys_stim_pulseNumber[ind] = float(i)
                else:
                    ephys_stim_pulseNumber = float(ephys_stim_pulseNumber)
            ephys_stim_pulseWidth = pulse_presets.loc[
                ind, 'ephys_stim_pulseWidth']
            if isinstance(ephys_stim_pulseWidth, str):
                if ephys_stim_pulseWidth[0] == '[':
                    ephys_stim_pulseWidth = ephys_stim_pulseWidth[1:-1]
                    ephys_stim_pulseWidth = list(
                        ephys_stim_pulseWidth.split(','))
                    for ind, i in enumerate(ephys_stim_pulseWidth):
                        ephys_stim_pulseWidth[ind] = float(i)
                else:
                    ephys_stim_pulseWidth = float(ephys_stim_pulseWidth)
            ephys_stim_pulseAmplitude = \
                pulse_presets.loc[ind, 'ephys_stim_pulseAmplitude']
            if isinstance(ephys_stim_pulseAmplitude, str):
                if ephys_stim_pulseAmplitude[0] == '[':
                    ephys_stim_pulseAmplitude = ephys_stim_pulseAmplitude[1:-1]
                    ephys_stim_pulseAmplitude = list(
                        ephys_stim_pulseAmplitude.split(','))
                    for ind, i in enumerate(ephys_stim_pulseAmplitude):
                        ephys_stim_pulseAmplitude[ind] = float(i)
                else:
                    ephys_stim_pulseAmplitude = float(ephys_stim_pulseAmplitude)
            noise_rms = pulse_presets.loc[ind, 'noise_rms']
            patch_rms = pulse_presets.loc[ind, 'patch_rms']
            membrane_potential = pulse_presets.loc[ind, 'membrane_potential']
    if amp_select:
        if amp_select in amps:
            if isinstance(amp_select, list):
                sweep_table = sweep_table[
                    sweep_table['amplifier'].isin(amp_select)]
            elif isinstance(amp_select, str):
                sweep_table = sweep_table[
                    sweep_table['amplifier'].isin([amp_select])]
        else:
            print("amp_select input doesn't match amplifier names in " +
                  "experiment: " + str(amps))
            return sweep_table
    if mode_select:
        if mode_select in modes:
            if isinstance(mode_select, list):
                sweep_table = sweep_table[
                    sweep_table['amp_mode'].isin(mode_select)]
            elif isinstance(mode_select, str):
                sweep_table = sweep_table[
                    sweep_table['amp_mode'].isin([mode_select])]
        else:
            print("mode_select input doesn't match amplifier names in " +
                  "experiment: " + str(modes))
            return sweep_table
    if hide_discarded:
        sweep_table = sweep_table[sweep_table['discard'] != True]
    if not show_mapper:
        sweep_table = sweep_table[sweep_table['mapper'] != True]
    if show_mapper:
        sweep_table = sweep_table[sweep_table['mapper'] == True]
    if ephys_stim_enabled:
        sweep_table = sweep_table[sweep_table['ephys_stim_enabled'] == True]
    if ephys_stim_pulseType:
        pulse_types = sweep_table['ephys_stim_pulseType'].unique()
        if ephys_stim_pulseType in pulse_types:
            sweep_table = sweep_table[sweep_table['ephys_stim_pulseType'].isin(
                [ephys_stim_pulseType])]
        else:
            print("mode_select input doesn't match amplifier names in " +
                  "experiment: " + str(modes))
            return sweep_table
    if ephys_stim_pulseNumber:
        sweep_table = sweep_table[sweep_table['ephys_stim_pulseNumber'] ==
                                  ephys_stim_pulseNumber]
    if ephys_stim_pulseWidth:
        samplerate = sweep_table['samplerate'].unique()
        samplerate = samplerate[0]
        if isinstance(ephys_stim_pulseWidth, float) or \
                isinstance(ephys_stim_pulseWidth, int):
            ephys_stim_pulseWidth *= samplerate
            sweep_table = sweep_table[sweep_table['ephys_stim_pulseWidth'] ==
                                      ephys_stim_pulseWidth]
        elif isinstance(ephys_stim_pulseWidth, list):
            ephys_stim_pulseWidth.sort()
            ephys_stim_pulseWidth = np.array(ephys_stim_pulseWidth) * samplerate
            sweep_table = sweep_table[sweep_table['ephys_stim_pulseWidth'] <=
                                      ephys_stim_pulseWidth[1]]
            sweep_table = sweep_table[sweep_table['ephys_stim_pulseWidth'] >=
                                      ephys_stim_pulseWidth[0]]
    if ephys_stim_pulseAmplitude:
        if isinstance(ephys_stim_pulseAmplitude, float) or \
                isinstance(ephys_stim_pulseAmplitude, int):
            sweep_table = sweep_table[sweep_table['ephys_stim_pulseAmplitude']
                                      == ephys_stim_pulseAmplitude]
        elif isinstance(ephys_stim_pulseAmplitude, list):
            ephys_stim_pulseAmplitude.sort()
            sweep_table = sweep_table[sweep_table['ephys_stim_pulseAmplitude']
                                      <= ephys_stim_pulseAmplitude[1]]
            sweep_table = sweep_table[sweep_table['ephys_stim_pulseAmplitude']
                                      >= ephys_stim_pulseAmplitude[0]]
    if noise_rms:
        sweep_table = sweep_table[sweep_table['noise_rms'] < noise_rms]
    if patch_rms:
        sweep_table = sweep_table[sweep_table['patch_rms'] < patch_rms]
    if membrane_potential:
        sweep_table = sweep_table[sweep_table['membrane_potential'] <
                                  membrane_potential]
    sweeps = list(sweep_table['sweep'])
    cellAmpIDs = list(sweep_table['cellAmpID'])
    return sweeps, cellAmpIDs


def isolate_action_potential(trace, start=None, end=None, start_relative=None,
                             end_relative=None, spike_index=0,
                             samplerate=10000):
    """Isolates action potential trace from sweep.

    Uses get_spike_features to isolate one single action potential. By default
    uses the spike threshold as start and the through after spike as end.
    Optional, select start and end data point relative to ap peak, or specific
    start/end point.

    Args:
        - trace: numpy array. Make sure that this is the chopped version if
                 stimulation was given.
        - start: datapoint startpoint to be included in the calculation.
        - end: first datapoint endpoint not to included in calculation.
        - start_relative: time prior threshold (ms).
        - end_relative=False: time after through (ms).
        - spike_index = integer of the x-th spike in the train.
        - samplerate = samplerate of data acquisition for trace.

    Returns:
        - trace: numpy array of action potential.
    """

    _, spike_features = get_spike_features(trace)
    try:
        if not start:
            start = (spike_features['time_peak'][spike_index] * samplerate -
                     spike_features['time_threshold'][spike_index] *
                     (samplerate / 1000))
            if start_relative:
                start_relative *= (samplerate / 1000)
                start -= start_relative
                if start < 0:
                    start = 0
        if not end:
            end = (spike_features['time_through'][spike_index] *
                   (samplerate / 1000) +
                   spike_features['time_peak'][spike_index] * samplerate)
            if end_relative:
                end_relative *= (samplerate / 1000)
                end += end_relative
                if end > len(trace):
                    end = len(trace)
        trace = trace[int(start):int(end)]
    except (IndexError):
        print('spike index out of bounds, trace = input trace')
        trace = trace
    return trace


def gen_drug_conditions(sweep_table):
    """Names and subdivided sweeps in drug conditions.

    Iterates trough a sweep_table containing drug description columns. Based
    on changes in drug concentrations a new drug condition name is generated.
    This way you can subset parts of an experiment.

    Args:
        - sweep_table: dataframe containing columns with name drug_#.

    Returns:
        - sweep_table: dataframe with new column: drug_condition.
    """
    columns = list(sweep_table.columns)
    r = re.compile("drug_.*")
    drug_cols = list(filter(r.match, columns))
    overwritten = False
    if len(drug_cols) == 0:
        print('Warning! no drug columns were found, please check sweep_table')
        return sweep_table
    if 'drug_condition' in drug_cols:
        print('Warning! drug_condition is already determined and will be over-'
              'written')
        overwritten = True
        drug_cols.pop(drug_cols.index('drug_condition'))
    condition_num = 0
    count = 0
    for sweep in sweep_table.index:
        condition = 'condition_%d' % condition_num
        if count != 0:
            change = np.nansum(sweep_table.loc[sweep, drug_cols].fillna(0) - \
                               sweep_table.loc[sweep - 1, drug_cols].fillna(
                                   0)) != 0
            if change:
                condition_num += 1
                condition = 'condition_%d' % condition_num
        sweep_table.loc[sweep, 'drug_condition'] = condition
        count += 1
    return sweep_table, overwritten


def find_sweep_median(sweep_table, column):
    """Finds the sweep name that is associated with the median of the column.

    In case of tie it will provide the first sweep for sweep and both sweeps for
    sweeps.

    Args:
        - sweep_table: dataframe containing sweeps and the column.
        - column: column for which the median needs to be calculated.

    Returns:
        - median: float
        - sweep: sweep representing the median, in case of tie the first sweep
        - sweeps: in case of ties both the sweeps.
    """
    median = sweep_table[column].median()
    sweeps = sweep_table.loc[sweep_table[column] == median]
    if len(sweeps) == 0:
        sweep_table.sort_values(by=column, inplace=True)
        sweeps_high = sweep_table[sweep_table[column] > median].iloc[0]
        sweeps_low = sweep_table[sweep_table[column] < median].iloc[-1]
        sweeps = pd.concat([sweeps_high, sweeps_low], axis=1).T
    sweep = sweeps.iloc[0]['sweep']
    return median, sweep, sweeps


def find_sweep_extreme(sweep_table, column, extreme):
    """Finds the sweep name that is associated with the median of the column.

    In case of tie it will provide the first sweep for sweep and both sweeps for
    sweeps.

    Args:
        - sweep_table: dataframe containing sweeps and the column.
        - column: column for which the median needs to be calculated.
        - extreme: string; 'min' or 'max' .

    Returns:
        - extreme: float
        - sweep: sweep representing the median, in case of tie the first sweep
        - sweeps: in case of ties both the sweeps.
    """
    if extreme == 'min':
        extreme = sweep_table[column].min()
    elif extreme == 'max':
        extreme = sweep_table[column].max()
    else:
        extreme = np.nan
    if np.isnan(extreme):
        sweep = ""
        sweeps = ""
    else:
        sweeps = sweep_table.loc[sweep_table[column] == extreme]
        sweep = sweeps.iloc[0]['sweep']
    return extreme, sweep, sweeps


def find_sweep_bool(sweep_table, column):
    """Finds the sweep name that is representing >= 50% of the column.

        Determines what the dominant boolean is (>=50%) in the column
        (if 50% True, it will use True). And take the first sweep that is True.

        Args:
            - sweep_table: dataframe containing sweeps and the column.
            - column: column for which the median needs to be calculated.

        Returns:
            - value: bool
            - sweep: sweep representing the median, in case of tie the first
            sweep
            - sweeps: in case of ties both the sweeps.
        """
    booleans = sweep_table[column].dropna()
    if len(booleans) == 0:
        value = np.nan
        sweeps = None
        sweep = None
        return value, sweep, sweeps
    value = False
    if sum(booleans) > 0:
        if len(booleans) / sum(booleans) >= 0.5:
            value = True
    sweeps = sweep_table.loc[sweep_table[column] == value]
    sweep = sweeps.iloc[0]['sweep']
    return value, sweep, sweeps


def fill_appending_table(at, ats, sweep_table, column, parameter, alt_col=None):
    """Calculates median and fill both median and sweep ref to dataframes.

    Args:
        - at: appending table containing the medians.
        - ats: appending table containing the sweep references.
        - sweep_table: source table from which median is being calculated.
        - column: column to calculate the median in.
        - parameter: string: 'min', 'max', 'median'.
        - alt_col: string of column name the data should be stored at in the
                   appending tables.

    Returns:
        - at: appending table containing the medians. Updated with median
        - ats: appending table containing the sweep references. Updated with
               sweep reference
    """
    # test whether values present
    value = np.nan
    sweep = ''
    if column not in sweep_table.columns:
        return at, ats
    if len(sweep_table) - np.sum(sweep_table[column].isna()) == 0:
        return at, ats
    if parameter == 'median':
        value, sweep, _ = find_sweep_median(sweep_table, column)
    if parameter == 'min' or parameter == 'max':
        value, sweep, _ = find_sweep_extreme(sweep_table, column, parameter)
    if parameter == 'bool':
        value, sweep, _ = find_sweep_bool(sweep_table, column)
    if alt_col:
        column = alt_col
    at.loc[0, column] = value
    ats.loc[0, column] = sweep
    return at, ats


def filter_by_criteria(sweep_table,
                       patch_rms=0.5,
                       noise_rms=0.07,
                       parameters=None):
    """Filters sweep table by patch and noise rms thresholds for specific
    parameters

    :param sweep_table: pandas dataframe
    :param patch_rms: float, threshold above which parameters are turned into
    np.nan
    :param noise_rms: float, threshold above which parameters are turned into
    np.nan
    :param parameters: selected parameters for which the filtering should take
    place
    :return: sweep_tabl: pandas dataframe
    """
    if isinstance(parameters, list):
        sweep_table.at[
            sweep_table['patch_rms'] > patch_rms, parameters] = np.nan
        sweep_table.at[
            sweep_table['patch_rms'] > noise_rms, parameters] = np.nan
    return sweep_table


def calculate_features(experiment, experiment_folder,
                       save_sweep_feature_table=True,
                       sweep_feature_savepath=
                       os.path.join(bcs_global.path_ephys_analysis,
                                    'sweep_feature_tables'),
                       skip_analyzed_experiment=False,
                       number_spikes_to_extract=3):
    """Sweep-wise calculation of features.

    Args:
        - experiment: nested dictionary.
        - experiment_folder: fullpath to directory where traces are stored.
        - save_sweep_feature_table: bool, True: saves/updates
                                    sweep_feature_table in traces directory.
        - skip_analyzed_experiment: bool, True: stop with extraction and leave
                                     message that the experiment already has
                                     been analyzed.
        - number_spikes_to_extract: integer, number of spikes for which the
                                    spike-specific features are extracted from.
    Returns:
        - sweep_feature_table: pandas dataframe, copy of sweep_table and
                               sweep_drug_note_table.
    """
    # open and combine sweep_table and sweep_drug_note_table .csv and append
    # to each other. Add feature columns to the dataframe.
    cellID = os.path.basename(experiment_folder)
    sweep_table_filename = os.path.join(experiment_folder,
                                        (cellID + '_sweep_table.csv'))
    sweep_drug_note_table_filename = os.path.join(experiment_folder,
                                                  (cellID +
                                                   '_sweep_drug_note_table.csv'
                                                   ))
    sweep_feature_table_filename = os.path.join(sweep_feature_savepath,
                                                (cellID +
                                                 '_sweep_feature_table.csv'))
    if not os.path.exists(sweep_table_filename):
        print('Warning! no sweep_table found for :' + experiment_folder)
        sweep_feature_table = None
        return sweep_feature_table
    if not os.path.exists(sweep_drug_note_table_filename):
        print('Warning! no sweep_drug_note_table found for :' +
              experiment_folder)
        sweep_feature_table = None
        return sweep_feature_table
    if os.path.exists(sweep_feature_table_filename):
        if skip_analyzed_experiment:
            print('Skipping feature calculation for :' + experiment_folder)
            sweep_feature_table = pd.read_csv(sweep_feature_table_filename)
            return sweep_feature_table
        else:
            print('Sweep_feature_table was found but is rebuild :' +
                  experiment_folder + '\nTo avoid rebuild use skip_analyzed_' +
                  'experiment')

    sweep_table = pd.read_csv(sweep_table_filename)
    sweep_drug_note_table = pd.read_csv(sweep_drug_note_table_filename)
    # empty cells in columns discard and discard_comments result in dtype:object
    # therefore make sure that both columns in both sweep_tables are converted
    # to bool and dtype respectively.
    sweep_table['discard_comments'] = sweep_table['discard_comments']. \
        astype(str)
    sweep_drug_note_table['discard_comments'] = sweep_drug_note_table \
        ['discard_comments'].astype(str)
    if len(sweep_table) == len(sweep_drug_note_table):
        for i in sweep_table.index:
            if isinstance(sweep_table.loc[i, 'discard'], float):
                sweep_table.loc[i, 'discard'] = False
            if isinstance(sweep_drug_note_table.loc[i, 'discard'], float):
                sweep_drug_note_table.loc[i, 'discard'] = False
            if sweep_table.loc[i, 'discard'] == True or \
                    sweep_drug_note_table.loc[i, 'discard'] == True:
                sweep_table.loc[i, 'discard'] = True
                sweep_drug_note_table.loc[i, 'discard'] = True
                comment_1 = sweep_table.loc[i, 'discard_comments']
                comment_2 = sweep_drug_note_table.loc[i, 'discard_comments']
                if comment_1 == 'nan':
                    comment_1 = ''
                if comment_2 == 'nan':
                    comment_2 = ''
                comments = (comment_1 + ' ' + comment_2)
                sweep_table.loc[i, 'discard_comments'] = comments
                sweep_drug_note_table.loc[i, 'discard_comments'] = comments
    else:
        print('Inequal size dataframes, inspect sweep_table and '
              'sweep_drug_note_table')
        sweep_feature_table = None
        return sweep_feature_table
    sweep_feature_table = pd.merge(sweep_table,
                                   sweep_drug_note_table,
                                   on=['sweep', 'filepath', 'cellAmpID',
                                       'amplifier', 'amp_mode', 'discard',
                                       'discard_comments'])
    feature_file = os.path.join(os.path.dirname(__file__),
                                'featureSelection.csv')
    feature_details = pd.read_csv(feature_file, index_col='feature')
    features = list(feature_details.index)
    features_all = list()
    for i in features:
        if re.search('_#_', i):
            for x in np.arange(1, number_spikes_to_extract + 1):
                insert_feature = i.split('#')[0] + str(x) + i.split('#')[1]
                features_all.append(insert_feature)
        else:
            features_all.append(i)
    current_cols = list(sweep_feature_table.columns)
    sweep_feature_table = (sweep_feature_table.reindex(columns=(current_cols +
                                                                features_all)))

    for sweep in sweep_feature_table.index:
        if sweep_feature_table.loc[sweep, 'discard']:
            continue
        if not isinstance(sweep_table.loc[1, 'ephys_stim_pulseType'], str):
            continue
        if not sweep_feature_table.loc[sweep, 'ephys_stim_enabled']:
            continue
        samplerate = int(sweep_feature_table.loc[sweep, 'samplerate'])
        stim_start = int(
            sweep_feature_table.loc[sweep, 'ephys_stim_pulseDelay'])
        if not sweep_feature_table.loc[sweep, 'ephys_stim_pulseWidth'] == 0 or\
            np.isnan(sweep_feature_table.loc[sweep, 'ephys_stim_pulseWidth'] == 0):
            stim_stop = int(stim_start + \
                            sweep_feature_table.loc[
                                sweep, 'ephys_stim_pulseWidth'])
        else:
            stim_stop = int(stim_start)
        stim_amplitude = sweep_feature_table.loc[sweep,
                                                 'ephys_stim_pulseAmplitude']
        amplifier = sweep_feature_table.loc[sweep, 'amplifier']
        sweep_name = sweep_feature_table.loc[sweep, 'sweep']
        trace = experiment['acquired_data'][sweep_name]['ephys'][amplifier][
            'data']
        if sweep_feature_table.loc[sweep, 'amp_mode'] == 'V-Clamp':
            # get resistance features
            Rs = get_rs(trace, start=stim_start, end=stim_stop,
                        voltage_step=stim_amplitude)
            Ri = get_rm(trace, start=stim_start, end=stim_stop,
                        voltage_step=stim_amplitude)
            sweep_feature_table.loc[sweep, 'input_resistance'] = Ri
            sweep_feature_table.loc[sweep, 'series_resistance'] = Rs
        elif sweep_feature_table.loc[sweep, 'amp_mode'] == 'I-Clamp':
            if sweep_feature_table.loc[sweep, 'ephys_stim_pulseType'] == 'ramp':
                pre_stim = 0.1 * samplerate
                if stim_start < pre_stim:
                    pre_stim = stim_start
                if pre_stim != 0:
                    rmp = get_vm(trace, start=0, end=pre_stim)
                else:
                    rmp = trace[0]
                sweep_feature_table.loc[sweep, 'resting_membrane_potential'] = \
                    rmp
                # get ramp spike features
                stim_trace = \
                experiment['stim_data'][sweep_name]['ephys'][amplifier] \
                    ['stim_array']
                _, spike_features = get_spike_features(trace, start=stim_start,
                                                       end=stim_stop,
                                                       samplerate=samplerate)
                if np.isnan(spike_features['time_peak'][0]):
                    continue
                ap_number = len(spike_features['time_peak'])
                sweep_feature_table.loc[sweep, 'ap_number'] = ap_number
                train_features, _ = get_spike_train_features(
                    trace,
                    start=stim_start,
                    end=stim_stop,
                    samplerate=samplerate)
                first_spike = int((spike_features['time_peak'][0] -
                                   (spike_features['time_threshold'][0] *
                                    10 ** -3)) * samplerate)
                trace_to_fit = trace[stim_start:first_spike]
                time_to_fit = np.arange(0, len(trace_to_fit)) / samplerate
                if len(trace_to_fit) > 2:
                    try:
                        popt_lin, pcov_lin = curve_fit(fit_2lin,
                                                       time_to_fit,
                                                       trace_to_fit,
                                                       p0=(10, trace_to_fit[0]))
                        ramp_rate = popt_lin[0]
                        if ramp_rate <= 0:
                            # should not be negative
                            ramp_rate = np.nan
                        sweep_feature_table.loc[sweep, 'ramp_rate'] = ramp_rate
                    except (TypeError, RuntimeError):
                        pass
                if isinstance(train_features, np.ndarray):
                    if train_features['average_isi'] > 5:
                        sweep_feature_table.loc[sweep, 'ramp_burst'] = \
                            bool(train_features['ini_burst'])
                    else:
                        sweep_feature_table.loc[sweep, 'ramp_burst'] = False
                extract_spike_number = number_spikes_to_extract
                if ap_number < extract_spike_number:
                    extract_spike_number = ap_number
                for x in np.arange(0, extract_spike_number):
                    ind = x + 1
                    sweep_feature_table.loc[sweep,
                                            ('ap_' + str(ind) + '_height')] = \
                        spike_features['v_peak'][x]
                    sweep_feature_table.loc[sweep,
                                            ('ap_' + str(ind) + '_width')] = \
                        spike_features['width_spike'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_threshold')] = \
                        spike_features['vm_threshold'][x]
                    sweep_feature_table.loc[sweep,
                                            ('ap_' + str(ind) + '_through')] = \
                        spike_features['vm_through'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_rheobase')] = \
                        stim_trace[int((spike_features['time_peak'][x] -
                                        (spike_features['time_threshold'][
                                             x] / 1000)) *
                                       samplerate + stim_start)]
                    sweep_feature_table.loc[
                        sweep, ('ap_' + str(ind) +
                                '_relative_time_through')] = \
                        spike_features['rel_time_spend_through'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_fast_through')] = \
                        spike_features['vm_fast_through'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_slow_through')] = \
                        spike_features['vm_slow_through'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_upstroke_rate')] = \
                        spike_features['max_upstroke_rate'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_downstroke_rate')] = \
                        spike_features['max_downstroke_rate'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_up_downstroke_ratio')] = \
                        spike_features['upstroke_downstroke_ratio'][x]
            elif sweep_feature_table.loc[sweep, 'ephys_stim_pulseType'] == \
                    'squarePulseTrain' and \
                    sweep_feature_table.loc[sweep,
                                            'ephys_stim_pulseAmplitude'] < 0:
                # get subthreshold features
                pre_stim = 0.1 * samplerate
                if stim_start < pre_stim:
                    pre_stim = stim_start
                if pre_stim != 0:
                    rmp = get_vm(trace,
                                 start=0,
                                 end=pre_stim)
                else:
                    rmp = trace[0]
                sweep_feature_table.loc[sweep, 'resting_membrane_potential'] = \
                    rmp
                # calculate steady-state membrane potential
                ss_mp = get_vm(trace,
                               start=int(stim_stop -
                                         ((samplerate / 1000) * 50)),
                               end=stim_stop)
                ss_delta = rmp - ss_mp
                if 0 < ss_delta <= 10:
                    # get the membrane time constant of the initial trace,
                    # only when steady-state delta to resting membrane potential
                    # is equal or smaller than 10 mV and positive.
                    sweep_feature_table.loc[sweep,
                                            'membrane_time_constant_ini'] = \
                        get_mts_ini(trace,
                                    rmp=rmp,
                                    start=stim_start,
                                    end=stim_stop,
                                    samplerate=samplerate)
                # get the membrane time constant from the -25 pA current step
                if sweep_feature_table.loc[sweep,
                'ephys_stim_pulseAmplitude'] == -25:
                    sweep_feature_table.loc[sweep, 'membrane_time_constant'] = \
                        get_mts(trace,
                                rmp=rmp,
                                start=stim_stop,
                                samplerate=samplerate)
                sag_features = get_vsag_fraction(trace,
                                                 rmp,
                                                 start=stim_start,
                                                 end=stim_stop,
                                                 samplerate=samplerate)
                if not isinstance(sag_features, float):
                    sweep_feature_table.loc[sweep, 'vsag_fraction'] = \
                        sag_features['vsag_fraction']
                    sweep_feature_table.loc[sweep, 'vsag_time_point'] = \
                        sag_features['time_vsag']
                    sweep_feature_table.loc[sweep, 'vsag_minimum_potential'] = \
                        sag_features['vm_max_hyperpolarization']
            elif sweep_feature_table.loc[sweep, 'ephys_stim_pulseType'] == \
                    'squarePulseTrain' and \
                    sweep_feature_table.loc[sweep,
                                            'ephys_stim_pulseAmplitude'] == 0:
                # get resting membrane potential
                pre_stim = 0.1 * samplerate
                rmp = get_vm(trace, start=0, end=pre_stim)
                sweep_feature_table.loc[sweep, 'resting_membrane_potential'] = \
                    rmp
            elif sweep_feature_table.loc[sweep, 'ephys_stim_pulseType'] == \
                    'squarePulseTrain' and \
                    sweep_feature_table.loc[sweep,
                                            'ephys_stim_pulseAmplitude'] > 0:
                pre_stim = 0.1 * samplerate
                if stim_start < pre_stim:
                    pre_stim = stim_start
                if pre_stim != 0:
                    rmp = get_vm(trace, start=0, end=pre_stim)
                else:
                    rmp = trace[0]
                sweep_feature_table.loc[sweep, 'resting_membrane_potential'] = \
                    rmp
                _, spike_features = get_spike_features(trace, start=stim_start,
                                                       end=stim_stop,
                                                       samplerate=samplerate)
                if np.isnan(spike_features['time_peak'][0]):
                    continue
                ap_number = len(spike_features['time_peak'])
                sweep_feature_table.loc[sweep, 'ap_number'] = ap_number
                train_features, _ = get_spike_train_features(
                    trace,
                    start=stim_start,
                    end=stim_stop,
                    samplerate=samplerate)
                if ap_number >= 1:
                    sweep_feature_table.loc[sweep, 'ap_latency'] = \
                        train_features['latency']
                if ap_number >= 3:
                    sweep_feature_table.loc[sweep, 'first_ap_isi'] = \
                        train_features['first_isi']
                    sweep_feature_table.loc[sweep, 'average_isi'] = \
                        train_features['average_isi']
                    sweep_feature_table.loc[sweep, 'cv_isi'] = \
                        train_features['isi_cv']
                    sweep_feature_table.loc[sweep, 'average_ap_rate'] = \
                        train_features['average_firing_rate']
                    sweep_feature_table.loc[sweep, 'ap_adaptation'] = \
                        train_features['adaptation_index']
                    sweep_feature_table.loc[sweep, 'ap_latency'] = \
                        train_features['latency']
                    sweep_feature_table.loc[sweep, 'ap_delay'] = \
                        bool(train_features['delay'])
                    if train_features['average_isi'] > 5:
                        sweep_feature_table.loc[sweep, 'ap_burst'] = \
                            bool(train_features['ini_burst'])
                    else:
                        sweep_feature_table.loc[sweep, 'ap_burst'] = False
                    sweep_feature_table.loc[sweep, 'ap_pause'] = \
                        bool(train_features['pause'])
                    sweep_feature_table.loc[sweep, 'ap_desens_height'] = \
                        spike_features['v_peak'][-1] / \
                        spike_features['v_peak'][0]
                    sweep_feature_table.loc[sweep, 'ap_desens_width'] = \
                        spike_features['width_spike'][-1] / \
                        spike_features['width_spike'][0]
                if ap_number >= 10:
                    sweep_feature_table.loc[sweep, 'potential_burst'] = \
                        train_features['potential_burst']
                    sweep_feature_table.loc[sweep, 'r_cv'] = \
                        train_features['r_cv']
                    sweep_feature_table.loc[sweep, 'fraction_long_isi'] = \
                        train_features['fr_long']
                    sweep_feature_table.loc[sweep, 'fraction_short_isi'] = \
                        train_features['fr_short']
                    sweep_feature_table.loc[sweep, 'initial_burst'] = \
                        train_features['ini_burst']
                    sweep_feature_table.loc[sweep, 'longest_burst'] = \
                        train_features['longest_burst']
                ####################
                extract_spike_number = number_spikes_to_extract
                if ap_number < extract_spike_number:
                    extract_spike_number = ap_number
                for x in np.arange(0, extract_spike_number):
                    ind = x + 1
                    sweep_feature_table.loc[sweep,
                                            ('ap_' + str(ind) + '_height')] = \
                        spike_features['v_peak'][x]
                    sweep_feature_table.loc[sweep,
                                            ('ap_' + str(ind) + '_width')] = \
                        spike_features['width_spike'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_threshold')] = \
                        spike_features['vm_threshold'][x]
                    sweep_feature_table.loc[sweep,
                                            ('ap_' + str(ind) + '_through')] = \
                        spike_features['vm_through'][x]
                    sweep_feature_table.loc[sweep,
                                            ('ap_' + str(ind) +
                                             '_relative_time_through')] = \
                        spike_features['rel_time_spend_through'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_fast_through')] = \
                        spike_features['vm_fast_through'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_slow_through')] = \
                        spike_features['vm_slow_through'][x]
                    # added fAHP and sAHP
                    fAHP = spike_features['vm_threshold'][x] -\
                           spike_features['vm_fast_through'][x]
                    # make sure that fAHP is always positive, if negative this
                    # means that the fast_through is more depolarized than
                    # the threshold.
                    if fAHP < 0:
                        fAHP = 0
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_fAHP')] = fAHP
                    sAHP = spike_features['vm_threshold'][x] - \
                           spike_features['vm_slow_through'][x]
                    # make sure that sAHP is always positive, if negative this
                    # means that the slow_through is more depolarized than
                    # the threshold.
                    if sAHP < 0:
                        sAHP = 0
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_sAHP')] = sAHP
                    # added above to add sAHP and fAHP
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_upstroke_rate')] = \
                        spike_features['max_upstroke_rate'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_downstroke_rate')] = \
                        spike_features['max_downstroke_rate'][x]
                    sweep_feature_table.loc[sweep, ('ap_' + str(ind) +
                                                    '_up_downstroke_ratio')] = \
                        spike_features['upstroke_downstroke_ratio'][x]
            else:
                continue

        else:
            continue
    if save_sweep_feature_table:
        cell_filename = cellID + '.csv'
        sweep_feature_table.to_csv(os.path.join(sweep_feature_savepath,
                                                cell_filename), index=False)
    return sweep_feature_table


def remove_depol_block(fi_table, thres=2):
    """Removes sweeps that show depolarization block.

    Iterates through the sweep_table (must include sweeps with APs)
    If at 'thres' number subsequent current stimulation amplitudes the ap
    firing rate drops or stabilizes. The second stimulation amplitude and
    subsequent are removed from the sweep_table.

    Args:
        - fi_table: pandas, sweep_table containing only sweeps with >=1 ap per
        sweep and more than 1 current injection.
        - thres: integer, number of subsequent stimulation amplitudes that show
        reduction in or plateaus.

    Returns:
        - fi_table: updated fi_table
        - ap_fire_current_rate: increase in firing frequency per pA.
    """
    fi_table = fi_table.dropna(axis=0, how='any')
    if len(fi_table.index) <= 1:
        ap_fire_current_rate = None
        r_squared = None
        return fi_table, ap_fire_current_rate, r_squared
    deriv = np.gradient(fi_table)
    below = deriv <= 0
    ind = None
    for i in np.arange(0, len(below)):
        try:
            chunk = below[i:i + thres]
            if sum(chunk) == thres:
                ind = i
                break
        except IndexError:
            break
    if ind:
        fi_table = fi_table.head(ind + 1)
        if ind == 0:
            ap_fire_current_rate = None
            r_squared = None
            return fi_table, ap_fire_current_rate, r_squared
    popt, pcov = curve_fit(fit_2lin_fixb_null,
                           fi_table.index,
                           fi_table,
                           p0=0.01)
    ap_fire_current_rate = popt[0]
    res = fi_table - fit_2lin_fixb_null(fi_table.index, *popt)
    ss_res = np.sum(res ** 2)
    ss_tot = np.sum((fi_table - np.mean(fi_table)) ** 2)
    r_squared = 1 - (ss_res / ss_tot)
    # check whether r_squared is inf:
    if np.isinf(r_squared):
        r_squared = 1
    return fi_table, ap_fire_current_rate, r_squared


def generate_cellAmpID_feature_table(db_path=bcs_global.path_database,
                                     cell_database_filename=
                                     bcs_global.filename_cell_db,
                                     sweep_feature_table_path=
                                     os.path.join(
                                         bcs_global.path_ephys_analysis,
                                         'sweep_feature_tables'),
                                     save_feature_table=True,
                                     update_sweep_table=False,
                                     ap_trains_criteria='rheobase_plus_125pA',
                                     feature_savepath=
                                     bcs_global.path_ephys_analysis):
    """Generates a feature_table based on all sweep_feature_tables of cells.

    Iterates through a directory containing sweep_feature_tables, extracts
    cell-wide features and combines it with the already extracted features into
    a dataframe called feature_table.

    Args:
        - db_path: full path to the folder containing the databases.
        - cell_database_filename: full name of the cell database.
        - sweep_feature_table_path: full path to the folder containing all
                                    sweep_feature_tables.
        - save_feature_table: bool, True: saves the feature_table.
        - ap_trains_criteria: string, select the current injection at which
            the spike trains need to be analyzed. Current options: 'rheobase_plus_50pA'
            and '10_spikes'.
        - feature_savepath: full path to the location where to save the feature_
                            table.


    Returns:
        - feature_table: dataframe containing all features organized per
                         cellAmpID.
    """
    assert ap_trains_criteria in ['rheobase_plus_50pA', 'rheobase_plus_100pA', 'rheobase_plus_125pA', '10_spikes'], \
    "Use either 'rheobase_plus_125pA', 'rheobase_plus_100pA', 'rheobase_plus_50pA' or '10_spikes' for ap_trains_criteria"
    df_cell = readDBCSV(db_path, cell_database_filename)
    df_cell = df_cell.set_index('cellAmpID')
    table_list = os.listdir(sweep_feature_table_path)
    manual_readouts_path = os.path.join(bcs_global.path_ephys_data,
                                        'manual_ephys_readouts.csv')
    if os.path.isfile(manual_readouts_path):
        df_manual = readDBCSV(bcs_global.path_ephys_data,
                              'manual_ephys_readouts.csv')
        df_manual = df_manual.set_index('cellAmpID')
    else:
        df_manual = None
    columns = ['cellAmpID',
               'filename',
               'rel_soma_position',
               'distance_soma_pia',
               'noise_rms',
               'patch_rms',
               'drug_condition',
               'drug_TTX',
               'drug_CPP',
               'drug_NBQX',
               'drug_CGP-55845',
               'drug_SR-95531',
               'drug_MPEP',
               'drug_picrotoxin',
               'drug_4AP',
               'drug_dopamine',
               'drug_SCH23390',
               'drug_quinpirole',
               'drug_sulpiride',
               'drug_SKF-821297',
               'drug_propranolol',
               'drug_MNI-glutamate',
               'drug_CGP-52432',
               'resting_membrane_potential',
               'input_resistance',
               'series_resistance',
               'membrane_time_constant',
               'membrane_time_constant_ini',
               'membrane_time_constant_ini_-25',
               'vsag_fraction_at-300pA',
               'vsag_time_point_at-300pA',
               'vsag_minimum_potential_at-300pA',
               'vsag_fraction_delta15mV',
               'vsag_time_point_delta15mV',
               'vsag_minimum_potential_delta15mV',
               'vsag_fraction_at100mV',
               'vsag_time_point_at100mV',
               'vsag_minimum_potential_at100mV',
               'ramp_rate',
               'ramp_burst',
               'ramp_ap_1_rheobase',
               'ramp_ap_2_rheobase',
               'ramp_ap_3_rheobase',
               'rheobase',
               'charge_rheobase',
               'ap_number_rheobase',
               'ap_1_height',
               'ap_2_height',
               'ap_3_height',
               'ap_1_width',
               'ap_2_width',
               'ap_3_width',
               'ap_1_threshold',
               'ap_2_threshold',
               'ap_3_threshold',
               'ap_1_through',
               'ap_2_through',
               'ap_3_through',
               'ap_1_relative_time_through',
               'ap_2_relative_time_through',
               'ap_3_relative_time_through',
               'ap_1_fast_through',
               'ap_2_fast_through',
               'ap_3_fast_through',
               'ap_1_slow_through',
               'ap_2_slow_through',
               'ap_3_slow_through',
               'ap_1_upstroke_rate',
               'ap_2_upstroke_rate',
               'ap_3_upstroke_rate',
               'ap_1_downstroke_rate',
               'ap_2_downstroke_rate',
               'ap_3_downstroke_rate',
               'ap_1_up_downstroke_ratio',
               'ap_2_up_downstroke_ratio',
               'ap_3_up_downstroke_ratio',
               'ap_1_fAHP',
               'ap_1_AHP',
               'ap_1_latency',
               'ap_2_AHP_train',
               'current_train_features',
               'ap_1_threshold_train', # for quality control purpose; can be compared to ap_1_threshold at rheobase
               'first_ap_isi',
               'average_isi',
               'cv_isi',
               'average_ap_rate',
               'ap_adaptation',
               'ap_fire_current_rate',
               'ap_fire_current_rsquared',
               'ap_latency',
               'ap_delay',
               'ap_burst',
               'ap_pause',
               'ap_desens_height',
               'ap_desens_width',
               'ap_potential_burst',
               'ap_r_cv',
               'ap_fraction_long_isi',
               'ap_fraction_short_isi',
               'ap_initial_burst',
               'ap_longest_burst']

    # with median values
    feature_table = pd.DataFrame(columns=columns)
    # with sweep names of the median value
    feature_sweep_table = pd.DataFrame(columns=columns)

    for file in table_list:
        if not '.csv' in file:
            continue
        print(file)
        sweep_table = None
        sweep_table = pd.read_csv(os.path.join(sweep_feature_table_path,
                                               file))
        sweep_table, overwritten = gen_drug_conditions(sweep_table)
        if not overwritten and update_sweep_table:
            sweep_table.to_csv(os.path.join(sweep_feature_table_path,
                                            file), index=False)
        sweep_table = sweep_table.loc[sweep_table['discard'] == False, :]
        cellAmpIDs = sweep_table['cellAmpID'].unique()
        for cellAmpID in cellAmpIDs:
            if not cellAmpID in df_cell.index:
                print('Warning!: ' + cellAmpID + ' is not described in the ' +
                      'cell database. Please add information first before '
                      'adding the features of this cell.')
                continue
            subset = None
            subset = sweep_table.loc[sweep_table['cellAmpID'] == cellAmpID, :]
            # run a discard function on the subset table
            subset = filter_by_criteria(sweep_table=subset,
                                        patch_rms=0.5,
                                        noise_rms=0.07,
                                        parameters=[
                                            'membrane_time_constant',
                                        ])
            conditions = list(subset['drug_condition'].unique())
            for condition in conditions:
                rheobase = None
                print(condition)
                subset_cond = None
                subset_cond = subset.loc[subset['drug_condition'] == condition,
                              :]
                samplerate = subset_cond.iloc[0]['samplerate']
                nandata = np.empty((1, len(columns)))
                nandata[:] = np.nan
                at = pd.DataFrame(nandata, columns=columns)  # appending table
                ats = pd.DataFrame(nandata, columns=columns)  # appending table
                at.loc[0, 'cellAmpID'] = cellAmpID
                ats.loc[0, 'cellAmpID'] = cellAmpID
                at.loc[0, 'filename'] = os.path.join(sweep_feature_table_path,
                                                     file)
                ats.loc[0, 'filename'] = os.path.join(sweep_feature_table_path,
                                                      file)
                at.loc[0, 'rel_soma_position'] = df_cell.loc[cellAmpID,
                                                             'somaCortex' +
                                                             'Position']
                ats.loc[0, 'rel_soma_position'] = df_cell.loc[cellAmpID,
                                                              'somaCortex' +
                                                              'Position']
                if df_cell.loc[cellAmpID, 'cortexThicknessImage']:
                    cortexThickness = df_cell.loc[
                        cellAmpID, 'cortexThicknessImage']
                elif df_cell.loc[cellAmpID, 'cortexThicknessSWC']:
                    cortexThickness = df_cell.loc[
                        cellAmpID, 'cortexThicknessSWC']
                else:
                    cortexThickness = None
                if cortexThickness:
                    somaPos = (df_cell.loc[cellAmpID, 'somaCortexPosition'] *
                               cortexThickness)
                    at.loc[0, 'distance_soma_pia'] = somaPos
                    ats.loc[0, 'distance_soma_pia'] = somaPos
                at, ats = fill_appending_table(at, ats, subset_cond,
                                               'noise_rms', 'median')
                at, ats = fill_appending_table(at, ats, subset_cond,
                                               'patch_rms', 'median')
                r = re.compile("drug_.*")
                drug_cols = list(filter(r.match, columns))
                # make sure that the drug_cols are also present in the
                # drug_feature_table
                drug_cols_cell = list(filter(r.match,
                                             subset_cond.columns.to_list()))
                drug_cols = list(set(drug_cols).intersection(drug_cols_cell))
                at.loc[0, drug_cols] = subset_cond.iloc[0][drug_cols]
                ats.loc[0, drug_cols] = subset_cond.iloc[0][drug_cols]
                subset_cond_s = None
                subset_cond_s = subset_cond[
                    subset_cond['resting_membrane_potential'].notnull()]

                if len(subset_cond_s) >= 1:
                    at, ats = fill_appending_table(at, ats,
                                                   subset_cond_s.iloc[:20],
                                                   'resting_membrane_potential',
                                                   'median')
                subset_cond_s = None
                subset_cond_s = subset_cond[np.logical_and(
                    subset_cond['ephys_stim_pulseAmplitude'].
                        between(-150, -1, inclusive=True),
                    subset_cond['ephys_stim_pulseWidth'] >= 100 *
                    (samplerate/1000))]
                if len(subset_cond_s) >= 1:
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'membrane_time_constant_ini',
                                                   'median',
                                                   alt_col=
                                                   'membrane_time_constant_ini')
                    fit_pA = subset_cond_s['ephys_stim_pulseAmplitude']
                    fit_rmp = subset_cond_s['resting_membrane_potential']
                    fit_mV = subset_cond_s['vsag_minimum_potential']
                    fit_mV = fit_mV - fit_rmp
                    fit_pA = fit_pA[fit_mV.notna()]
                    fit_mV = fit_mV[fit_mV.notna()]
                    # fit_pA need to contain at least three steps
                    if len(np.unique(fit_pA)) >= 3:
                        popt_lin, _ = curve_fit(fit_2lin_fixb_null,
                                                fit_pA,
                                                fit_mV,
                                                p0=0.05)
                        # changed fitting to fixed to 0 and made sure the IV
                        # is based on the delta voltage relative to resting
                        # potential.
                        Rin = popt_lin[0] * 1000  # MOhm
                        at.loc[0, 'input_resistance'] = Rin
                    else:
                        at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                       'input_resistance',
                                                       'median')
                    if at.loc[0, 'input_resistance'] <= 0:
                        # cannot be negative so replace by nan
                        at.loc[0, 'input_resistance'] = np.nan
                    if np.isnan(at.loc[0, 'input_resistance']) and \
                            isinstance(df_manual, pd.DataFrame):
                        if cellAmpID in df_manual.index:
                            at.loc[0, 'input_resistance'] = \
                                df_manual.loc[cellAmpID, 'input_resistance']
                            ats.loc[0, 'input_resistance'] = None
                rs_subset = None
                rs_subset = subset_cond[subset_cond['ephys_stim_pulseDelay'] <
                                        589000]
                # in a big subset of experiments this 589000 pulseDelay was used
                # to give a rs check pulse during a 60 seconds recording of
                # spontaneous inputs. However for some unclear reason it did not
                # give the pulse. But in the meta data it is not clear how to
                # pick this up. So for now these epochs are just ignored for
                # estimating rs.
                at, ats = fill_appending_table(at, ats, rs_subset,
                                               'series_resistance', 'median')
                if at.loc[0, 'series_resistance'] > 30 and \
                    isinstance(df_manual, pd.DataFrame):
                    # in this case it is good to compare the initial value and
                    # take the initial value from df_manual and provide a
                    # warning that user needs to inspect the Rs during
                    # experiment
                    if cellAmpID in df_manual.index:
                        rs = float(df_manual.loc[cellAmpID, 'series_resistance'])
                        measured_rs = float(at.loc[0, 'series_resistance'])
                        if rs < at.loc[0, 'series_resistance']:

                            at.loc[0, 'series_resistance'] = rs
                            ats.loc[0, 'series_resistance'] = None
                            print('Warning, initial Rs is used for summary. : '
                                  + str(rs) + ' MOhm \n'
                                  'However the data suggests that there is a '
                                  'incline in Rs during the experiment, '
                                  'median: ' + str(measured_rs) + ' MOhm \n'
                                  'Please review cellAmpID: ' + cellAmpID)
                if np.isnan(at.loc[0, 'series_resistance']) and \
                        isinstance(df_manual, pd.DataFrame):
                    if cellAmpID in df_manual.index:
                        at.loc[0, 'series_resistance'] = \
                            df_manual.loc[cellAmpID, 'series_resistance']
                        ats.loc[0, 'series_resistance'] = None

                at, ats = fill_appending_table(at, ats, subset_cond,
                                               'membrane_time_constant',
                                               'median')
                subset_cond_s = None
                subset_cond_s = subset_cond[np.logical_and(
                    subset_cond['ephys_stim_pulseAmplitude'] == -25,
                    subset_cond['ephys_stim_pulseWidth'] >= 100 *
                    (samplerate/1000))]
                at, ats = fill_appending_table(at, ats, subset_cond_s,
                                               'membrane_time_constant_ini',
                                               'median',
                                               alt_col=
                                               'membrane_time_constant_ini-25')
                subset_cond_s = None
                subset_cond_s = subset_cond[np.logical_and(
                    subset_cond['ephys_stim_pulseAmplitude'] == -300,
                    subset_cond['ephys_stim_pulseWidth'] >= 100 *
                    (samplerate/1000))]
                at, ats = fill_appending_table(at, ats, subset_cond_s,
                                               'vsag_fraction',
                                               'median',
                                               alt_col='vsag_fraction_at-300pA')
                at, ats = fill_appending_table(at, ats, subset_cond_s,
                                               'vsag_time_point',
                                               'median',
                                               alt_col='vsag_time_point_' +
                                                       'at-300pA')
                at, ats = fill_appending_table(at, ats, subset_cond_s,
                                               'vsag_minimum_potential',
                                               'median',
                                               alt_col='vsag_minimum_' +
                                                       'potential_at-300pA')
                if not np.isnan(at.loc[0, 'resting_membrane_potential']):
                    delta15 = at.loc[0, 'resting_membrane_potential'] - 15
                    subset_cond_s = None
                    subset_cond_s = subset_cond[
                        subset_cond['vsag_minimum_potential'].
                            between(delta15 - 1, delta15 + 1, inclusive=True)]
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'vsag_fraction',
                                                   'median',
                                                   alt_col='vsag_fraction' +
                                                           '_delta15mV')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'vsag_time_point',
                                                   'median',
                                                   alt_col='vsag_time_point' +
                                                           '_delta15mV')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'vsag_minimum_potential',
                                                   'median',
                                                   alt_col='vsag_minimum_' +
                                                           'potential_' +
                                                           'delta15mV')
                    try:
                        subset_cond_s = None
                        subset_cond_s = \
                            subset_cond.loc[(subset_cond[
                                                 'vsag_minimum_potential'] +
                                             100).
                                            abs().
                                            sort_values().
                                            index[:3]]
                        at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                       'vsag_fraction',
                                                       'min',
                                                       alt_col='vsag_fraction' +
                                                               '_at100mV')
                        at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                       'vsag_time_point',
                                                       'min',
                                                       alt_col='vsag_time' +
                                                               '_point_at' +
                                                               '100mV')
                        at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                       'vsag_minimum_potential',
                                                       'min',
                                                       alt_col='vsag_minimum_' +
                                                               'potential_at' +
                                                               '100mV')
                    except (KeyError, IndexError):
                        pass
                at, ats = fill_appending_table(at, ats, subset_cond,
                                               'ramp_rate',
                                               'median')
                subset_cond_s = None
                subset_cond_s = subset_cond[subset_cond
                                            ['ephys_stim_pulseType'] == 'ramp']
                ramps = len(subset_cond_s)
                bursts = sum(subset_cond_s['ramp_burst'])
                burst = False
                if ramps >= 1 and not subset_cond_s['ramp_burst'].empty:
                    if bursts / ramps >= 0.5:
                        # if 50% or more of the ramp trials show
                        # bursts then consider ramp burst true
                        burst = True
                        sweeps = None
                        sweeps = subset_cond_s[
                            subset_cond_s['ramp_burst'] == True]
                        sweep = sweeps.iloc[0]['sweep']
                    else:
                        sweeps = None
                        sweeps = subset_cond_s[
                            subset_cond_s['ramp_burst'] == False]
                        try:
                            sweep = sweeps.iloc[0]['sweep']
                        except IndexError:
                            sweep = None
                            pass
                    at.loc[0, 'ramp_burst'] = burst
                    ats.loc[0, 'ramp_burst'] = sweep
                if 'ap_1_rheobase' in list(subset_cond.columns):
                    at, ats = fill_appending_table(at, ats, subset_cond,
                                                   'ap_1_rheobase',
                                                   'median',
                                                   alt_col='ramp_ap_1_rheobase')
                if 'ap_2_rheobase' in list(subset_cond.columns):
                    at, ats = fill_appending_table(at, ats, subset_cond,
                                                   'ap_2_rheobase',
                                                   'median',
                                                   alt_col='ramp_ap_2_rheobase')
                if 'ap_3_rheobase' in list(subset_cond.columns):
                    at, ats = fill_appending_table(at, ats, subset_cond,
                                                   'ap_3_rheobase',
                                                   'median',
                                                   alt_col='ramp_ap_3_rheobase')
                subset_cond_s = None
                subset_cond_sl = None
                subset_cond_sl = subset_cond[
                    subset_cond['ephys_stim_pulseType'] == 'squarePulseTrain']
                samplerate = subset_cond_sl.iloc[0]['samplerate']
                sweeps = \
                    list(subset_cond_sl.loc[
                             ((subset_cond_sl['ephys_stim_pulseWidth'] <= 0.5 *
                               samplerate) &
                              (subset_cond_sl['amp_mode'] == 'I-Clamp')),
                             'sweep'])
                subset_cond_s = \
                    subset_cond_sl[subset_cond['sweep'].isin(sweeps)]
                subset_cond_s = subset_cond_s.sort_values(
                    by='ephys_stim_pulseAmplitude')
                rheo_s = None
                rheo_s = subset_cond_s[subset_cond_s['ap_number'] >= 1]
                if rheo_s.empty:
                    subset_cond_s = None
                    sweeps = \
                        list(subset_cond_sl.loc[(
                                (subset_cond_sl['ephys_stim_pulseWidth'] > 0.5 *
                                 samplerate) &
                                (subset_cond_sl['amp_mode'] == 'I-Clamp')),
                                                'sweep'])
                    subset_cond_s = \
                        subset_cond_sl[subset_cond_sl['sweep'].isin(sweeps)]
                    subset_cond_s = subset_cond_sl.sort_values(
                        by='ephys_stim_pulseAmplitude')
                rheo_s = subset_cond_s[np.logical_and(
                    subset_cond_s['ap_number'] >= 1,
                    subset_cond_s['ephys_stim_pulseWidth'] >= 25 * (samplerate
                                                                    / 1000))]
                # ap should be found in stimu pulse traces longer than 25 ms.
                if not rheo_s.empty:
                    rheo_s = rheo_s.sort_values(by='ap_number')
                    sweep_ind = rheo_s.index[0]
                    rheobase = subset_cond_s.loc[sweep_ind,
                                                 'ephys_stim_pulseAmplitude']
                    at.loc[0, 'rheobase'] = rheobase
                    ats.loc[0, 'rheobase'] = subset_cond_s.loc[sweep_ind,
                                                               'sweep']
                    charge_rheobase = rheobase * subset_cond_s.loc[sweep_ind,
                                                                   'ap_latency']
                    at.loc[0, 'charge_rheobase'] = charge_rheobase  # pA*ms
                    ats.loc[0, 'charge_rheobase'] = subset_cond_s.loc[
                        sweep_ind, 'sweep']
                    at.loc[0, 'ap_number_rheobase'] = \
                        subset_cond_s.loc[sweep_ind, 'ap_number']
                    ats.loc[0, 'ap_number_rheobase'] = subset_cond_s.loc[
                        sweep_ind, 'sweep']

                subset_cond_s = None
                sweeps = \
                    list(subset_cond_sl.loc[
                             ((subset_cond_sl['ephys_stim_pulseWidth'] > 0.5 *
                               samplerate) &
                             (subset_cond_sl['amp_mode'] == 'I-Clamp')),
                             'sweep'])
                subset_cond_s = subset_cond_sl[
                    subset_cond_sl['sweep'].isin(sweeps)]
                subset_cond_s = subset_cond_sl.sort_values(
                    by='ephys_stim_pulseAmplitude')
                rheo_s = subset_cond_s[subset_cond_s['ap_number'] >= 1]
                rheo_s['average_ap_rate'] = \
                    rheo_s['ap_number'] / \
                    (rheo_s['ephys_stim_pulseWidth'] / samplerate)
                if len(rheo_s['ephys_stim_pulseAmplitude'].unique()) > 1:
                    # determine firing rate/pA
                    rheo_s['ephys_stim_pulseAmplitude'] = \
                        rheo_s['ephys_stim_pulseAmplitude'].astype(int)
                    fi_table = rheo_s.groupby('ephys_stim_pulseAmplitude')[
                        'average_ap_rate'].mean()
                    fi_table, ap_fire_current_rate, r_squared = \
                        remove_depol_block(fi_table, thres=2)
                    at.loc[0, 'ap_fire_current_rate'] = ap_fire_current_rate
                    at.loc[0, 'ap_fire_current_rsquared'] = r_squared
                    # save the fi_table
                    fi_table.to_csv(os.path.join(feature_savepath, 'fi_tables',
                                                 cellAmpID + '.csv'))
                if rheobase:
                    # fill out all parameters related to first ap at rheobase
                    rheo_sweeps = subset_cond_sl[
                        subset_cond_sl['ephys_stim_pulseAmplitude'] ==
                        rheobase]
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_height', 'median')
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_width', 'median')
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_threshold', 'median')
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_through', 'median')
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_relative_time_through',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_fast_through',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_slow_through',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_upstroke_rate',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_downstroke_rate',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_up_downstroke_ratio',
                                                   'median')
                    # determine fAHP and sAHP based per sweep
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_fAHP',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_1_sAHP',
                                                   'median',
                                                   alt_col='ap_1_AHP')
                    # changed above to determine fAHP and sAHP per sweep.
                    at, ats = fill_appending_table(at, ats, rheo_sweeps,
                                                   'ap_latency',
                                                   'median',
                                                   alt_col='ap_1_latency')
                subset_cond_s = None
                subset_cond_sl = \
                    subset_cond[(subset_cond['ephys_stim_pulseType'] ==
                                 'squarePulseTrain') &
                                (subset_cond['ephys_stim_pulseWidth'] >= 0.5 *
                                 samplerate)]
                # choose between using the first current step that provides at least
                # 10 spikes, or the current step that is at least 50 pA above the rheobase
                if ap_trains_criteria == '10_spikes':
                    subset_cond_s = subset_cond_sl[
                        subset_cond_sl['ap_number'] >= 10]
                elif ap_trains_criteria == 'rheobase_plus_50pA':
                    if rheobase:
                        current_selection = np.ceil((rheobase+50)/50)*50
                        subset_cond_s = subset_cond_sl[
                            subset_cond_sl['ephys_stim_pulseAmplitude'] ==
                            current_selection]
                    else:
                        # in this case there is no rheobase known, in other words
                        # there is no spiking in this neuron. So just subset the data
                        # which will leave a empty dataframe. This will allow the code
                        # below to continue and return an empty entry for spike train
                        # analysis.
                        subset_cond_s = subset_cond_sl[subset_cond_sl['ap_number']>=1]
                elif ap_trains_criteria == 'rheobase_plus_100pA':
                    if rheobase:
                        current_selection = np.ceil((rheobase+100)/50)*50
                        subset_cond_s = subset_cond_sl[
                            subset_cond_sl['ephys_stim_pulseAmplitude'] ==
                            current_selection]
                    else:
                        # in this case there is no rheobase known, in other words
                        # there is no spiking in this neuron. So just subset the data
                        # which will leave a empty dataframe. This will allow the code
                        # below to continue and return an empty entry for spike train
                        # analysis.
                        subset_cond_s = subset_cond_sl[subset_cond_sl['ap_number']>=1]
                elif ap_trains_criteria == 'rheobase_plus_125pA':
                    if rheobase:
                        current_selection = np.ceil((rheobase+125)/50)*50
                        subset_cond_s = subset_cond_sl[
                            subset_cond_sl['ephys_stim_pulseAmplitude'] ==
                            current_selection]
                    else:
                        # in this case there is no rheobase known, in other words
                        # there is no spiking in this neuron. So just subset the data
                        # which will leave a empty dataframe. This will allow the code
                        # below to continue and return an empty entry for spike train
                        # analysis.
                        subset_cond_s = subset_cond_sl[subset_cond_sl['ap_number']>=1]
                if len(subset_cond_s) >= 1:
                    current_train_features = subset_cond_s[
                        'ephys_stim_pulseAmplitude'].min()
                    subset_cond_s = subset_cond_s[
                        subset_cond_s['ephys_stim_pulseAmplitude'] ==
                        current_train_features]
                    at.loc[0, 'current_train_features'] = current_train_features
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'ap_2_sAHP',
                                                   'median',
                                                   alt_col='ap_2_AHP_train')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'ap_1_threshold',
                                                   'median',
                                                   alt_col='ap_1_threshold_train')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'first_ap_isi',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'average_isi',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'cv_isi',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'average_ap_rate',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'ap_adaptation',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'ap_latency',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'ap_desens_height',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'ap_desens_width',
                                                   'median')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'r_cv',
                                                   'median',
                                                   alt_col='ap_r_cv')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'fraction_long_isi',
                                                   'median',
                                                   alt_col=
                                                   'ap_fraction_long_isi')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'fraction_short_isi',
                                                   'median',
                                                   alt_col=
                                                   'ap_fraction_short_isi')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'longest_burst',
                                                   'median',
                                                   alt_col=
                                                   'ap_longest_burst')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'ap_delay',
                                                   'bool')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'ap_burst',
                                                   'bool')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'ap_pause',
                                                   'bool')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'potential_burst',
                                                   'bool',
                                                   alt_col='ap_potential_burst')
                    at, ats = fill_appending_table(at, ats, subset_cond_s,
                                                   'initial_burst',
                                                   'bool',
                                                   alt_col='ap_initial_burst')
                # save
                feature_table = feature_table.append(at, ignore_index=True)
                feature_sweep_table = \
                    feature_sweep_table.append(ats, ignore_index=True)

    feature_table = feature_table[columns]
    feature_sweep_table = feature_sweep_table[columns]
    feature_table = feature_table.sort_values(by='cellAmpID')
    feature_sweep_table = feature_sweep_table.sort_values(by='cellAmpID')
    if save_feature_table:
        date_string = datetime.datetime.now()
        # generate feature file
        feature_filename = bcs_global.filebase_ephys_feature + \
                           date_string.strftime("%Y%m%d") + '.csv'
        feature_table.to_csv(os.path.join(feature_savepath,
                                          feature_filename), index=False)
        # generate key file
        key_feature_df = pd.read_csv(os.path.join(
            os.path.dirname(__file__), 'key_template_feature.csv'))
        key_feature_df.to_csv(
            os.path.join(feature_savepath, ('key_' + feature_filename)),
            index=False)
        # generate sweep refs file
        feature_sweep_filename = bcs_global.filebase_ephys_feature_sweeps + \
                                 date_string.strftime("%Y%m%d") + '.csv'
        feature_sweep_table.to_csv(
            os.path.join(feature_savepath, feature_sweep_filename), index=False)
        # generate key file
        key_feature_sweep_df = pd.read_csv(os.path.join(
            os.path.dirname(__file__), ('key_template_featuresweep.csv')))
        key_feature_sweep_df.to_csv(
            os.path.join(feature_savepath, ('key_' + feature_sweep_filename)),
            index=False)
    return feature_table, feature_sweep_table, subset_cond_s
