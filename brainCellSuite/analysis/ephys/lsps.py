"""Analysis of laser-scanning photostimulation experiments.

Based on ephus generated data; for maps this means that 1 acquired trace
contains all the stimulations that were given during one LSPS map.

Helper functions:
    - gen_feature_maps: Generate maps of features measured for each pixel.
    - gen_traces_matrix: Untangles the acquired trace to a 3D array.
    - flatten_trace_matrix: Flatten a 3d array of traces to 2d.
    - rotate_matrix: Rotation of the matrix based on pattern orientation.
    - plot_trace_matrix: Plot traces from a matrix.
    - check_sweep_table_lsps_parameters: Checks sweep_table and provides UI to
                                         update LSPS parameters.
    - transform_soma_position: Transform soma position relative to map.
    - cart2pol: Convert cartesian to polar coordinate.
    - pol2cart: Convert polar to cartesian coordinate.
    - gen_lsps_analysis_settings_table: Generates a table containing all
                                        settings required for analysis of the
                                        maps.
    - gen_features_average_maps: Generate maps of features measured from each
                                 pixel of averaged maps.


Execution functions:
    - get_lsps_data: Get all LSPS data based on sweep and experiment.
    - plot_lsps_feature_maps: Plot feature maps of selected sweeps.
    - combine_lsps_maps: Based on analysis_settings_table combines lsps maps.
    - plot_features_average_maps: Plot feature maps per mapping group.


    TODO: finish all functions. This script is not finalized!!!
    - feature calculation based on the individual and averaged maps
    - work on visualization: annotate location pia/cc.
    - functionality to plot both the (s)CRACM/LSPS maps on top of the morphology
    - DONE: functionality to define pia, soma position, corpus callosum.
    -
"""

import numpy as np
import pandas as pd
import datetime
import os
import matplotlib.pyplot as plt
import copy
from matplotlib.pyplot import cm

from brainCellSuite._io.xsg2NWB import sanitised_input
from brainCellSuite._io.xsg2NWB import check_columns_dataframe
from brainCellSuite._io.readDBCSV import readDBCSV
from brainCellSuite.ini_cell import Cell
from brainCellSuite.analysis.ephys.features import select_sweeps
from brainCellSuite.analysis.ephys.features import get_rms
from brainCellSuite.analysis.ephys.features import get_rs
from brainCellSuite.analysis.ephys.features import get_vm
from brainCellSuite.analysis.ephys.features import filter_bessel
from brainCellSuite.analysis.ephys.features import get_spike_train_features
from brainCellSuite import bcs_global




def get_lsps_data(experiment, sweep, experiment_folder):
    """Get LSPS data based sweep and experiment.

    Based on the sweep number and experiment dictionary, distill all the LSPS
    data and process for analysis.

    Args:
        - experiment: nested dictionary.
        - sweep: full sweep name. e.g. 'sweep0002'.
        - experiment_folder: full path to directory where data is stored.
        e.g. '/Users/bartjongbloets/Box/insularDatabases/xsg/BJ1583'

    Returns:
        - experiment: nested dictionary with updated lsps matrixes
        - lsps_feature_maps: dictionary containing numpy arrays of the features.
        - lsps_qc_plots: plots summarizing all features to quality check the
                         data.
    """
    experiment = (
        check_sweep_table_lsps_parameters(experiment, experiment_folder))
    if sweep:
        experiment['sweep_table'] = experiment['sweep_table'].\
            set_index(experiment['sweep_table']['sweep'])
        if not isinstance(sweep, list) and not isinstance(sweep, str):
            sweep = list(sweep)
        elif isinstance(sweep, str):
            sweep = [sweep]
        subset = experiment['sweep_table'].loc[sweep]
        cellAmpIDs = subset['cellAmpID']
    else:
        sweep, cellAmpIDs = select_sweeps(experiment, preset_name='map_all')
    tuples = list(zip(experiment['sweep_table']['sweep'],
                      experiment['sweep_table']['cellAmpID']))
    inds = pd.MultiIndex.from_tuples(tuples, names=['sweep', 'cellAmpID'])
    experiment['sweep_table'] = experiment['sweep_table'].set_index(inds)
    # make a dedicated dataframe holding nested numpy arrays for lsps data.
    # this we avoid the piling up of nested information in sweep_table and allow
    # for saving of the sweep_tabl into a .csv file.
    cols = ['spatial_rotation', 'soma_x_raw', 'soma_x_map', 'soma_y_raw',
            'soma_y_map', 'pattern_offset_x', 'pattern_offset_y',
            'grid_spacing_x', 'grid_spacing_y', 'ephys_traces',
            'photodiode_traces', 'Ileak', 'baseline_correction', 'rs',
            'noiseRMS', 'patchRMS', 'pattern_array',
            'laser_intensity', 'peak_current', 'charge',
            'onset_inward_10percent', 'onset_outward_10percent',
            'risetime_outward_20to80percent', 'risetime_inward_20to80percent',
            'spike_location', 'spike_number_detected']
    experiment['lsps_matrixes'] = pd.DataFrame(index=inds, columns=cols)
    # Probably can be deleted below
    sweep_map_df = experiment['sweep_table'].loc[zip(sweep, cellAmpIDs)]
    for row, sweep_info in sweep_map_df.iterrows():
        print(sweep_info['sweep'] + ' ' + sweep_info['cellAmpID'])
        if experiment['stim_data'][sweep_info['sweep']]['mapper']:
            photodiode_chan = experiment['stim_data'][sweep_info['sweep']]\
                ['mapper']['photodiodeChannels']
            ephys_trace = experiment['acquired_data'][sweep_info['sweep']] \
                ['ephys'][sweep_info['amplifier']]['data']
            photodiode_trace = experiment['acquired_data'][sweep_info['sweep']]\
                ['acquirer'][photodiode_chan][
                'data']
            # Correct photodiode_trace matrix with the calibration curve
            photodiode_slope = \
            experiment['stim_data'][sweep_info['sweep']]['mapper'] \
                ['calibrationSlope']
            photodiode_offset = \
            experiment['stim_data'][sweep_info['sweep']]['mapper'] \
                ['calibrationOffset']
            lensTransmission = \
            experiment['stim_data'][sweep_info['sweep']]['mapper'] \
                ['lensTransmission'] / 100
            photodiode_trace = lensTransmission * (photodiode_trace *
                                                        photodiode_slope +
                                                        photodiode_offset)
            pattern_array = experiment['stim_data'][sweep_info['sweep']]\
                ['mapper']['mapPatternArray']
            experiment['lsps_matrixes'].at[
                row, 'pattern_array'] = pattern_array
            ephys_trace_matrix, photodiode_trace_matrix = gen_traces_matrix(
                sweep_info, ephys_trace, photodiode_trace, pattern_array)
            experiment['lsps_matrixes'].at[
                row, 'ephys_traces'] = ephys_trace_matrix
            experiment['lsps_matrixes'].at[
                row, 'photodiode_traces'] = photodiode_trace_matrix
            experiment['lsps_matrixes'].at[
                row, 'spatial_rotation'] = experiment['stim_data']\
                [sweep_info['sweep']]['mapper']['spatialRotation']
            experiment['lsps_matrixes'].at[
                row, 'grid_spacing_x'] = experiment['stim_data']\
                [sweep_info['sweep']]['mapper']['xSpacing']
            experiment['lsps_matrixes'].at[
                row, 'grid_spacing_y'] = experiment['stim_data'] \
                [sweep_info['sweep']]['mapper']['ySpacing']
            experiment['lsps_matrixes'].at[
                row, 'pattern_offset_x'] = experiment['stim_data'] \
                [sweep_info['sweep']]['mapper']['xPatternOffset']
            experiment['lsps_matrixes'].at[
                row, 'pattern_offset_y'] = experiment['stim_data'] \
                [sweep_info['sweep']]['mapper']['yPatternOffset']
            experiment['lsps_matrixes'].at[
                row, 'soma_x_raw'] = experiment['stim_data'] \
                [sweep_info['sweep']]['mapper']['soma' + str(
                row[1].split('a')[-1]) + 'Coordinates'][0]
            experiment['lsps_matrixes'].at[
                row, 'soma_y_raw'] = experiment['stim_data'] \
                [sweep_info['sweep']]['mapper']['soma' + str(
                row[1].split('a')[-1]) + 'Coordinates'][1]
            # calculate the position of the soma relative to the map
            soma_x = experiment['lsps_matrixes'].at[row, 'soma_x_raw']
            soma_y = experiment['lsps_matrixes'].at[row, 'soma_y_raw']
            spatial_rotation = experiment['lsps_matrixes']\
                .at[row, 'spatial_rotation']
            pattern_offset_x = experiment['lsps_matrixes'] \
                .at[row, 'pattern_offset_x']
            pattern_offset_y = experiment['lsps_matrixes'] \
                .at[row, 'pattern_offset_y']
            (experiment['lsps_matrixes'].at[
                row, 'soma_x_map'],
             experiment['lsps_matrixes'].at[
                 row, 'soma_y_map']) = transform_soma_position(soma_x,
                                                               soma_y,
                                                               spatial_rotation,
                                                               pattern_offset_x,
                                                               pattern_offset_y)
            experiment = gen_feature_maps(experiment, row, experiment_folder)
    return experiment


def combine_lsps_maps(experiment,
                      experiment_folder,
                      cellDB_filepath=os.path.join(
                          bcs_global.path_database, bcs_global.filename_cell_db
                      ),
                      time_pre_stim=0,
                      time_post_stim=75):
    """Based on analysis_settings_table combines lsps maps.

    Uses analysis_settings_tables!.

    Args:
        - experiment: nested dictionary.
        - experiment_folder: full path to directory where data is stored.
        e.g. '/Users/bartjongbloets/Box/insularDatabases/xsg/BJ1583'
        - cellDB_filepath: full path to file where cell database is stored.
        e.g. '/Users/bartjongbloets/Box/insularDatabases/
            insularCellDatabase.csv'
        - time_pre_stim: integer in ms. For plotting raw traces, starting point
                         relative to stimulus.
        - time_post_stim: integer in ms. For plotting raw traces, end point
                          relative to stimulus.

    Returns:
        - experiment: nested dictionary with updated lsps parameters.
    """
    db = readDBCSV(os.path.dirname(cellDB_filepath),
                   os.path.basename(cellDB_filepath))
    db = db.set_index('cellAmpID')
    current_date = datetime.datetime.now()
    experiment = gen_lsps_analysis_settings_table(experiment, experiment_folder)
    analysis_settings = experiment['lsps_analysis_settings']
    lsps_matrixes = experiment['lsps_matrixes']
    sweep_table = experiment['sweep_table']
    lsps_matrixes['map_group'] = analysis_settings['map_group']
    sweep_table['map_group'] = analysis_settings['map_group']
    analysis_settings = analysis_settings.dropna(subset=['map_group'])
    lsps_matrixes = lsps_matrixes.dropna(subset=['map_group'])
    sweep_table = sweep_table.dropna(subset=['map_group'])
    shutter_chan = experiment['stim_data'][sweep_table.index[0][0]]['mapper'] \
        ['shutterChannels']
    rows = list(zip(analysis_settings['map_group'],
                    analysis_settings.index.get_level_values('cellAmpID')))
    index = pd.MultiIndex.from_tuples(rows,
                                      names=['map_group', 'cellAmpID']).unique()
    columns = ['start_datapoint', 'end_datapoint', 'samplerate',
               'spatial_rotation', 'soma_x_raw', 'soma_x_map', 'soma_y_raw',
               'soma_y_map', 'soma_x_grid', 'soma_y_grid', 'pattern_offset_y',
               'grid_spacing_x', 'grid_spacing_y', 'local_cortical_thickness',
               'normalized_soma_depth', 'pia_row_include', 'cc_row_include',
               'modification_date', 'integration_time', 'signal_unit',
               'block_postsynaptic_activation', 'postsynaptic_threshold',
               'ephys_traces', 'photodiode_traces', 'rs', 'noiseRMS',
               'patchRMS', 'laser_intensity', 'baseline_current_plus_SD',
               'SD_factor', 'mean_current', 'peak_current',
               'peak_timing', 'charge', 'onset_inward_10percent',
               'onset_outward_10percent', 'risetime_inward_20to80percent',
               'risetime_outward_20to80percent', 'spike_location',
               'spike_number_detected']
    average_maps = pd.DataFrame(index=index, columns=columns)

    for row in average_maps.index:
        average_maps.at[row, 'modification_date'] = current_date
        select_amp = analysis_settings.index.get_level_values('cellAmpID') == \
            row[1]
        select_group = analysis_settings['map_group'] == row[0]
        average_map_sub = analysis_settings[np.all([select_amp, select_group],
                                                   axis=0)]
        lsps_matrixes_sub = lsps_matrixes[np.all([select_amp, select_group],
                                                 axis=0)]
        sweep_table_sub = sweep_table[np.all([select_amp,
                                                            select_group],
                                                           axis=0)]

        # Check that parameters eg. integration_time, spatial_rotation are
        # annotated in the same way for each map. Otherwise print warnings and
        # halt the procedure. This should ensure that the right meta-data is
        # used
        parameter_dict = {'spatial_rotation': 'spatial rotations',
                          'soma_x_raw': 'soma Xs raw',
                          'soma_x_map': 'soma Xs map',
                          'soma_y_raw': 'soma Ys raw',
                          'soma_y_map': 'soma Ys map',
                          'pattern_offset_x': 'pattern offsets X',
                          'pattern_offset_y': 'pattern offsets Y',
                          'grid_spacing_x': 'grid spacings X',
                          'grid_spacing_y': 'grid spacings Y'}
        for key, value in parameter_dict.items():
            # BCJ 07122019: added int to make precision a little lower
            if int(np.std(lsps_matrixes_sub[key])) != 0:
                print('Warning, ', value, ' are not the same across the '
                      'different sweeps. Please check the '
                      'lsps_analysis_settings.csv')
                experiment['average_maps'] = None
                return experiment
            else:
                average_maps.at[row, key] = \
                    lsps_matrixes_sub[key].max()
        # repeat this for the sweep_table information (map_signal_polarity,
        # map_source, map_signal_threshold, map_synaptic_activation)
        parameter_dict = {'map_signal_polarity': 'map signal polarities',
                          'map_source': 'map sources',
                          'map_signal_threshold': 'map signal thresholds',
                          'map_synaptic_activation': 'map synaptic activation'}
        for key, value in parameter_dict.items():
            if not all(sweep_table_sub[key][0] == c
                       for c in sweep_table_sub[key]):
                print('Warning, ', value, ' are not the same across the '
                                          'different sweeps. Please check the '
                                          'sweep_table.csv')
                experiment['average_maps'] = None
                return experiment
            else:
                average_maps.at[row, key] = sweep_table_sub[key][0]
        # repeat this for the sweep_table information (shutter_pulseDelay,
        # shutter_pulseWidth, ephys_stim_pulseDelay, ephys_stim_pulseWidth)
        parameter_dict = {
            (shutter_chan + '_pulseDelay'): (shutter_chan + ' pulse delays'),
            (shutter_chan + '_pulseWidth'): (shutter_chan + ' pulse widths'),
            'ephys_stim_pulseDelay': 'ephys stim pulse delays',
            'ephys_stim_pulseWidth': 'ephys stim pulse widths'}
        for key, value in parameter_dict.items():
            if int(np.std(sweep_table_sub[key])) != 0:
                print('Warning, ', value,
                      ' are not the same across the '
                      'different sweeps. Please check the '
                      'sweep_table.csv')
                experiment['average_maps'] = None
                return experiment
            else:
                average_maps.at[row, key] = sweep_table_sub[key][0]
        # combine and copy other parameters
        if int(np.std(average_map_sub['integration_time'])) != 0:
            print('Warning, integration times are not the same across the '
                  'different sweeps. Please check the lsps_analysis_settings'
                  '.csv')
            experiment['average_maps'] = None
            return experiment
        else:
            average_maps.at[row, 'integration_time'] = \
                average_map_sub['integration_time'].max()
        # get locations of soma, pia, and corpus callosum
        amp = row[1]
        if amp in db.index:
            average_maps.at[row, 'normalized_soma_depth'] = \
                db.loc[amp, 'somaCortexPosition']
            if ~np.isnan(db.loc[amp, 'cortexThicknessImage']):
                average_maps.at[row, 'local_cortical_thickness'] = \
                    db.loc[amp, 'cortexThicknessImage']
            else:
                try:
                    average_maps.at[row, 'local_cortical_thickness'] = \
                    db.loc[amp, 'cortexThicknessSWC'] * \
                    db.loc[amp, 'XYShrinkage']
                except (ValueError, KeyError):
                    print('Warning, local cortical_thickness not found')
                    average_maps.at[row, 'local_cortical_thickness'] = \
                        sanitised_input('please provide local cortical'
                                        'thickness in µm', type_=float, min_=0)
        else:
            print('Warning, ' + amp + ' could not be found in the db')
            average_maps.at[row, 'local_cortical_thickness'] = \
                sanitised_input('please provide local cortical'
                                'thickness in µm', type_=float, min_=0)
            average_maps.at[row, 'normalized_soma_depth'] = \
                sanitised_input('please provide normalized soma depth in '
                                'fraction', type_=float, min_=0)
        bool_post_block = average_map_sub['block_postsynaptic_activation']
        if not np.sum(bool_post_block) == 0 or\
                np.sum(bool_post_block) == \
                len(average_map_sub['block_postsynaptic_activation']):
            print('Warning, blockage of postsynaptic activation is not the'
                  'same across the different sweeps. Please check the '
                  'lsps_analysis_settings.csv')
            experiment['average_maps'] = None
            return experiment
        else:
            average_maps.at[row, 'block_postsynaptic_activation'] = \
                average_map_sub.iloc[0]['block_postsynaptic_activation']
            if np.all(bool_post_block):
                if np.std(average_map_sub['postsynaptic_threshold']) != 0:
                    print('Warning, postsynaptic thresholds are not the same '
                          'across the different sweeps. Please check the '
                          'lsps_analysis_settings.csv')
                    experiment['average_maps'] = None
                    return experiment
                else:
                    average_maps.at[row, 'postsynaptic_threshold'] = \
                        average_map_sub['postsynaptic_threshold'].max()

        # Get the traces and average based on number of maps
        # Future: make it possible to include NaN in the map, such that these
        # spots are averaged based on the number of replicates with no-nan's.
        # This would be a nice way to remove specific tracelets that have noise
        items = len(lsps_matrixes_sub.index)
        count = 0
        for r in lsps_matrixes_sub.index:
            baseline_correction = lsps_matrixes_sub.loc[r,
                                                        'baseline_correction']
            ephys_trace = lsps_matrixes_sub.loc[r, 'ephys_traces']
            _, _, timepoints = np.shape(ephys_trace)
            baseline_correction = np.repeat(baseline_correction[:, :,
                                            np.newaxis], timepoints, axis=2)
            ephys_trace = ephys_trace - baseline_correction
            photodiode_trace = lsps_matrixes_sub.loc[r, 'photodiode_traces']
            if count == 0:
                ephys_traces = ephys_trace
                photodiode_traces = photodiode_trace
            else:
                ephys_traces += ephys_trace
                photodiode_traces += photodiode_trace
            count += 1
        ephys_trace = np.divide(ephys_traces, items)
        photodiode_trace = np.divide(photodiode_traces, items)
        average_maps.at[row, 'ephys_traces'] = ephys_trace
        average_maps.at[row, 'photodiode_traces'] = photodiode_trace

        # Calculate locations of soma, and blanks for pia and corpus callosum
        # rows in the matrixes are 0 - end from top - bottom
        # cols in the matrixes are 0 - end from left - right
        # soma coordinates map are all centric relative to the image, where the
        # center of the image/map is 0,0 in x,y.
        pattern_rows, pattern_cols, _ = np.shape(
            rotate_matrix(ephys_trace, average_maps.at[row,
                                                       'spatial_rotation']))
        soma_x, soma_y = [average_maps.loc[row, 'soma_x_map'],
                          average_maps.loc[row, 'soma_y_map']]
        x_spacing = average_maps.loc[row, 'grid_spacing_x']
        y_spacing = average_maps.loc[row, 'grid_spacing_y']
        unit_dict = {'V-Clamp': 'pA', 'I-Clamp': 'mV'}
        sweep_ind = (lsps_matrixes_sub.index.get_level_values('sweep')[0], amp)
        signal_unit = unit_dict[experiment['sweep_table'].loc[sweep_ind,
                                                              'amp_mode']]
        average_maps.at[row, 'soma_x_grid'] = round((pattern_cols / 2) +
                                                   int(soma_x / x_spacing))
        average_maps.at[row, 'soma_y_grid'] = round((pattern_rows / 2) -
                                                   int(soma_y / y_spacing))
        distance_pia_soma_um = average_maps.at[row, 'normalized_soma_depth'] * \
            average_maps.at[row, 'local_cortical_thickness']
        distance_cc_soma_um = (average_maps.at[row, 'local_cortical_thickness']
            - distance_pia_soma_um)
        distance_pia_soma_grid = int(distance_pia_soma_um / y_spacing)
        distance_cc_soma_grid = int(distance_cc_soma_um / y_spacing)
        average_maps.at[row, 'pia_row_include'] = \
            average_maps.at[row, 'soma_y_grid'] - distance_pia_soma_grid
        average_maps.at[row, 'cc_row_include'] = \
            average_maps.at[row, 'soma_y_grid'] + distance_cc_soma_grid
        average_maps.at[row, 'signal_unit'] = signal_unit
        # save parameter for showing traces
        samplerate = experiment['sweep_table'].loc[sweep_ind, 'samplerate']
        time_pre_stim_time = int(samplerate * (time_pre_stim / 1000))
        time_post_stim_time = int(samplerate * (time_post_stim / 1000))
        shutter_chan = experiment['stim_data'][sweep_ind[0]]['mapper']\
            ['shutterChannels']
        start_datapoint = (
                experiment['sweep_table'].loc[
                    sweep_ind, shutter_chan + '_pulseDelay'] -
                time_pre_stim_time)
        end_datapoint = (
                experiment['sweep_table'].loc[
                    sweep_ind, shutter_chan + '_pulseDelay'] +
                time_post_stim_time)
        average_maps.at[row, 'start_datapoint'] = start_datapoint
        average_maps.at[row, 'end_datapoint'] = end_datapoint
        average_maps.at[row, 'samplerate'] = samplerate
        # plot traces
        plot_trace_matrix(ephys_trace,
                          average_maps.at[row, 'spatial_rotation'],
                          start_datapoint, end_datapoint, ('average map ' +
                          amp + ' ' + row[0]), samplerate, signal_unit,
                          average_maps.at[row, 'soma_x_grid'],
                          average_maps.at[row, 'soma_y_grid'],
                          average_maps.at[row, 'pia_row_include'],
                          average_maps.at[row, 'cc_row_include'])
        experiment['average_maps'] = average_maps
    experiment = gen_features_average_maps(experiment)
    return experiment


def gen_lsps_analysis_settings_table(experiment, experiment_folder):
    """Generates a table containing all settings required for analysis of the
        maps.

    Args:
        - experiment: nested dictionary.
        - experiment_folder: full path to directory where data is stored.
        e.g. '/Users/bartjongbloets/Box/insularDatabases/xsg/BJ1583'

    Returns:
        - experiment: nested dictionary with updated lsps parameters.
        - lsps_analysis_settings: saved as csv.
    """
    # Added extra parameters for
    #
    # This ui-based. Should be asked only once, as long as the
    # sweep_table is saved.
    cellID = os.path.basename(experiment_folder)
    data_filename = os.path.join(experiment_folder,
                                 (cellID +
                                  '_lsps_analysis_settings.csv'))
    current_date = str(datetime.datetime.today())
    if not 'lsps_matrixes' in experiment:
        experiment = get_lsps_data(experiment, sweep=None,
                                   experiment_folder=experiment_folder)
    rows = experiment['lsps_matrixes'].index
    if os.path.exists(data_filename):
        analysis_settings = pd.read_csv(data_filename, index_col=[0, 1])
        if sanitised_input(cellID + '_lsps_analysis_settings.csv already exists'
                                    ' and will be opened. Do you want to make '
                                    'changes to the analysis settings? '
                                    '1: yes, 0: no',
                           type_=int, min_=0, max_=1) == 0:
            experiment['lsps_analysis_settings'] = analysis_settings
            return experiment
    else:
        colnames = ["sweep", "cellAmpID", "modification_date", "map_group",
                    "integration_time", "block_postsynaptic_activation",
                    "postsynaptic_threshold"]
        analysis_settings = pd.DataFrame(index=rows, columns=colnames)
    plot_lsps_feature_maps(experiment)
    plt.show(block=False)
    plt.draw()
    for row in rows:
        print(row)
        print(analysis_settings.loc[row])
        if isinstance(experiment['lsps_matrixes'].loc[row,'ephys_traces'],
                      np.ndarray):
            analysis_settings.at[row, 'modification_date'] = current_date
            analysis_settings.at[row, 'map_group'] = sanitised_input(
                'Provide groupname to group by eg. EPSC_sCRACM_1', str)
            if sanitised_input(
                    'Using 75ms for integration of signal;'
                    'default, as signal integration time. 1:yes, 0:no',
                    type_=int, min_=0, max_=1) == 1:
                analysis_settings.at[row, 'integration_time'] = 75
            else:
                analysis_settings.at[row, 'integration_time'] = sanitised_input(
                    'Provide time in ms to integrate traces by', int)
            analysis_settings.at[row, 'block_postsynaptic_activation'] = False
            if sanitised_input('Do spots with direct postsynaptic activations'
                    ' need to be blocked? 1: True, 0:False', int) == 1:
                analysis_settings.at[row, 'block_postsynaptic_activation'] = \
                    True
            if analysis_settings.at[row, 'block_postsynaptic_activation']:
                analysis_settings.at[row, 'postsynaptic_threshold'] = 10
                if sanitised_input('Using 10 ms onset threshold to exclude '
                                   'spots as a default, want to change '
                                   'threshold? 1:yes, 0: no') == 1:
                    analysis_settings.at[row, 'postsynaptic_threshold'] =\
                        sanitised_input('Provide onset threshold', int)
    experiment['lsps_analysis_settings'] = analysis_settings
    analysis_settings.to_csv(data_filename)
    return experiment


def check_sweep_table_lsps_parameters(experiment, experiment_folder):
    """Checks sweep_table and provides UI to update LSPS parameters

    Several initial parameters are required to perform analysis of lsps data
    to analysis more streamlined, the user is asked to input settings
    that describe the lsps experiment. Parameters are subsequently stored in
    the sweep_table

    Args:
        - experiment: nested dictionary.
        - experiment_folder: full path to directory where data is stored.
        e.g. '/Users/bartjongbloets/Box/insularDatabases/xsg/BJ1583'

    Returns:
        - experiment: nested dictionary with updated lsps parameters
        - sweep_table: saved as csv
    """
    # Added extra parameters for
    # signal type; map_signal_polarity: negative, positive, mix),
    # signal source; map_source: uncaging vs channelrhodopsin
    # signal type relative to spike; map_signal_threshold: sub vs supra
    # synaptic activation location; map_synaptic_activation: pre vs post
    # vs pre_post
    # This ui-based. Should be asked only once, as long as the
    # sweep_table is saved.
    sweep_table = experiment['sweep_table']
    appending_colnames = ["map_signal_polarity", "map_source",
                          "map_signal_threshold", "map_synaptic_activation"]
    colnames = sweep_table.columns
    c = 0
    # First check whether all the lsps map parameters are already in the
    # sweep_table in that case stop the function
    for colname in appending_colnames:
        if colname in colnames:
            c += 1
    if c == len(appending_colnames):
        print('sweep_table already contains lsps map parameters, table has not'
              ' been changed!')
        return experiment
    for ind in sweep_table.index:
        if sweep_table.loc[ind, 'mapper'] and \
                sweep_table.loc[ind, 'discard'] is not True:
            print(os.path.basename(sweep_table.loc[ind, 'filepath']))
            # UI for map_signal_polarity
            sweep_table = check_columns_dataframe(sweep_table,
                                                  'map_signal_polarity')
            ui = sanitised_input('define signal polarity; 1: "negative", '
                                 '2: "positive", or 3: " mix", if unsure use '
                                 '3:"mix"', int, 1, 3 )
            ui_options = {1: "negative", 2: "positive", 3: "mix"}
            sweep_table.loc[ind, 'map_signal_polarity'] = ui_options[ui]
            # UI for map source
            ui = sanitised_input('define map_source: 1: "channelrhodopsin" or '
                                 '2: "glutamate_uncaging"', int, 1, 2)
            ui_options = {1: "channelrhodopsin", 2: "glutamate_uncaging"}
            sweep_table.loc[ind, 'map_source'] = ui_options[ui]
            # UI for map_signal_threshold
            sweep_table = check_columns_dataframe(sweep_table,
                                                  'map_signal_threshold')
            ui = sanitised_input('define map_signal_threshold: 1:"sub" or '
                                 '2:"supra" if unsure use 2:"supra"', int, 1, 2)
            ui_options = {1:"sub", 2:"supra"}
            sweep_table.loc[ind, 'map_signal_threshold'] = ui_options[ui]
            # UI for map_synaptic_activation
            sweep_table = check_columns_dataframe(sweep_table,
                                                  'map_synaptic_activation')
            ui = sanitised_input('define map_synaptic_activation: 1:"pre" or '
                                 '2:"post", 3:"pre_post" if unsure use 3:"pre_'
                                 'post"',
                                 int, 1, 3)
            ui_options = {1: "pre", 2: "post", 3: "pre_post"}
            sweep_table.loc[ind, 'map_synaptic_activation'] = ui_options[ui]
    experiment['sweep_table'] = sweep_table
    cellID = os.path.basename(experiment_folder)
    experiment['sweep_table'].to_csv(os.path.join(experiment_folder,
                                     (cellID + '_sweep_table.csv')),
                                     index=False)
    return experiment


def gen_feature_maps(experiment, sweep_cellAmpID_index, experiment_folder,
                     signal_timespan=50, stim_lag=1, noise_rms_time=1.5,
                     patch_rms_time=50, shutter_lag=.5, pre_stim_correction=10):
    """Generate maps of features measured for each pixel.

    Trace recorded at each stimulation site is referred here as tracelet.
    Calculates a list of features for each stimulation site in the map. Features
    included are:
        - Ileak: mean current between start tracelet till start photostimulation
                 or any other stimulation
        - rs: series resistance measured during voltage step of each tracelet.
        - noiseRMS: root-mean-square of first 1.5ms of each tracelet.
        - patchRMS: root-mean-square of first 50 ms of each tracelet.
        - laser_intensity: mean current during photostimulation (mW)-based on
                           calibration by user prior to experiment.
    Args:
        - experiment: nested dictionary.
        - sweep_cellAmpID_index: tuple combining: sweep and cellAmpID.
        - experiment_folder: full path to directory where data is stored.
        e.g. '/Users/bartjongbloets/Box/insularDatabases/xsg/BJ1583'
        - signal_timespan: float, time in milliseconds. Length of time after
                           start_stim to use for quantifying the response to the
                           stimulation. Default : 50 milliseconds
        - stim_lag: float, time in milliseconds. Length of time after start_stim
                    to correct lag of the system.
        - noise_rms_time: time epoch used to calculate noise, relative to start
                          of trace.
        - patch_rms_time: time epoch used to calculate patch instability,
                          relative to start of trace.
        - shutter_lag: float, time in milliseconds. Length of time after
                        end_stim, that the shutter remains open.
        - pre_stim_correction: float, time in milliseconds, length of time
                               before stimulation used to calculate leak current
                               used for baseline correction of the ephys traces.
    Returns:
        - lsps_feature_maps: dictionary containing numpy arrays of the features.
    """
    if not isinstance(sweep_cellAmpID_index, tuple):
        print('warning ' + str(sweep_cellAmpID_index) + ' is not in the '
                                                        'experiment. Should be'
                                                        'a tuple. ' +
              str(type(sweep_cellAmpID_index)) + ' was provided')
        return experiment
    if not 'lsps_matrixes' in experiment:
        print('First extracting all lsps data')
        experiment = get_lsps_data(experiment, None, experiment_folder)
    row = sweep_cellAmpID_index
    sweep_info = experiment['sweep_table'].loc[row]
    samplerate = sweep_info['samplerate']
    # datapoints:
    signal_timespan = int(samplerate * (signal_timespan / 1000))
    stim_lag = int(samplerate * (stim_lag / 1000))
    shutter_lag = int(samplerate * (shutter_lag / 1000))
    pre_stim_correction = int(samplerate * (pre_stim_correction / 1000))
    rows, cols = np.shape(experiment['stim_data'][sweep_info['sweep']]
                          ['mapper']['mapPatternArray'])
    Ileak = np.zeros((rows, cols))
    baseline_correction = np.zeros((rows, cols))
    rs = np.zeros((rows, cols))
    noiseRMS = np.zeros((rows, cols))
    patchRMS = np.zeros((rows, cols))
    laser_intensity = np.zeros((rows, cols))
    std_baseline = np.zeros((rows, cols))
    shutter_chan = experiment['stim_data'][sweep_info['sweep']]['mapper']\
        ['shutterChannels']
    # numbers are in datapoints:
    start_stim = int(sweep_info[shutter_chan + '_pulseDelay'] + stim_lag)
    end_stim = int(sweep_info[shutter_chan + '_pulseDelay'] +
               sweep_info[shutter_chan + '_pulseWidth'] + stim_lag)
    pre_stim = int(start_stim - pre_stim_correction)
    start_Rs_stim = int(sweep_info['ephys_stim_pulseDelay'])
    end_Rs_stim = int(sweep_info['ephys_stim_pulseDelay'] +
                  sweep_info['ephys_stim_pulseWidth'])
    ephys_trace_matrix = experiment['lsps_matrixes'].loc[row, 'ephys_traces']
    photodiode_trace_matrix = (
        experiment['lsps_matrixes'].loc[row,'photodiode_traces'])
    for r in np.arange(0, rows):
        for c in np.arange(0, cols):
            Ileak[r, c] = get_vm(ephys_trace_matrix[r, c], start=0,
                                 end=start_stim)
            baseline_correction[r, c] = get_vm(ephys_trace_matrix[r, c],
                                               start=pre_stim,
                                               end=start_stim)
            rs[r, c] = get_rs(ephys_trace_matrix[r, c], start=start_Rs_stim,
                              end=start_Rs_stim + end_Rs_stim)
            noiseRMS[r, c] = get_rms(ephys_trace_matrix[r, c], start=0,
                                     end=(noise_rms_time /
                                          1000) * samplerate,
                                     correct_for_vm=True)
            # 1.5 ms for noise RMS
            patchRMS[r, c] = get_rms(ephys_trace_matrix[r, c], start=0,
                                     end=(patch_rms_time /
                                          1000) * samplerate,
                                     correct_for_vm=True)
            # 50 ms for patch RMS
            laser_intensity[r, c] = np.mean(
                photodiode_trace_matrix[r, c, start_stim: end_stim +
                                        shutter_lag])
            std_baseline[r, c] = np.std(ephys_trace_matrix[r, c,
                                        0: start_stim])
    # save all matrixes in the experiment
    experiment['lsps_matrixes'].at[row, 'Ileak'] = Ileak
    experiment['lsps_matrixes'].at[row, 'baseline_correction'] = \
        baseline_correction
    experiment['lsps_matrixes'].at[row, 'rs'] = rs
    experiment['lsps_matrixes'].at[row, 'noiseRMS'] = noiseRMS
    experiment['lsps_matrixes'].at[row, 'patchRMS'] = patchRMS
    experiment['lsps_matrixes'].at[row, 'laser_intensity'] = laser_intensity
    return experiment


def gen_traces_matrix(sweep_info, ephys_trace, photodiode_trace, pattern_array):
    """Untangles the acquired trace to a 3D array.

    Tracelets are recorded at systematic random locations along the grid, and
    saved chronologically in a 2D array. To ease analysis the 2D trace is
    untangled based on the pattern_array, and a new 3D array is composed. For
    which X:grid rows, Y: grid cols, and T: time for each tracelet.

    Args:
        - sweep_info: pandas dataframe with the sweep_table information of the
                      given trace.
        - ephys_trace: trace from the recorded cell.
        - photodiode_trace: trace from the photodiode used to measure laser
                            intensity.
        - pattern_array: map pattern array, that provides the sequence of
                         stimulation pattern.

    Returns:
        - ephys_trace_matrix: 3D numpy array, with X:grid rows, Y: grid cols,
                              and T: time for each tracelet.
        - photodiode_trace_matrix: 3D numpy array, with X:grid rows, Y:
                                   grid cols, and T: time for each tracelet.

    :return:
    """
    if not isinstance(pattern_array, np.ndarray):
        pattern_array = np.asarray(pattern_array)
    pattern_array -= 1 # conversion of indexing in matlab to python
    rows, cols = np.shape(pattern_array)
    try:
        tracelet_length = int(sweep_info['xMirror_acquired_data_len'])
    except KeyError:
        prompt = ('Could not find length of tracelet, please provide integer' +
        'length of datapoints tracelet')
        tracelet_length = sanitised_input(prompt, type_=int)
    start_vector = np.arange(0, len(ephys_trace), tracelet_length)
    stop_vector = np.arange(tracelet_length, len(ephys_trace)+1,
                            tracelet_length)
    ephys_trace_matrix = np.zeros((rows, cols, tracelet_length))
    photodiode_trace_matrix = np.zeros((rows, cols, tracelet_length))
    # make a matrix with same dimensions as the feature, to hold start and end
    # data point indexes that reference to the correct spot in the traces.
    start_ind = start_vector[pattern_array]
    stop_ind = stop_vector[pattern_array]
    for row in np.arange(0, rows):
        for col in np.arange(0, cols):
            ephys_tracelet = ephys_trace[start_ind[row][col]:
                                         stop_ind[row][col]]
            photodiode_tracelet = photodiode_trace[start_ind[row][col]:
                                                   stop_ind[row][col]]
            ephys_trace_matrix[row, col, :] = ephys_tracelet
            try:
                photodiode_trace_matrix[row, col, :] = photodiode_tracelet
            except ValueError:
                print('warning empty photodiode tracelet; took previous spot!')
                photodiode_tracelet = photodiode_trace[start_ind[row][col-1]:
                                                   stop_ind[row][col-1]]
                photodiode_trace_matrix[row, col, :] = photodiode_tracelet
    return ephys_trace_matrix, photodiode_trace_matrix


def cart2pol(x, y):
    """Convert cartesian to polar coordinate

    Args:
        - x and y: number, cartesian coordinate.

    Returns:
        - radius and angle: number, polar coordinate
    """
    radius = np.sqrt(x**2 + y**2)
    angle = np.arctan2(y, x)
    return angle, radius


def pol2cart(angle, radius):
    """Convert polar to cartesian coordinate

    Args:
        - angle and radius: number, polar coordinate

    Returns:
        - x and y: number, cartesian coordinate.
    """
    x = radius * np.cos(angle)
    y = radius * np.sin(angle)
    return x, y


def transform_soma_position(soma_x, soma_y, spatial_rotation, pattern_offset_x,
                            pattern_offset_y):
    """Transform soma position relative to map.

    Takes soma positions and converts the positions based on the pattern
    rotation and offsets.

    Args:
        - soma_x: position in um. Relative to the center of the field of view;
                  coordinates are following cartesian coordinate system (left:
                  negative, right: positive)
        - soma_y: position in um. Relative to the center of the field of view;
                  coordinates are following cartesian coordinate system (down:
                  negative, right: positive
        - spatial_rotation: rotation between -360 and 360 in degrees.
        - pattern_offset_x: distance in um. Distance moved of the pattern center
                            relative to the center of the field of view along x
                            axis. See soma_x for info on coordinate system
        - pattern_offset_y: distance in um. Distance moved of the pattern center
                            relative to the center of the field of view along x
                            axis. See soma_x for info on coordinate system

    Returns:
        - soma_x_new: new soma position relative to the map
        - soma_y_new: new soma position relative to the map
    """
    soma_x_offset = soma_x - pattern_offset_x
    soma_y_offset = soma_y - pattern_offset_y
    radian_rotation = (-1) * spatial_rotation * (np.pi / 180)
    # theta, rho = cart2pol(soma_x_offset, soma_y_offset)
    radian_angle, radius = cart2pol(soma_x_offset, soma_y_offset)
    # soma_x_new, soma_y_new = pol2cart(theta + radian_rotation, rho)
    soma_x_new, soma_y_new = pol2cart(radian_angle + radian_rotation, radius)
    soma_x_new = soma_x_new * -1
    return soma_y_new, soma_x_new


def rotate_matrix(matrix, spatial_rotation):
    """Rotation of the matrix based on pattern orientation

    Only rotates matrix if pattern_rotation falls within each quadrant

    Args:
        - matrix: numpy matrix
        - spatial_rotation: integer value between -360 and 360

    Returns:
        - rot_matrix: rotated numpy matrix.
    """
    limits = [45, 135, 225, 315]
    if spatial_rotation < 0:
        spatial_rotation = 360 + spatial_rotation
    if limits[1] >= spatial_rotation > limits[0]:
        rot_matrix = np.rot90(matrix)
    elif limits[2] >= spatial_rotation > limits[1]:
        rot_matrix = np.rot90(matrix,2)
    elif limits[3] >= spatial_rotation > limits[2]:
        rot_matrix = np.rot90(matrix, 3)
    elif spatial_rotation > limits[3] or spatial_rotation <= limits[0]:
        # pattern does not require rotation
        rot_matrix = matrix
    else:
        rot_matrix = matrix
    return rot_matrix


def plot_lsps_feature_maps(experiment, sweeps=None, amps=None, features=None,
                           show_ephys=True, show_photodiode=False,
                           time_pre_stim=0, time_post_stim=75):
    """Plot feature maps of selected sweeps.

    Requires lsps feature maps (see gen_feature_maps). Based on selection of
    sweeps, the selected features are shown individually or together as either
    maps or timelines,

    Args:
        - experiment: nested dictionary, should contain 'lsps_matrixes'
        - sweeps: list of sweeps to plot, if None all maps will be plotted from
                  all sweeps and combined
        - amps: list of amps to plot, if None all amps will be plotted. e.g.
                [1,2] for amps 1 and 2, or [1] for only amp 1.
        - features: list of features to plot, if None all features will be
                    plotted. If 'off', no features will be plotted.
        - show_ephys: bool, True: plot the raw traces of ephys.
        - show_photodiode: bool, True: plot the raw traces of photodiode
        - time_pre_stim: integer in ms. For plotting raw traces, starting point
                         relative to stimulus.
        - time_post_stim: integer in ms. For plotting raw traces, end point
                          relative to stimulus.

    Returns:
        - None
        """
    if not 'lsps_matrixes' in experiment:
        print('Warning, lsps_matrixes are not present. Please run get_lsps_data'
              ' first!')
        return None
    units = {'Ileak': 'pA',
             'rs': 'MOhm',
             'noiseRMS': 'pA',
             'patchRMS': 'pA',
             'laser_intensity': 'mW'}
    plt.ion()
    # make sure that all ui-provided features are actual features that can be
    # printed
    print(list(units.keys()))
    if features:
        if isinstance(features, str):
            if features != 'off':
                features = [features]
                for feature in features:
                    if not feature in units:
                        features.pop(feature)
    else:
        # plot all features
        features = list(units.keys())

    # first select the correct sweeps and amps
    sweeps_all, cellAmpIDs = select_sweeps(experiment, preset_name='map_all')
    select_sweep = None
    if amps:
        if isinstance(amps, str):
            amps = [int(amps)]
        elif isinstance(amps, int):
            amps = [int(amps)]
        amp_num = [int(item[-1]) for item in cellAmpIDs]
        select_sweep = np.zeros(len(amp_num), dtype=bool)
        for amp in amps:
            if amp in amp_num:
                select_sweep += np.array(amp_num) == amp
        sweeps_all = np.array(sweeps_all)[select_sweep]
        cellAmpIDs = np.array(cellAmpIDs)[select_sweep]
    if sweeps:
        # if not isinstance(select_sweep, np.ndarray):
        select_sweep = np.zeros(len(sweeps_all), dtype=bool)
        if not isinstance(sweeps, list) and not isinstance(sweeps, str):
            sweeps = list(sweeps)
        elif isinstance(sweeps, str):
            sweeps = [sweeps]
        # select_sweep = np.zeros(len(sweeps), dtype=bool)
        for sweep in sweeps:
            if sweep in sweeps_all:
                select_sweep += np.array(sweeps_all) == sweep
        ### RESOLVE BELOW!!
        # sweeps_all = np.array(sweeps_all)[[select_sweep]]
        # cellAmpIDs = np.array(cellAmpIDs)[[select_sweep]]
        sweeps_all = np.array(tuple(sweeps_all))[[select_sweep]]
        cellAmpIDs = np.array(tuple(cellAmpIDs))[[select_sweep]]
        ### RESOLVE ABOVE !!!
    # print(list(zip(sweeps, cellAmpIDs)))
    sweeps = sweeps_all
    lsps_matrixes = experiment['lsps_matrixes'].loc[zip(sweeps, cellAmpIDs)]
    unit_dict = {'V-Clamp': 'pA', 'I-Clamp': 'mV'}
    # print per feature and/or raw traces the plots
    if show_ephys or show_photodiode:
        for ind in lsps_matrixes.index:
            spatial_rotation = lsps_matrixes.loc[ind, 'spatial_rotation']
            shutter_chan = experiment['stim_data'][ind[0]]['mapper']\
                ['shutterChannels']
            samplerate = experiment['sweep_table'].loc[ind, 'samplerate']
            time_pre_stim_time = int(samplerate * (time_pre_stim/1000))
            time_post_stim_time = int(samplerate * (time_post_stim / 1000))
            start_datapoint = (
                    experiment['sweep_table'].loc[
                        ind, shutter_chan + '_pulseDelay'] -
                    time_pre_stim_time)
            end_datapoint = (
                    experiment['sweep_table'].loc[
                        ind, shutter_chan + '_pulseDelay'] +
                    time_post_stim_time)
            if show_ephys and isinstance(lsps_matrixes.loc[ind, 'ephys_traces'],
                                         np.ndarray):
                soma_x, soma_y = [lsps_matrixes.loc[ind, 'soma_x_map'],
                                  lsps_matrixes.loc[ind, 'soma_y_map']]
                x_spacing = lsps_matrixes.loc[ind, 'grid_spacing_x']
                y_spacing = lsps_matrixes.loc[ind, 'grid_spacing_y']
                signal_unit = unit_dict[experiment['sweep_table'].loc[ind,
                                                                   'amp_mode']]
                matrix = lsps_matrixes.loc[ind, 'ephys_traces']
                pattern_rows, pattern_cols, _ = np.shape(
                    rotate_matrix(matrix, spatial_rotation))
                soma_x = round((pattern_cols / 2) + int(soma_x / x_spacing))
                soma_y = round((pattern_rows / 2) - int(soma_y / y_spacing))
                try:
                    matrix = filter_bessel(matrix)
                except RuntimeError:
                    print('Problems filtering with bessel on matrix')
                _, _, timepoints = np.shape(matrix)
                leak_matrix = lsps_matrixes.loc[ind, 'baseline_correction']
                leak_matrix = np.repeat(leak_matrix[:, :, np.newaxis],
                                        timepoints, axis=2)
                baseline_corrected_matrix = matrix - leak_matrix
                plot_trace_matrix(baseline_corrected_matrix,
                                  spatial_rotation, start_datapoint,
                                  end_datapoint, str(ind), samplerate,
                                  signal_unit, soma_x, soma_y, None, None)
            if show_photodiode and isinstance(
                    lsps_matrixes.loc[ind, 'photodiode_traces'], np.ndarray):
                plot_trace_matrix(lsps_matrixes.loc[ind, 'photodiode_traces'],
                                  spatial_rotation, start_datapoint,
                                  end_datapoint, str(ind), samplerate,
                                  'mW')
    if features == 'off':
        return None
    for feature in features:
        if len(np.unique(sweeps)) == 1:
            # plot each feature per amplifier
            if isinstance(cellAmpIDs, str):
                cellAmpIDs = [cellAmpIDs]
            if isinstance(sweeps, str):
                sweeps = [sweeps]
            amps = len(cellAmpIDs)
            plt.figure()
            rows = list(zip(sweeps, cellAmpIDs))
            for n, row in enumerate(rows):
                if isinstance(lsps_matrixes.loc[row, 'ephys_traces'],
                              np.ndarray):
                    spatial_rotation = lsps_matrixes.loc[row,
                                                         'spatial_rotation']
                    map_pattern = lsps_matrixes.loc[row, 'pattern_array']
                    x_spacing = lsps_matrixes.loc[row, 'grid_spacing_x']
                    y_spacing = lsps_matrixes.loc[row, 'grid_spacing_y']
                    plt.subplot(amps, 3, n * 3 + 1)
                    # heatmap plot of data
                    plt.imshow(rotate_matrix(lsps_matrixes.loc[row, feature],
                                             spatial_rotation),
                               cmap='jet',
                               aspect='equal')
                    soma_x, soma_y = [lsps_matrixes.loc[row, 'soma_x_map'],
                                      lsps_matrixes.loc[row, 'soma_y_map']]
                    plt.colorbar()
                    plt.tick_params(axis='x',
                                    which='both',
                                    top=False,
                                    bottom=False,
                                    labelbottom=False)
                    plt.tick_params(axis='y',
                                    which='both',
                                    left=False,
                                    right=False,
                                    labelleft=False)
                    pattern_rows, pattern_cols = np.shape(
                        rotate_matrix(map_pattern, spatial_rotation))
                    y_loc_scale = pattern_rows
                    if x_spacing == y_spacing:
                        plt.plot([0, 10], [y_loc_scale, y_loc_scale], 'k-',
                                 linewidth=2.0)
                        plt.text(0, y_loc_scale + 2,
                                 str(x_spacing * 10) + ' µm')
                    else:
                        plt.plot([0, 10], [y_loc_scale, y_loc_scale], 'k-',
                                 linewidth=2.0)
                        plt.text(0, y_loc_scale + 2,
                                 str(x_spacing * 10) + ' µm')
                        plt.plot([0, 0], [y_loc_scale, y_loc_scale + 10], 'k-',
                                 linewidth=2.0)
                        plt.text(0-1, y_loc_scale + 1,
                                 str(y_spacing * 10) + ' µm', rotation=-90)

                    soma_x = (pattern_cols / 2) + int(soma_x / x_spacing)
                    print('soma_x:' + str(soma_x))
                    soma_y = (pattern_rows / 2) - int(soma_y / y_spacing)
                    print('soma_y:' + str(soma_y))
                    plt.plot(soma_x, soma_y, 'k^', markersize=8)
                    plt.axis('off')
                    plt.subplot(amps, 3, n * 3 + 2)
                    # timeline version of parameters
                    plt.title(feature)
                    plt.plot(map_pattern, lsps_matrixes.loc[row, feature], 'ok')
                    plt.ylabel(feature + '(' + units[feature] + ')')
                    if n != amps - 1:
                        plt.tick_params(axis='x',
                                        which='both',
                                        top=False,
                                        bottom=False,
                                        labelbottom=False)
                    else:
                        plt.xlabel('# tracelet (sequential in time)')
                    ylim_low = np.min(lsps_matrixes.loc[row, feature].flatten())
                    ylim_max = np.max(lsps_matrixes.loc[row, feature].flatten())
                    plt.ylim(ylim_low, ylim_max)
                    plt.subplot(amps, 3, n * 3 + 3)
                    # histogram of parameter distribution
                    plt.hist(lsps_matrixes.loc[row, feature].flatten(),
                             orientation='horizontal')
                    plt.xlim(0, np.size(lsps_matrixes.loc[row, feature]))
                    plt.tick_params(axis='y',
                                    which='both',
                                    left=False,
                                    right=False,
                                    labelleft=False)
                    if n != amps - 1:
                        plt.tick_params(axis='x',
                                        which='both',
                                        top=False,
                                        bottom=False,
                                        labelbottom=False)
                    else:
                        plt.xlabel('# tracelets (cumulative)')
                    plt.ylim(ylim_low, ylim_max)
            plt.tight_layout()
        if len(np.unique(sweeps)) > 1:
            # plot each feature as an overlay
            # plot each feature per amplifier
            map_pattern = lsps_matrixes['pattern_array'].iloc[0]
            if isinstance(cellAmpIDs, str):
                cellAmpIDs = [cellAmpIDs]
            if isinstance(sweeps, str):
                sweeps = [sweeps]
            amps = len(np.unique(cellAmpIDs))
            plt.figure()
            rows = list(zip(sweeps, cellAmpIDs))
            amp_dict = dict(zip(np.unique(cellAmpIDs),
                                np.arange(0, len(cellAmpIDs))))
            color = iter(cm.rainbow
                         (np.linspace(0, 1, len(np.unique(sweeps)) * amps)))
            for n, row in enumerate(rows):
                c = next(color)
                plt.subplot(amps, 2, amp_dict[row[1]]*2 + 1)
                # timeline version of parameters
                plt.title(feature + " amplifier: " + row[1])
                plt.plot(map_pattern, lsps_matrixes.loc[row, feature], 'o',
                         color=c)
                plt.ylabel(feature + '(' + units[feature] + ')')
                if n != amps - 1:
                    plt.tick_params(axis='x',
                                    which='both',
                                    top=False,
                                    bottom=False,
                                    labelbottom=False)
                else:
                    plt.xlabel('# tracelet (sequential in time)')
                axes = plt.gca()
                ylim_low, ylim_max = axes.get_ylim()

                plt.subplot(amps, 2, amp_dict[row[1]]*2 + 2)
                # histogram of parameter distribution
                plt.hist(lsps_matrixes.loc[row, feature].flatten(),
                         orientation='horizontal',
                         alpha=0.5,
                         color=c,
                         label=row[0])
                plt.xlim(0, np.size(lsps_matrixes.loc[row, feature]))
                plt.tick_params(axis='y',
                                which='both',
                                left=False,
                                right=False,
                                labelleft=False)
                if n != amps - 1:
                    plt.tick_params(axis='x',
                                    which='both',
                                    top=False,
                                    bottom=False,
                                    labelbottom=False)
                else:
                    plt.xlabel('# tracelets (cumulative)')
                plt.legend()
                plt.ylim(ylim_low, ylim_max)
            plt.tight_layout()
            plt.pause(0.01)
        plt.pause(0.01)
    plt.pause(0.01)
    return None


def plot_trace_matrix(matrix, spatial_rotation,
                      start_datapoint, end_datapoint, plot_title,
                      samplerate, signal_unit, soma_col=None, soma_row=None,
                      pia_row_include=None, cc_row_include=None):
    """Plot traces from a matrix

    Uses 3 dimensional matrixes to plot along x and y the tracelets (z)

    Args:
        - matrix: 3D numpy matrix with rows: x, cols: y, depht: tracelet in time
        - spatial_rotation: integer value between -360 and 360.
        - start_datapoint: integer used as starting point for plotting.
        - end_datapoint: integer used as end point for plotting.
        - plot_title: str, used for plot title.
        - samplerate: integer, used for scalebar.
        - signal_unit: str, used for y-axis scalebar.
        - soma_row: integer, row that is closest to the location of the soma.
                    Default: None, nothing will be shown. If integer, that trace
                    will have thicker line.
        - soma_col: integer, column that is closest to the location of the soma.
                    Default: None, nothing will be shown. If integer, that trace
                    will have thicker line.
        - pia_row_include: integer, first row - representing pia that should be
                           included in the map.
        - cc_row_include: integer, last row - representing corpus callosum that
                          should be included in the map.

    Returns:
        - plot figure
    """
    plt.figure()
    plt.title(plot_title)
    matrix = rotate_matrix(matrix, spatial_rotation)
    print(start_datapoint)
    matrix = matrix[:, :, int(start_datapoint):int(end_datapoint)]
    # make a time matrix containing the same time for each tracelet
    time = np.arange(0, end_datapoint - start_datapoint)/samplerate
    elapsed_time = len(time) / samplerate
    rows, cols, _ = np.shape(matrix)
    time_matrix = np.repeat(np.repeat(time[np.newaxis, :], rows, axis=0)
                            [:, np.newaxis, :], cols, axis=1)
    time_gap = 10/1000  # gap between each tracelet : 5 ms
    time_transform = np.arange(0,
                               cols * (elapsed_time + time_gap),
                               elapsed_time + time_gap)
    time_transform = time_transform[:cols]
    time_transform_matrix = np.repeat(
        np.repeat(time_transform[:, np.newaxis], len(time), axis=1)
        [np.newaxis, :, :], rows, axis=0)
    time_matrix = time_matrix + time_transform_matrix
    # to get a good distance between traces we want to know the median size of
    # any signal during the plotting window
    # median_signal = np.median(np.max(np.abs(matrix), axis=2))
    # signal_gap = median_signal * 2
    max_signal = np.max(np.max(np.abs(matrix), axis=2))
    signal_gap = max_signal
    signal_transform = np.arange(0, rows * signal_gap, signal_gap)
    signal_transform = signal_transform[:rows]
    signal_transform_matrix = np.repeat(
        np.repeat(signal_transform[:, np.newaxis], len(time), axis=1)
        [:, np.newaxis, :], cols, axis=1)
    matrix_plot = matrix - signal_transform_matrix
    # approximate total charge transfer to make a color index
    approx_charge_matrix = np.sum(matrix, axis=2)
    min_charge = np.min(approx_charge_matrix)
    max_charge = np.max(approx_charge_matrix)
    approx_charge_matrix = (approx_charge_matrix - min_charge) /\
        (np.abs(min_charge) + np.abs(max_charge))
    for row in np.arange(0, rows):
        for col in np.arange(0, cols):
            linewidth = 1
            if soma_col and soma_row:
                if soma_col-1 == col and soma_row-1 == row:
                    linewidth = 3
                else:
                    linewidth = 1
            color_select = (1 - approx_charge_matrix[row, col], 0.2, 0.2)
            # blank traces traces
            if pia_row_include:
                if pia_row_include - 1 > row:
                    color_select = (1, 1, 1)
                else:
                    if cc_row_include:
                        if cc_row_include <= row:
                            color_select = (1, 1, 1)
                    else:
                        color_select = (
                            1 - approx_charge_matrix[row, col], 0.2, 0.2)
            plt.plot(time_matrix[row, col, :], matrix_plot[row, col, :],
                     linewidth=linewidth,
                     color=color_select)
    # draw scalebars
    # time bar:
    timebar_length = elapsed_time
    timebar_xstart = 0
    timebar_xend = timebar_xstart + timebar_length
    timebar_yposition = (rows) * signal_gap * -1
    plt.plot([timebar_xstart, timebar_xend],
             [timebar_yposition, timebar_yposition], 'k-', linewidth=1)
    plt.text(timebar_xstart,
             timebar_yposition - signal_gap * 1.3,
             str(int(timebar_length * 1000)) + ' ms')
    # signal bar:
    if signal_unit == 'pA' and max_signal <= 200:
        signalbar_length = 100
    elif signal_unit == 'mV' and max_signal <= 20:
        signalbar_length = 10
    else:
        signalbar_length = max_signal * 2
    signalbar_xposition = 0 - 0.3 * elapsed_time
    signalbar_ystart = timebar_yposition # rows * signal_gap * -1
    signalbar_yend = signalbar_ystart + signalbar_length
    plt.plot([signalbar_xposition, signalbar_xposition],
             [signalbar_yend, signalbar_ystart], 'k-', linewidth=1)
    plt.text(signalbar_xposition - 0.8 * elapsed_time,
             signalbar_ystart + signalbar_length,
             str("{0:.1f} ".format(signalbar_length) + signal_unit),
             rotation=90)
    plt.axis('off')
    return None


def plot_features_average_maps(experiment, cellDB_filepath=os.path.join(
    bcs_global.path_database, bcs_global.filename_cell_db),
                               features=None,
                               blank_rows_outside_slice=True,
                               blank_below_threshold_peak=True,
                               show_pia_cc_borders=True,
                               show_morphology=True):
    """Plot feature maps per mapping group.

    Requires lsps feature average maps (see combine_lsps_maps).
    Based on selection features are shown.

    Args:
        - experiment: nested dictionary, should contain 'average_maps'
        - cellDB_filepath: full path to file where cell database is stored.
        e.g. '/Users/bartjongbloets/Box/insularDatabases/
            insularCellDatabase.csv'
        - features: list of features to plot, if None all features will be
                    plotted. If 'off', no features will be plotted.
        - blank_rows_outside_slice: bool, True: remove first the rows that are
                                    outside of the slice/cortex. This is based
                                    on pia_row_include, cc_row_include.
        - blank_below_threshold_peak: bool, True: remove any location for which
                                      peak_current <= mean peak
        - show_pia_cc_borders: bool, True: show border of pia and corpus
                               callosum.
        - show_morphology: bool, True: plot two separate figures; 1) heatmap
                           with morhology superimposed, 2) density plots of both
                           morphology and feature across vertical and horizontal
                           axes. REQUIRES, cellDB_filepath.


    Returns:
        - None
    """
    if not 'average_maps' in experiment:
        print('Warning, average_maps are not present. '
              'Please run combine_lsps_maps first!')
        return None
    units = {'rs': 'MOhm',
             'noiseRMS': 'pA',
             'patchRMS': 'pA',
             'laser_intensity': 'mW',
             'mean_current': 'pA',
             'peak_current': 'pA',
             'peak_timing': 'ms',
             'charge': 'fC',
             'onset_inward_10percent': 'ms',
             'onset_outward_10percent': 'ms',
             'risetime_inward_20to80percent': 'ms',
             'risetime_outward_20to80percent': 'ms',
             'spike_location': 'ms',
             'spike_number_detected': '# spikes',
             }
    correct_sign = {'rs': 1,
                    'noiseRMS': 1,
                    'patchRMS': 1,
                    'laser_intensity': 1,
                    'mean_current': -1,
                    'peak_current': -1,
                    'peak_timing': 1,
                    'charge': -1,
                    'onset_inward_10percent': 1,
                    'onset_outward_10percent': 1,
                    'risetime_inward_20to80percent': 1,
                    'risetime_outward_20to80percent': 1,
                    'spike_location': 1,
                    'spike_number_detected': 1,
                    }
    density_dict = {'rs': 'mean',
                    'noiseRMS': 'mean',
                    'patchRMS': 'mean',
                    'laser_intensity': 'mean',
                    'mean_current': 'sum',
                    'peak_current': 'mean',
                    'peak_timing': 'mean',
                    'charge': 'sum',
                    'onset_inward_10percent': 'mean',
                    'onset_outward_10percent': 'mean',
                    'risetime_inward_20to80percent': 'mean',
                    'risetime_outward_20to80percent': 'mean',
                    'spike_location': 'mean',
                    'spike_number_detected': 'mean',
                    }
    # make sure that all ui-provided features are actual features that can be
    # printed
    print(list(units.keys()))
    if features:
        if isinstance(features, str):
            if features != 'off':
                features = [features]
                for feature in features:
                    if not feature in units:
                        features.pop(feature)
    else:
        # plot all features
        features = list(units.keys())
    lsps_matrixes = experiment['average_maps']
    for feature in features:
        rows = lsps_matrixes.index
        for n, row in enumerate(rows):
            plt.figure(figsize=(4.72, 6))
            plt.title((feature + ': ' + str(row)))
            if isinstance(lsps_matrixes.loc[row, 'ephys_traces'],
                          np.ndarray):
                # map_pattern in the average map is based on normal grid
                # sequence: col=0, row=0 upper left, and increase
                # left - right and top - bottom.
                # average map is already oriented such that pia is top row
                # left-right.
                feature_map = copy.deepcopy(
                    rotate_matrix(lsps_matrixes.loc[row, feature],
                                            lsps_matrixes.loc[row,
                                            'spatial_rotation']))
                if blank_below_threshold_peak:
                    peak_matrix = copy.deepcopy(
                        rotate_matrix(
                        lsps_matrixes.loc[row, 'peak_current'],
                        lsps_matrixes.loc[row, 'spatial_rotation']))
                    threshold_matrix = copy.deepcopy(
                        rotate_matrix(
                        lsps_matrixes.loc[row, 'baseline_current_plus_SD'],
                        lsps_matrixes.loc[row, 'spatial_rotation']))
                    if lsps_matrixes.loc[row, 'map_signal_polarity'] == \
                    'negative':
                        mask = peak_matrix > threshold_matrix * -1
                        feature_map[mask] = np.nan
                    elif lsps_matrixes.loc[row, 'map_signal_polarity'] == \
                    'positive':
                        mask = peak_matrix < threshold_matrix
                        feature_map[mask] = np.nan
                    else:
                        mask = np.abs(peak_matrix) < threshold_matrix
                        feature_map[mask] = np.nan

                # flip sign when displaying inward currents
                if lsps_matrixes.loc[row, 'map_signal_polarity'] == \
                        'negative':
                    feature_map = feature_map * correct_sign[feature]
                if np.ndim(feature_map) == 2:
                    pattern_rows, pattern_cols = np.shape(feature_map)
                if np.ndim(feature_map) == 3:
                    pattern_rows, pattern_cols, _ = np.shape(feature_map)
                if blank_rows_outside_slice:
                    pia_include = lsps_matrixes.loc[row, 'pia_row_include']
                    cc_include = lsps_matrixes.loc[row, 'cc_row_include']
                    if pia_include > 0:
                        pia_rows = np.arange(0, pia_include)
                    else:
                        pia_rows = None
                    if cc_include < pattern_rows:
                        cc_rows = np.arange(cc_include + 1,
                                            pattern_rows + 1)
                    else:
                        cc_rows = None
                    if isinstance(pia_rows, np.ndarray) and \
                            isinstance(cc_rows, np.ndarray):
                        feature_map = \
                            np.delete(feature_map,
                                      np.append(pia_rows, cc_rows), 0)
                    elif isinstance(pia_rows, np.ndarray) and not \
                            isinstance(cc_rows, np.ndarray):
                        feature_map = \
                            np.delete(feature_map,
                                      pia_rows, 0)
                    elif isinstance(cc_rows, np.ndarray) and not \
                            isinstance(pia_rows, np.ndarray):
                        feature_map = \
                            np.delete(feature_map,
                                      cc_rows, 0)
                if np.ndim(feature_map) == 2:
                    pattern_rows, pattern_cols = np.shape(feature_map)
                if np.ndim(feature_map) == 3:
                    pattern_rows, pattern_cols, _ = np.shape(feature_map)
                x_spacing = lsps_matrixes.loc[row, 'grid_spacing_x']
                y_spacing = lsps_matrixes.loc[row, 'grid_spacing_y']
                plt.subplot(2, 2, 1)
                plt.imshow(feature_map,
                           cmap='jet',
                           aspect='equal')
                soma_x, soma_y = [lsps_matrixes.loc[row, 'soma_x_grid'],
                                  lsps_matrixes.loc[row, 'soma_y_grid']]
                if blank_rows_outside_slice:
                    soma_y = soma_y - pia_include
                    if show_pia_cc_borders:
                        plt.plot(0, pia_include - pia_include, 'r>')
                        plt.text(1, pia_include, 'pia', color='red')
                        plt.plot(0, cc_include - pia_include, 'r>')
                        plt.text(1, cc_include - pia_include, 'cc',
                                 color='red')
                else:
                    if show_pia_cc_borders:
                        pia_include = lsps_matrixes.loc[
                            row, 'pia_row_include']
                        cc_include = lsps_matrixes.loc[
                            row, 'cc_row_include']
                        plt.plot(0, pia_include, 'r>')
                        plt.text(1, pia_include, 'pia', color='red')
                        plt.plot(0, cc_include, 'r>')
                        plt.text(1, cc_include, 'cc',
                                 color='red')
                plt.tick_params(axis='x', which='both', top=False,
                                bottom=False,
                                labelbottom=False)
                plt.tick_params(axis='y', which='both', left=False,
                                right=False,
                                labelleft=False)
                y_loc_scale = pattern_rows
                if x_spacing == y_spacing:
                    plt.plot([0, 10], [y_loc_scale, y_loc_scale], 'r-',
                             linewidth=2.0)
                    plt.text(0, y_loc_scale + 2,
                             str(x_spacing * 10) + ' µm', color='red')
                else:
                    plt.plot([0, 10], [y_loc_scale, y_loc_scale], 'r-',
                             linewidth=2.0)
                    plt.text(0, y_loc_scale + 2,
                             str(x_spacing * 10) + ' µm', color='red')
                    plt.plot([0, 0], [y_loc_scale, y_loc_scale + 10], 'r-',
                             linewidth=2.0)
                    plt.text(0-1, y_loc_scale + 1,
                             str(y_spacing * 10) + ' µm', rotation=-90,
                             color='red')
                print('soma_x:' + str(soma_x))
                print('soma_y:' + str(soma_y))
                plt.plot(soma_x-1, soma_y, 'k^', markersize=8)
                plt.axis('off')
                plt.colorbar()

                # get metric for parameter distribution
                if density_dict[feature] == 'sum':
                    metric_vertical = np.nansum(feature_map, axis=1)
                    metric_horizontal = np.nansum(feature_map, axis=0)
                    print('Total current: ' + str(np.nansum(metric_vertical)))
                elif density_dict[feature] == 'mean':
                    metric_vertical = np.nanmean(feature_map, axis=1)
                    metric_horizontal = np.nanmean(feature_map, axis=0)
                    print('Total current: ' + str(np.nansum(metric_vertical)))
                vertical_axis = np.arange(0, pattern_rows) * -1
                horizontal_axis = np.arange(0, pattern_cols)
                plt.subplot(2, 2, 2)
                plt.plot(metric_vertical, vertical_axis, 'k-')
                plt.box(False)
                # draw soma position
                plt.plot(0, soma_y * -1, 'k^', markersize=8)
                # the imshow needs 1.5 rows for the legend.
                plt.ylim(pattern_rows * -1 - 1.5, 0.5)
                plt.tick_params(axis='y', which='both', left=False,
                                right=False,
                                labelleft=False)
                # plot scale
                plt.xlabel((density_dict[feature] + ' of ' + feature +
                            ' (' + units[feature] + ')'))
                plt.plot([0, 0], [pattern_rows * -1, (pattern_rows - 10) *
                                  -1], 'r-', linewidth=2.0)
                # plt.text(0, y_loc_scale + 2,
                #          str(x_spacing * 10) + ' µm', color='red')
                if show_pia_cc_borders and blank_rows_outside_slice:
                    plt.plot(0, (pia_include - pia_include) * -1, 'r>')
                    plt.plot(0, (cc_include - pia_include) * -1, 'r>')
                elif show_pia_cc_borders:
                    plt.plot(0, pia_include * -1, 'r>')
                    plt.plot(0, cc_include * -1, 'r>')
                plt.subplot(2, 2, 3)
                plt.plot(horizontal_axis, metric_horizontal, 'k-')
                plt.box(False)
                # draw soma position
                plt.plot(soma_x, 0, 'k^', markersize=8)
                # the imshow needs 3.5 cols for the legend.
                plt.xlim(-0.5, pattern_cols + 3.5)
                plt.ylabel(('(' + units[feature] + ')'))
                plt.gca().yaxis.set_label_position("right")
                plt.tick_params(axis='x', which='both', top=False,
                                bottom=False,
                                labelbottom=False)
                plt.tick_params(axis='y', which='both', left=False,
                                right=True,
                                labelleft=False, labelright=True)
                # plot scale
                plt.plot([0, 10], [0, 0], 'r-',
                         linewidth=2.0)
                # plt.text(0, y_loc_scale + 2,
                #          str(x_spacing * 10) + ' µm', color='red')
            plt.tight_layout()
            db = readDBCSV(os.path.dirname(cellDB_filepath),
                           os.path.basename(cellDB_filepath))
            db = db.set_index('cellAmpID')
            amp = row[1]
            if show_morphology and amp in db.index:
                cell = Cell(amp, os.path.dirname(cellDB_filepath))
                if isinstance(cell.swc_data, np.ndarray):
                    plt.figure()
                    plt.imshow(feature_map,
                               cmap='jet',
                               aspect='equal')
                    soma_x, soma_y = [lsps_matrixes.loc[row, 'soma_x_grid'],
                                      lsps_matrixes.loc[row, 'soma_y_grid']]
                    if blank_rows_outside_slice:
                        soma_y = soma_y - pia_include
                        if show_pia_cc_borders:
                            plt.plot(0, pia_include - pia_include, 'r>')
                            plt.text(1, pia_include, 'pia', color='red')
                            plt.plot(0, cc_include - pia_include, 'r>')
                            plt.text(1, cc_include - pia_include, 'cc',
                                     color='red')
                    else:
                        if show_pia_cc_borders:
                            pia_include = lsps_matrixes.loc[
                                row, 'pia_row_include']
                            cc_include = lsps_matrixes.loc[
                                row, 'cc_row_include']
                            plt.plot(0, pia_include, 'r>')
                            plt.text(1, pia_include, 'pia', color='red')
                            plt.plot(0, cc_include, 'r>')
                            plt.text(1, cc_include, 'cc',
                                     color='red')
                    plt.tick_params(axis='x', which='both', top=False,
                                    bottom=False,
                                    labelbottom=False)
                    plt.tick_params(axis='y', which='both', left=False,
                                    right=False,
                                    labelleft=False)
                    y_loc_scale = pattern_rows
                    if x_spacing == y_spacing:
                        plt.plot([0, 10], [y_loc_scale, y_loc_scale], 'r-',
                                 linewidth=2.0)
                        plt.text(0, y_loc_scale + 2,
                                 str(x_spacing * 10) + ' µm', color='red')
                    else:
                        plt.plot([0, 10], [y_loc_scale, y_loc_scale], 'r-',
                                 linewidth=2.0)
                        plt.text(0, y_loc_scale + 2,
                                 str(x_spacing * 10) + ' µm', color='red')
                        plt.plot([0, 0], [y_loc_scale, y_loc_scale + 10], 'r-',
                                 linewidth=2.0)
                        plt.text(0 - 1, y_loc_scale + 1,
                                 str(y_spacing * 10) + ' µm', rotation=-90,
                                 color='red')
                    print('soma_x:' + str(soma_x))
                    print('soma_y:' + str(soma_y))
                    plt.plot(soma_x - 1, soma_y, 'k^', markersize=8)
                    plt.axis('off')
                    plt.colorbar()


    return None


def gen_features_average_maps(experiment, stim_lag=1, noise_rms_time=1.5,
                              patch_rms_time=50, shutter_lag=.5,
                              pre_stim_correction=10, SD_factor=15):
    """Generate maps of features measured from each pixel of averaged maps.

    Trace recorded at each stimulation site is referred here as tracelet.
    Calculates a list of features for each stimulation site in the map. Features
    included are:
        - rs: series resistance measured during voltage step of each tracelet.
        - noiseRMS: root-mean-square of first 1.5ms of each tracelet.
        - patchRMS: root-mean-square of first 50 ms of each tracelet.
        - laser_intensity: mean current during photostimulation (mW)-based on
                           calibration by user prior to experiment.
        - baseline_current_plus_SD: mean current during baseline prior to
                                     stimulation plus SD_factor x the SD.
                                     Can be used to blank locations which do
                                     not receive input.
        - mean_current: mean current from start stimulation till start
                        stimulation + integration time. Calculated as integrated
                        current / time.
        - peak_current: peak amplitude of current after stimulation within
                        integration window.
        - peak_timing: time between stimulus and peak.
        - charge: integrated current after stimulation within integration window
        - onset_inward_10percent: time between stimulus and 10% of inward peak.
        - onset_outward_10percent: time between stimulus and 10% of outward
                                   peak.
        - risetime_inward_20to80percent: time between 20% and 80% of inward
                                         peak.
        - risetime_outward_20to80percent: time between 20% and 80% of outward
                                         peak.
        - spike_location: time between stimulation and first spike peak.

        - spike_number_detected: number of spikes observed within integration
                                 window.

    Requires that average_maps is available.
    Will calculate features for all the groups.

    Args:
        - experiment: nested dictionary.
        - stim_lag: float, time in milliseconds. Length of time after start_stim
                    to correct lag of the system.
        - noise_rms_time: time epoch used to calculate noise, relative to start
                          of trace.
        - patch_rms_time: time epoch used to calculate patch instability,
                          relative to start of trace.
        - shutter_lag: float, time in milliseconds. Length of time after
                        end_stim, that the shutter remains open.
        - pre_stim_correction: float, time in milliseconds, length of time
                               before stimulation used to calculate leak current
                               used for baseline correction of the ephys traces.
        - SD_factor: float, number of SDs added to the mean baseline current
                     prior to stimulation.

    Returns:
        - experiment: nested dictionary, with updated average_maps.
    """
    if not 'average_maps' in experiment:
        print('warning average_maps is not in the experiment. Please run'
              'combine_lsps_maps.py first')
        return experiment
    average_maps = experiment['average_maps']
    samplerate = int(average_maps.iloc[0]['samplerate'])
    # datapoints:
    # signal_timespan = int(samplerate * (signal_timespan / 1000))
    stim_lag = int(samplerate * (stim_lag / 1000))
    shutter_lag = int(samplerate * (shutter_lag / 1000))
    pre_stim_correction = int(samplerate * (pre_stim_correction / 1000))
    for map_row in average_maps.index:
        # get data from ephys and photodiode traces. Please note no rotations
        # are used to correct the data for orientation! This is only done during
        # presentation of the data.
        ephys_trace_matrix = average_maps.loc[
            map_row, 'ephys_traces']
        photodiode_trace_matrix = average_maps.loc[
            map_row, 'photodiode_traces']
        rows, cols, timepoints = np.shape(ephys_trace_matrix)
        # baseline_correction has already been taken care of before obtaining
        # averaged maps
        # predefine all features that will be calculated.
        rs = np.zeros((rows, cols))
        noiseRMS = np.zeros((rows, cols))
        patchRMS = np.zeros((rows, cols))
        laser_intensity = np.zeros((rows, cols))
        baseline_current_plus_SD = np.zeros((rows, cols))
        mean_current = np.zeros((rows, cols))
        peak_current = np.zeros((rows, cols))
        peak_location = np.zeros((rows, cols))
        charge = np.zeros((rows, cols))
        onset_inward_10percent = np.zeros((rows, cols))
        onset_outward_10percent = np.zeros((rows, cols))
        risetime_inward_20to80percent = np.zeros((rows, cols))
        risetime_outward_20to80percent = np.zeros((rows, cols))
        spike_location = np.zeros((rows, cols))
        spike_number_detected = np.zeros((rows, cols))
        map_cols = average_maps.keys()
        res = []
        shutter_chan = [res.append(col.split('_')[0])
                        if 'shutter' in col
                        else None
                        for col in map_cols]
        shutter_chan = res[0]
        # numbers are in datapoints:
        start_stim = int(average_maps.loc[map_row,
                                          shutter_chan + '_pulseDelay'] +
                         stim_lag)
        end_stim = int(average_maps.loc[map_row, shutter_chan + '_pulseDelay'] +
                       average_maps.loc[map_row, shutter_chan + '_pulseWidth'] +
                       stim_lag)
        end_signal = int(start_stim + (average_maps.loc[map_row,
                                                        'integration_time']
                                       / 1000) * samplerate)
        pre_stim = int(start_stim - pre_stim_correction)
        start_Rs_stim = int(average_maps.loc[map_row, 'ephys_stim_pulseDelay'])
        end_Rs_stim = int(average_maps.loc[map_row, 'ephys_stim_pulseDelay'] +
                          average_maps.loc[map_row, 'ephys_stim_pulseWidth'])

        for r in np.arange(0, rows):
            for c in np.arange(0, cols):
                rs[r, c] = get_rs(ephys_trace_matrix[r, c], start=start_Rs_stim,
                                  end=start_Rs_stim + end_Rs_stim)
                noiseRMS[r, c] = get_rms(ephys_trace_matrix[r, c], start=0,
                                         end=(noise_rms_time /
                                              1000) * samplerate,
                                         correct_for_vm=True)
                # 1.5 ms for noise RMS
                patchRMS[r, c] = get_rms(ephys_trace_matrix[r, c], start=0,
                                         end=(patch_rms_time /
                                              1000) * samplerate,
                                         correct_for_vm=True)
                # 50 ms for patch RMS
                laser_intensity[r, c] = np.mean(
                    photodiode_trace_matrix[r, c, start_stim: end_stim +
                                            shutter_lag])
                # calculate mean baseline current prior to stimulation
                baseline_current_plus_SD[r, c] = \
                    np.abs(
                        np.mean(ephys_trace_matrix[r, c,
                                                   np.arange(pre_stim,
                                                             start_stim)])) + \
                    np.abs(
                        np.std(ephys_trace_matrix[r, c,
                                                  np.arange(pre_stim,
                                                            start_stim)])) * \
                    SD_factor
                # charge fC
                time = np.arange(0, (average_maps.loc[map_row,
                                                     'integration_time'] * (
                    samplerate / 1000))) / \
                    (samplerate / 1000)  # in ms
                time_data = np.arange(start_stim, end_signal)
                trace = ephys_trace_matrix[r, c, time_data]
                charge[r, c] = np.trapz(trace,
                                        time)  # pA*ms; unit = fC
                # mean_current pA
                mean_current[r, c] = charge[r, c] / \
                    average_maps.loc[map_row, 'integration_time']
                # peak_current pA
                # make detection dependent on sign of the signal
                if average_maps.loc[map_row,
                                    'map_signal_polarity'] == 'negative':
                    peak_current[r, c] = np.min(trace)
                elif average_maps.loc[map_row,
                                      'map_signal_polarity'] == 'positive':
                    peak_current[r, c] = np.max(trace)
                else:
                    min_ = np.min(trace)
                    max_ = np.max(trace)
                    if abs(min_) > abs(max_):
                        peak_current[r, c] = min_
                    else:
                        peak_current[r, c] = max_  # This means that it will
                        # always use the max value when abs(min_) == abs(max_)!
                # define where the peak was, used for onset timing/risetimes
                try:
                    peak_location[r, c] = \
                        np.where(trace == peak_current[r, c])[0][0]
                except (IndexError):
                    peak_location[r, c] = np.nan
                if average_maps.loc[map_row,
                                    'map_signal_polarity'] == 'negative' or \
                        average_maps.loc[map_row,
                                         'map_signal_polarity'] == 'both':
                    try:
                        onset_10percent = np.where(trace <= peak_current[r, c] *
                                                   0.1)[0][0]
                    except (IndexError):
                        onset_10percent = np.nan
                    onset_inward_10percent[r, c] = \
                        onset_10percent / (samplerate / 1000)
                    try:
                        location_20percent = np.where(
                            trace <= peak_current[r, c] * 0.2)[0][0]
                        location_80percent = np.where(
                            trace <= peak_current[r, c] * 0.8)[0][0]
                    except (IndexError):
                        location_20percent = np.nan
                        location_80percent = np.nan
                    risetime_inward_20to80percent[r, c] = \
                        (location_80percent - location_20percent) /\
                        (samplerate / 1000)
                if average_maps.loc[map_row,
                                    'map_signal_polarity'] == 'positive' or \
                        average_maps.loc[map_row,
                                         'map_signal_polarity'] == 'both':
                    try:
                        onset_10percent = np.where(trace >= peak_current[r, c] *
                                                   0.1)[0][0]
                    except (IndexError):
                        onset_10percent = np.nan
                    onset_outward_10percent[r, c] = \
                        onset_10percent / (samplerate / 1000)
                    try:
                        location_20percent = np.where(trace >= peak_current[r,
                                                                            c] *
                                                      0.2)[0][0]
                        location_80percent = np.where(trace >= peak_current[r,
                                                                            c] *
                                                      0.8)[0][0]
                    except (IndexError):
                        location_20percent = np.nan
                        location_80percent = np.nan
                    risetime_outward_20to80percent[r, c] = \
                        (location_80percent - location_20percent) /\
                        (samplerate / 1000)
                if average_maps.loc[map_row, 'map_signal_threshold'] == 'supra':
                    isi, feature_time = \
                        get_spike_train_features(trace, samplerate=samplerate)
                    spike_location[r, c] = feature_time['latency']
                    spike_number_detected[r, c] = len(isi)
            # save all matrixes in the experiment
        experiment['average_maps'].at[map_row, 'rs'] = rs
        experiment['average_maps'].at[map_row, 'noiseRMS'] = noiseRMS
        experiment['average_maps'].at[map_row, 'patchRMS'] = patchRMS
        experiment['average_maps'].at[map_row, 'laser_intensity'] = \
            laser_intensity
        experiment['average_maps'].at[map_row, 'baseline_current_plus_SD'] = \
            baseline_current_plus_SD
        experiment['average_maps'].at[map_row, 'SD_factor'] = SD_factor
        experiment['average_maps'].at[map_row, 'mean_current'] = \
            mean_current
        experiment['average_maps'].at[map_row, 'peak_current'] = \
            peak_current
        experiment['average_maps'].at[map_row, 'peak_timing'] = \
            peak_location / (samplerate / 1000)  # ms
        experiment['average_maps'].at[map_row, 'charge'] = charge
        experiment['average_maps'].at[map_row, 'onset_inward_10percent'] = \
            onset_inward_10percent
        experiment['average_maps'].at[map_row,
                                      'onset_outward_10percent'] = \
            onset_outward_10percent
        experiment['average_maps'].at[map_row,
                                      'risetime_inward_20to80percent'] = \
            risetime_inward_20to80percent
        experiment['average_maps'].at[map_row,
                                      'risetime_outward_20to80percent'] = \
            risetime_outward_20to80percent
        experiment['average_maps'].at[map_row, 'spike_location'] = \
            spike_location
        experiment['average_maps'].at[map_row, 'spike_number_detected'] = \
            spike_number_detected
    return experiment
