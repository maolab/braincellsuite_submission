"""Report ephys traces and electric features.
____> needs updating!! exact copy of visualize_ephys
All functions related to visualizing electrophysiology traces and electrical
features related to those traces. functions are build around experiment files,
containing sweep_tables.
Only supports up to two amplifiers for now!

Functions:
- Helper functions:
    - gen_plotly_amp_graphs: generates graph scheme for plotting in plotly.

- Execute functions:
    - plot_sweeps: generate graph of recorded sweeps and stimuli.
    - import_and_plot: Opens experiment and shows sweeps.
    - show_feature_sweep: Looks up sweep affiliated with feature value of
                          cellAmpID.
"""

import numpy as np
import plotly as py
import plotly.graph_objs as go
from brainCellSuite.analysis.ephys.features import get_spike_location

def gen_plotly_report_graphs(cellAmpID, feature_name='feature_test',
                             extra_plot_name='test'):
    """Generates graph scheme for plotting in plotly.

    To make sure that the plots in plot_sweeps are allocated to the correct
    subplots a graph scheme is generated based on number of amps. Make sure
    to remove the mock traces after populating the graphs with the real traces.

    Args:
        - cellAmpID: string, cell that is recorded within experiment, in case
                     cellAmpID is given the experiment name is based on the
                     cellAmpID.
        - feature_name: string, name of the table.
        - extra_plot_name: string, name of the extra plot.

    Returns:
        - fig: plotly object containing the graph scheme.
    """
    colors = ['teal', 'indigo']
    mock_trace = [0]
    mock_time = [0]
    plot_row = 2
    plot_col = 2
    subplot_titles = [cellAmpID, feature_name, 'stimulus', extra_plot_name]
    fig = py.subplots.make_subplots(rows=plot_row,
                                    cols=plot_col,
                                    shared_xaxes=True,
                                    shared_yaxes=False,
                                    subplot_titles=subplot_titles,
                                    specs=[[{"type": "scatter"},
                                            {"type": "table"}],
                                           [{"type": "scatter"},
                                            {"type": "scatter"}]])
    posc = [1, 1, 2, 2]
    posr = [1, 2, 1, 2]
    ypos = [1, 3, 2, 4]
    for i in np.arange(0, 3):
        if i == 2:
            trace = go.Table(header=dict(values=[0, 1]),
                            cells=dict(values=[0, 2]))
        else:
            trace = go.Scattergl(x=mock_time, y=mock_trace,
                                 legendgroup='scales', name='scales',
                                 showlegend=False, mode='lines')
        fig.append_trace(trace, posr[i], posc[i])
        if i != 2:
            fig['data'][-1].update(yaxis=('y' + str(ypos[i])))
    fig['layout']['xaxis'].update(title='time (s)', showgrid=False,
                                  zeroline=False, ticks='outside')
    fig['layout']['xaxis2'].update(title='extra_plot_x', showgrid=False,
                                  zeroline=False, ticks='outside')
    fig['layout']['yaxis'].update(title='membrane potential (mV)',
                                  color=colors[0], showgrid=False,
                                  zeroline=False, showline=False,
                                  showticklabels=True, ticks='outside')
    # fig['layout']['yaxis2'].update(title='tableY',
    #                               color=colors[0], showgrid=False,
    #                               zeroline=False, showline=False,
    #                               showticklabels=True, ticks='outside')
    fig['layout']['yaxis2'].update(title='current injection (pA)',
                                  color=colors[0], showgrid=False,
                                  zeroline=False, showline=False,
                                  showticklabels=True, ticks='outside')
    fig['layout']['yaxis3'].update(title='extra_plot_y',
                                  color=colors[0], showgrid=False,
                                  zeroline=False, showline=False,
                                  showticklabels=True, ticks='outside')
    fig['layout'].update(showlegend=True, legend=dict(x=1.05, y=1))
    return fig


def plot_sweeps_report(experiment, report_name=None, table_feature_list=None,
                       feature_table=None,
                       cellAmpID=None, sweeps=None, amp_select=None,
                       mode_select=None, show_spikes=True,
                       restrict_spike_detection=True, hide_discarded=False,
                       isolate_spike=False, save_path=None,
                       jupyter_inline=False):
    """Generate graph of recorded sweeps and stimuli.

    Uses sweep_table from experiment (requires read_ephus_experiment-function)
    to plot a selection of sweeps. Only supports up to two amplifiers for now!

    Args:
        - experiment: nested dictionary.
        - report_name: string, name of the report plot for saving.
        - table_feature_list: list, containing features to be put in the table.
        - feature_table: pandas dataframe, containing features
        - cellAmpID: string, cell that is recorded within experiment, in case
                     cellAmpID is given the experiment name is based on the
                     cellAmpID.
        - sweeps: either list of integers that correspond to sweep numbers, or
                  list of full names of sweeps, e.g., fullname: 'sweep0000' = 0.
        - amp_select: string, exact match to name of amplifier in experiment.
        - mode_select: string, exact match to amplifier mode.
        - show_spikes: bool, True: shows spike peaks for each trace
        - restrict_spike_detection: bool, True: only displays spikes within
                                    stimulation period.
        - hide_discarded: bool, True: shows all sweep excluding the discarded.
        - isolate_spike =  bool, True: if spikes present will crop trace to
                           onset of stimulation till onset second spike.
        - save_path = fullpath to directory where plotly plot should be saved.
        - jupyter_inline: bool, True: produces inline plot (jupyter only!)
    Returns:
        - fig: plotly object
        - sweep_table: subset version of experiment['sweep_table'] used for
                       plotting graphs.

    """
    fig = None
    if isinstance(sweeps, np.ndarray):
        sweeps = np.ndarray.tolist(sweeps)
    if isinstance(sweeps, int):
        sweeps = [("sweep" + str(sweeps).zfill(4))]
    if isinstance(sweeps, str):
        sweeps = [sweeps]
    if isinstance(sweeps, list) and isinstance(sweeps[0], int):
        sweeps = ["sweep" + str(num_sweep).zfill(4) for num_sweep in sweeps]
    sweep_table = experiment['sweep_table'][
        experiment['sweep_table']['sweep'].isin(sweeps)]
    if hide_discarded:
        sweep_table = sweep_table[sweep_table['discard'] != True]
    amps = sweep_table['amplifier'].unique()
    modes = sweep_table['amp_mode'].unique()
    if amp_select:
        if amp_select in amps:
            if isinstance(amp_select, list):
                sweep_table = sweep_table[
                    sweep_table['amplifier'].isin(amp_select)]
            elif isinstance(amp_select, str):
                    sweep_table = sweep_table[
                        sweep_table['amplifier'].isin([amp_select])]
        else:
            print("amp_select input doesn't match amplifier names in " +
                  "experiment: " + str(amps))
            return fig, sweep_table
    if mode_select:
        if mode_select in modes:
            if isinstance(mode_select, list):
                sweep_table = sweep_table[
                    sweep_table['amp_mode'].isin(mode_select)]
            elif isinstance(mode_select, str):
                sweep_table = sweep_table[
                    sweep_table['amp_mode'].isin([mode_select])]
        else:
            print("mode_select input doesn't match amplifier names in " +
                  "experiment: " + str(modes))
            return fig, sweep_table
    amps = sweep_table['amplifier'].unique()
    colorI = 'teal'
    colorV = 'indigo'
    fig = gen_plotly_report_graphs(cellAmpID, feature_name='feature_test',
                             extra_plot_name='test')
    sweep_pre = experiment['sweep_table']['sweep'][0]
    step = 0
    group_amp1 = 'group' + str(0 + step)
    spike_pos = 0
    for ind, sweep_info in sweep_table.iterrows():
        amp = sweep_info['amplifier']
        sweep = sweep_info['sweep']
        trace = experiment['acquired_data'][sweep]['ephys'][amp]['data']
        stim = experiment['stim_data'][sweep]['ephys'][amp]['stim_array']
        samplerate = sweep_info['samplerate']
        time = np.arange(0, len(trace)) / samplerate
        amp_mode_sweep = sweep_info['amp_mode']
        if amp_mode_sweep == 'I-Clamp' and show_spikes:
            if restrict_spike_detection:
                start_t = sweep_info['ephys_stim_pulseDelay']
                end_t = start_t + sweep_info['ephys_stim_pulseWidth']
            else:
                start_t = 0
                end_t = -1
            time_peak, _, _, _ = get_spike_location(trace=trace,
                                                    start=start_t,
                                                    end=end_t,
                                                    samplerate=samplerate)
            if not isinstance(time_peak, float):
                peak_times = (time_peak + start_t) / samplerate
                peaks = np.ones(len(peak_times))*(60 + spike_pos)
                spike_pos += 1
        if sweep != sweep_pre:
            step += 2
            group_amp1 = 'group' + str(0+step)
        if len(amps) == 1:
            if amp == amps[0] and amp_mode_sweep == 'I-Clamp':
                amp1_IC = go.Scattergl(x=time, y=trace, legendgroup=group_amp1,
                                       name=sweep, yaxis='y1',
                                       line=dict(color=colorI, width=2))
                amp1_IC_stim = go.Scattergl(x=time, y=stim,
                                            legendgroup=group_amp1,
                                            name=amp, yaxis='y3',
                                            line=dict(color=colorI, width=2))
                fig.append_trace(amp1_IC, 1, 1)
                fig['data'][-1].update(yaxis='y1')
                fig.append_trace(amp1_IC_stim, 2, 1)
                fig['data'][-1].update(yaxis='y3')
                if show_spikes:
                    if not isinstance(time_peak, float):
                        spike_amp1_IC = go.Scattergl(x=peak_times, y=peaks,
                                                     legendgroup=group_amp1,
                                                     name='action potential',
                                                     yaxis='y1', mode='markers',
                                                     marker=dict(
                                                         color=colorI, size=2))
                        fig.append_trace(spike_amp1_IC, 1, 1)
                        fig['data'][-1].update(yaxis='y1')
                if isolate_spike:
                    if not isinstance(time_peak, float):
                        if len(peak_times) > 1:
                            fig['layout']['xaxis'].update(
                                range=[peak_times[0]-0.005, peak_times[1]])

    # plot table with features


    fig['layout']['showlegend']=False
    if jupyter_inline:
        from plotly.offline import init_notebook_mode, iplot
        init_notebook_mode(
            connected=True)  # initiate notebook for offline plot
        iplot(fig)
    else:
        py.offline.plot(fig)
    # if save_path == 0 or save_path is None:
    #     return fig, sweep_table
    # else:
    #     save_path = os.path.join(save_path, report_name,str(cellAmpID) +
    #     '.html')
    #     py.offline.plot(fig, filename=save_path)
    return fig, sweep_table
