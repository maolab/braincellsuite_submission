"""Visualize ephys traces and electric features.

All functions related to visualizing electrophysiology traces and electrical
features related to those traces. functions are build around experiment files,
containing sweep_tables.
Only supports up to two amplifiers for now!

Functions:
- Helper functions:
    - gen_plotly_amp_graphs: generates graph scheme for plotting in plotly.

- Execute functions:
    - plot_sweeps: generate graph of recorded sweeps and stimuli.
    - import_and_plot: Opens experiment and shows sweeps.
    - show_feature_sweep: Looks up sweep affiliated with feature value of
                          cellAmpID.
"""

import numpy as np
import pandas as pd
import os
import plotly as py
import plotly.graph_objs as go
import plotly.tools as tools
from brainCellSuite.analysis.ephys.features import get_spike_location
from brainCellSuite._io.xsg2NWB import read_ephus_experiment
from brainCellSuite._io.readDBCSV import read_mostrecent_DBCSV
from brainCellSuite import bcs_global


def gen_plotly_amp_graphs(amps):
    """Generates graph scheme for plotting in plotly.

    To make sure that the plots in plot_sweeps are allocated to the correct
    subplots a graph scheme is generated based on number of amps. Make sure
    to remove the mock traces after populating the graphs with the real traces.

    Args:
        - amps: list of amplifier names.

    Returns:
        - fig: plotly object containing the graph scheme.
    """
    colors = ['teal', 'indigo']
    mock_trace = [0]
    mock_time = [0]
    plot_row = 2
    plot_col = len(amps)
    subplot_titles = [amps[0], 'stimulus']
    if len(amps) == 2:
        subplot_titles = [amps[0], amps[1], 'stimulus', 'stimulus']
    fig = tools.make_subplots(rows=plot_row, cols=plot_col, shared_xaxes=True,
                              shared_yaxes=False, subplot_titles=subplot_titles)
    if len(amps) == 1:
        pos = [1, 1, 2, 2]
        ypos = [1, 3, 2, 4]
        for i in np.arange(0, 4):
            trace = go.Scattergl(x=mock_time, y=mock_trace,
                                 legendgroup='scales',
                                 name='scales',
                                 showlegend=False, mode='lines')
            fig.append_trace(trace, pos[i], 1)
            fig['data'][-1].update(yaxis=('y' + str(ypos[i])))
        fig['layout']['xaxis'].update(title='time(s)', showgrid=False,
                                      zeroline=False, ticks='outside')
        fig['layout']['yaxis'].update(title='membrane potential (mV)',
                                      color=colors[0], showgrid=False,
                                      zeroline=False, showline=False,
                                      showticklabels=True, ticks='outside')
        fig['layout']['yaxis2'].update(title='current injected (pA)',
                                       color=colors[0], showgrid=False,
                                       zeroline=False, showline=False,
                                       showticklabels=True, ticks='outside')
        fig['layout']['yaxis3'] = dict(title='membrane current (pA)',
                                       overlaying='y1', side='right',
                                       color=colors[1], showgrid=False,
                                       zeroline=False, showline=False,
                                       showticklabels=True, ticks='outside')
        fig['layout']['yaxis4'] = dict(title='voltage injected (mV)',
                                       overlaying='y2', side='right',
                                       color=colors[1], showgrid=False,
                                       zeroline=False, showline=False,
                                       showticklabels=True, ticks='outside')
    if len(amps) == 2:
        posc = [1, 1, 1, 1, 2, 2, 2, 2]
        posr = [1, 2, 1, 2, 1, 2, 1, 2]
        ypos = [1, 3, 5, 7, 2, 4, 6, 8]
        for i in np.arange(0, 8):
            trace = go.Scattergl(x=mock_time, y=mock_trace,
                                 legendgroup='scales', name='scales',
                                 showlegend=False, mode='lines')
            fig.append_trace(trace, posr[i], posc[i])
            fig['data'][-1].update(yaxis=('y' + str(ypos[i])))
        fig['layout']['xaxis'].update(title='time (s)', showgrid=False,
                                      zeroline=False, ticks='outside')
        fig['layout']['xaxis2'].update(title='time (s)', showgrid=False,
                                      zeroline=False, ticks='outside')
        fig['layout']['yaxis'].update(title='membrane potential (mV)',
                                      color=colors[0], showgrid=False,
                                      zeroline=False, showline=False,
                                      showticklabels=True, ticks='outside')
        fig['layout']['yaxis2'].update(title='membrane potential (mV)',
                                      color=colors[0], showgrid=False,
                                      zeroline=False, showline=False,
                                      showticklabels=True, ticks='outside')
        fig['layout']['yaxis3'].update(title='current injection (pA)',
                                      color=colors[0], showgrid=False,
                                      zeroline=False, showline=False,
                                      showticklabels=True, ticks='outside')
        fig['layout']['yaxis4'].update(title='current injection (pA)',
                                      color=colors[0], showgrid=False,
                                      zeroline=False, showline=False,
                                      showticklabels=True, ticks='outside')
        fig['layout']['yaxis5'] = dict(title='membrane current (pA)',
                                       color=colors[1], showgrid=False,
                                       zeroline=False, showline=False,
                                       showticklabels=True, ticks='outside',
                                       overlaying='y1', side='right')
        fig['layout']['yaxis6'] = dict(title='membrane current (pA)',
                                       color=colors[1], showgrid=False,
                                       zeroline=False, showline=False,
                                       showticklabels=True, ticks='outside',
                                       overlaying='y2', side='right',
                                       anchor='x2')
        fig['layout']['yaxis7'] = dict(title='voltage injection (mV)',
                                       color=colors[1], showgrid=False,
                                       zeroline=False, showline=False,
                                       showticklabels=True, ticks='outside',
                                       overlaying='y3', side='right')
        fig['layout']['yaxis8'] = dict(title='voltage injection (mV)',
                                       color=colors[1], showgrid=False,
                                       zeroline=False, showline=False,
                                       showticklabels=True, ticks='outside',
                                       overlaying='y4', side='right',
                                       anchor='x2')
    fig['layout'].update(showlegend=True, legend=dict(x=1.05, y=1))
    return fig


def plot_sweeps(experiment, cellAmpID=None, sweeps=None, amp_select=None,
                mode_select=None, show_spikes=True,
                restrict_spike_detection=True, hide_discarded=False,
                isolate_spike=False, save_path=None,
                jupyter_inline=False):
    """Generate graph of recorded sweeps and stimuli.

    Uses sweep_table from experiment (requires read_ephus_experiment-function)
    to plot a selection of sweeps. Only supports up to two amplifiers for now!

    Args:
        - experiment: nested dictionary.
        - cellAmpID: string, cell that is recorded within experiment, in case
                     cellAmpID is given the experiment name is based on the
                     cellAmpID.
        - sweeps: either list of integers that correspond to sweep numbers, or
                  list of full names of sweeps, e.g., fullname: 'sweep0000' = 0.
        - amp_select: string, exact match to name of amplifier in experiment.
        - mode_select: string, exact match to amplifier mode.
        - show_spikes: bool, True: shows spike peaks for each trace
        - restrict_spike_detection: bool, True: only displays spikes within
                                    stimulation period.
        - hide_discarded: bool, True: shows all sweep excluding the discarded.
        - isolate_spike =  bool, True: if spikes present will crop trace to
                           onset of stimulation till onset second spike.
        - save_path = fullpath to directory where plotly plot should be saved.
        - jupyter_inline: bool, True: produces inline plot (jupyter only!)
    Returns:
        - fig: plotly object
        - sweep_table: subset version of experiment['sweep_table'] used for
                       plotting graphs.

    """
    fig = None
    if isinstance(sweeps, np.ndarray):
        sweeps = np.ndarray.tolist(sweeps)
    if isinstance(sweeps, int):
        sweeps = [("sweep" + str(sweeps).zfill(4))]
    if isinstance(sweeps, str):
        sweeps = [sweeps]
    if isinstance(sweeps, list) and isinstance(sweeps[0], int):
        sweeps = ["sweep" + str(num_sweep).zfill(4) for num_sweep in sweeps]
    sweep_table = experiment['sweep_table'][
        experiment['sweep_table']['sweep'].isin(sweeps)]
    if hide_discarded:
        sweep_table = sweep_table[sweep_table['discard'] != True]
    amps = sweep_table['amplifier'].unique()
    modes = sweep_table['amp_mode'].unique()
    if amp_select:
        if amp_select in amps:
            if isinstance(amp_select, list):
                sweep_table = sweep_table[
                    sweep_table['amplifier'].isin(amp_select)]
            elif isinstance(amp_select, str):
                    sweep_table = sweep_table[
                        sweep_table['amplifier'].isin([amp_select])]
        else:
            print("amp_select input doesn't match amplifier names in " +
                  "experiment: " + str(amps))
            return fig, sweep_table
    if mode_select:
        if mode_select in modes:
            if isinstance(mode_select, list):
                sweep_table = sweep_table[
                    sweep_table['amp_mode'].isin(mode_select)]
            elif isinstance(mode_select, str):
                sweep_table = sweep_table[
                    sweep_table['amp_mode'].isin([mode_select])]
        else:
            print("mode_select input doesn't match amplifier names in " +
                  "experiment: " + str(modes))
            return fig, sweep_table
    amps = sweep_table['amplifier'].unique()
    colorI = 'teal'
    colorV = 'indigo'
    fig = gen_plotly_amp_graphs(amps)
    sweep_pre = experiment['sweep_table']['sweep'][0]
    step = 0
    group_amp1 = 'group' + str(0 + step)
    group_amp2 = 'group' + str(1 + step)
    spike_pos = 0
    for ind, sweep_info in sweep_table.iterrows():
        amp = sweep_info['amplifier']
        sweep = sweep_info['sweep']
        trace = experiment['acquired_data'][sweep]['ephys'][amp]['data']
        stim = experiment['stim_data'][sweep]['ephys'][amp]['stim_array']
        samplerate = sweep_info['samplerate']
        time = np.arange(0, len(trace)) / samplerate
        amp_mode_sweep = sweep_info['amp_mode']
        if amp_mode_sweep == 'I-Clamp' and show_spikes:
            if restrict_spike_detection:
                start_t = sweep_info['ephys_stim_pulseDelay']
                end_t = start_t + sweep_info['ephys_stim_pulseWidth']
            else:
                start_t = 0
                end_t = -1
            time_peak, _, _, _ = get_spike_location(trace=trace,
                                                    start=start_t,
                                                    end=end_t,
                                                    samplerate=samplerate)
            if not isinstance(time_peak, float):
                peak_times = (time_peak + start_t) / samplerate
                peaks = np.ones(len(peak_times))*(60 + spike_pos)
                spike_pos += 1
        if sweep != sweep_pre:
            step += 2
            group_amp1 = 'group' + str(0+step)
            group_amp2 = 'group' + str(1+step)
        if len(amps) == 1:
            if amp == amps[0] and amp_mode_sweep == 'I-Clamp':
                amp1_IC = go.Scattergl(x=time, y=trace, legendgroup=group_amp1,
                                       name=sweep, yaxis='y1',
                                       line=dict(color=colorI, width=2))
                amp1_IC_stim = go.Scattergl(x=time, y=stim,
                                            legendgroup=group_amp1,
                                            name=amp, yaxis='y2',
                                            line=dict(color=colorI, width=2))
                fig.append_trace(amp1_IC, 1, 1)
                fig['data'][-1].update(yaxis='y1')
                fig.append_trace(amp1_IC_stim, 2, 1)
                fig['data'][-1].update(yaxis='y2')
                if show_spikes:
                    if not isinstance(time_peak, float):
                        spike_amp1_IC = go.Scattergl(x=peak_times, y=peaks,
                                                     legendgroup=group_amp1,
                                                     name='action potential',
                                                     yaxis='y1', mode='markers',
                                                     marker=dict(
                                                         color=colorI, size=2))
                        fig.append_trace(spike_amp1_IC, 1, 1)
                        fig['data'][-1].update(yaxis='y1')
                if isolate_spike:
                    if not isinstance(time_peak, float):
                        if len(peak_times) > 1:
                            fig['layout']['xaxis'].update(
                                range=[peak_times[0]-0.005, peak_times[1]])
            if amp == amps[0] and amp_mode_sweep == 'V-Clamp':
                amp1_VC = go.Scattergl(x=time,
                                       y=trace,
                                       legendgroup=group_amp1,
                                       name=sweep,
                                       yaxis='y3',
                                       line=dict(color=colorV,
                                                 width=2))
                amp1_VC_stim = go.Scattergl(x=time,
                                            y=stim,
                                            legendgroup=group_amp1,
                                            name=amp,
                                            yaxis='y4',
                                            line=dict(color=colorV,
                                                      width=2))
                fig.append_trace(amp1_VC, 1, 1)
                fig['data'][-1].update(yaxis='y3')
                fig.append_trace(amp1_VC_stim, 2, 1)
                fig['data'][-1].update(yaxis='y4')
        if len(amps) == 2:
            if amp == amps[0] and amp_mode_sweep == 'I-Clamp':
                amp1_IC = go.Scattergl(x=time, y=trace, legendgroup=group_amp1,
                                       name=sweep, yaxis='y1',
                                       line=dict(color=colorI, width=2))
                amp1_IC_stim = go.Scattergl(x=time, y=stim,
                                            legendgroup=group_amp1,
                                            name=amp, yaxis='y3',
                                            line=dict(color=colorI, width=2))
                fig.append_trace(amp1_IC, 1, 1)
                fig['data'][-1].update(yaxis='y1')
                fig.append_trace(amp1_IC_stim, 2, 1)
                fig['data'][-1].update(yaxis='y3')
                if show_spikes:
                    if not isinstance(time_peak, float):
                        spike_amp1_IC = go.Scattergl(x=peak_times, y=peaks,
                                                     legendgroup=group_amp1,
                                                     name='action potential',
                                                     yaxis='y1', mode='markers',
                                                     marker=dict(
                                                         color=colorI, size=2))
                        fig.append_trace(spike_amp1_IC, 1, 1)
                        fig['data'][-1].update(yaxis='y1')
            if amp == amps[0] and amp_mode_sweep == 'V-Clamp':
                amp1_VC = go.Scattergl(x=time, y=trace, legendgroup=group_amp1,
                                       name=sweep, yaxis='y5',
                                       line=dict(color=colorV, width=2))
                amp1_VC_stim = go.Scattergl(x=time, y=stim,
                                            legendgroup=group_amp1,
                                            name=amp, yaxis='y7',
                                            line=dict(color=colorV, width=2))
                fig.append_trace(amp1_VC, 1, 1)
                fig['data'][-1].update(yaxis='y5')
                fig.append_trace(amp1_VC_stim, 2, 1)
                fig['data'][-1].update(yaxis='y7')
            if amp == amps[1] and amp_mode_sweep == 'I-Clamp':
                amp2_IC = go.Scattergl(x=time, y=trace, legendgroup=group_amp2,
                                       name=sweep, yaxis='y2',
                                       line=dict(color=colorI, width=2))
                amp2_IC_stim = go.Scattergl(x=time, y=stim,
                                            legendgroup=group_amp2, name=amp,
                                            yaxis='y4',
                                            line=dict(color=colorI, width=2))
                fig.append_trace(amp2_IC, 1, 2)
                fig['data'][-1].update(yaxis='y2')
                fig.append_trace(amp2_IC_stim, 2, 2)
                fig['data'][-1].update(yaxis='y4')
                if show_spikes:
                    if not isinstance(time_peak, float):
                        spike_amp2_IC = go.Scattergl(x=peak_times, y=peaks,
                                                     legendgroup=group_amp2,
                                                     name='action potential',
                                                     yaxis='y2', mode='markers',
                                                     marker=dict(
                                                         color=colorI, size=2))
                        fig.append_trace(spike_amp2_IC, 1, 2)
                        fig['data'][-1].update(yaxis='y2')
            if amp == amps[1] and amp_mode_sweep == 'V-Clamp':
                amp2_VC = go.Scattergl(x=time, y=trace, legendgroup=group_amp2,
                                       name=sweep, yaxis='y6',
                                       line=dict(color=colorV, width=2))
                amp2_VC_stim = go.Scattergl(x=time, y=stim,
                                            legendgroup=group_amp2, name=amp,
                                            yaxis='y8',
                                            line=dict(color=colorV, width=2))
                fig.append_trace(amp2_VC, 1, 2)
                fig['data'][-1].update(yaxis='y6')
                fig.append_trace(amp2_VC_stim, 2, 2)
                fig['data'][-1].update(yaxis='y8')

    if jupyter_inline:
        from plotly.offline import init_notebook_mode, iplot
        init_notebook_mode(connected=True)  # initiate notebook for offline plot
        iplot(fig)
    else:
        py.offline.plot(fig)
    if save_path == 0 or save_path is None:
        return fig, sweep_table
    else:
        save_path = os.path.join(save_path, str(cellAmpID) + '.html')
        py.offline.plot(fig, filename=save_path)
        return fig, sweep_table


def import_and_plot(experiment_name=None,
                    db_path=bcs_global.path_database,
                    ephys_path=bcs_global.path_ephys_data,
                    sweeps=None,
                    cellAmpID=None,
                    isolate_spike=False,
                    save_path=None,
                    jupyter_inline=False):
    """Opens experiment and shows sweeps.

    Args:
        - experiment: string, name of the experiment.
        - experiment_folder: string, fullpath where experiment is saved.
        - sweeps: vector of string or integers of sweep names
        - cellAmpID: string, cell that is recorded within experiment, in case
                     cellAmpID is given the experiment name is based on the
                     cellAmpID.
        - isolate_spike =  bool, True: if spikes present will crop trace to
                           onset of stimulation till onset second spike.
        - save_path = fullpath to directory where plotly plot should be saved.
        - jupyter_inline: bool, True: produces inline plot (jupyter only!)

    Returns:
        None
    """
    amp_select = None

    if cellAmpID:
        experiment_folder = os.path.join(ephys_path, cellAmpID[:-2])
        if not os.path.exists(experiment_folder):
            print(experiment_folder + ' not found')

    experiment = read_ephus_experiment(experiment_name, experiment_folder)
    _, _ = plot_sweeps(experiment,
                       cellAmpID=cellAmpID,
                       sweeps=sweeps,
                       amp_select=amp_select,
                       isolate_spike=isolate_spike,
                       save_path=save_path,
                       jupyter_inline=jupyter_inline)


def show_feature_sweep(cellAmpID, feature,
                       feature_sweep_table_path=bcs_global.path_ephys_analysis,
                       feature_sweep_table_filename=None,
                       filebase=bcs_global.filebase_ephys_feature_sweeps,
                       db_path=bcs_global.path_database,
                       isolate_spike=False,
                       save_path=None,
                       jupyter_inline=False):
    """Looks up sweep affiliated with feature value of cellAmpID.


    Args:
        - cellAmpID: string, cellAmpID of cell.
        - feature: string, exact name of feature of which the corresponding
            sweep for the cellAmpID is shown.
        - feature_sweep_table_path: fullpath to directory where
            feature_sweep_table is stored.
        - feature_sweep_table_filename: exact filename of the
            feature_sweep_table, by default empty. In that case open the most
            recent feature_sweep_table.
        - filebase = basename of the .csv file containing the sweeps
        - isolate_spike =  bool, True: if spikes present will crop trace to
                           onset of stimulation till onset second spike.
        - save_path = fullpath to directory where plotly plot should be saved.
        - jupyter_inline= bool, True: produces inline plot (jupyter only!)

    Returns:
        None
    """
    if feature_sweep_table_filename:
        feature_sweep_table = pd.read_csv(
            os.path.join(feature_sweep_table_path,
                         feature_sweep_table_filename))
    else:
        feature_sweep_table, _ = read_mostrecent_DBCSV(feature_sweep_table_path,
                                                       filebase)

    feature_sweep_table = feature_sweep_table.set_index('cellAmpID')
    if feature in list(
            feature_sweep_table.columns) and \
            cellAmpID in feature_sweep_table.index:
        sweep = feature_sweep_table.loc[cellAmpID, feature]
        import_and_plot(experiment_name=None,
                        db_path=db_path,
                        ephys_path=bcs_global.path_ephys_data,
                        sweeps=sweep,
                        cellAmpID=cellAmpID,
                        jupyter_inline=jupyter_inline,
                        isolate_spike=isolate_spike,
                        save_path=save_path)
    else:
        if not feature in list(
            feature_sweep_table.columns):
            print(feature + ' was not found in the table')
        if not cellAmpID in feature_sweep_table.index:
            print(cellAmpID + ' was not found in the table')
        return


def visualize_features(features,
                       feature_table_path=bcs_global.path_ephys_analysis,
                       date=None,
                       filebase=bcs_global.filebase_ephys_feature_sweeps,
                       feature_table=None,
                       feature_sweep_table=None,
                       color_categories=None,
                       boxplot_cat=None,
                       select_cellAmpIDs=None,
                       jupyter_inline=False):
    """

    Args:
        features:
        feature_table_path:
        date:
        filebase:
        feature_table:
        feature_sweep_table:
        color_categories:
        boxplot_cat:
        select_cellAmpIDs:
        jupyter_inline:

    Returns:

    """
    if not feature_table and not feature_sweep_table:
        if date:
            feature_sweep_table = pd.read_csv(
                os.path.join(feature_table_path,
                             (filebase+date+'.csv')))
            feature_table = pd.read_csv(
                os.path.join(feature_table_path,
                             (filebase[:-11] + date + '.csv')))
        else:
            feature_sweep_table, _ = read_mostrecent_DBCSV(feature_table_path,
                                                           filebase)
            feature_table, _ = read_mostrecent_DBCSV(feature_table_path,
                                                     filebase[:-10]+'_')
    if not isinstance(features, list):
        print('Warning! Make sure to provide features as a list!')
        return
    feature_sweep_table = feature_sweep_table.set_index('cellAmpID')
    feature_table = feature_table.set_index('cellAmpID')
    features.append('rel_soma_position')
    # only look into baseline condition
    feature_table = feature_table[
        feature_table['drug_condition'] == 'condition_0']
    feature_sweep_table = feature_sweep_table[
        feature_sweep_table['drug_condition'] == 'condition_0']
    # select the requested features
    feature_table = feature_table[features]
    feature_sweep_table = feature_sweep_table[features]
    # generate selection mask
    mask = np.invert(np.any(feature_table.isna(), 1))
    feature_table = feature_table.loc[mask]
    feature_sweep_table = feature_sweep_table.loc[mask]

    if isinstance(select_cellAmpIDs, list):
        # only display select_cellAmpIDs that are in the feature_table
        select_cellAmpIDs = list(
            set(feature_table.index.tolist()).intersection(select_cellAmpIDs))
        feature_table = feature_table.loc[select_cellAmpIDs]
        feature_sweep_table = feature_sweep_table.loc[select_cellAmpIDs]

    if color_categories:
        cell_ID_cat = list(color_categories.keys())
        # only display cell_IDs that are in the feature_table
        cell_ID_cat = list(
            set(feature_table.index.tolist()).intersection(cell_ID_cat))

        feature_table = feature_table.loc[cell_ID_cat]
        feature_sweep_table = feature_sweep_table.loc[cell_ID_cat]

        select_cells = list(feature_table.index)
        colors = {key: color_categories[key] for key in select_cells}
        color = list(colors.values())
        dp_label = np.array(color)
    else:
        layers = [0, 0.1, 0.15, 0.3, 0.5, 0.7, 0.9, 1]
        labels = ['L1', 'L2', 'L3', 'L5a', 'L5b', 'L6', 'Clau']
        select_cells_layer = list(pd.cut(feature_table['rel_soma_position'],
                                         bins=layers,
                                         labels=labels))
        colors = {'L1': 'darkgray',
                  'L2': 'coral',
                  'L3': 'firebrick',
                  'L5a': 'aquamarine',
                  'L5b': 'deepskyblue',
                  'L6': 'blue',
                  'Clau': 'black',
                  'nan': 'grey'}
        color = [colors[str(layer)] for layer in select_cells_layer]
        dp_label = np.array(select_cells_layer)
    # make plots
    fig = None
    if len(features) == 2:
        if boxplot_cat:
            data = []
            for category, color in colors.items():
                box_data = feature_table.loc[dp_label == category]
                box_sweep_data = feature_sweep_table.loc[box_data.index]
                data.append(go.Box(
                    y=box_data[features[0]],
                    name=category,
                    boxpoints='all',
                    jitter=0.5,
                    whiskerwidth=0.2,
                    fillcolor=color,
                    marker=dict(
                        size=5),
                    text=[m + ' : ' + n + ' : ' + o
                          for m, n, o in zip(dp_label,
                                             box_data.index,
                                             box_sweep_data[features[0]].
                                             astype(str))],
                    line=dict(width=2),
                ))
            layout = go.Layout(
                title='category vs.' + features[0],
                xaxis=dict(title='category'),
                yaxis=dict(title=features[0])
            )

        elif not boxplot_cat:
            data = [
                go.Scatter(
                    x=np.arange(0, len(feature_table[features[0]])),
                    y=feature_table[features[0]],
                    mode='markers',
                    marker=dict(color=color),
                    text= [m + ' : ' + n + ' : ' + o
                           for m, n, o in zip(dp_label,
                                            feature_sweep_table.index,
                                            feature_sweep_table[features[0]].
                                            astype(str))]
                )
            ]
            layout = go.Layout(
                title='cells vs.' + features[0],
                xaxis=dict(title='cells'),
                yaxis=dict(title=features[0])
            )
    if len(features) == 3:
        fig = tools.make_subplots(rows=2, cols=2)
        scatter = go.Scatter(
                x=feature_table[features[0]],
                y=feature_table[features[1]],
                mode='markers',
                marker=dict(color=color),
                text=[m + ' : ' + n + ' : ' + o + ' : ' + p
                      for m, n, o, p in zip(dp_label,
                                            feature_sweep_table.index,
                                            feature_sweep_table[
                                                features[0]].astype(str),
                                            feature_sweep_table[
                                                features[1]].astype(str))]
            )

        for category, color in colors.items():
            histY = go.Histogram(y=feature_table.loc[dp_label == category,
                                                     features[1]],
                                 marker=dict(color=color),
                                 name=category,
                                 legendgroup=category)
            fig.append_trace(histY,2,2)
        # histX = list()
        for category, color in colors.items():
            histX = go.Histogram(
                x=feature_table.loc[dp_label == category, features[0]],
                marker=dict(color=color),
                name=category,
                legendgroup=category
            )
            fig.append_trace(histX, 1,1)


        fig.append_trace(scatter, 2, 1)
        # fig.append_trace(histY, 2, 2)
        # fig.append_trace(histX, 1, 1)
        fig['layout'].update(title=features[0] + ' vs. ' + features[1],
            xaxis1=dict(title=features[0]),
            yaxis1=dict(title='# cells'),
            xaxis3=dict(title=features[0]),
            yaxis3=dict(title=features[1]),
            yaxis4=dict(title=features[1]),
            xaxis4=dict(title='# cells'))

    if len(features) == 4:
        data = [
            go.Scatter3d(
                x=feature_table[features[0]],
                y=feature_table[features[1]],
                z=feature_table[features[2]],
                mode='markers',
                marker=dict(color=color),
                text = [m + ' : ' + n + ' : ' + o + ' : ' + p + ' : ' + r for
                             m, n, o, p, r in zip(dp_label,
                                               feature_sweep_table.index,
                                               feature_sweep_table[
                                                   features[0]].astype(str),
                                               feature_sweep_table[
                                                   features[1]].astype(str),
                                               feature_sweep_table[
                                                   features[2]].astype(str)
                                                  )]
            )
        ]
        layout = go.Layout(
            title=features[0] + ' vs. ' + features[1] + ' vs. ' + features[2],
            scene=dict(
                xaxis=dict(title=features[0]),
                yaxis=dict(title=features[1]),
                zaxis=dict(title=features[2]))
        )
    if not fig:
        fig = go.Figure(data=data, layout=layout)
    if jupyter_inline:
        from plotly.offline import init_notebook_mode, iplot
        init_notebook_mode(connected=True)  # initiate notebook for offline plot
        iplot(fig)
    else:
        py.offline.plot(fig, filename=(str(features)+'.html'))
    return feature_sweep_table, feature_table