"""Generate pages for website display

Requires morphology (SWC) and ephys (XSG with sweep_tables) data present and
described in the databases (slideDatabase.csv, insularCellDatabase.csv)

Functions
- generate_overview: Generates an interactive overview morphology page with
                     sub_pages.
- generate_overview_user: Generates overview page of morphologies based on list
                          of cellAmpIDs from user.
- generate_electric_properties: Generates spike train, waveform, ramp, passive
                                properties sites
- generate_dash_morph_ephys: Generates interactive graph of ephys and morphology
    data based on clusters.
- get_endpoints: extract endpoints of the morphology
- generate_morphology: Generates 3D morphology of neurons.
- generate_qr: Generate QR code refering to the
    neuro.muniak.com/cells/cellAmpID.html webpage
- generate_printable_morphology: Generate small printable images of the
    morphology w/o qr-code.
- generate_meta: Generates meta data site.
TODO
- generate_local_input: Generates local input maps for site
- generate_long_range_input: Generates long-range input map for site.
- generate_dash_morph_ephys: refine heat map. extend window fill into height,
    present sweeps? present morphology?
- generate_morphology: optimize the generation of the morphologies. Instead of
cellAmpID use a array of IDs and load once the df's
- generate_overview: Fill page with selected morphologies such that it allows
    the user to overview everything.
- generate_average_morphology: Generate a plotly figure containing the averaged
    cluster morphology.

Helper functions:
- show_feature_sweep: Looks up sweep affiliated with feature value of cellAmpID.
- import_and_plot: Opens experiment and shows sweeps.
- transform_cell: Transform a cell morphology based on radians.
"""

# first import all required packages (only need to run once!)

# import other important packages
import matplotlib as mpl
mpl.rcParams['savefig.pad_inches'] = 0
import os
from pathlib import Path
from PIL import Image
import copy
import sys
# required for plotting in plotly for websites:
import plotly as py
import plotly.graph_objs as go
import plotly.offline as py_offline
from IPython.core.display import display, HTML
from scipy.optimize import curve_fit
import seaborn as sns
from plotly.subplots import make_subplots

py_offline.init_notebook_mode()
from matplotlib import cm
import dash
from dash import dcc#import dash_core_components as dcc this is deprecated
from dash import html#import dash_html_components as html this is deprecated
from dash.dependencies import Input, Output
from sklearn import preprocessing
from sklearn import impute
import pyqrcode
import plotly.figure_factory as ff
from scipy.spatial.distance import pdist, squareform

from brainCellSuite.analysis.ephys.features import get_vm, \
    fit_2lin, get_spike_location
from brainCellSuite._io.xsg2NWB import read_ephus_experiment
from brainCellSuite._io.readDBCSV import readDBCSV, read_mostrecent_DBCSV, \
    get_databases
from brainCellSuite import bcs_global
from brainCellSuite import Cell
from brainCellSuite._io.readSWCplus import get_dataTree
from brainCellSuite.analysis.morphometry.calc_postFixDims import \
    extractContourData, extractsomaData, calc_cortexThickness
from itertools import compress
import matplotlib.pyplot as plt
from matplotlib import cm
from skimage import measure
import numpy as np
import pandas as pd
import re


def generate_overview():
    """

    Uses brainCellSuite.ini database links
    :return: site in html
    """
    # select all available morphologies [take most up-to-date database] that
    # are annotated, went through quality control, and have most of their
    # morphologies intact (completenessScore >= 3)
    db_path = bcs_global.path_database
    # df = readDBCSV(db_path=db_path, filename=bcs_global.filename_cell_db)
    # df = df.set_index('cellAmpID')
    # QC_path = bcs_global.path_qc_morph_data
    # df_morph_qc, _ = read_mostrecent_DBCSV(QC_path,
    #                                        bcs_global.filebase_morph_qc,
    #                                        index_on_cellAmpID=True)
    # df_ephys, _ = read_mostrecent_DBCSV(bcs_global.path_ephys_analysis,
    #                                        bcs_global.filebase_ephys_feature,
    #                                        index_on_cellAmpID=True)
    dbs = get_databases()
    df_ephys = dbs['ephys_df']
    df_morph_qc = dbs['morph_QC_df']
    df = dbs['cell_df']
    # Here we assume that htmls of the morphology as well thumbnails are
    # up-to-date
    all_cells = os.listdir(bcs_global.path_website)
    all_cells = [os.path.splitext(file_name)[0] for
                 file_name in all_cells if
                 os.path.splitext(file_name)[-1] == '.html' and
                 os.path.splitext(file_name)[0] in df.index]
    all_cells = df.loc[all_cells].sort_values(by='somaCortexPosition'). \
        index.tolist()
    QC3plus = df_morph_qc[df_morph_qc.completenessScore >= 3].index.tolist()
    QC3plus = df.loc[QC3plus].sort_values(by='somaCortexPosition').index.\
        tolist()
    QC3plusPYR = df.loc[QC3plus]
    QC3plusPYR = QC3plusPYR[QC3plusPYR.cellType == 'PYR'].sort_values(
        by='somaCortexPosition').index.tolist()
    QC3plusPYR_ephys = list(set(QC3plusPYR).intersection(df_ephys.index))
    QC3plusPYR_ephys = df_ephys.loc[QC3plusPYR_ephys]
    # subset further with presence of data on vsag and action potential
    QC3plusPYR_ephys = QC3plusPYR_ephys[
        np.logical_and(~np.isnan(QC3plusPYR_ephys['vsag_fraction_at-300pA']),
                       ~np.isnan(QC3plusPYR_ephys.ap_1_height))]
    # subset further for series resistance  under 40 MOhm
    QC3plusPYR_ephys = QC3plusPYR_ephys[
        QC3plusPYR_ephys.series_resistance <= 40]. \
        sort_values(by='rel_soma_position').index.tolist()
    ###
    QC2_5plus = df_morph_qc[df_morph_qc.completenessScore >= 2.5].index.tolist()
    QC2_5plus = df.loc[QC2_5plus].sort_values(by='somaCortexPosition').index. \
        tolist()
    QC2_5plusPYR = df.loc[QC2_5plus]
    QC2_5plusPYR = QC2_5plusPYR[QC2_5plusPYR.cellType == 'PYR'].sort_values(
        by='somaCortexPosition').index.tolist()
    QC2_5plusPYR_ephys = list(set(QC2_5plusPYR).intersection(df_ephys.index))
    QC2_5plusPYR_ephys = df_ephys.loc[QC2_5plusPYR_ephys]
    # subset further with presence of data on vsag and action potential
    QC2_5plusPYR_ephys = QC2_5plusPYR_ephys[
        np.logical_and(~np.isnan(QC2_5plusPYR_ephys['vsag_fraction_at-300pA']),
                       ~np.isnan(QC2_5plusPYR_ephys.ap_1_height))].\
        sort_values(by='rel_soma_position')
    # subset further for series resistance  under 40 MOhm
    QC2_5plusPYR_ephys = QC2_5plusPYR_ephys[
        QC2_5plusPYR_ephys.series_resistance <= 40].\
        sort_values(by='rel_soma_position').index.tolist()
    QC_tbd = df_morph_qc[np.isnan(df_morph_qc.completenessScore)].index.tolist()
    QC_tbd = df.loc[QC_tbd].sort_values(by='somaCortexPosition').index. \
        tolist()
    QC2_5 = df_morph_qc[df_morph_qc.completenessScore == 2.5].index.tolist()
    QC2_5 = df.loc[QC2_5].sort_values(by='somaCortexPosition').index. \
        tolist()
    print(QC2_5)
    QC2_5PYR = df.loc[QC2_5]
    QC2_5PYR = QC2_5PYR[QC2_5PYR.cellType == 'PYR'].sort_values(
        by='somaCortexPosition').index.tolist()
    print(QC2_5PYR)

    L23 = [cell for cell in all_cells
           if np.logical_and(df.loc[cell]['somaCortexPosition'] > 0.1,
                             df.loc[cell]['somaCortexPosition'] <= 0.3)]
    L5a = [cell for cell in all_cells
           if np.logical_and(df.loc[cell]['somaCortexPosition'] > 0.3,
                             df.loc[cell]['somaCortexPosition'] <= 0.5)]
    L5b = [cell for cell in all_cells
           if np.logical_and(df.loc[cell]['somaCortexPosition'] > 0.5,
                             df.loc[cell]['somaCortexPosition'] <= 0.7)]
    L6 = [cell for cell in all_cells
          if np.logical_and(df.loc[cell]['somaCortexPosition'] > 0.7,
                            df.loc[cell]['somaCortexPosition'] <= 0.9)]
    MD = [cell for cell in all_cells
          if df.loc[cell]['retrobeadsPresent'] == 'MDTh']
    PF = [cell for cell in all_cells
          if df.loc[cell]['retrobeadsPresent'] == 'PFTh']
    BLA = [cell for cell in all_cells
           if df.loc[cell]['retrobeadsPresent'] == 'BLA']
    CeA = [cell for cell in all_cells
           if df.loc[cell]['retrobeadsPresent'] == 'CeA']
    Ai = [cell for cell in all_cells
           if df.loc[cell]['retrobeadsPresent'] == 'Ai']
    PV = [cell for cell in all_cells
          if df.loc[cell]['cellType'] == 'PV']
    PYR = [cell for cell in all_cells
           if df.loc[cell]['cellType'] == 'PYR']
    D1 = [cell for cell in all_cells
          if df.loc[cell]['cellType'] == 'D1']
    GAD2 = [cell for cell in all_cells
            if df.loc[cell]['cellType'] == 'GAD2']
    overview_set = {'cell_overview': all_cells,
                    'cell_overview_QC_2_5_PYR': QC2_5PYR,
                    'cell_overview_QC_3plus': QC3plus,
                    'cell_overview_QC_3plus_PYR': QC3plusPYR,
                    'cell_overview_QC_3plus_PYR_w_Ephys': QC3plusPYR_ephys,
                    'cell_overview_QC_2_5plus': QC2_5plus,
                    'cell_overview_QC_2_5plus_PYR': QC2_5plusPYR,
                    'cell_overview_QC_2_5plus_PYR_w_Ephys': QC2_5plusPYR_ephys,
                    'cell_overview_QC_to_be_Determined': QC_tbd,
                    'cell_overview_L23': L23,
                    'cell_overview_L5a': L5a,
                    'cell_overview_L5b': L5b,
                    'cell_overview_L6': L6,
                    'cell_overview_CorticoThal_MD': MD,
                    'cell_overview_CorticoThal_PF': PF,
                    'cell_overview_CorticoBLA': BLA,
                    'cell_overview_CorticoCeA': CeA,
                    'cell_overview_CorticoAi': Ai,
                    'cell_overview_PV': PV,
                    'cell_overview_D1': D1,
                    'cell_overview_PYR': PYR,
                    'cell_overview_GAD2': GAD2}
    set_links = """<div class="wrapper">
                <div class="dropdown" id="button1">
                <button onclick="myFunction()"
                class="dropdown">Overviews</button>
                <div id="myDropdown"
                class="dropdown-content"
                style="width:500px;">"""
    for link in overview_set.keys():
        set_links += """<a href = "%s.html"> %s </a>\n""" % (
            link, (str(link) + ' : ' + str(len(overview_set[link])) +
                   ' neurons'))
    set_links += """</div></div>"""
    # make a list of links for cells
    cell_links = """
                <div class="dropdown" id="button2">
                <button onclick="myFunction2()"
                class="dropdown">Neurons</button>
                <div id="myDropdown2"
                class="dropdown-content"
                style="width:200px;">"""
    cells_sorted = np.array(all_cells)
    for cell in np.sort(cells_sorted):
        cell_links += """<a href = "%s.html"> %s </a>\n""" % (cell, cell)
    cell_links += """</div></div></div>"""

    for overview_name, neuron_set in overview_set.items():
        # make HTML page
        print(overview_name)
        image_links = """<div class="column"> """
        for ind, cellAmpID in enumerate(neuron_set):
            image_links += """<a href="./%s.html">
            <img src="./morph_thumbnails/%s.jpeg"
            style="max-width:75%s;"/> </a> \n""" % (
                cellAmpID, cellAmpID, "")
        image_links += """</div>"""

        # make an HTML plot with the plotly plot embedded.
        html_plot = HTML("""<html>
                            <head>
                            <link rel="stylesheet"
                            type="text/css" href="./stylz.css" />
                            </head>
                            <style>
                            .row {
                              display: flex;
                              flex-wrap: wrap;
                              padding: 0 4px;
                            }

                            /* Create two equal columns */
                            /*that sits next to each other */
                            .column {
                              flex: 25%;
                              padding: 0 4px;
                            }

                            .column img {
                              margin-top: 8px;
                              vertical-align: middle;
                            }
                            .btn-group {
                            display:inline-block;
                            float: top;}

                            </style>
                            <body>
                            <div class="btn-group">""" +

                         set_links +
                         cell_links +
                         """</div><div class="row">""" +

                         image_links +

                         """
                         </div>
                         </div>
                         <script>
    /* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
    function myFunction() {
      document.getElementById("myDropdown").classList.toggle("show");
    }
    function myFunction2() {
      document.getElementById("myDropdown2").classList.toggle("show");
    }
    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
      if (!event.target.matches('.dropdown')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }
    </script>
    </body>
    </html>""")
        with open(
                str(Path(bcs_global.path_website) / (overview_name + '.html')),
                'w') as f:
            f.write(html_plot.data)


def generate_overview_user(cellAmpIDs, save_name, modality='morph'):
    """Generate morphology overview websites based on user input.

    Uses brainCellSuite.ini database links
    Args:
        - cellAmpIDs: list, cellAmpIDs that are present in the cell database.
        - save_name: char, name of the webpage to be generated.
        - modality: char, name of the modality: 'morph or 'ephys'.

    :return: site in html
    """
    # select all available morphologies [take most up-to-date database] that
    # are annotated, went through quality control, and have most of their
    # morphologies intact (completenessScore >= 3)
    db_path = bcs_global.path_database
    # df = readDBCSV(db_path=db_path, filename=bcs_global.filename_cell_db)
    # df = df.set_index('cellAmpID')
    # QC_path = bcs_global.path_qc_morph_data
    # df_morph_qc, _ = read_mostrecent_DBCSV(QC_path,
    #                                        bcs_global.filebase_morph_qc,
    #                                        index_on_cellAmpID=True)
    # df_ephys, _ = read_mostrecent_DBCSV(bcs_global.path_ephys_analysis,
    #                                        bcs_global.filebase_ephys_feature,
    #                                        index_on_cellAmpID=True)
    dbs = get_databases()
    df_ephys = dbs['ephys_df']
    df_morph_qc = dbs['morph_QC_df']
    df = dbs['cell_df']
    # Here we assume that htmls of the morphology as well thumbnails are
    # up-to-date
    all_cells = os.listdir(bcs_global.path_website)
    all_cells = [os.path.splitext(file_name)[0] for
                 file_name in all_cells if
                 os.path.splitext(file_name)[-1] == '.html' and
                 os.path.splitext(file_name)[0] in df.index]
    all_cells = df.loc[all_cells].sort_values(by='somaCortexPosition'). \
        index.tolist()
    overview_set = {save_name: cellAmpIDs}
    set_links = """<div class="wrapper">
                <div class="dropdown" id="button1">
                <button onclick="myFunction()"
                class="dropdown">Overviews</button>
                <div id="myDropdown"
                class="dropdown-content"
                style="width:500px;">"""
    for link in overview_set.keys():
        set_links += """<a href = "%s.html"> %s </a>\n""" % (
            link, (str(link) + ' : ' + str(len(overview_set[link])) +
                   ' neurons'))
    set_links += """</div></div>"""
    # make a list of links for cells
    cell_links = """
                <div class="dropdown" id="button2">
                <button onclick="myFunction2()"
                class="dropdown">Neurons</button>
                <div id="myDropdown2"
                class="dropdown-content"
                style="width:200px;">"""
    cells_sorted = np.array(all_cells)
    for cell in np.sort(cells_sorted):
        cell_links += """<a href = "%s.html"> %s </a>\n""" % (cell, cell)
    cell_links += """</div></div></div>"""

    for overview_name, neuron_set in overview_set.items():
        # make HTML page
        print(overview_name)
        image_links = """<div class="column"> """
        for ind, cellAmpID in enumerate(neuron_set):
            image_links += """<a href="./%s.html">
            <img src="./%s_thumbnails/%s.jpeg"
            style="max-width:75%s;"/> </a> \n""" % (
                cellAmpID, modality,cellAmpID, "")
        image_links += """</div>"""

        # make an HTML plot with the plotly plot embedded.
        html_plot = HTML("""<html>
                            <head>
                            <link rel="stylesheet"
                            type="text/css" href="./stylz.css" />
                            </head>
                            <style>
                            .row {
                              display: flex;
                              flex-wrap: wrap;
                              padding: 0 4px;
                            }

                            /* Create two equal columns */
                            /*that sits next to each other */
                            .column {
                              flex: 25%;
                              padding: 0 4px;
                            }

                            .column img {
                              margin-top: 8px;
                              vertical-align: middle;
                            }
                            .btn-group {
                            display:inline-block;
                            float: top;}

                            </style>
                            <body>
                            <div class="btn-group">""" +

                         set_links +
                         cell_links +
                         """</div><div class="row">""" +

                         image_links +

                         """
                         </div>
                         </div>
                         <script>
    /* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
    function myFunction() {
      document.getElementById("myDropdown").classList.toggle("show");
    }
    function myFunction2() {
      document.getElementById("myDropdown2").classList.toggle("show");
    }
    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
      if (!event.target.matches('.dropdown')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }
    </script>
    </body>
    </html>""")
        with open(
                str(Path(bcs_global.path_website) / (overview_name + '.html')),
                'w') as f:
            f.write(html_plot.data)


def generate_electric_properties(cellAmpIDs):
    """
    Uses database links from brainCellSuite.ini
    Args:
        cellAmpIDs: vector of cellAmpIds to generate website for.

    """
    ephys_data_path = bcs_global.path_ephys_analysis
    df_ephys_data, _ = \
        read_mostrecent_DBCSV(ephys_data_path,
                              bcs_global.filebase_ephys_feature,
                              index_on_cellAmpID=True)

    df_ephys_sweep, _ = \
        read_mostrecent_DBCSV(ephys_data_path,
                              bcs_global.filebase_ephys_feature_sweeps,
                              index_on_cellAmpID=True)

    # get the path to where feature_unit.csv is stored.
    #  This is a little jimmy-rigged.
    # path_website = os.path.dirname(__file__)
    # path_bcs = os.path.dirname(path_website)
    # feature_file_path = Path(path_bcs) / 'analysis' / 'feature_units.csv'
    # f = pkg_resources.resource_stream(__name__, feature_file_path)  # added for reading from EGG
    # feature_units = pd.read_csv(StringIO(f), index_col='feature')# added for reading from EGG
    # feature_units = pd.read_csv(str(feature_file_path),
    #                             index_col='feature')
    feature_units = pd.read_csv(str(Path(bcs_global.path_database) / 'feature_units.csv'), index_col='feature')
    plot_dict = {'spike_waveform': {
                     'feature': 'ap_1_height',
                     'table_feature_list': ['rheobase', 'ap_1_threshold',
                                            'ap_1_height', 'ap_1_width',
                                            'ap_1_fAHP', 'ap_1_AHP',
                                            'ap_1_upstroke_rate',
                                            'ap_1_up_downstroke_ratio', ],
                     'extra_plot_name': 'action potential',
                     'table_name': 'Action potential waveform',
                     'isolate_spike': True},
                 'spike_ramp': {
                     'feature': 'ramp_ap_1_rheobase',
                     'table_feature_list': ['ramp_ap_1_rheobase',
                                            'ramp_rate',
                                            'ramp_burst',
                                            'charge_rheobase', ],
                     'extra_plot_name': 'action potential progression',
                     'table_name': 'Spike ramp',
                     'isolate_spike': False},
                 'spike_train': {
                     'feature': 'first_ap_isi',
                     'table_feature_list': ['ap_1_latency', 'first_ap_isi',
                                            'average_isi', 'cv_isi',
                                            'average_ap_rate', 'ap_adaptation',
                                            'ap_fire_current_rate', 'ap_burst',
                                            'ap_desens_height', ],
                     'extra_plot_name': 'spike/pA',
                     'table_name': 'Spike train',
                     'isolate_spike': False},
                 'passive_properties': {
                     'feature': 'vsag_fraction_at-300pA',
                     'table_feature_list': ['resting_membrane_potential',
                                            'input_resistance',
                                            'series_resistance',
                                            'membrane_time_constant_ini',
                                            'vsag_fraction_at-300pA',
                                            'vsag_minimum_potential_at-300pA'],
                     'extra_plot_name': 'I/V',
                     'table_name': 'Passive properties',
                     'isolate_spike': False}}
    for cellAmpID in cellAmpIDs:
        xsg_folder = Path(bcs_global.path_ephys_data) / str(cellAmpID[:-2])
        print('Processing: ' + str(cellAmpID))
        if xsg_folder.exists():
            experiment = read_ephus_experiment(cellID=cellAmpID[:-2])
            unique_amplifiers = (experiment['sweep_table']
                                 [experiment['sweep_table']['cellAmpID'] ==
                                  cellAmpID]['amplifier'].unique())
            if len(unique_amplifiers) > 0:
                amp_select = unique_amplifiers[0]
            else:
                print('amplifier not in sweep_table of experiment')
                amp_select = None
        else:
            experiment=None
            amp_select = None
        for plot_name, plot_info in plot_dict.items():
            print('Generating: ' + str(plot_name))
            show_feature_sweep(cellAmpID,
                               experiment=experiment,
                               amp_select=amp_select,
                               feature=plot_info['feature'],
                               plot_name=plot_name,
                               table_feature_list=
                               plot_info['table_feature_list'],
                               feature_table=df_ephys_data,
                               feature_units=feature_units,
                               table_name=plot_info['table_name'],
                               extra_plot_name=plot_info['extra_plot_name'],
                               feature_sweep_table=df_ephys_sweep,
                               db_path=bcs_global.path_database,
                               isolate_spike=plot_info['isolate_spike'])


def gen_plotly_report_graphs(cellAmpID,
                             table_name=None,
                             extra_plot_name=None):
    """Generates graph scheme for plotting in plotly.

    To make sure that the plots in plot_sweeps are allocated to the correct
    subplots a graph scheme is generated based on number of amps. Make sure
    to remove the mock traces after populating the graphs with the real traces.

    Args:
        - cellAmpID: string, cell that is recorded within experiment, in case
                     cellAmpID is giventhe experiment name is based on the
                     cellAmpID.
        - table_name: string, name of the table.
        - extra_plot_name: string, name of the extra plot.

    Returns:
        - fig: plotly object containing the graph scheme.
    """
    colors = ['teal', 'indigo']
    mock_trace = [0]
    mock_time = [0]
    plot_row = 2
    plot_col = 2
    subplot_titles = [cellAmpID, table_name, 'stimulus', extra_plot_name]
    fig = py.subplots.make_subplots(rows=plot_row, cols=plot_col,
                                    shared_xaxes=False,
                                    shared_yaxes=False,
                                    subplot_titles=subplot_titles,
                                    specs=[
                                        [{"type": "scatter", "rowspan": 1},
                                         {"type": "table", "rowspan": 1}],
                                        [{"type": "scatter", "rowspan": 1},
                                         {"type": "scatter", "rowspan": 1}]]
                                    )
    posc = [1, 1, 2, 2]
    posr = [1, 2, 1, 2]
    ypos = [1, 3, 2, 4]
    for i in np.arange(0, 4):
        if i == 2:
            trace = go.Table(header=dict(values=[0, 1]),
                             cells=dict(values=[0, 2]))
        else:
            trace = go.Scattergl(x=mock_time, y=mock_trace,
                                 legendgroup='scales', name='scales',
                                 showlegend=False, mode='lines')
        fig.append_trace(trace, posr[i], posc[i])
        if i != 2:
            fig['data'][-1].update(yaxis=('y' + str(ypos[i])))
    fig['layout']['xaxis'].update(title='time (s)', showgrid=False,
                                  zeroline=False, ticks='outside',
                                  fixedrange=True)
    fig['layout']['xaxis2'].update(title='time (s)', showgrid=False,
                                   zeroline=False, ticks='outside',
                                   fixedrange=True)
    fig['layout']['xaxis3'].update(title='extra_plot_x', showgrid=False,
                                   zeroline=False, ticks='outside')
    fig['layout']['yaxis'].update(title='membrane potential (mV)',
                                  color=colors[0], showgrid=False,
                                  zeroline=False, showline=False,
                                  showticklabels=True, ticks='outside',
                                  fixedrange=True)
    fig['layout']['yaxis2'].update(title='current injection (pA)',
                                   color=colors[0], showgrid=False,
                                   zeroline=False, showline=False,
                                   showticklabels=True, ticks='outside',
                                   fixedrange=True)
    fig['layout']['yaxis3'].update(title='extra_plot_y',
                                   color=colors[0], showgrid=False,
                                   zeroline=False, showline=False,
                                   showticklabels=True, ticks='outside')
    fig['layout'].update(showlegend=True, legend=dict(x=1.05, y=1))
    return fig

def plot_sweeps_report(experiment,
                       db_path=bcs_global.path_database,
                       plot_name=None,
                       table_name=None,
                       table_feature_list=None,
                       feature_table=None,
                       feature_units=None,
                       cellAmpID=None,
                       sweeps=None,
                       amp_select=None,
                       mode_select=None,
                       show_spikes=True,
                       extra_plot_name=None,
                       restrict_spike_detection=True,
                       hide_discarded=False,
                       isolate_spike=False,
                       by_pass=False):
    """Generate graph of recorded sweeps and stimuli.

    Uses sweep_table from experiment (requires read_ephus_experiment-function)
    to plot a selection of sweeps. Only supports up to two amplifiers for now!

    Args:
        - experiment: nested dictionary.
        - db_path: full path to the folder containing the databases.
        - plot_name: string, name of the report plot for saving.
        - table_feature_list: list, containing features to be put in the table.
        - feature_table: pandas dataframe, containing features
        - feature_units: pandas dataframe, containing units per feature
                         (features should be index)
        - table_name: string, name of the table.
        - cellAmpID: string, cell that is recorded within experiment, in case
                     cellAmpID is given
                     the experiment name is based on the cellAmpID.
        - sweeps: either list of integers that correspond to sweep numbers, or
                  list of full names of sweeps, e.g., fullname: 'sweep0000' = 0.
        - amp_select: string, exact match to name of amplifier in experiment.
        - mode_select: string, exact match to amplifier mode.
        - show_spikes: bool, True: shows spike peaks for each trace
        - restrict_spike_detection: bool, True: only displays spikes within
                                    stimulation period.
        - hide_discarded: bool, True: shows all sweep excluding the discarded.
        - extra_plot_name: string, name of the extra plot.
        - isolate_spike :  bool, True: if spikes present will crop trace to
                           onset of stimulation till onset second spike.
        - by_pass: bool, False: include plotly in HTML. True: leave html empty.
        This is usefull when there is no
                   sweep to be plotted.
    Returns:
        - fig: plotly object
        - sweep_table: subset version of experiment['sweep_table'] used for
                       plotting graphs.

    """
    if not by_pass:
        fig = None
        colors = ['teal', 'indigo']
        if isinstance(sweeps, np.ndarray):
            sweeps = np.ndarray.tolist(sweeps)
        if isinstance(sweeps, int):
            sweeps = [("sweep" + str(sweeps).zfill(4))]
        if isinstance(sweeps, str):
            sweeps = [sweeps]
        if isinstance(sweeps, list) and isinstance(sweeps[0], int):
            sweeps = ["sweep" + str(num_sweep).zfill(4) for num_sweep in
                      sweeps]
        sweep_table = experiment['sweep_table'][
            experiment['sweep_table']['sweep'].isin(sweeps)]
        if hide_discarded:
            sweep_table = sweep_table[sweep_table['discard'] != True]
        amps = sweep_table['amplifier'].unique()
        modes = sweep_table['amp_mode'].unique()
        if amp_select:
            if amp_select in amps:
                if isinstance(amp_select, list):
                    sweep_table = sweep_table[
                        sweep_table['amplifier'].isin(amp_select)]
                elif isinstance(amp_select, str):
                    sweep_table = sweep_table[
                        sweep_table['amplifier'].isin([amp_select])]
            else:
                print("amp_select input doesn't match amplifier names in " +
                      "experiment: " + str(amps))
                return fig, sweep_table
        if mode_select:
            if mode_select in modes:
                if isinstance(mode_select, list):
                    sweep_table = sweep_table[
                        sweep_table['amp_mode'].isin(mode_select)]
                elif isinstance(mode_select, str):
                    sweep_table = sweep_table[
                        sweep_table['amp_mode'].isin([mode_select])]
            else:
                print(
                    "mode_select input doesn't match amplifier names in " +
                    "experiment: " + str(modes))
                return fig, sweep_table
        amps = sweep_table['amplifier'].unique()
        colorI = 'teal'
        fig = gen_plotly_report_graphs(cellAmpID,
                                       table_name=table_name,
                                       extra_plot_name=extra_plot_name)
        # empty placeholders within fig
        temp_list = list(fig['data'])
        for i in np.arange(0, 4):
            temp_list.pop(0)
        fig['data'] = tuple(temp_list)
        sweep_pre = experiment['sweep_table']['sweep'][0]
        step = 0
        group_amp1 = 'group' + str(0 + step)
        spike_pos = 0
        for ind, sweep_info in sweep_table.iterrows():
            amp = sweep_info['amplifier']
            sweep = sweep_info['sweep']
            trace = experiment['acquired_data'][sweep]['ephys'][amp]['data']
            stim = experiment['stim_data'][sweep]['ephys'][amp][
                'stim_array']
            samplerate = sweep_info['samplerate']
            time = np.arange(0, len(trace)) / samplerate
            amp_mode_sweep = sweep_info['amp_mode']
            if amp_mode_sweep == 'I-Clamp' and show_spikes:
                if restrict_spike_detection:
                    start_t = sweep_info['ephys_stim_pulseDelay']
                    end_t = start_t + sweep_info['ephys_stim_pulseWidth']
                else:
                    start_t = 0
                    end_t = -1
                time_peak, _, time_threshold, _ = get_spike_location(
                    trace=trace,
                    start=start_t,
                    end=end_t,
                    samplerate=samplerate)
                if not isinstance(time_peak, float):
                    peak_times = (time_peak + start_t) / samplerate
                    peaks = np.ones(len(peak_times)) * (60 + spike_pos)
                    spike_pos += 1
            if sweep != sweep_pre:
                step += 2
                group_amp1 = 'group' + str(0 + step)
            if len(amps) == 1:
                if amp == amps[0] and amp_mode_sweep == 'I-Clamp':
                    amp1_IC = go.Scattergl(x=time,
                                           y=trace,
                                           legendgroup=group_amp1,
                                           name=sweep,
                                           yaxis='y1',
                                           line=dict(color=colorI,
                                                     width=2),
                                           marker=dict(size=0))
                    amp1_IC_stim = go.Scatter(x=time,
                                              y=stim,
                                              legendgroup=group_amp1,
                                              name=amp,
                                              yaxis='y2',
                                              line=dict(color=colorI,
                                                        width=2),
                                              marker=dict(size=0,
                                                          opacity=0))
                    fig.append_trace(amp1_IC, 1, 1)
                    fig['data'][-1].update(yaxis='y1')
                    fig.append_trace(amp1_IC_stim, 2, 1)
                    fig['data'][-1].update(yaxis='y2')
                    # set correct range
                    fig['layout']['xaxis'].update(
                        range=[0, time[-1]])
                    fig['layout']['xaxis2'].update(
                        range=[0, time[-1]])
                    if show_spikes:
                        if not isinstance(time_peak, float):
                            spike_amp1_IC = go.Scattergl(x=peak_times,
                                                         y=peaks,
                                                         legendgroup=group_amp1,
                                                         name='action ' +
                                                              'potential',
                                                         yaxis='y1',
                                                         mode='markers',
                                                         marker=dict(
                                                             color=colorI,
                                                             size=2))
                            fig.append_trace(spike_amp1_IC, 1, 1)
                            fig['data'][-1].update(yaxis='y1')
                    if isolate_spike:
                        if not isinstance(time_peak, float):
                            if len(peak_times) > 1:
                                fig['layout']['xaxis'].update(
                                    range=[peak_times[0] - 0.005, (
                                                time_threshold[
                                                    1] + start_t) / samplerate])
                                fig['layout']['xaxis2'].update(
                                    range=[peak_times[0] - 0.005, (
                                                time_threshold[
                                                    1] + start_t) / samplerate])
                            if len(peak_times) == 1:
                                fig['layout']['xaxis'].update(
                                    range=[peak_times[0] - 0.005,
                                           (end_t) / samplerate])
                                fig['layout']['xaxis2'].update(
                                    range=[peak_times[0] - 0.005,
                                           (end_t) / samplerate])
                    if extra_plot_name == 'action potential':
                        trace_dvdt = np.gradient(trace)
                        if isolate_spike:
                            if not isinstance(time_peak, float):
                                if len(peak_times) > 1:
                                    time_start = time_peak[0] - 50
                                    if time_start < 0:
                                        time_start = 0
                                    trace_range = np.array(
                                        [time_start, time_threshold[1]],
                                        'int') + int(start_t)
                                    trace = trace[int(trace_range[0]):int(
                                        trace_range[1])]
                                    trace_dvdt = trace_dvdt[
                                                 int(trace_range[0]):int(
                                                     trace_range[1])]
                                if len(peak_times) == 1:
                                    time_start = time_peak[0] - 50
                                    if time_start < 0:
                                        time_start = 0
                                    trace_range = np.array(
                                        [time_start, end_t], 'int') + int(
                                        start_t)
                                    trace = trace[int(trace_range[0]):int(
                                        trace_range[1])]
                                    trace_dvdt = trace_dvdt[
                                                 int(trace_range[0]):int(
                                                     trace_range[1])]
                        spike_dvdt = go.Scattergl(x=trace, y=trace_dvdt,
                                                  legendgroup=group_amp1,
                                                  name=amp, yaxis='y3',
                                                  line=dict(color=colorI,
                                                            width=2),
                                                  marker=dict(size=0))
                        fig.append_trace(spike_dvdt, 2, 2)
                        fig['data'][-1].update(yaxis='y3')
                        fig['layout']['xaxis3'].update(
                            title='membrane potential (mV)', showgrid=False,
                            zeroline=False, color=colors[0],
                            ticks='outside')
                        fig['layout']['yaxis3'].update(
                            title='velocity (dV/dt, mV/ms)',
                            color=colors[0], showgrid=False,
                            zeroline=False, showline=False,
                            showticklabels=True, ticks='outside')
                    fi_table_path = (Path(bcs_global.path_ephys_analysis) /
                                         'fi_tables' / (str(cellAmpID)
                                         + '.csv'))
                    if extra_plot_name == 'spike/pA' and fi_table_path.exists():
                        fi_table = pd.read_csv(str(fi_table_path),
                            header=None, index_col=None,
                            names=['current', 'spike_rate'])
                        spikerate = go.Scattergl(x=fi_table.current,
                                                 y=fi_table.spike_rate,
                                                 legendgroup=group_amp1,
                                                 name=amp, yaxis='y3',
                                                 line=dict(color=colorI,
                                                           width=2),
                                                 marker=dict(size=10))
                        fig.append_trace(spikerate, 2, 2)
                        fig['data'][-1].update(yaxis='y3')
                        fig['layout']['xaxis3'].update(
                            title='current injection (pA)', showgrid=False,
                            zeroline=False, color=colors[0],
                            ticks='outside')
                        fig['layout']['yaxis3'].update(
                            title='spike rate (Hz)',
                            color=colors[0], showgrid=False,
                            zeroline=False, showline=False,
                            showticklabels=True, ticks='outside')
                    if extra_plot_name == 'action potential progression':
                        trace_dvdt = np.gradient(trace)
                        if not isinstance(time_peak, float):
                            if len(peak_times) > 1:
                                # make color scheme and convert to plotly
                                # standard tuples
                                palette = sns.cubehelix_palette(
                                    len(peak_times))
                                palette_tuple = ['rgb(' +
                                                 str(int(palette[i][0] * 255)) +
                                                 ',' +
                                                 str(int(palette[i][1] * 255)) +
                                                 ',' +
                                                 str(int(palette[i][2] * 255)) +
                                                 ')'
                                                 for i in range(0, len(palette))
                                                 ]
                                for i in np.arange(0, len(peak_times) - 1):
                                    trace_range = np.array(
                                        [time_peak[i] - 50,
                                         time_threshold[i + 1]],
                                        'int') + int(start_t)
                                    tracelet = trace[
                                               int(trace_range[0]):int(
                                                   trace_range[1])]
                                    tracelet_dvdt = trace_dvdt[
                                                    int(trace_range[0]):int(
                                                        trace_range[1])]
                                    spike_dtdv = go.Scattergl(x=tracelet,
                                                              y=tracelet_dvdt,
                                                              legendgroup=
                                                              group_amp1,
                                                              name=amp,
                                                              yaxis='y3',
                                                              line=dict(
                                                                  color=
                                                                  palette_tuple[
                                                                      i],
                                                                  width=2),
                                                              marker=dict(
                                                                  size=0)
                                                              )
                                    fig.append_trace(spike_dtdv, 2, 2)
                                    fig['data'][-1].update(yaxis='y3')
                        fig['layout']['xaxis3'].update(
                            title='membrane potential (mV)',
                            showgrid=False,
                            zeroline=False,
                            color=colors[0],
                            ticks='outside')
                        fig['layout']['yaxis3'].update(
                            title='velocity (mV/ms)',
                            color=colors[0],
                            showgrid=False,
                            zeroline=False,
                            showline=False,
                            showticklabels=True,
                            ticks='outside')
                    sweep_feature_path = (
                            Path(bcs_global.path_ephys_analysis) /
                                         'sweep_feature_tables' /
                                         (cellAmpID[:-2] + '.csv'))
                    if extra_plot_name == 'I/V' and sweep_feature_path.exists():
                        sweep_feature_tables = pd.read_csv(
                            str(sweep_feature_path),
                            index_col='sweep')
                        # get amp_name
                        amp_name = sweep_feature_tables[
                            sweep_feature_tables.cellAmpID == cellAmpID].\
                            amplifier.unique()[0]
                        # subset to only sweep with amp_name
                        sweep_feature_tables = sweep_feature_tables[
                            sweep_feature_tables.cellAmpID == cellAmpID]
                        # subset to only sweeps that are within condition_0
                        sweep_feature_tables = sweep_feature_tables[
                            sweep_feature_tables.drug_condition ==
                            'condition_0']
                        # subset to only undiscarded sweeps
                        sweep_feature_tables = sweep_feature_tables[
                            sweep_feature_tables.discard == False]
                        # subset to only short pulses
                        sweep_feature_tables = sweep_feature_tables[
                            sweep_feature_tables.ephys_stim_pulseWidth <= 2000]
                        # subset to only current clamp
                        sweep_feature_tables = sweep_feature_tables[
                            sweep_feature_tables.amp_mode == 'I-Clamp']
                        # subset to no spike
                        sweep_feature_tables = sweep_feature_tables[
                            sweep_feature_tables.ap_number.isna()]
                        sweeps = []
                        sweeps = sweep_feature_tables.index.tolist()
                        current_injected = []
                        current_injected = np.array(
                            sweep_feature_tables.ephys_stim_pulseAmplitude.
                                tolist())
                        voltage_response = np.zeros(len(current_injected))
                        end_data = np.array(
                            (sweep_feature_tables.ephys_stim_pulseDelay +
                             sweep_feature_tables.ephys_stim_pulseWidth).
                             tolist())

                        start_data = end_data - (
                                    50 * (samplerate / 1000))  # last 50 ms
                        # print(amp_name)
                        # print(sweeps)
                        for i, sweep in enumerate(sweeps):
                            trace_IV = \
                            experiment['acquired_data'][sweep]['ephys'][
                                amp_name]['data']
                            voltage_response[
                                i] = get_vm(
                                trace=trace_IV,
                                start=start_data[i],
                                end=end_data[i])
                        # correct voltage_response by resting membrane potential
                        rpm = np.array(
                            sweep_feature_tables.resting_membrane_potential.
                            tolist())
                        voltage_response = voltage_response - rpm
                        # remove all current injcetions > 0 pA for the linear
                        # regression
                        current_injected_4fit = current_injected[
                            current_injected <= 0]
                        voltage_response_4fit = voltage_response[
                            current_injected <= 0]
                        # fit linear regression
                        popt, pcov = curve_fit(
                            fit_2lin,
                            current_injected_4fit, voltage_response_4fit,
                            p0=(1, 0))
                        # plot
                        IV = go.Scatter(x=current_injected, y=voltage_response,
                                        mode='markers',
                                        legendgroup=group_amp1,
                                        name=amp, yaxis='y3',
                                        marker=dict(size=10, color=colorI))

                        IV_fit = go.Scatter(x=current_injected,
                                            y=current_injected * popt[0] + popt[
                                                1],
                                            legendgroup=group_amp1,
                                            name=amp, yaxis='y3',
                                            line=dict(color='red', width=2,
                                                      dash='dash'))
                        fig.append_trace(IV_fit, 2, 2)
                        # this order will make sure IV is above the fit
                        fig.append_trace(IV, 2, 2)
                        fig['data'][-1].update(yaxis='y3')
                        fig['layout']['xaxis3'].update(
                            title='current injection (pA)', showgrid=False,
                            zeroline=True, color=colors[0], ticks='outside',
                            zerolinecolor='black')
                        fig['layout']['yaxis3'].update(
                            title='Δ membrane potential (mV)',
                            color=colors[0], showgrid=False,
                            zeroline=True, showline=False,
                            showticklabels=True, ticks='outside',
                            zerolinecolor='black')


        table_datapoints = np.array(feature_table.loc[
                                        cellAmpID, table_feature_list].
                                    values.tolist())
        units = np.array(feature_units.loc[table_feature_list, 'unit'])
        table_feature_list = np.array(table_feature_list)
        if 'vsag_fraction_at-300pA' in table_feature_list:
            # calculate as percentage
            table_datapoints[table_feature_list == 'vsag_fraction_at-300pA'] = \
            table_datapoints[
                table_feature_list == 'vsag_fraction_at-300pA'] * 100
            units[table_feature_list == 'vsag_fraction_at-300pA'] = '%'
        table_datapoints = table_datapoints.round(1)
        fig.append_trace(go.Table(
            header=dict(values=['Feature', 'Value', 'Unit']),
            cells=dict(
                values=[table_feature_list, table_datapoints, units])),
            row=1, col=2)

        fig['layout']['showlegend'] = False

        plotly_embedding = str(
            py_offline.plot(fig,
                            filename='cellAmpID',
                            output_type="div",
                            include_plotlyjs=True))
    else:
        fig = None
        plotly_embedding = str(
            '<center><big> No data available </big></center>')
    # print(str(plotly_embedding) + ' status plotly_embedding')
    # make an HTML plot with the plotly plot embedded.
    html_plot = HTML("""
    <html>
    <head>
        <link rel="stylesheet" type="text/css" href="../stylz.css" />
    </head>
    <body>
    <div class="wrapper">
        <div class="dropdown">

          <button onclick="myFunction()" class="dropdown">View..</button>
          <div id="myDropdown" class="dropdown-content">
            <a href="../""" + str(cellAmpID) +
                     """.html">Morphology</a>
            <a href="../spike_train/""" + str(cellAmpID) +
                     """.html">Spike_train</a>
            <a href="../spike_waveform/""" + str(cellAmpID) +
                     """.html">Spike waveform</a>
            <a href="../spike_ramp/""" + str(cellAmpID) +
                     """.html">Ramp</a>
            <a href="../passive_properties/""" + str(cellAmpID) +
                     """.html">Passive properties</a>
            <a href="../local_input/""" + str(cellAmpID) +
                     """.html">Local input</a>
            <a href="../long-range_input/""" + str(cellAmpID) +
                     """.html">Long-range input</a>
            <a href="../meta/""" + str(cellAmpID) +
                     """.html">Projection and genetic properties [meta]</a>
            <a href="../cell_overview.html">back to dataset overview</a>
          </div>
        </div>
        <div class="reportButton">
            <button class="reportButton" id='reportButton'>""" +
            """<a href="../report_bug.html/?origin=/spike_waveform/ """ +
            str(cellAmpID) +
                     """ "> Report bug </a></button>
        </div>"""
         + plotly_embedding
         + '</div>' +
         """<script>
         /* When the user clicks on the button,
         toggle between hiding and showing the dropdown content */
         function myFunction() {
           document.getElementById("myDropdown").classList.toggle("show");
         }

         // Close the dropdown if the user clicks outside of it
         window.onclick = function(event) {
           if (!event.target.matches('.dropdown')) {
             var dropdowns = document.
             getElementsByClassName("dropdown-content");
             var i;
             for (i = 0; i < dropdowns.length; i++) {
               var openDropdown = dropdowns[i];
               if (openDropdown.classList.contains('show')) {
                 openDropdown.classList.remove('show');
               }
             }
           }
         }
         </script>""")
    html_plot = html_plot.data
    website_path = (Path(bcs_global.path_website) / plot_name /
        (str(cellAmpID) + '.html'))
    print(str(website_path))
    with open(str(website_path), 'w') as f:
        f.write(html_plot)
    print('wrote html sucessfull')
    # The main piece of code, with which we embed the buttons is shown above
    # , first we define the plotly plot, then using display and html
    # functions, we can embed the buttons inside a div with class
    # button-group and with CSS we position that button.


def import_and_plot(experiment_name=None,
                    experiment=None,
                    amp_select=None,
                    db_path=bcs_global.path_database,
                    plot_name=None,
                    table_feature_list=None,
                    feature_table=None,
                    feature_units=None,
                    table_name=None,
                    sweeps=None,
                    cellAmpID=None,
                    isolate_spike=None,
                    extra_plot_name=None,
                    by_pass=None):
    """Opens experiment and shows sweeps.

    Args:
        - experiment: string, name of the experiment.
        - experiment_folder: string, fullpath where experiment is saved.
        - sweeps: vector of string or integers of sweep names
        - cellAmpID: string, cell that is recorded within experiment, in case
                     cellAmpID is given the experiment name is based on the
                     cellAmpID.
        - isolate_spike :  bool, True: if spikes present will crop trace to
                           onset of stimulation till onset second spike.
        - by_pass: bool, False: include plotly in HTML. True: leave html empty.
                   This is usefull when there is no sweep to be plotted.

    Returns:
        None
    """
    if by_pass:
        experiment = None
        amp_select = None
    plot_sweeps_report(experiment,
                       db_path=db_path,
                       plot_name=plot_name,
                       table_feature_list=table_feature_list,
                       feature_table=feature_table,
                       table_name=table_name,
                       feature_units=feature_units,
                       cellAmpID=cellAmpID,
                       sweeps=sweeps,
                       amp_select=amp_select,
                       isolate_spike=isolate_spike,
                       extra_plot_name=extra_plot_name,
                       by_pass=by_pass)


def show_feature_sweep(cellAmpID,
                       feature,
                       experiment=None,
                       amp_select=None,
                       plot_name=None,
                       table_feature_list=None,
                       feature_table=None,
                       feature_units=None,
                       table_name=None,
                       feature_sweep_table_path=None,
                       feature_sweep_table_filename=None,
                       feature_sweep_table=None,
                       extra_plot_name=None,
                       db_path=None,
                       isolate_spike=None):
    """Looks up sweep affiliated with feature value of cellAmpID.


    Args:
        - cellAmpID: string, cellAmpID of cell.
        - feature: string, exact name of feature of which the corresponding
                   sweep for the cellAmpID is shown.
        - feature_sweep_table_path: fullpath to directory where
                                    feature_sweep_table is stored.
        - feature_sweep_table_filename: exact filename of the
                                        feature_sweep_table.by default empty.
                                        In that case open the most recent
                                        feature_sweep_table.
        - feature_table: dataframe containing feature data per cellAmpID.
        - feature_sweep_table: dataframe containing the sweeps ref.
        - filebase = basename of the .csv file containing the sweeps
        - isolate_spike =  bool, True: if spikes present will crop trace to
                           onset of stimulation till onset second spike.

    Returns:
        None
    """
    if not isinstance(feature_sweep_table, pd.DataFrame):
        if feature_sweep_table_filename:
            feature_sweep_table = pd.read_csv(
                str(Path(feature_sweep_table_path)
                    / feature_sweep_table_filename))
        else:
            feature_sweep_table_path = bcs_global.path_ephys_analysis
            feature_sweep_table, _ = \
                read_mostrecent_DBCSV(feature_sweep_table_path,
                                      bcs_global.filebase_ephys_feature_sweeps,
                                      index_on_cellAmpID=True)
    if feature_sweep_table.index.name != 'cellAmpID':
        feature_sweep_table = feature_sweep_table.set_index('cellAmpID')
    # make sure to only have condition 0
    feature_sweep_table = feature_sweep_table[
        feature_sweep_table.drug_condition == 'condition_0']
    feature_table = feature_table[
        feature_table.drug_condition == 'condition_0']
    if feature in list(
            feature_sweep_table.columns) and \
            cellAmpID in feature_sweep_table.index:
        sweep = feature_sweep_table.loc[cellAmpID, feature]
        if isinstance(sweep, float):
            print('Feature is there but: No sweep present')
            import_and_plot(experiment_name=cellAmpID[:-2],
                            experiment=experiment,
                            amp_select=amp_select,
                            db_path=db_path,
                            sweeps=sweep,
                            plot_name=plot_name,
                            table_name=table_name,
                            table_feature_list=table_feature_list,
                            feature_table=feature_table,
                            feature_units=feature_units,
                            cellAmpID=cellAmpID,
                            isolate_spike=isolate_spike,
                            extra_plot_name=extra_plot_name,
                            by_pass=True)
        else:
            print('Feature is there and: Sweep present')
            import_and_plot(experiment_name=cellAmpID[:-2],
                            experiment=experiment,
                            amp_select=amp_select,
                            db_path=db_path,
                            sweeps=sweep,
                            plot_name=plot_name,
                            table_feature_list=table_feature_list,
                            table_name=table_name,
                            feature_table=feature_table,
                            feature_units=feature_units,
                            cellAmpID=cellAmpID,
                            isolate_spike=isolate_spike,
                            extra_plot_name=extra_plot_name,
                            by_pass=False)
    else:
        if not feature in list(
                feature_sweep_table.columns):
            print(feature + ' was not found in the table')
        if not cellAmpID in feature_sweep_table.index:
            print('Feature is there and: Sweep not present')
            sweep = None
            import_and_plot(experiment_name=cellAmpID[:-2],
                            experiment=experiment,
                            amp_select=amp_select,
                            db_path=db_path,
                            sweeps=sweep,
                            plot_name=plot_name,
                            table_name=table_name,
                            table_feature_list=table_feature_list,
                            feature_table=feature_table,
                            feature_units=feature_units,
                            cellAmpID=cellAmpID,
                            isolate_spike=isolate_spike,
                            extra_plot_name=extra_plot_name,
                            by_pass=True)
            print(str(cellAmpID) + ' was not found in the table')


def extract_clusters(dendro_plot, orientation='vertical'):
    """Extracts the clusters based on the dendrogram color scheme.

    Args:
        dendro_plot: nested dictionary generated by plotly (ff.dendrogram)
        orientation: 'vertical', or 'horizontal', orientation of the dendrogram.

    Returns:
        dendro_plot: updated dendrogram with distinct colors per cluster
        cluster_HCA_dict: dictionary with cluster_name as key and numpy array
            of items that were clustered.
    """
    orientations = ['vertical', 'horizontal']
    assert orientation in orientations, '%s is not option, choose from : %s' \
                                        % (orientation, orientations)
    cluster_names = list()
    cluster_it = 0
    prev_color = dendro_plot['data'][0]['marker']['color']

    cluster_num_list = np.zeros(len(dendro_plot['layout']['yaxis']['ticktext']))
    cellAmpID_index = 0
    for i in range(len(dendro_plot['data'])):
        current_color = dendro_plot['data'][i]['marker']['color']
        # determine the number of clusters and label them using legendgroup
        # this number reflects the amount of cellAmpIDs that are directly linked
        # to this line drawing of dendro_plot.
        num_cells = len(np.where(dendro_plot['data'][i]['x'] == 0)[0])
        if prev_color != current_color and num_cells > 0:
            # new cluster
            cluster_it += 1
        # give a cluster name
        dendro_plot['data'][i]['legendgroup'] = 'cluster_%3d' % cluster_it
        prev_color = current_color
        # determine wich cellAmpIDs should get this cluster number
        if num_cells > 0:
            index_ = np.arange(cellAmpID_index, cellAmpID_index + num_cells)
            cluster_num_list[index_] = cluster_it
            cellAmpID_index += num_cells

    num_clusters = cluster_it + 1
    # generate appropriate most discriminating colorscheme that accomodates for
    # the number of clusters
    viridis = cm.get_cmap('viridis', num_clusters+1)
    # recolor the clusters
    for i in range(len(dendro_plot['data'])):
        current_cluster = int(dendro_plot['data'][i]['legendgroup'].
                              split('_')[1])
        # make color string
        color_1 = viridis((current_cluster + 1) / (num_clusters + 1))
        fill_color = 'rgb(%f,%f,%f)' % (
        255 * color_1[0], 255 * color_1[1], 255 * color_1[2])
        # give a cluster name
        dendro_plot['data'][i]['marker']['color'] = fill_color
    cluster_cell_list = np.array(list(i.split('_')[1]
                                      for i in dendro_plot['layout']
                                          ['yaxis']['ticktext']))
    # make dict to lookup cellAmpIDs that belong to a certain cluster
    cluster_HCA_dict = dict()
    for i in range(num_clusters):
        color_1 = viridis((i + 1) / (num_clusters + 1))
        cluster_HCA_dict['HCA_cluster_' + str(i)] = \
            [cluster_cell_list[np.where(cluster_num_list == i)],
                                'rgb(%f,%f,%f)' % (
                                255 * color_1[0],
                                255 * color_1[1],
                                255 * color_1[2])]
    return dendro_plot, cluster_HCA_dict


def generate_dash_morph_ephys(dash_title='clustering',
                              cellAmpID_cluster_dict=None,
                              cluster_folder=None,
                              cluster_by='both',
                              filter_cellType='PYR',
                              filter_lower_limit_QC_morph=2.5,
                              filter_ephys_rs_upper_limit=40,
                              filter_ephys_present=True,
                              filter_ephys_train_present=True,
                              filter_cortical_depth_range=(0, 1),
                              filter_low_variance=0.00001,
                              filter_features=None,
                              filter_remove_features_regx=None,
                              transform_heatmap='minmax',
                              impute_output_data=False,
                              transform_pca='z-score',
                              obtain_cluster_data=False,
                              path_cell_data=bcs_global.path_database,
                              path_ephys_data=bcs_global.path_ephys_analysis,
                              path_morph_data=bcs_global.path_morph_analysis,
                              filename_cell_data=bcs_global.filename_cell_db,
                              filebase_ephys=bcs_global.filebase_ephys_feature,
                              filebase_morph=bcs_global.filebase_morph_feature):
    """Generates interactive graph of ephys and morphology
    data based on clusters.

    Args:
        group_name_cluster: name of the cluster.
        cellAmpID_cluster_dict: dictionary with cluster as key and list of
            cellAmpID as value.
        cluster_folder: fullpath to folder containing .csv files each containing
            a list of cellAmpIDs that are affiliated to a specific cluster.
        cluster_by: string, has to HCA the data by.
            Requires to be one of the following:
            'both', 'ephys', 'morph'.
        filter_cellType: string to subset data by in terms of cell type.
            Requires to be one of the following:
            'all', 'PYR', 'GAD2', 'PV', 'D1', 'nonPYRnonPV'
        filter_lower_limit_QC_morph: float, minimum QC score the morphology has
            to fullfill. If 0 it will also include all cells without morphology.
        filter_ephys_rs_upper_limit: float, maximum series resistance that a
            recording can have in order to be included.
            Requires filter_ephys_present to be True. Includes the limit itself.
        filter_ephys_present: boolean. Default False. If True only cells with
            ephys will be displayed. Criteria are: vsag_fraction_at-300pA and
            ap_1_height are not 'nan'.
        filter_ephys_train_present: boolean. Default True. If True only cells with
            ephys for spike trains present.
        filter_cortical_depth_range: tuple with floats. Range within which the
            cells are selected. Includes the limits themselves.
        filter_low_variance: float. Any feature with less than
            filter_low_variance is removed
        filter_remove_features_regx: string. Matches string to features. Those
            that match will be removed. eg. 'hist' wil remove all features with
            'hist' in the feature name.
        filter_features: list of string. Must be in the available features. If
            None (default) no selection will be made and all will be shown.
        transform_heatmap: string, method for transform data before displaying.
            Requires to be one of the following:
            'z-score', 'minmax'.
        impute_output_data: boolean, True: impose a mean imputation on all nan
            entries.
        transform_pca: string, method for transform data before displaying.
            Requires to be one of the following:
            'z-score', 'minmax'.
        obtain_cluster_data: boolean, if True only the cluster_dict is returned.
        path_cell_data: fullpath to folder containing cell database.
        path_ephys_data: fullpath to folder containing feature ephys data.
        path_morph_data: fullpath to folder containing feature morphology data.
        filename_cell_data: filename of the cell database.
        filebase_ephys: filebase of ephys feature data files to select most
            recent file.
        filebase_morph: filebase of morphology feature data files to select most
            recent file.

    Returns:
        cluster_HCA_dict: dictionary, containing cellAmpIDs and their relative
            cluster allocations based on the HCA.
        df_features_sorted: list of features used to perform HCA with.
        pca_data: pandas dataframe, containing data normalized/scaled based on
            argument transform_pca.
        pca_data_preprocess: clean feature data, containing raw data with
            imputed data (using scipy impute with 'mean') for all NaN's.

    """
    celltypes = ['all', 'PYR', 'GAD2', 'PV', 'D1', 'nonPYRnonPV']
    assert filter_cellType in celltypes, '%s not available option: options' \
                                         'are %s' % (filter_cellType, celltypes)
    transform_metrics = ['z-score', 'minmax']
    assert transform_heatmap in transform_metrics, '%s not available option: ' \
                                                   'options are %s' \
                                                   % (transform_heatmap,
                                                      transform_metrics)
    assert transform_pca in transform_metrics, '%s not available option: ' \
                                                   'options are %s' \
                                                   % (transform_pca,
                                                      transform_metrics)

    heatmap_scheme = 'Viridis'
    if transform_heatmap == 'z-score':
        heatmap_scheme = 'RdBu'

    cluster_by_options = ['both', 'ephys', 'morph']
    assert cluster_by in cluster_by_options, \
        '%s not available option: options' 'are %s' \
        % (cluster_by, cluster_by_options)
    # import dataframes in pandas
    df_cell = readDBCSV(path_cell_data, filename_cell_data)
    df_cell = df_cell.set_index('cellAmpID')
    df_cell = df_cell[
        ['sliceID', 'slideID', 'animalID', 'surgeryID', 'storageboxID',
         'genotype', 'dob', 'sex', 'siDate', 'somaCortexPosition', 'cellType',
         'retrobeadsPresent']]
    # fill all None for retrobeadsPresent to unidentified
    df_cell.loc[
        df_cell.retrobeadsPresent.isnull(),
        'retrobeadsPresent'] = 'unidentified'

    df_cell = df_cell.rename(columns={'somaCortexPosition': 'soma_location'})
    df_ephys, _ = read_mostrecent_DBCSV(path_ephys_data,
                                        filebase_ephys,
                                        index_on_cellAmpID=True)

    # only use drug_condition == condition_0
    df_ephys = df_ephys[df_ephys.drug_condition == 'condition_0']
    df_morph, _ = read_mostrecent_DBCSV(path_morph_data,
                                        filebase_morph,
                                        index_on_cellAmpID=True)
    # leave out cellType, geneticMarker, projection from df_morph
    df_morph_cols = df_morph.columns.tolist()
    df_morph_cols = list(set(df_morph_cols).difference(['cellType', 'geneticMarker', 'projection']))
    df_morph = df_morph[df_morph_cols]

    # get the path to where feature_unit.csv is stored.
    #  This is a little jimmy-rigged.
    # path_website = os.path.dirname(__file__)
    # path_bcs = os.path.dirname(path_website)
    # feature_file_path = Path(path_bcs) / 'analysis' / 'feature_units.csv'
    # f = pkg_resources.resource_stream(__name__, feature_file_path)  # added for reading from EGG
    # feature_units = pd.read_csv(StringIO(f), index_col='feature')  # added for reading from EGG
    # unit_dict = pd.read_csv(str(feature_file_path), index_col='feature')
    feature_units = pd.read_csv(str(Path(bcs_global.path_database) / 'feature_units.csv'), index_col='feature')

    # combine three dataframes and clean up
    df_features = df_cell.join(df_ephys,
                               how='outer',
                               sort=True,
                               rsuffix='ephys')
    df_features = df_features.join(df_morph,
                                   how='outer',
                                   sort=True,
                                   rsuffix='morph')
    # add cluster column based on group
    df_features['group_name_cluster'] = np.nan
    df_features['group_name_cluster_marker'] = None
    df_features['HCA_cluster_color'] = None
    df_features['HCA_cluster_name'] = None

    if cluster_folder is not None:
        # read cluster ID data from specific folder
        # assumption is that files in the cluster ID folder are either csv
        # (computer generated) or xlsx QR code reader for andriod
        cluster_file_list = os.listdir(cluster_folder)
        group_name_cluster = (cluster_file_list[0].split('.')[0].
                              split('_')[0] + '_cluster')
        timepoint_name = cluster_file_list[0].split('.')[0].split('_')[1][1:]

        for file in cluster_file_list:
            if re.match(pattern='csv', string=file.split('.')[-1]):
                cluster_name = file.split('.')[0].split('_')[1][5:]
                clust_data_append = pd.read_csv(
                    str(Path(cluster_folder) / file),
                    header=None,
                    index_col=None)
                clust_data_append = [os.path.basename(cell.split(',')[0]).
                                         split('.')[0]
                                     for cell in clust_data_append[1]]
                df_features.loc[clust_data_append, 'group_name_cluster'] = \
                    str(cluster_name)
            elif re.match(pattern='xlsx', string=file.split('.')[-1]):
                cluster_name = file.split('.')[0].split('_')[2][2:]
                clust_data_append = pd.read_excel(
                    str(Path(cluster_folder) / file))
                clust_data_append = [os.path.basename(cell.split(',')[0]).
                                         split('.')[0]
                                     for cell in clust_data_append[
                                         'Barcode Content,TimeStamp']]
                df_features.loc[clust_data_append, 'group_name_cluster'] = \
                    str(cluster_name)
    elif cellAmpID_cluster_dict is not None:
        for cluster, cellAmpIDs in cellAmpID_cluster_dict.items():
            df_features.loc[
                df_features.index.isin(
                    cellAmpID_cluster_dict[cluster]), 'group_name_cluster'] = \
                str(cluster)
    else:
        print(
            'Warning no classification into clusters, please provice either a '
            'cluster_folder, or cellAmpID_cluster_dict!')
        print('Showing now the classification based on layers')
        layer_bins = [0, 0.1, 0.15, 0.3, 0.5, 0.7, 0.9, 1]
        layer_labels = ['L1', 'L2', 'L3', 'L5a', 'L5b', 'L6', 'Claustrum']
        df_features.group_name_cluster = pd.cut(df_features.soma_location,
                                                bins=layer_bins,
                                                labels=layer_labels,
                                                right=True)
        # remove all unclustered IDs
        df_features.dropna(inplace=True, subset=['group_name_cluster'], axis=0)

    # subset based on celltype, morph QC, ephys_rs_upper_limit,
    # filter_ephys_present, cortical_depth_range
    df_features = df_features[df_features.QCCriterium >=
                              filter_lower_limit_QC_morph]
    if filter_cellType != 'all':
        df_features = df_features[df_features.cellType == filter_cellType]
    df_features = df_features[np.logical_and(
        df_features.soma_location >= filter_cortical_depth_range[0],
        df_features.soma_location <= filter_cortical_depth_range[1])]
    if filter_ephys_present:
        df_features = df_features[np.logical_and(
            ~np.isnan(df_features['vsag_fraction_at-300pA']),
            ~np.isnan(df_features.ap_1_height))]
        df_features = df_features[df_features.series_resistance <=
                                  filter_ephys_rs_upper_limit]
    if filter_ephys_train_present:
        df_features = df_features[
            ~np.isnan(df_features['ap_adaptation'])]

    # remove all unclustered iDs
    mask_ids = np.array(
        df_features.loc[:, 'group_name_cluster'].astype(str).tolist()) != 'nan'
    df_features = df_features.iloc[mask_ids]
    columns = df_features.columns.tolist()
    available_features = np.array(columns[34:-4])
    available_features = np.hstack((available_features, ['soma_location']))
    remove_items = ['filenamemorph', 'location_soma', 'membrane_time_constant']
    # identify columns with very low variance
    low_vars = dict(df_features.var(axis=0) <= filter_low_variance)
    for i, is_lowvar in low_vars.items():
        if is_lowvar:
            remove_items.append(i)
    if filter_remove_features_regx:
        for feature in available_features:
            if re.match(filter_remove_features_regx, feature):
                remove_items.append(feature)
    # remove items
    for i in remove_items:
        available_features = available_features[available_features != i]
    if cluster_by == 'ephys':
        ephys_cols = df_ephys.columns.tolist()
        available_features = np.array(
            list(set(available_features).intersection(set(ephys_cols))))
    elif cluster_by == 'morph':
        morph_cols = df_morph.columns.tolist()
        available_features = np.array(
            list(set(available_features).intersection(set(morph_cols))))

        # add an heatmap to the website
    df_features_sorted = df_features.sort_values(by=['group_name_cluster'])
    data = np.array(df_features_sorted[available_features])
    row, cols = np.shape(data)
    mask_remove_feature_nan = np.invert(np.sum(
        np.isnan(data.astype('float')), 0) == row)
    available_features = available_features[mask_remove_feature_nan]

    if filter_features:
        # only display selected features
        in_available_features = list()
        for feature_sel in filter_features:
            if feature_sel in available_features:
                in_available_features.append(feature_sel)
            else:
                print(
                    '%s was not found in the available_features' % feature_sel)
        available_features = in_available_features

    data = np.array(df_features_sorted[available_features]).astype(float)
    # remove inf's and replace with nan
    data[np.logical_and(np.invert(np.isfinite(data)),
                        np.invert(np.isnan(data)))] = np.nan
    pca_data = copy.deepcopy(data)
    # impute nan's
    imp = impute.SimpleImputer(strategy="mean")
    data = imp.fit_transform(data)

    if impute_output_data:
        pca_data = copy.deepcopy(data)
    # pack pca_data_preprocess into dataframe
    pca_data_preprocess = pd.DataFrame(data=pca_data,
                            columns=available_features,
                            index=df_features_sorted.index)
    pca_data = copy.deepcopy(data)

    if transform_heatmap == 'z-score':
        # standardize using mean=0 and SD = 1
        std_scale = preprocessing.StandardScaler().fit(data)
        data = std_scale.transform(data)
    elif transform_heatmap == 'minmax':
        # standardize using min max scaling
        minmax_scale = preprocessing.MinMaxScaler().fit(data)
        data = minmax_scale.transform(data)

    # transform the data for PCA plots
    if transform_pca == 'z-score':
        # standardize using mean=0 and SD = 1
        std_scale = preprocessing.StandardScaler().fit(pca_data)
        pca_data = std_scale.transform(pca_data)
    elif transform_pca == 'minmax':
        # standardize using min max scaling
        minmax_scale = preprocessing.MinMaxScaler().fit(pca_data)
        pca_data = minmax_scale.transform(pca_data)

    # pack pca_data into dataframe
    pca_data = pd.DataFrame(data=pca_data,
                            columns=available_features,
                            index=df_features_sorted.index)

    heatmap_x = available_features
    heatmap_y = [df_features_sorted.group_name_cluster.tolist()[i] +
                 '_' +
                 df_features_sorted.index.tolist()[i]
                 for i in np.arange(0, len(df_features_sorted.index))]

    data = data.T
    heatmap_fig = ff.create_dendrogram(data, orientation='bottom',
                                       labels=heatmap_x)
    for i in range(len(heatmap_fig['data'])):
        heatmap_fig['data'][i]['yaxis'] = 'y2'
    # for the dendro leaves
    dendro_top = ff.create_dendrogram(data, orientation='bottom',
                                      labels=heatmap_x)

    dendro_side = ff.create_dendrogram(data.T, orientation='right',
                                       labels=heatmap_y)
    for i in range(len(dendro_side['data'])):
        dendro_side['data'][i]['xaxis'] = 'x2'

    dendro_leaves_side = dendro_side['layout']['yaxis']['ticktext']
    dendro_leaves_side_df = pd.DataFrame(
        data=np.arange(0, len(dendro_leaves_side)), index=heatmap_y,
        columns=['inds'])
    dendro_leaves_side = dendro_leaves_side_df.loc[
        dendro_leaves_side, 'inds'].tolist()
    dendro_leaves_top = dendro_top['layout']['xaxis']['ticktext']
    dendro_leaves_top_df = pd.DataFrame(
        data=np.arange(0, len(dendro_leaves_top)), index=heatmap_x,
        columns=['inds'])
    dendro_leaves_top = dendro_leaves_top_df.loc[
        dendro_leaves_top, 'inds'].tolist()

    data_dist = pdist(data.T)
    data_dist = squareform(data_dist)
    heat_data = data.T[:, dendro_leaves_top]
    heat_data = heat_data[dendro_leaves_side, :]

    customdata_x = np.reshape(
        np.repeat(np.array(dendro_side['layout']['yaxis']['ticktext'].tolist()),
                  len(dendro_leaves_top)),
        (len(dendro_leaves_side), len(dendro_leaves_top)))
    customdata_y = np.reshape(
        np.tile(np.array(dendro_top['layout']['xaxis']['ticktext'].tolist()),
                len(dendro_leaves_side)),
        (len(dendro_leaves_side), len(dendro_leaves_top)))

    heatmap = [
        go.Heatmap(
            x=dendro_leaves_side,
            y=dendro_leaves_top,
            z=heat_data,
            customdata=customdata_x,
            hovertemplate='<b>cellAmpID: %{customdata}',
            colorscale=heatmap_scheme,
            zmin=-1 * (np.max(np.abs(heat_data))),
            zmax=np.max(np.abs(heat_data))
        )
    ]

    heatmap[0]['x'] = heatmap_fig['layout']['xaxis']['tickvals']
    heatmap[0]['y'] = dendro_side['layout']['yaxis']['tickvals']

    heatmap_fig.add_traces(heatmap)

    # Edit Layout

    heatmap_fig['layout'].update({'width': 800, 'height': 800,
                                  'showlegend': False, 'hovermode': 'closest',
                                  })

    # Edit xaxisP
    heatmap_fig['layout']['xaxis'].update({'domain': [.15, 1],
                                           'mirror': False,
                                           'showgrid': False,
                                           'showline': False,
                                           'zeroline': False,
                                           'ticks': ""})

    # Edit xaxis2
    heatmap_fig['layout'].update({'xaxis2': {'domain': [0, .15],
                                             'mirror': False,
                                             'showgrid': False,
                                             'showline': False,
                                             'zeroline': False,
                                             'showticklabels': False,
                                             'ticks': ""}})

    # Edit yaxis
    heatmap_fig['layout']['yaxis'].update({'domain': [0, .85],
                                           'mirror': False,
                                           'showgrid': False,
                                           'showline': False,
                                           'zeroline': False,
                                           'showticklabels': False,
                                           'ticks': ""})

    # Edit yaxis2
    heatmap_fig['layout'].update({'yaxis2': {'domain': [.825, .975],
                                             'mirror': False,
                                             'showgrid': False,
                                             'showline': False,
                                             'zeroline': False,
                                             'showticklabels': False,
                                             'ticks': ""}})

    # update df_features with the cluster information
    # determine first the cluster labels per cellAMPID and update the color
    # scheme of dendro_side
    dendro_side, cluster_HCA_dict = extract_clusters(dendro_side,
                                                     orientation='vertical')

    # update heatmap_fig
    heatmap_fig.add_traces(dendro_side['data'])

    # update df_features for HCA clusters
    for key, packed_ in cluster_HCA_dict.items():
        cellAmpIDs, color_ = packed_
        df_features.loc[cellAmpIDs, 'HCA_cluster_color'] = color_
        df_features.loc[cellAmpIDs, 'HCA_cluster_name'] = key

    # update df_features for markers of the group_names
    for i, c in enumerate(np.unique(df_features.group_name_cluster)):
        df_features.loc[
            df_features.group_name_cluster == c,
            'group_name_cluster_marker'] = i

    # fill None with black color
    df_features.loc[
        df_features.HCA_cluster_color.isnull(),
        'HCA_cluster_color'] = 'rgb(0,0,0)'

    # bring back the categories
    selected_columns = np.hstack(
        (np.arange(0, 12), np.arange(34, len(df_features.columns))))
    available_features = np.array(
        df_features.columns[selected_columns].tolist())

    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
    # app = dash.Dash(__name__, )

    available_indicators = available_features
    if obtain_cluster_data:
        return dendro_side,  cluster_HCA_dict  # test

    app.layout = html.Div(children=[
        html.H1(children=dash_title),

        html.Div(children="""Cluster-based plots of electrical and morphological
            features of mouse insular cortical neurons"""),

        html.Div([
            dcc.Dropdown(
                id='yaxis-column',
                options=[{'label': i, 'value': i} for i in available_features],
                value='ap_1_height')],
            style={'width': '48%', 'display': 'inline-block'}),

        html.Div([
            dcc.Dropdown(
                id='xaxis-column',
                options=[{'label': i, 'value': i} for i in available_features],
                value='soma_location')],
            style={'width': '48%', 'display': 'inline-block'}),

        html.Div([
            html.Div([
                dcc.Graph(
                    id='indicator-graphic')
            ],
                className="six columns",
                style={'display': 'inline-block'}
            ),

            html.Div([
                dcc.Graph(id="heatmap",
                          figure=heatmap_fig)

            ],
                className="six columns",
                style={'display': 'inline-block'}
            )],
            className="row",
            style={'width': '100%', 'display': 'inline-block'}
        )

    ])

    @app.callback(
        Output('indicator-graphic', 'figure'),
        [Input('yaxis-column', 'value'),
         Input('xaxis-column', 'value')])

    def update_graph(yaxis_column_name, xaxis_column_name):
        """Local update function for dash graph"""
        data_array = []
        try:
            x_unit = '\n (' + str(
                unit_dict.loc[xaxis_column_name, 'unit']) + ')'
        except KeyError:
            x_unit = ' '

        try:
            y_unit = '\n (' + str(
                unit_dict.loc[yaxis_column_name, 'unit']) + ')'
        except KeyError:
            y_unit = ' '

        color_count = 0
        for cluster_name in df_features.group_name_cluster.unique().tolist():
            df_features_sub = df_features[
                df_features.group_name_cluster == cluster_name]
            data_array.append(
                go.Scatter
                (x=df_features_sub.loc[:, xaxis_column_name],
                 y=df_features_sub.loc[:, yaxis_column_name],
                 mode='markers',
                 name='Cluster ' + str(cluster_name),
                 legendgroup='Cluster ' + str(cluster_name),
                 marker={
                     'size': 10,
                     'color': df_features_sub.loc[:, 'HCA_cluster_color'],
                     'symbol': df_features_sub.loc[:,
                               'group_name_cluster_marker'] + 2
                 }),
            )
            color_count += 1
        return {
            'data': data_array,
            'layout': go.Layout(
                xaxis={'title': f'{xaxis_column_name + x_unit}',
                       'showgrid': False},
                yaxis={'title': f'{yaxis_column_name + y_unit}',
                       'showgrid': False},
                showlegend=True,
                width=1000,
                height=800)
        }

    app.run_server(
        debug=False,
        port=8050,
        host='0.0.0.0')

    return cluster_HCA_dict, df_features_sorted, pca_data, pca_data_preprocess

def get_endpoints(swc_data):
    """Gathers coordinate IDs that are endpoints

    Takes SWC data, will be filtered to be part of neuronal process TYPE >1
    and TYPE < 5. Any coordinate ID which is not referred to in the position
    (P) parameter is considered an endpoint.

    Args:
        - swc_data: numpy.ndarray swc data per row organized: ID,
        TYPE, X, Y, Z, R, P of only soma, contour,s and borders.

    Return:
        - endPointData: data that resemble endpoint
    """
    dataTree = get_dataTree(swc_data)
    IDs, P = (list(dataTree['ID']), list(dataTree['P']))
    endpointIDs = set(IDs) - set(P)
    endpointData = swc_data[np.isin(swc_data['ID'], list(endpointIDs))]
    return endpointData


def generate_morphology(cellAmpID,
                 db_path=bcs_global.path_database,
                 viewEndpoints=False,
                 viewContours=False,
                 viewLocalContours=False,
                 show_grid=False,
                 line_thickness_factor=2):
    """ Use SWC data to get data of the dendritic and axonal
        arborization and contours.

    SWC data contains ID of coordinate and P (link to
    previous coordinate).Segments are drawn based on coordinate for
    which ID == P-1. Linking nodes are found by searching for the ID
    defined by P of a segments first coordinate.

    Args:
        - cellAmpID: unique identifier for the recorded cell eg. BJ1832a1
        - db_path: fullpath to folder containing insularDatabase
            and maoLabSlideDatabase.
        - viewEndpoints: default is None; so by default will not show the
            endpoints. Any assignment will trigger visualization of
            endpoints eg. viewEndpoints=False will show endpoints neuronal
            processes.
        - viewContours: default is False; so by default will not show all the
            contours. viewContours=True will show contours.
        - viewLocalContours: default is False; so by default will not show the
            local contours ie pia and corpus callosum contours.
        - show_grid: default is False, so no grids,labels, and zerolines shown.
        - line_thickness_factor: default is 2, thickness as defined by tracing,
                                 multiplied with factor line_thickness_factor.

    Returns:
        - plotly based interactive 3D scatterplot with lines
    """

    cell = Cell(cellAmpID, verbose=True)
    if hasattr(cell, 'swc_data'):
        dataTree = get_dataTree(cell.swc_data)

        processNameDict = {1: 'soma',
                           2: 'axon',
                           3: 'dendrite',
                           4: 'apical dendrite'}
        processColorDict = {1: 'rgb(0, 0, 0)',
                            2: 'rgb(43, 106, 108)',
                            3: 'rgb(22, 0, 249)',
                            4: 'rgb(254, 0, 0)'}
        # In order to only draw only once a segment in the legend make dict
        visible_legend_entry_dict = {2: True,
                                     3: True,
                                     4: True}
        processDiameterDict = {1: 6, 2: 3, 3: 4, 4: 5}
        contourNameList = ['CCSurface',
                           'PiaSurface',
                           'PiaSlide',
                           'Outline',
                           'CC',
                           'IC',
                           'fornix',
                           'latVentrical',
                           'ACA']
        localContourNameList = ['CCSurface',
                                'PiaSurface',
                                'PiaSlide']
        contourColorDict = {'CCSurface': 'rgba(57, 158, 14, 1)',
                            'PiaSurface': 'rgba(10, 77, 100, 1)',
                            'PiaSlide': 'rgba(42, 77, 154, 1)',
                            'Outline': 'rgba(39, 109, 255, 0.7)',
                            'CC': 'rgba(39, 109, 63, 0.7)',
                            'IC': 'rgba(255, 30, 40, 0.7)',
                            'fornix': 'rgba(196, 11, 40, 0.7)',
                            'latVentrical': 'rgba(5, 45, 58, 0.7)',
                            'ACA': 'rgba(38, 145, 133, 0.7)'}
        contourGroup = {'CCSurface': 'thicknessBorders',
                        'PiaSurface': 'thicknessBorders',
                        'PiaSlide': 'thicknessBorders',
                        'Outline': 'AnatomicalBorders',
                        'CC': 'AnatomicalBorders',
                        'IC': 'AnatomicalBorders',
                        'fornix': 'AnatomicalBorders',
                        'latVentrical': 'AnatomicalBorders',
                        'ACA': 'AnatomicalBorders'}

        dataPlot = []
        # print contours
        if viewContours is False and viewLocalContours is False:
            viewContours = 'legendonly'
        elif viewContours is False and viewLocalContours is True:
            viewContours = False
        contourData = extractContourData(cell.swc_data, cell.swc_meta)
        for contourName in contourNameList:
            if contourName in contourData:
                # In some cases there are multiple startingpoints for borders
                # eg lateral ventricles. Make sure to treat the as seperate
                P = list(contourData[contourName]['P'] == -1)
                startPoints = list(compress(range(len(P)), P))
                startPoints.append(len(contourData[contourName]))
                for idx, start in enumerate(startPoints[:-1]):
                    dataPlot.append(
                        go.Scatter3d(
                            x=contourData[contourName]['X'][start:
                                                            startPoints[
                                                                idx + 1]],
                            y=contourData[contourName]['Y'][start:
                                                            startPoints[
                                                                idx + 1]],
                            z=contourData[contourName]['Z'][start:
                                                            startPoints[
                                                                idx + 1]],
                            mode='lines',
                            name=contourName,
                            visible=viewContours,
                            legendgroup=contourGroup[contourName],
                            line={'width': 4 * line_thickness_factor,
                                  'reversescale': False,
                                  'color': contourColorDict[contourName]}
                        )
                    )
        # print only local contours
        if viewLocalContours is True:
            contourData = extractContourData(cell.swc_data, cell.swc_meta)
            for contourName in localContourNameList:
                if contourName in contourData:
                    # In some cases there are multiple startingpoints for
                    # borderseg lateral ventricles. Make sure to treat the as
                    # seperate
                    P = list(contourData[contourName]['P'] == -1)
                    startPoints = list(compress(range(len(P)), P))
                    startPoints.append(len(contourData[contourName]))
                    for idx, start in enumerate(startPoints[:-1]):
                        dataPlot.append(
                            go.Scatter3d(
                                x=contourData[contourName]['X'][start:
                                                                startPoints[
                                                                    idx + 1]],
                                y=contourData[contourName]['Y'][start:
                                                                startPoints[
                                                                    idx + 1]],
                                z=contourData[contourName]['Z'][start:
                                                                startPoints[
                                                                    idx + 1]],
                                mode='lines',
                                name=contourName,
                                legendgroup=contourGroup[contourName],
                                line={'width': 4 * line_thickness_factor,
                                      'reversescale': False,
                                      'color': contourColorDict[contourName]}
                            )
                        )
        # print all endpoint
        if viewEndpoints is True:
            endpointData = get_endpoints(cell.swc_data)
            dataPlot.append(
                go.Scatter3d(
                    x=endpointData['X'],
                    y=endpointData['Y'],
                    z=endpointData['Z'],
                    mode='markers',
                    name='endpoint',
                    legendgroup='endpoints',
                    marker={'size': 5 * line_thickness_factor,
                            'color': 'rgba(79,93,105,0.8)'}))

        # also print soma
        dataPlot.append(
            go.Scatter3d(
                x=cell.swc_data['X'][cell.swc_data['TYPE'] == 1],
                y=cell.swc_data['Y'][cell.swc_data['TYPE'] == 1],
                z=cell.swc_data['Z'][cell.swc_data['TYPE'] == 1],
                mode='lines',
                name='soma',
                line={'width': processDiameterDict[1] *
                               line_thickness_factor,
                      'color': processColorDict[1],
                      'reversescale': False}))

        # print neuronal processes
        startSegment = list()
        stopSegment = list()
        # the Soma center of Mass can act as the startpoint for all neuronal
        # processes. In that case there is no P=-1 for TYPE:2,3,4 and a extra
        # initial segment need to be added from center of mass to startSegment
        stemIDs = list()
        stemIDs = dataTree['ID'][dataTree['P'] == -1]
        comON = False
        if stemIDs.size < 1:
            comON = True
            somaData = cell.swc_data[cell.swc_data['TYPE'] == 1]
        for ID, P in list(zip(dataTree['ID'], dataTree['P'])):
            if int(ID) - (int(P)) != 1:
                startSegment.append(int(ID))
        np.array(startSegment)
        stopSegment = np.array(startSegment[1:])
        stopSegment = np.append(stopSegment, dataTree['ID'][len(dataTree) - 1])
        stopSegment = stopSegment

        for start, stop in list(zip(startSegment, stopSegment)):
            data2Plot = ()
            if (comON and dataTree[int(np.where(dataTree['ID'] == start)[0])]
            ['P'] == 1):
                data2Plot = somaData[somaData['P'] == -1]
            try:
                connectedNodeP = dataTree['P'][int(np.where(dataTree['ID'] ==
                                                            start)[0])]
                data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                       connectedNodeP)[0])]
                if data2Plot:
                    data2Plot = np.append(data2Plot, data2PlotAp)
                else:
                    data2Plot = data2PlotAp

            except (KeyError, ValueError, TypeError):
                pass
            if data2Plot:
                data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                       start)[0]):
                                          int(np.where(dataTree['ID'] ==
                                                       stop)[0])]
                data2Plot = np.append(data2Plot, data2PlotAp)
            else:
                data2Plot = dataTree[:][int(np.where(dataTree['ID'] ==
                                                     start)[0]):
                                        int(np.where(dataTree['ID'] ==
                                                     stop)[0])]
            process = dataTree['TYPE'][int(
                np.where(dataTree['ID'] == start)[0])]
            dataPlot.append(
                go.Scatter3d(
                    x=data2Plot['X'],
                    y=data2Plot['Y'],
                    z=data2Plot['Z'],
                    mode='lines',
                    name=processNameDict[dataTree['TYPE'][int(
                        np.where(dataTree['ID'] == start)[0])]],
                    legendgroup=str(dataTree['TYPE'][int(
                        np.where(dataTree['ID'] == start)[0])]),
                    showlegend=visible_legend_entry_dict[process],
                    line={'width': processDiameterDict[dataTree['TYPE'][int(
                        np.where(dataTree['ID'] == start)[0])]] *
                                   line_thickness_factor,
                          'reversescale': False,
                          'color': processColorDict[process]}))
            # Shut down the visiblity for this particular process
            visible_legend_entry_dict[process] = False
        if show_grid:
            ticks = 'outside'
            ticks_name = ('y', 'x', 'z')
        else:
            ticks = ''
            ticks_name = ('', '', '')
        layout = go.Layout({'scene': {
            'yaxis': {'title': ticks_name[0],
                      'showgrid': show_grid,
                      'showline': show_grid,
                      'zeroline': show_grid,
                      'ticks': ticks,
                      'showticklabels': show_grid},
            'xaxis': {'title': ticks_name[1],
                      'showgrid': show_grid,
                      'showline': show_grid,
                      'zeroline': show_grid,
                      'ticks': ticks,
                      'showticklabels': show_grid},
            'zaxis': {'title': ticks_name[2],
                      'showgrid': show_grid,
                      'showline': show_grid,
                      'zeroline': show_grid,
                      'ticks': ticks,
                      'showticklabels': show_grid}}})

        fig = go.Figure(data=dataPlot, layout=layout)
        camera = dict(
            up=dict(x=0, y=1, z=1),
            center=dict(x=0, y=0, z=0),
            eye=dict(x=0.1, y=0.1, z=2.5))
        fig['layout'].update(scene=dict(aspectmode="data"))
        fig['layout'].update(scene=dict(camera=camera))
        fig['layout'].update(title=go.layout.Title(text=str(cellAmpID),
                                                   xref='paper', x=0))
    else:
        fig = go.Figure()
        fig.update_layout(
            title=str(cellAmpID) + ' no morphology available'
        )
    # Add buttons with html references.
    html_plot = HTML("""
    <html>
    <head>
        <link rel="stylesheet" type="text/css" href="stylz.css" />
    </head>
    <body>

    <div class="wrapper">
        <div class="dropdown">

          <button onclick="myFunction()" class="dropdown">View..</button>
          <div id="myDropdown" class="dropdown-content">
            <a href='""" + str(cellAmpID) + """.html'>Morphology</a>
            <a href="spike_train/""" + str(cellAmpID) +
                     """.html">Spike_train</a>
            <a href="spike_waveform/""" + str(cellAmpID) +
                     """.html">Spike waveform</a>
            <a href="spike_ramp/""" + str(cellAmpID) +
                     """.html">Ramp</a>
            <a href="passive_properties/""" + str(cellAmpID) +
                     """.html">Passive properties</a>
            <a href="local_input/""" + str(cellAmpID) +
                     """.html">Local input</a>
            <a href="long-range_input/""" + str(cellAmpID) +
                     """.html">Long-range input</a>
            <a href="meta/""" + str(cellAmpID) +
                     """.html">Projection and genetic properties [meta]</a>
            <a href="cell_overview.html">back to dataset overview</a>
          </div>
        </div>
        <div class="reportButton">
            <button class="reportButton" id='reportButton'>""" +
            """<a href="report_bug.html/?origin=/morphology/ """ +
            str(cellAmpID) +
                     """ "> Report bug </a></button>
        </div>
    """
        + str(py_offline.plot(fig, filename=cellAmpID, output_type="div",
                        include_plotlyjs=True))
        + '</div>' +
         """<script>
         /* When the user clicks on the button,
         toggle between hiding and showing the dropdown content */
         function myFunction() {
           document.getElementById("myDropdown").classList.toggle("show");
         }

         // Close the dropdown if the user clicks outside of it
         window.onclick = function(event) {
           if (!event.target.matches('.dropdown')) {
             var dropdowns = document.getElementsByClassName("dropdown-content");
             var i;
             for (i = 0; i < dropdowns.length; i++) {
               var openDropdown = dropdowns[i];
               if (openDropdown.classList.contains('show')) {
                 openDropdown.classList.remove('show');
               }
             }
           }
         }
         </script>
         </body>
         </html>""")
    html_plot = html_plot.data
    with open(str(Path(bcs_global.path_website) /
                           (str(cellAmpID) + '.html')), 'w') as f:
        f.write(html_plot)
    # The main piece of code, with which we embed the buttons is shown above,
    # first we define the plotly plot , then using display and html functions,
    # we can embed the buttons inside a div with class button-group and with CSS
    # we position that button.


def generate_qr(cellAmpIDs):
    """Generate QR code referring to the neuro.muniak.com/cells/cellAmpID.html
    webpage.

    Args:
        - cellAmpIDs: array of strings with cellAmpID of cell.
    """
    source_path = Path(bcs_global.path_website) / 'QR_html_logo'
    for cellAmpID in cellAmpIDs:
        # html can/should be changed to link to a different website
        html = 'neuro.muniak.com/cells/' + str(cellAmpID) + '.html'
        data = pyqrcode.create(html)
        image_path = Path(source_path) / (str(cellAmpID) + '.png')
        data.png(str(image_path), scale=16,
                 quiet_zone=0)
        # Now open that png image to put the logo
        img = Image.open(str(image_path))
        width, height = img.size
        img = img.convert('RGB')
        # How big the logo we want to put in the qr code png
        logo_size = 210

        # Open the logo image
        logo = Image.open(str(Path(source_path) / 'logo' / 'MAOlab.png'))

        # Calculate xmin, ymin, xmax, ymax to put the logo
        xmin = ymin = int((width / 2) - (logo_size / 2))
        xmax = ymax = int((width / 2) + (logo_size / 2))

        # resize the logo as calculated
        logo = logo.resize((xmax - xmin, ymax - ymin))

        # put the logo in the qr code
        img.paste(logo, (xmin, ymin, xmax, ymax))
        img.save(str(image_path))


def generate_printable_morphology(cellAmpIDs, generate_qr=False):
    """Generate small printable images of the morphology w/o qr-code.

    Args:
        cellAmpIDs: array of strings with cellAmpID of cell.
        generate_qr: Default is False, otherwise generate qr
    """
    if generate_qr:
        # generate qr code for the print moprhology images
        generate_qr(cellAmpIDs)
    # load databases
    # first determine outer dimensions of the morphologies
    df_cell = readDBCSV(db_path=bcs_global.path_database,
                        filename=bcs_global.filename_cell_db)
    df_slide = readDBCSV(db_path=bcs_global.path_database,
                         filename=bcs_global.filename_slide_db)
    source_path = Path(bcs_global.path_website) / 'QR_html_logo'
    # generate dictionary of cellAmpIDs with tuples containing the logo_names
    logo_tuple = ('genetic_properties.png', 'electric_properties.png',
                  'long-range_input_map.png', 'local_input_map.png',
                  'output_targets.png')
    logo_dict = {cellAmpID: logo_tuple for cellAmpID in cellAmpIDs}
    df_cell_indexed = df_cell.set_index('cellAmpID')
    ## Get the maximum outer dimensions of the dataset:
    # open all swc files, read the max Xleft, Xright , Ytop, Ybottom, Ztop, Zbottom
    # these outer dimensions sets the canvas
    cols = ['Xleft', 'Xright', 'Ybottom', 'Ytop', 'Z', 'Ysoma_center']
    outer_dimensions_canvas = pd.DataFrame(
        np.zeros((len(cellAmpIDs), len(cols))),
        index=cellAmpIDs,
        columns=cols)
    for cellAmpID in cellAmpIDs:
        select_logo = ()
        # check if genetic info is present, for now this is only based on cre
        # lines and specifically checked
        if isinstance(df_cell_indexed.loc[cellAmpID, 'cellType'], str):
            if df_cell_indexed.loc[cellAmpID, 'cellType'] == 'PV' or \
                    df_cell_indexed.loc[cellAmpID, 'cellType'] == 'D1' or \
                    df_cell_indexed.loc[cellAmpID, 'cellType'] == 'GAD2':
                select_logo = select_logo + (logo_tuple[0],)
        if re.match('intrinsicP',
                    str(df_cell_indexed.loc[cellAmpID, 'ephysDataType'])):
            select_logo = select_logo + (logo_tuple[1],)
        if re.match('sCRACM',
                    str(df_cell_indexed.loc[cellAmpID, 'ephysDataType'])):
            select_logo = select_logo + (logo_tuple[2],)
        if re.match('LSPS',
                    str(df_cell_indexed.loc[cellAmpID, 'ephysDataType'])) or \
                re.match('MNIMap',
                         str(df_cell_indexed.loc[cellAmpID, 'ephysDataType'])):
            select_logo = select_logo + (logo_tuple[3],)
        if isinstance(df_cell_indexed.loc[cellAmpID, 'retrobeadsPresent'], str):
            select_logo = select_logo + (logo_tuple[4],)
        logo_dict[cellAmpID] = select_logo
    ### Newest iteration with logo-QR
    # now lets print out all cells iteratively
    for cellAmpID in cellAmpIDs:
        fig, (ax1) = plt.subplots(nrows=1, ncols=1, sharey=True, figsize=(
        4.5, 6.75))  # note aspect ratio of 1.5
        fig.subplots_adjust(wspace=0)
        fig2, (ax2) = plt.subplots(nrows=1, ncols=1, sharey=True, figsize=(
            4.5, 6.75))  # note aspect ratio of 1.5
        fig2.subplots_adjust(wspace=0)
        defWidth = 1
        cell = Cell(cellAmpID, bcs_global.path_database,
                    df_cell=df_cell, df_slide=df_slide)
        if hasattr(cell, 'swc_data'):
            dataTree = get_dataTree(cell.swc_data)
            dataTree = dataTree[dataTree['TYPE'] >= 3]
            if len(dataTree) > 1:
                for col in cols:
                    if col == 'Ybottom':
                        outer_dimensions_canvas.at[cellAmpID, col] = \
                            min(dataTree['Y'])
                    elif col == 'Ytop':
                        outer_dimensions_canvas.at[cellAmpID, col] = \
                            max(dataTree['Y'])
                    elif col == 'Ysoma_center':
                        soma = cell.swc_data[cell.swc_data['TYPE'] == 1]
                        outer_dimensions_canvas.at[cellAmpID, col] = soma['Y'][
                            soma['P'] == -1]
                    elif col == 'Xleft':
                        outer_dimensions_canvas.at[cellAmpID, col] = \
                            min(dataTree['X'])
                    elif col == 'Xright':
                        outer_dimensions_canvas.at[cellAmpID, col] = \
                            max(dataTree['X'])
                    else:
                        outer_dimensions_canvas.at[cellAmpID, col] = \
                            max(np.abs(dataTree[col]))
                # Now make corrections of the morphology data, such that the whole
                # cell sits as centered as possible (x,y) = 0,0 will be the center
                # of the plot with boundaries set at xlim=(-500,500) and
                # ylim=(-750,750)
                Y_span = outer_dimensions_canvas.loc[cellAmpID, 'Ytop'] - \
                         outer_dimensions_canvas.loc[cellAmpID, 'Ybottom']
                Y_center_orig = \
                    outer_dimensions_canvas.loc[cellAmpID, 'Ybottom'] + (Y_span / 2)
                X_span = outer_dimensions_canvas.loc[cellAmpID, 'Xright'] - \
                         outer_dimensions_canvas.loc[cellAmpID, 'Xleft']
                X_center_orig = outer_dimensions_canvas.loc[cellAmpID, 'Xright'] - (
                            X_span / 2)
                # correct the dataTree
                dataTree['X'] = dataTree['X'] - X_center_orig
                dataTree['Y'] = dataTree['Y'] - Y_center_orig
                # save soma
                soma = cell.swc_data[cell.swc_data['TYPE'] == 1]
                # correct also the soma
                soma['X'] = soma['X'] - X_center_orig
                soma['Y'] = soma['Y'] - Y_center_orig
                colorDendrite = np.array((22, 0, 249)) / 255
                colorApical = np.array((254, 0, 0)) / 255
                colorSelect = {3: colorDendrite,
                               4: colorApical}
                # print neuronal processes
                startSegment = list()
                stopSegment = list()
                stemIDs = list()
                stemIDs = dataTree['ID'][dataTree['P'] == -1]
                for ID, P in list(zip(dataTree['ID'], dataTree['P'])):
                    if int(ID) - (int(P)) != 1:
                        startSegment.append(int(ID))
                np.array(startSegment)
                stopSegment = np.array(startSegment[1:])
                stopSegment = np.append(stopSegment, dataTree['ID']
                [len(dataTree) - 1])
                stopSegment = stopSegment

                for start, stop in list(zip(startSegment, stopSegment)):
                    data2Plot = ()
                    try:
                        connectedNodeP = dataTree['P'][int(
                            np.where(dataTree['ID'] == start)[0])]
                        data2PlotAp = dataTree[:][int(np.where(
                            dataTree['ID'] == connectedNodeP)[0])]
                        if data2Plot:
                            data2Plot = np.append(data2Plot, data2PlotAp)
                        else:
                            data2Plot = data2PlotAp

                    except (KeyError, ValueError, TypeError):
                        pass
                    if data2Plot:
                        data2PlotAp = dataTree[:][int(np.where(dataTree['ID'] ==
                                                               start)[0]):
                                                  int(np.where(dataTree['ID'] ==
                                                               stop)[0])]
                        data2Plot = np.append(data2Plot, data2PlotAp)
                    else:
                        data2Plot = dataTree[:][int(np.where(dataTree['ID'] ==
                                                             start)[0]):
                                                int(np.where(dataTree['ID'] ==
                                                             stop)[0])]
                    cS = colorSelect[data2Plot['TYPE'][len(data2Plot) - 1]]
                    ax1.plot(data2Plot['X'], data2Plot['Y'], '-', color=cS,
                             linewidth=defWidth)
                    ax2.plot(data2Plot['X'], data2Plot['Y'], '-', color=cS,
                             linewidth=defWidth)

            # print soma
            ax1.plot(soma['X'][1:], soma['Y'][1:], '-',
                     color=(0, 0, 0), linewidth=1)
            ax1.fill(soma['X'][1:], soma['Y'][1:], color=(0, 0, 0),
                     zorder=100)
            ax2.plot(soma['X'][1:], soma['Y'][1:], '-',
                     color=(0, 0, 0), linewidth=1)
            ax2.fill(soma['X'][1:], soma['Y'][1:], color=(0, 0, 0),
                     zorder=100)
            # zorder is required to make the fill appear above all other items
        if not hasattr(cell, 'swc_data'):
            ax1.text(-350, 0 - 50, 'morphology \nnot available',
                     fontdict={'fontsize': 16})
            ax2.text(-350, 0 - 50, 'morphology \nnot available',
                     fontdict={'fontsize': 16})
        ax1.text(-75, 750 - 50, str(cellAmpID), fontdict={'fontsize': 8})
        ax2.text(-75, 750 - 50, str(cellAmpID), fontdict={'fontsize': 8})
        # make markings for cutting
        # bottom left
        ax1.plot([-500, -500], [-750, -740], '-', color='grey', linewidth=1, )
        ax1.plot([-500, -490], [-750, -750], '-', color='grey', linewidth=1, )
        ax2.plot([-500, -500], [-750, -740], '-', color='white', linewidth=1,)
        # bottom right
        ax1.plot([500, 500], [-750, -740], '-', color='grey', linewidth=1, )
        ax1.plot([500, 490], [-750, -750], '-', color='grey', linewidth=1, )
        ax2.plot([500, 500], [-750, -740], '-', color='white', linewidth=1, )
        # top left
        ax1.plot([-500, -500], [750, 740], '-', color='grey', linewidth=1, )
        ax1.plot([-500, -490], [750, 750], '-', color='grey', linewidth=1, )
        ax2.plot([-500, -500], [750, 740], '-', color='white', linewidth=1, )
        # top right
        ax1.plot([500, 500], [750, 740], '-', color='grey', linewidth=1, )
        ax1.plot([500, 490], [750, 750], '-', color='grey', linewidth=1, )
        ax2.plot([500, 500], [750, 740], '-', color='white', linewidth=1, )
        ax1.set_xlim([-500, 500])
        ax2.set_xlim([-500, 500])
        ax1.set_aspect('equal')
        ax2.set_aspect('equal')
        ax1.axis('off')
        ax2.axis('off')
        # Add information symbols
        logos = logo_dict[cellAmpID]
        logo_path = Path(bcs_global.path_website) / 'QR_html_logo' / 'logo'
        log_pos = 5425
        log_pos2 = 750
        for logo in logos:
            logo_sub_path = Path(logo_path) / logo
            img = Image.open(str(logo_sub_path))
            width, height = img.size
            img = img.convert('RGB')
            width_resize = 300
            width_resize2 = 50
            height_resize = int(height / (width / width_resize))
            height_resize2 = int(height / (width / width_resize2))
            img1 = img.resize((width_resize, height_resize))
            img2 = img.resize((width_resize2, height_resize2))
            # place logo
            fig.figimage(img1, 50, log_pos)
            fig2.figimage(img2, 50, log_pos2)
            log_pos -= 350
            log_pos2 -= 50
        # set axes properties
        fig.axes[0].get_xaxis().set_visible(False)
        fig2.axes[0].get_xaxis().set_visible(False)
        fig.axes[0].get_yaxis().set_visible(False)
        fig2.axes[0].get_yaxis().set_visible(False)
        fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0,
                            hspace=0)
        fig2.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0,
                            hspace=0)
        fig2.savefig(
            str(Path(bcs_global.path_website) / 'morph_thumbnails' /
                         (str(cellAmpID) + '.jpeg')), dpi=120,
            bbox_inches='tight',
            pad_inches=0)
        if generate_qr:
            # Add QR code
            qr_path = Path(source_path) / (str(cellAmpID) + '.png')
            image = plt.imread(str(qr_path))
            # place image in only fig
            fig.figimage(image, 50, 350, zorder=101)
            fig.savefig(
                str(Path(bcs_global.path_website) / 'morph_prints' /
                    (str(cellAmpID) + '.png')), dpi=900, bbox_inches='tight',
                pad_inches=0)
        plt.close('all')


def plot_metaData(cellAmpIDs):
    """Make an html containing meta data from insularCellDatabase

    Args:
        - cellAmpIDs: array of strings with cellAmpID of cell.

    """
    fig = []
    # plot table with metadata
    table_feature_list = ['AnimalID',
                          'SurgeryID',
                          'SliceID',
                          'SlideID',
                          'Cell type',
                          'Projection target',
                          'Slide location',
                          'Genotype',
                          'Sex',
                          'Age (days)',
                          'Slice orientation',
                          'Slice thickness',
                          'Slicing method',
                          'Internal',
                          'Relative cortical depth',
                          'Projection target',
                          'Injection location 1',
                          'Injectant location 1',
                          'Injection location 2',
                          'Injectant location 2']
    cell_df = readDBCSV(db_path=bcs_global.path_database,
                        filename=bcs_global.filename_cell_db)
    cell_df = cell_df.set_index('cellAmpID', drop=False)
    for cellAmpID in cellAmpIDs:
        table_datapoints = list()
        table_datapoints.append(cell_df.loc[cellAmpID, 'animalID'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'surgeryID'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'sliceID'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'slideID'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'cellType'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'retrobeadsPresent'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'storageboxID'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'genotype'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'sex'])
        try:
            table_datapoints.append(int(
                float(cell_df.loc[cellAmpID, 'ephysDate']) - float(
                    cell_df.loc[cellAmpID, 'dob'])))
        except ValueError:
            table_datapoints.append('NaN')
        table_datapoints.append(cell_df.loc[cellAmpID, 'orientation'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'slicingThickness'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'sliceMethod'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'internalID'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'somaCortexPosition'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'retrobeadsPresent'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'injection1Target'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'injectant1'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'injection2Target'])
        table_datapoints.append(cell_df.loc[cellAmpID, 'injectant2'])

        fig.append(go.Table(header=dict(values=['Feature', str(cellAmpID)]),
                            cells=dict(
                                values=[table_feature_list, table_datapoints])))

        plotly_embedding = str(
            py_offline.plot(fig, filename='cellAmpID', output_type="div",
                            include_plotlyjs=True))

        html_plot = HTML("""
        <html>
        <head>
            <link rel="stylesheet" type="text/css" href="../stylz.css" />
        </head>
        <body>
        <div class="wrapper">
            <div class="dropdown">

              <button onclick="myFunction()" class="dropdown">View..</button>
              <div id="myDropdown" class="dropdown-content">
                <a href="../""" + str(cellAmpID) +
                         """.html">Morphology</a>
                <a href="../spike_train/""" + str(cellAmpID) +
                         """.html">Spike_train</a>
                <a href="../spike_waveform/""" + str(cellAmpID) +
                         """.html">Spike waveform</a>
                <a href="../spike_ramp/""" + str(cellAmpID) +
                         """.html">Ramp</a>
                <a href="../passive_properties/""" + str(cellAmpID) +
                         """.html">Passive properties</a>
                <a href="../local_input/""" + str(cellAmpID) +
                         """.html">Local input</a>
                <a href="../long-range_input/""" + str(cellAmpID) +
                         """.html">Long-range input</a>
                <a href="../meta/""" + str(cellAmpID) +
                         """.html">Projection and genetic properties [meta]</a>
                <a href="../cell_overview.html">back to dataset overview</a>
              </div>
            </div>
            <div class="reportButton">
                <button class="reportButton" id='reportButton'>
                <a href="../report_bug.html/?origin=/spike_waveform/ """ +
                         str(cellAmpID) +
                         """ "> Report bug </a></button></div> """
            + plotly_embedding
            + '</div>' +
            """<script>
            /* When the user clicks on the button,
            toggle between hiding and showing the dropdown content */
            function myFunction() {
            document.getElementById("myDropdown").classList.toggle("show");
            }

            // Close the dropdown if the user clicks outside of it
            window.onclick = function(event) {
            if (!event.target.matches('.dropdown')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
             openDropdown.classList.remove('show');
            }
            }
            }
            }
            </script>""")
        html_plot = html_plot.data
        website_path = (Path(bcs_global.path_website) / 'meta' /
                               (str(cellAmpID) + '.html'))
        print(str(website_path))
        with open(website_path, 'w') as f:
            f.write(html_plot)
        print('wrote html sucessfull')
        # The main piece of code, with which we embed the buttons is shown
        # above, first we define the plotly plot, then using display and html
        # functions, we can embed the buttons inside a div with class button-
        # group and with CSS we position that button.


def transform_cell(cell, axial, azimu, polar, normalizer,
                   voxel_multiplier):
    """Transform a cell morphology based on radians.

    Args:
        - cell, bcs instance of a neuron (Cell object).
        - axial, rotation along the Y axis (cortical thickness).
        - azimu, rotation along the XZ plane.
        - polar, rotation along the XY plane.

    Returns:
        - updated cell with rotated morphology.
    """
    # locate soma at center (0,0,0)
    com = cell.swc_data[np.logical_and(cell.swc_data['P'] == -1, cell.swc_data[
        'TYPE'] == 1)]  # here we assume that the origin of all processes are
    # Transform morphology coordinate system to align with a specific feature to
    # become 0,0,0 (x,y,z)
    matrix_tree = pd.DataFrame(cell.swc_data[['X', 'Y', 'Z']])
    matrix_tree['X'] = matrix_tree['X'] - com['X']
    matrix_tree['Y'] = matrix_tree['Y'] - com['Y']
    matrix_tree['Z'] = matrix_tree['Z'] - com['Z']
    # replace matrix_tree data for centered data
    # coming from the center of mass of the cellbody contour
    original_tree = np.array(matrix_tree)
    original_tree[:, 0] = (original_tree[:, 0] / normalizer) * voxel_multiplier
    original_tree[:, 1] = (original_tree[:, 1] / normalizer) * voxel_multiplier
    original_tree[:, 2] = (original_tree[:, 2] / normalizer) * voxel_multiplier
    # generate a transformation matrix to transform the data along all the
    # combinations of angles in polar, azimuth and axial rotation.
    # transform matrix for data
    # Contributed by Cesar Celis and Michael Muniak 2019
    tform = np.zeros((3, 3, 1))
    tform[0, 0, 0] = (np.cos(azimu) * np.cos(polar) * np.cos(axial)
                      - np.sin(azimu) * np.sin(axial))
    tform[0, 1, 0] = (-np.sin(azimu) * np.cos(polar) * np.cos(axial)
                      - np.cos(azimu) * np.sin(axial))
    tform[0, 2, 0] = (np.sin(polar) * np.cos(axial))
    tform[1, 0, 0] = (np.cos(azimu) * np.cos(polar) * np.sin(axial)
                      + np.sin(azimu) * np.cos(axial))
    tform[1, 1, 0] = (-np.sin(azimu) * np.cos(polar) * np.sin(axial)
                      + np.cos(azimu) * np.cos(axial))
    tform[1, 2, 0] = (np.sin(polar) * np.sin(axial))
    tform[2, 0, 0] = (-np.cos(azimu) * np.sin(polar))
    tform[2, 1, 0] = (np.sin(azimu) * np.sin(polar))
    tform[2, 2, 0] = (np.cos(polar))

    total_coordinates, _ = np.shape(original_tree)
    _, _, total_rotations = np.shape(tform)
    switch_tree = np.vstack((original_tree[:, 0],
                             original_tree[:, 2],
                             original_tree[:, 1])).T
    transformed_trees_switched = (np.reshape(switch_tree[:, 0],
                                             (total_coordinates, 1, 1)) *
                                  tform[0, :, :] +
                                  np.reshape(switch_tree[:, 1],
                                             (total_coordinates, 1, 1)) *
                                  tform[1, :, :] +
                                  np.reshape(switch_tree[:, 2],
                                             (total_coordinates, 1, 1)) *
                                  tform[2, :, :])
    transformed_trees = np.hstack((
        np.reshape(transformed_trees_switched[:, 0, :],
                   (total_coordinates, 1, total_rotations)),
        np.reshape(transformed_trees_switched[:, 2, :],
                   (total_coordinates, 1, total_rotations)),
        np.reshape(transformed_trees_switched[:, 1, :],
                   (total_coordinates, 1, total_rotations))))

    cell.swc_data['X'] = np.squeeze(transformed_trees[:, 0])
    cell.swc_data['Y'] = np.squeeze(transformed_trees[:, 1])
    cell.swc_data['Z'] = np.squeeze(transformed_trees[:, 2])
    return cell


def generate_average_morphology(cellAmpIDs_cluster=None,
                                align_parameter='fraction_overlap_dendrites',
                                morphology_voxel_array_path=
                                Path(bcs_global.path_morph_analysis)/
                                '__voxel_arrays__',
                                morphology_match_summary_file=
                                Path(bcs_global.path_morph_analysis)/
                                    'cell_matching'/
                                    'xyz-normby-cortical_thickness0.05'/
                                    'match_summary.h5',
                                orien_lib_filename=
                                Path(bcs_global.path_morph_analysis)/
                                '__voxel_arrays__' /
                                'lib_xyz-normby-cortical_thickness'
                                '0.05_1104_orientations.csv',
                                title=None
                                ):
    """ Generate a plotly figure containing the averaged cluster morphology.


    Args:
        cellAmpIDs_cluster: list of cellAmpIDs that belong to the cluster to be
            averaged.
        morphology_voxel_array_path: full path where directories are located
            which contain the rotated morphology voxel arrays.
        morphology_match_summary_file: full path to the file holding the
            summary HDF5 file.
        orien_lib_filename: full path to the file holding the library of
            orientations used to transform morphology into voxel arrays.
        title: string, if not None; plot title with the figure.

    Returns:
        fig: plotly figure containing the averaged morphology
        stats_cluster: pandas dataframe containing stats of the morphologies
            relative to the averaged morphology.
    """
    align_parameters = ['fraction_overlap_dendrites',
                        'fraction_overlap_basal',
                        'fraction_overlap_apical',
                        'distance_overlap_only_dendrites',
                        'distance_overlap_only_apical',
                        'distance_overlap_only_basal',
                        'distance_any_present_dendrites',
                        'distance_any_present_apical',
                        'distance_any_present_basal',
                        'corrcoef_overlap_only_dendrites',
                        'corrcoef_overlap_only_apical',
                        'corrcoef_overlap_only_basal',
                        'corrcoef_any_present_dendrites',
                        'corrcoef_any_present_apical',
                        'corrcoef_any_present_basal',]
    assert align_parameter in align_parameters, \
        '%s is not option, choose from : %s' %(align_parameter,
                                               align_parameters)
    if isinstance(cellAmpIDs_cluster, list):
        cellAmpIDs_cluster = np.array(cellAmpIDs_cluster)
    dbs = get_databases()
    df_cell = dbs['cell_df']
    df_slide = dbs['slide_df']
    hdf = pd.read_hdf(str(morphology_match_summary_file), 'match_summary/score',
                      columns=[align_parameter],
                      where="template = cellAmpIDs_cluster")
    idx = pd.IndexSlice
    hdf = hdf.loc[idx[:, cellAmpIDs_cluster], idx[:]]
    mask_self = zip(cellAmpIDs_cluster, cellAmpIDs_cluster)
    hdf = hdf.drop(mask_self, axis=0)
    hdf = hdf.sort_values(align_parameter, ascending=False)
    hdf['score'] = np.arange(0, len(hdf))
    grouped = hdf.groupby('template').sum() + hdf.groupby('match').sum()
    grouped = grouped.sort_values('score', ascending=True)
    exemplar = grouped.iloc[0].name
    hdf_flip = pd.read_hdf(str(morphology_match_summary_file),
                           'match_summary/flip',
                           columns=[align_parameter],
                           where="template = exemplar")
    hdf_flip = hdf_flip.loc[idx[:, cellAmpIDs_cluster], idx[:]]
    hdf_axial = pd.read_hdf(str(morphology_match_summary_file),
                            'match_summary/axial',
                            columns=[align_parameter],
                            where="template = exemplar")
    hdf_axial = hdf_axial.loc[idx[:, cellAmpIDs_cluster], idx[:]]
    hdf_azimuth = pd.read_hdf(str(morphology_match_summary_file),
                              'match_summary/azimuth',
                              columns=[align_parameter],
                              where="template = exemplar")
    hdf_azimuth = hdf_azimuth.loc[idx[:, cellAmpIDs_cluster], idx[:]]
    hdf_polar = pd.read_hdf(str(morphology_match_summary_file),
                            'match_summary/polar',
                            columns=[align_parameter],
                            where="template = exemplar")
    hdf_polar = hdf_polar.loc[idx[:, cellAmpIDs_cluster], idx[:]]
    file_dict = {cellAmpID: str(morphology_voxel_array_path / cellAmpID /
        'xyz-normby-cortical_thickness0.05' /
        str('_'.join([cellAmpID,
                      '0.05',
                      str(hdf_axial.loc[
                              idx[:, cellAmpID],
                              idx[align_parameter]].tolist()[0]),
                      str(hdf_azimuth.loc[
                              idx[:, cellAmpID],
                              idx[align_parameter]].tolist()[0]),
                      str(hdf_polar.loc[
                              idx[:, cellAmpID],
                              idx[align_parameter]].tolist()[0]),
                      '.csv'])))
                 for cellAmpID in cellAmpIDs_cluster}
    degree_dict = {
        cellAmpID: [hdf_flip.loc[idx[:, cellAmpID],
                                 idx[align_parameter]].tolist()[0],
                    hdf_axial.loc[idx[:, cellAmpID],
                                  idx[align_parameter]].tolist()[0],
                    hdf_azimuth.loc[idx[:, cellAmpID],
                                    idx[align_parameter]].tolist()[
                        0],
                    hdf_polar.loc[idx[:, cellAmpID],
                                  idx[align_parameter]].tolist()[0]]
        for cellAmpID in cellAmpIDs_cluster}
    # now obtain the exact rads for the degrees
    orien_lib = pd.read_csv(str(orien_lib_filename),
                            index_col=['flip', 'axial', 'azimuth', 'polar'])
    rad_dict = dict()
    for cellAmpID, packed_trans in degree_dict.items():
        flip, ax_deg, az_deg, po_deg = packed_trans
        ax_rad, az_rad, po_rad = orien_lib.loc[
            (flip, ax_deg, az_deg, po_deg), ['axial_rads', 'azimuth_rads',
                                             'polar_rads']]
        rad_dict[cellAmpID] = [packed_trans[0], ax_rad, az_rad, po_rad]
    # Generate a dictionary with all voxel arrays per cellAmpID. The voxel array is based on the rotated morphology that
    # in the match_summary gave the best match
    density_data_dict = {cellAmpID: pd.read_csv(file_path, index_col=['ID']) for
                         cellAmpID, file_path in file_dict.items()}
    # determine extremes
    x_extremes = list()
    y_extremes = list()
    z_extremes = list()
    for cellAmpID, density_data in density_data_dict.items():
        # filter for only voxels with data with apical and basal info
        density_data = density_data[
            np.logical_or(density_data.sum_length_basal > 0,
                          density_data.sum_length_apical > 0)]
        # update the dict
        density_data_dict[cellAmpID] = density_data
        # obtain extremes
        x_extremes.append(np.max(np.abs(density_data.X)))
        y_extremes.append(np.max(np.abs(density_data.Y)))
        z_extremes.append(np.max(np.abs(density_data.Z)))
    x_extreme = np.max(np.array(x_extremes))
    y_extreme = np.max(np.array(y_extremes))
    z_extreme = np.max(np.array(z_extremes))
    x_id_range = 2 * x_extreme + 1
    y_id_range = 2 * y_extreme + 1
    z_id_range = 2 * z_extreme + 1
    # generate a mean array, binary array, sd array for basal, apical and
    # dendrites
    dendrites_mean = np.zeros((x_id_range, y_id_range, z_id_range))
    dendrites_binary = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    dendrites_sd = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    apical_mean = np.zeros((x_id_range, y_id_range, z_id_range))
    apical_binary = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    apical_sd = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    basal_mean = np.zeros((x_id_range, y_id_range, z_id_range))
    basal_binary = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    basal_sd = np.zeros(
        (len(cellAmpIDs_cluster), x_id_range, y_id_range, z_id_range))
    mean_arrays = {'dendrites': dendrites_mean, 'apical': apical_mean,
                   'basal': basal_mean}
    binary_arrays = {'dendrites': dendrites_binary, 'apical': apical_binary,
                     'basal': basal_binary}
    sd_arrays = {'dendrites': dendrites_sd, 'apical': apical_sd,
                 'basal': basal_sd}
    density_dataarray_dict = dict()
    # fill arrays
    c = 0
    for cellAmpID, density_data in density_data_dict.items():
        # translate the voxel coordinates
        density_data.X += x_extreme
        density_data.Y += y_extreme
        density_data.Z += z_extreme
        # make arrays from the dataframes
        dendrites_ = np.zeros((x_id_range, y_id_range, z_id_range))
        apical_ = np.zeros((x_id_range, y_id_range, z_id_range))
        basal_ = np.zeros((x_id_range, y_id_range, z_id_range))
        # fill arrays
        dendrites_[np.array(density_data.X.astype(int)),
                   np.array(density_data.Y.astype(int)),
                   np.array(density_data.Z.astype(
                       int))] = density_data.sum_length_basal + \
                                density_data.sum_length_apical
        basal_[np.array(density_data.X.astype(int)),
               np.array(density_data.Y.astype(int)),
               np.array(density_data.Z.astype(int))] = \
            density_data.sum_length_basal

        apical_[np.array(density_data.X.astype(int)),
                np.array(density_data.Y.astype(int)),
                np.array(
                    density_data.Z.astype(int))] = \
            density_data.sum_length_apical

        # check for flips
        if hdf_flip.loc[idx[:, cellAmpID],
                        idx[align_parameter]].tolist()[0] == 1:
            dendrites_ = np.flip(dendrites_, axis=0)
            basal_ = np.flip(basal_, axis=0)
            apical_ = np.flip(apical_, axis=0)

        # to aid the iterative process:
        local_array_dict = {'dendrites': dendrites_, 'apical': apical_,
                            'basal': basal_}
        density_dataarray_dict[cellAmpID] = local_array_dict
        # place the data into the mean, binary, or SD arrays
        for process in mean_arrays:
            mean_arrays[process] += local_array_dict[process]

        for process in binary_arrays:
            mask_local = local_array_dict[process] > 0
            mask_local = mask_local.astype(int)
            binary_arrays[process][c] = mask_local

        for process in sd_arrays:
            sd_arrays[process][c] += local_array_dict[process]
        c += 1
    for process in sd_arrays:
        sd_arrays[process] = np.std(sd_arrays[process], axis=0)
    for process in mean_arrays:
        mean_arrays[process] = mean_arrays[process] / len(cellAmpIDs_cluster)
    # for process in binary_arrays:
    x = np.arange(0, x_id_range)
    y = np.arange(0, y_id_range)
    z = np.arange(0, z_id_range)
    Xy, xY = np.meshgrid(x, y)
    Xz, xZ = np.meshgrid(z, x)
    Yz, yZ = np.meshgrid(z, y)

    # make color lookup tables:
    # custom binary colorscale
    # ideal situation we have a blank for 0 and a visually good enough visible
    # grey value for the first step with gradual increase to black
    steps = 0.1
    start = 0.00000001
    end = 1
    color_steps = np.hstack((0, np.arange(start, end, steps)))
    bcj_binary = list()
    start_grey_setting = 25
    start_grey = 255 - start_grey_setting
    bcj_binary.append([0, "rgb(255, 255, 255)"])
    for i, value in enumerate(color_steps[1:]):
        if i == 0:
            grey_ = start_grey
        else:
            grey_ = int(start_grey - (steps * (255 - start_grey_setting)))
        color_ = "rgb(%i, %i, %i)" % (grey_, grey_, grey_)
        bcj_binary.append([value, color_])
        start_grey = grey_
    bcj_binary.append([1, "rgb(0, 0, 0)"])

    colorscale_heatmap = bcj_binary

    fig = make_subplots(
        rows=1, cols=1,
        specs=[[{}]])

    # plot for heatmap of XY
    XY_heatmap = np.flip(np.rot90(np.sum(mean_arrays['dendrites'], axis=2), 1),
                         axis=0)
    fig.add_trace(go.Heatmap(z=XY_heatmap,
                             colorscale=colorscale_heatmap,
                             name='dendritic density',
                             colorbar=dict(len=.25,
                                           thicknessmode='fraction',
                                           thickness=0.05,
                                           xanchor='left',
                                           yanchor='bottom',
                                           x=.85,
                                           y=0,
                                           showticklabels=True,
                                           nticks=2,
                                           tickmode='array',
                                           tickvals=[0, np.round(np.nanmax(
                                               np.nanmax(XY_heatmap, axis=0),
                                               axis=0), 1)],
                                           ticktext=['0%',
                                                     str(np.round(np.nanmax(
                                               np.nanmax(XY_heatmap, axis=0),
                                               axis=0) * 100, 1)) + '%'],
                                           ticks='outside',
                                           ypad=0,
                                           xpad=0, )),
                  row=1, col=1)

    # calculate contours for 0 and 1
    f_XY_heat_contour_value = np.sum(binary_arrays['dendrites'], axis=3)
    f_XY_heat_contour_value = f_XY_heat_contour_value > 0
    f_XY_heat_contour_value = f_XY_heat_contour_value.astype(int)
    f_XY_heat_contour_value = np.sum(f_XY_heat_contour_value, axis=0) / len(
        cellAmpIDs_cluster)
    f_XY_heat_contour_value = np.flip(np.rot90(f_XY_heat_contour_value, 1),
                                      axis=0)
    contours_0 = measure.find_contours(f_XY_heat_contour_value, 0)
    contours_80 = measure.find_contours(f_XY_heat_contour_value,
                                        0.8)  # 80% of the population
    contours_1 = measure.find_contours(f_XY_heat_contour_value,
                                       1 - sys.float_info.epsilon)  # cannot be
    # exactly 1
    # variables for restricting plotting area
    y_min = list()
    y_max = list()
    x_min = list()
    x_max = list()

    # plot contours
    for n, contours in enumerate(contours_0):
        if n == 0:
            showlegend = True
        else:
            showlegend = False
        # annotate what the extremes are for contours_0,
        # this will be used to restrict the plotting area
        y_min.append(np.min(contours[:, 0]))
        y_max.append(np.max(contours[:, 0]))
        x_min.append(np.min(contours[:, 1]))
        x_max.append(np.max(contours[:, 1]))

        fig.add_trace(go.Scattergl(x=contours[:, 1],
                                   y=contours[:, 0],
                                   mode='lines',
                                   name='any neuron',
                                   legendgroup='contour',
                                   showlegend=showlegend,
                                   line=dict(
                                       width=2,
                                       color="rgb(172, 233, 71)")),
                      row=1, col=1)
    # calculate the actual boundaries
    y_min = np.min(np.array(y_min))
    y_max = np.max(np.array(y_max))
    x_min = np.min(np.array(x_min))
    x_max = np.max(np.array(x_max))
    for n, contours in enumerate(contours_80):
        if n == 0:
            showlegend = True
        else:
            showlegend = False
        fig.add_trace(go.Scattergl(x=contours[:, 1],
                                   y=contours[:, 0],
                                   mode='lines',
                                   name='80% neurons',
                                   legendgroup='contour',
                                   showlegend=showlegend,
                                   line=dict(
                                       width=2,
                                       color="rgb(214, 131, 41)")),
                      row=1, col=1)
    for n, contours in enumerate(contours_1):
        if n == 0:
            showlegend = True
        else:
            showlegend = False
        fig.add_trace(go.Scattergl(x=contours[:, 1],
                                   y=contours[:, 0],
                                   mode='lines',
                                   name='100% neurons',
                                   legendgroup='contour',
                                   showlegend=showlegend,
                                   line=dict(
                                       width=2,
                                       color="rgb(183, 18, 249)")),
                      row=1, col=1)
    # add x and y axes as a reference for 0 of the distribution plots
    # x-axis
    fig.add_trace(go.Scattergl(x=[x_min, x_max],
                               y=[y_max + 1, y_max + 1],
                               mode='lines+text',
                               name='x_axis_distribution',
                               showlegend=False,
                               text=['', ''],
                               textposition='middle right',
                               line=dict(
                                   width=2,
                                   color="rgb(0, 0, 0)")),
                  row=1, col=1)
    # y-axis
    fig.add_trace(go.Scattergl(x=[x_max + 1, x_max + 1],
                               y=[y_min, y_max],
                               mode='lines',
                               name='x_axis_distribution',
                               showlegend=False,
                               line=dict(
                                   width=2,
                                   color="rgb(0, 0, 0)")),
                  row=1, col=1)
    # add x and y 1 cortical thickness line as a reference for the
    # distribution plots x-axis
    fig.add_trace(go.Scattergl(x=[x_min, x_max],
                               y=[y_max + 2, y_max + 2],
                               mode='lines+text',
                               name='x_axis_distribution',
                               showlegend=False,
                               text=['', ''],
                               textposition='middle right',
                               line=dict(
                                   width=1.5,
                                   color="rgb(0, 0, 0)",
                                   dash='dash')),
                  row=1, col=1)
    # y-axis
    fig.add_trace(go.Scattergl(x=[x_max + 2, x_max + 2],
                               y=[y_min, y_max],
                               mode='lines',
                               name='x_axis_distribution',
                               showlegend=False,
                               line=dict(
                                   width=1.5,
                                   color="rgb(0, 0, 0)",
                                   dash='dash')),
                  row=1, col=1)
    # add text

    fig.add_trace(go.Scattergl(x=[x_max + .5],
                               y=[y_max + 2],
                               mode='lines+text',
                               name='x_axis_distribution',
                               showlegend=False,
                               text=['100%'],
                               textposition='middle right',
                               line=dict(
                                   width=1.5,
                                   color="rgb(255, 255, 255)",
                                   dash='dash')),
                  row=1, col=1)
    fig.add_trace(go.Scattergl(x=[x_max + .5],
                               y=[y_max + 1],
                               mode='lines+text',
                               name='x_axis_distribution',
                               showlegend=False,
                               text=['0%'],
                               textposition='middle right',
                               line=dict(
                                   width=2,
                                   color="rgb(255, 255, 255)")),
                  row=1, col=1)

    # add scalebar
    fig.add_trace(go.Scattergl(x=[1, 3], y=[1 + y_min, 1 + y_min],
                               mode='lines+text',
                               name='10% cortical thickness',
                               showlegend=False,
                               line=dict(
                                   color='rgb(0,0,0)',
                                   width=2)),
                  row=1, col=1)
    # add scalebar text
    fig.add_trace(go.Scattergl(x=[2], y=[1 + y_min],
                               mode='lines+text',
                               name='10% cortical thickness',
                               text=['10%'],
                               textposition='top center',
                               showlegend=False,
                               line=dict(
                                   color='rgb(255,255,255)',
                                   width=2)),
                  row=1, col=1)

    # plot distribution of apical and basal dendrites over Y
    Y_density_apical = np.sum(
        np.flip(np.rot90(np.sum(mean_arrays['apical'], axis=2), 1), axis=0),
        axis=1)
    Y_density_apical[Y_density_apical == 0] = np.nan
    fig.add_trace(go.Scattergl(x=Y_density_apical + x_max + 1,
                               y=np.arange(0, len(Y_density_apical)),
                               mode='lines',
                               name='apical',
                               legendgroup='apical',
                               line=dict(
                                   color='rgb(255, 0, 0)')),
                  row=1, col=1)
    Y_density_basal = np.sum(
        np.flip(np.rot90(np.sum(mean_arrays['basal'], axis=2), 1), axis=0),
        axis=1)
    Y_density_basal[Y_density_basal == 0] = np.nan
    fig.add_trace(go.Scattergl(x=Y_density_basal + x_max + 1,
                               y=np.arange(0, len(Y_density_basal)),
                               mode='lines',
                               name='basal',
                               legendgroup='basal',
                               line=dict(
                                   color='rgb(0, 0, 255)')),
                  row=1, col=1)

    # plot distribution of apical and basal dendrites over X
    X_density_apical = np.sum(
        np.flip(np.rot90(np.sum(mean_arrays['apical'], axis=2), 1), axis=0),
        axis=0)
    X_density_apical[X_density_apical == 0] = np.nan
    fig.add_trace(go.Scattergl(y=X_density_apical + y_max + 1,
                               x=np.arange(0, len(X_density_apical)),
                               mode='lines',
                               showlegend=False,
                               name='apical',
                               legendgroup='apical',
                               line=dict(
                                   color='rgb(255, 0, 0)')),
                  row=1, col=1)
    X_density_basal = np.sum(
        np.flip(np.rot90(np.sum(mean_arrays['basal'], axis=2), 1), axis=0),
        axis=0)
    X_density_basal[X_density_basal == 0] = np.nan
    fig.add_trace(go.Scattergl(y=X_density_basal + y_max + 1,
                               x=np.arange(0, len(X_density_basal)),
                               mode='lines',
                               showlegend=False,
                               name='basal',
                               legendgroup='basal',
                               line=dict(
                                   color='rgb(0, 0, 255)')),
                  row=1, col=1)
    # calculate the statistics of the morphologies in relation to the averaged
    # morhology.
    stat_cols = ['fraction_voxel_represented_at_75percent_average',
                 'fraction_voxel_represented_at_85percent_average',
                 'corrcoef_column_density_apical',
                 'corrcoef_width_density_apical',
                 'corrcoef_density_basal',
                 'corrcoef_density_dendrite']
    stats_cluster = pd.DataFrame(data=None,
                                 index=cellAmpIDs_cluster,
                                 columns=stat_cols)
    # get representation fractions
    mask_75 = copy.deepcopy(np.sum(binary_arrays['dendrites'], axis=0)/
                            len(cellAmpIDs_cluster))
    mask_85 = copy.deepcopy(np.sum(binary_arrays['dendrites'], axis=0) /
                            len(cellAmpIDs_cluster))
    # make all voxels that have dendrites present in 75% or 85% of all cells 1
    mask_75[mask_75 >= 0.75] = 1
    mask_85[mask_85 >= 0.85] = 1
    # for density set nan to 0
    Y_density_apical[np.isnan(Y_density_apical)] = 0
    X_density_apical[np.isnan(X_density_apical)] = 0
    for i in range(len(cellAmpIDs_cluster)):
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'fraction_voxel_represented_at_75percent_average'] = (
            np.sum(binary_arrays['dendrites'][i, :, :, :][mask_75 == 1])/
            np.sum(binary_arrays['dendrites'][i, :, :, :]))
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'fraction_voxel_represented_at_85percent_average'] = (
            np.sum(binary_arrays['dendrites'][i, :, :, :][mask_85 == 1]) /
            np.sum(binary_arrays['dendrites'][i, :, :, :]))
        # generate distribution along column, width and whole grid
        #for column (Y)
        column_dist_cell = np.zeros((len(Y_density_apical)))
        column_index_cell = (np.sum(
            density_data_dict[cellAmpIDs_cluster[i]].groupby('Y')
            ['sum_length_apical']).index).tolist()
        column_dist_cell[column_index_cell] = (np.sum(
            density_data_dict[cellAmpIDs_cluster[i]].groupby('Y')
            ['sum_length_apical'])).tolist()
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'corrcoef_column_density_apical'] = np.corrcoef(
            Y_density_apical, column_dist_cell)[0, 1]  # correlation coefficient
            # of distribution cell against mean distribution(the 'standard')

        # for width (X)
        width_dist_cell = np.zeros((len(X_density_apical)))
        width_index_cell = (np.sum(
            density_data_dict[cellAmpIDs_cluster[i]].groupby('X')
            ['sum_length_apical']).index).tolist()
        width_dist_cell[width_index_cell] = (np.sum(
            density_data_dict[cellAmpIDs_cluster[i]].groupby('X')
            ['sum_length_apical'])).tolist()
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'corrcoef_width_density_apical'] = np.corrcoef(
            X_density_apical, width_dist_cell)[0, 1]  # correlation coefficient
            # of distribution cell against mean distribution(the 'standard')

        # for 3D all dendrites
        dendrites_dist_cell = (density_dataarray_dict[cellAmpIDs_cluster[i]]
            ['dendrites'][mean_arrays['dendrites'] > 0])
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'corrcoef_density_dendrite'] = np.corrcoef(
            mean_arrays['dendrites'][mean_arrays['dendrites'] > 0],
            dendrites_dist_cell)[0, 1]  # correlation coefficient
            # of distribution cell against mean distribution(the 'standard')

        # for 3D basal
        basal_dist_cell = (density_dataarray_dict[cellAmpIDs_cluster[i]]
            ['basal'][mean_arrays['basal'] > 0])
        stats_cluster.loc[cellAmpIDs_cluster[i],
                          'corrcoef_density_basal'] = np.corrcoef(
            mean_arrays['basal'][mean_arrays['basal'] > 0],
            basal_dist_cell)[0, 1]  # correlation coefficient
        # of distribution cell against mean distribution(the 'standard')


    # Update layouts
    fig.update_layout(width=600, height=500)
    if title:
        fig.update_layout(title=title)
    fig.update_layout(paper_bgcolor='rgba(0,0,0,0)',
                      plot_bgcolor='rgba(0,0,0,0)')
    fig.update_layout(yaxis={'scaleanchor': 'x',
                             'showgrid': False,
                             'showline': False,
                             'zeroline': False,
                             'showticklabels': False})
    fig.update_layout(xaxis={'showgrid': False,
                             'showline': False,
                             'zeroline': False,
                             'showticklabels': False})

    fig.update_layout(xaxis4=dict(tick0=0))

    # plot exemplar neuron
    # Left here commented for reference:
    # processNameDict = {1: 'soma',
    #                    2: 'axon',
    #                    3: 'dendrite',
    #                    4: 'apical dendrite'}
    processColorDict = {1: 'rgb(0, 0, 0)',
                        2: 'rgb(0, 0, 0)',
                        3: 'rgb(22, 0, 249)',
                        4: 'rgb(254, 0, 0)'}
    line_thickness_factor = 2
    # In order to only draw only once a segment in the legend make dict
    visible_legend_entry = True
    processDiameterDict = {1: .6, 2: .3, 3: .4, 4: .5}

    cell = Cell(exemplar, df_cell=df_cell, df_slide=df_slide)
    contourData = extractContourData(cell.swc_data, cell.swc_meta)
    contourData = extractsomaData(cell.swc_data, cell.swc_meta, contourData)
    contourData = calc_cortexThickness(contourData, verbose=False)

    # place morphology at soma center = 0,0,0
    data_tree = get_dataTree(cell.swc_data)
    data_tree = data_tree[
        np.logical_or(data_tree['TYPE'] == 3, data_tree['TYPE'] == 4)]
    com = cell.swc_data[np.logical_and(cell.swc_data['P'] == -1, cell.swc_data[
        'TYPE'] == 1)]
    data_tree['X'] = data_tree['X'] - com['X']
    data_tree['Y'] = data_tree['Y'] - com['Y']
    data_tree['Z'] = data_tree['Z'] - com['Z']
    # normalize to cortical thickness
    data_tree['X'] = (data_tree['X'] / contourData['cortexThickness']) * 20
    data_tree['Y'] = (data_tree['Y'] / contourData['cortexThickness']) * 20
    data_tree['Z'] = (data_tree['Z'] / contourData['cortexThickness']) * 20
    # translate to middle of plot
    data_tree['X'] += x_extreme
    data_tree['Y'] += y_extreme
    data_tree['Z'] += z_extreme

    data_soma = cell.swc_data[
        np.logical_and(cell.swc_data['P'] != -1, cell.swc_data[
            'TYPE'] == 1)]
    data_soma['X'] = data_soma['X'] - com['X']
    data_soma['Y'] = data_soma['Y'] - com['Y']
    data_soma['Z'] = data_soma['Z'] - com['Z']
    # normalize to cortical thickness
    data_soma['X'] = (data_soma['X'] / contourData['cortexThickness']) * 20
    data_soma['Y'] = (data_soma['Y'] / contourData['cortexThickness']) * 20
    data_soma['Z'] = (data_soma['Z'] / contourData[
        'cortexThickness']) * 20  # since density plot was 5%
    # translate to middle of plot
    data_soma['X'] += x_extreme
    data_soma['Y'] += y_extreme
    data_soma['Z'] += z_extreme
    # replace location of com
    com['X'] = x_extreme
    com['Y'] = y_extreme
    com['Z'] = z_extreme

    # print soma
    fig.append_trace(
        go.Scattergl(
            x=data_soma['X'],
            y=data_soma['Y'],
            mode='lines',
            name='exemplar: %s' % exemplar,
            legendgroup='exemplar: %s' % exemplar,
            showlegend=visible_legend_entry,
            line={'width': processDiameterDict[1],
                  'color': processColorDict[1]}),
        row=1, col=1)
    visible_legend_entry = False
    # print neuronal processes
    startSegment = list()
    stopSegment = list()
    # the Soma center of Mass can act as the startpoint for all neuronal
    # processes. In that case there is no P=-1 for TYPE:2,3,4 and a extra
    # initial segment need to be added from center of mass to startSegment
    stemIDs = list()
    stemIDs = data_tree['ID'][data_tree['P'] == -1]
    comON = False
    if stemIDs.size < 1:
        comON = True
    for ID, P in list(zip(data_tree['ID'], data_tree['P'])):
        if int(ID) - (int(P)) != 1:
            startSegment.append(int(ID))
    np.array(startSegment)
    stopSegment = np.array(startSegment[1:])
    stopSegment = np.append(stopSegment, data_tree['ID'][len(data_tree) - 1])
    stopSegment = stopSegment

    for start, stop in list(zip(startSegment, stopSegment)):
        data2Plot = ()
        if (comON and data_tree[int(np.where(data_tree['ID'] == start)[0])]
        ['P'] == 1):
            data2Plot = com
        try:
            connectedNodeP = data_tree['P'][int(np.where(data_tree['ID'] ==
                                                         start)[0])]
            data2PlotAp = data_tree[:][int(np.where(data_tree['ID'] ==
                                                    connectedNodeP)[0])]
            if data2Plot:
                data2Plot = np.append(data2Plot, data2PlotAp)
            else:
                data2Plot = data2PlotAp

        except (KeyError, ValueError, TypeError):
            pass
        if data2Plot:
            data2PlotAp = data_tree[:][int(np.where(data_tree['ID'] ==
                                                    start)[0]):
                                       int(np.where(data_tree['ID'] ==
                                                    stop)[0])]
            data2Plot = np.append(data2Plot, data2PlotAp)
        else:
            data2Plot = data_tree[:][int(np.where(data_tree['ID'] ==
                                                  start)[0]):
                                     int(np.where(data_tree['ID'] ==
                                                  stop)[0])]
        process = data_tree['TYPE'][int(
            np.where(data_tree['ID'] == start)[0])]
        fig.append_trace(
            go.Scattergl(
                x=data2Plot['X'],
                y=data2Plot['Y'],
                mode='lines',
                name='exemplar: %s' % exemplar,
                legendgroup='exemplar: %s' % exemplar,
                showlegend=visible_legend_entry,
                line={'width': processDiameterDict[data_tree['TYPE'][int(
                    np.where(data_tree['ID'] == start)[0])]] *
                               line_thickness_factor,
                      'color': processColorDict[process]}),
            row=1, col=1)
        # Shut down the visiblity for this particular process
    # show all other cells too
    cellAmpIDs_2plot = cellAmpIDs_cluster[np.where(cellAmpIDs_cluster !=
                                                   exemplar)]

    for cellAmpID in cellAmpIDs_2plot:
        cell = Cell(cellAmpID, df_cell=df_cell, df_slide=df_slide)
        flip, axial, azimu, polar = rad_dict[cellAmpID]
        contourData = extractContourData(cell.swc_data, cell.swc_meta)
        contourData = extractsomaData(cell.swc_data, cell.swc_meta, contourData)
        contourData = calc_cortexThickness(contourData, verbose=False)
        normalizer = contourData['cortexThickness']
        voxel_multiplier = 20  # voxelsize in this round is 5% of
        # the cortical thickness
        cell = transform_cell(cell, axial, azimu, polar, normalizer,
                              voxel_multiplier)

        # place morphology at soma center = 0,0,0
        data_tree = get_dataTree(cell.swc_data)
        data_tree = data_tree[
            np.logical_or(data_tree['TYPE'] == 3, data_tree['TYPE'] == 4)]
        com = cell.swc_data[np.logical_and(cell.swc_data['P'] == -1,
                                           cell.swc_data[
            'TYPE'] == 1)]
        if rad_dict[cellAmpID][0] == 1:
            # is flipped
            data_tree['X'] *= -1
            com['X'] *= -1
        data_tree['X'] = data_tree['X'] - com['X']
        data_tree['Y'] = data_tree['Y'] - com['Y']
        data_tree['Z'] = data_tree['Z'] - com['Z']
        # translate to middle of plot
        data_tree['X'] += x_extreme
        data_tree['Y'] += y_extreme
        data_tree['Z'] += z_extreme

        data_soma = cell.swc_data[
            np.logical_and(cell.swc_data['P'] != -1, cell.swc_data[
                'TYPE'] == 1)]
        if rad_dict[cellAmpID][0] == 1:
            # is flipped
            data_soma['X'] *= -1
        data_soma['X'] = data_soma['X'] - com['X']
        data_soma['Y'] = data_soma['Y'] - com['Y']
        data_soma['Z'] = data_soma['Z'] - com['Z']

        # translate to middle of plot
        data_soma['X'] += x_extreme
        data_soma['Y'] += y_extreme
        data_soma['Z'] += z_extreme
        # replace location of com
        com['X'] = x_extreme
        com['Y'] = y_extreme
        com['Z'] = z_extreme

        visible_legend_entry = True
        # print soma
        fig.append_trace(
            go.Scattergl(
                x=data_soma['X'],
                y=data_soma['Y'],
                mode='lines',
                name='%s' % cellAmpID,
                legendgroup='%s' % cellAmpID,
                showlegend=visible_legend_entry,
                line={'width': processDiameterDict[1],
                      'color': processColorDict[1]}),
            row=1, col=1)
        visible_legend_entry = False
        # print neuronal processes
        startSegment = list()
        stopSegment = list()
        # the Soma center of Mass can act as the startpoint for all neuronal
        # processes. In that case there is no P=-1 for TYPE:2,3,4 and a extra
        # initial segment need to be added from center of mass to startSegment
        stemIDs = list()
        stemIDs = data_tree['ID'][data_tree['P'] == -1]
        comON = False
        if stemIDs.size < 1:
            comON = True
        for ID, P in list(zip(data_tree['ID'], data_tree['P'])):
            if int(ID) - (int(P)) != 1:
                startSegment.append(int(ID))
        np.array(startSegment)
        stopSegment = np.array(startSegment[1:])
        stopSegment = np.append(stopSegment,
                                data_tree['ID'][len(data_tree) - 1])
        stopSegment = stopSegment

        for start, stop in list(zip(startSegment, stopSegment)):
            data2Plot = ()
            if (comON and data_tree[int(np.where(data_tree['ID'] == start)[0])]
            ['P'] == 1):
                data2Plot = com
            try:
                connectedNodeP = data_tree['P'][int(np.where(data_tree['ID'] ==
                                                             start)[0])]
                data2PlotAp = data_tree[:][int(np.where(data_tree['ID'] ==
                                                        connectedNodeP)[0])]
                if data2Plot:
                    data2Plot = np.append(data2Plot, data2PlotAp)
                else:
                    data2Plot = data2PlotAp

            except (KeyError, ValueError, TypeError):
                pass
            if data2Plot:
                data2PlotAp = data_tree[:][int(np.where(data_tree['ID'] ==
                                                        start)[0]):
                                           int(np.where(data_tree['ID'] ==
                                                        stop)[0])]
                data2Plot = np.append(data2Plot, data2PlotAp)
            else:
                data2Plot = data_tree[:][int(np.where(data_tree['ID'] ==
                                                      start)[0]):
                                         int(np.where(data_tree['ID'] ==
                                                      stop)[0])]
            process = data_tree['TYPE'][int(
                np.where(data_tree['ID'] == start)[0])]
            fig.append_trace(
                go.Scattergl(
                    x=data2Plot['X'],
                    y=data2Plot['Y'],
                    mode='lines',
                    name='%s' % cellAmpID,
                    legendgroup='%s' % cellAmpID,
                    showlegend=visible_legend_entry,
                    line={'width': processDiameterDict[data_tree['TYPE'][int(
                        np.where(data_tree['ID'] == start)[0])]] *
                                   line_thickness_factor,
                          'color': processColorDict[process]}),
                row=1, col=1)


    return fig, stats_cluster
