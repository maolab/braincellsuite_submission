"""Helper functions for managing data.

Functions:
    - change_filenames: Replace a particular part of the filename by a
                        new part.
    - diff: calculates difference.
    - euclidian: calculates distance between two points.
    - cross: cross-product of two 3d points.
    - dist2line: orthogonal distance from a point to a line.
    - rdp: Ramer-Douglas-Peucker recursive algorithm for point reduction.
        Contributed by Michael Muniak 2019
"""
import os
import re
import pandas as pd
import numpy as np
from numpy.linalg import eig, inv


def change_filenames(old_part, new_part, start_path):
    """Replace a particular part of the filename by a new part.

    Args:
        - old_part: string to be replaced. Use backslash to escape regular
                   expressions.
        - new_part: string used to replace with.
        - start_path: full path to folder that need searching/replacing.

    Returns:
        - changes all files and folders containing old_part string.
    """
    name_map = {old_part: new_part}
    for root, dirs, files in os.walk(start_path):
        for f in files:
            print(f)
            for name in name_map.keys():
                if re.search(name, f) is not None:
                    new_name = re.sub(name, name_map[name], f)
                    try:
                        os.rename(os.path.join(root, f),
                                  os.path.join(root, new_name))
                    except OSError:
                        print("No such file or directory!")
            for d in dirs:
                for name in name_map.keys():
                    if re.search(name, d) is not None:
                        new_name = re.sub(name, name_map[name], d)
                        try:
                            os.rename(os.path.join(root, d),
                                      os.path.join(root, new_name))
                        except OSError:
                            print("No such directory!")


def diff(a, b):
    """ Returns vector a -> b (e.g., b-a).
    """
    return [(x[1] - x[0]) for x in zip(a, b)]


def euclidian(a, b=None):
    """ Euclidian distance between two n-d points.
    """
    if b is None:
        b = [0] * len(a)
    return sum(x ** 2 for x in diff(a, b)) ** 0.5


def cross(a, b):
    """ Cross-product of two 3d points.
    """
    return [a[1] * b[2] - a[2] * b[1],
            a[2] * b[0] - a[0] * b[2],
            a[0] * b[1] - a[1] * b[0]]


def dist2line(a, b1, b2):
    """ Orthogonal distance from a point a to a line defined by points b1 & b2.
        Works for both 2d and 3d points.
    """
    c = diff(b1, b2)
    d = diff(a, b1)
    if len(a) == 2:
        return abs(c[0] * d[1] - d[0] * c[1]) / euclidian(b1, b2)
    else:
        return euclidian(cross(c, d)) / euclidian(b1, b2)


def rdp(p, tol=0.5):
    """ Ramer-Douglas-Peucker recursive algorithm for point reduction.
        http://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_
        algorithm
        p is a list of 2d or 3d point tuples (e.g., [(0,1), (2,3)]).
        tol is the orthogonal distance over which point elimination is allowed.
    """
    dmax = 0
    idx = 0
    for i in range(1, len(p) - 1):
        d = dist2line(p[i], p[0], p[-1])
        if (d > dmax):
            idx = i
            dmax = d
    if (dmax > tol):
        # Recursive calls.
        rec1 = rdp(p[0:idx + 1], tol)
        rec2 = rdp(p[idx:], tol)
        # Build result list.
        res = rec1[0:-1] + rec2[0:]
    else:
        res = [p[0], p[-1]]
    return res


def rdp_from_Dataframe(df, use_columns, tol):
    """Takes data from dataframe for point reduction and saves it back to
    dataframe.

    Args:
        - df: pandas dataframe, should contain at least 2 columns with numbers.
        - use_columns: list of column names, should be present in df.
        - tol: orthogonal distance that is used to eliminate points that are
               at shorter distance.

    Returns:
        - df: updated and reduced version of the dataframe * could put some
        effort in to actually maintain other information related to the rows
        that are kept.
    """
    if not isinstance(use_columns, list):
        print('use_columns is not a list, stopped function')
        return df
    for column in use_columns:
        if not column in df.columns:
            print(str(column) + ' is not in df, stopped function')
            return df
    subset = df[use_columns]
    tuples = [tuple(x) for x in subset.values]
    tuples = rdp(tuples, tol=tol)
    df = pd.DataFrame(tuples, columns=use_columns)
    return df


def rdp_from_waveform(waveform, time, tol):
    """Takes data from waveform w/o time for point reduction and saves it back
    to waveform and time.

    Args:
        - waveform: numpy array of numbers.
        - time: numpy array of numbers of same length as waveform. If None a
        numpy array will be generated
        - tol: orthogonal distance that is used to eliminate points that are
               at shorter distance.

    Returns:
        - time: updated and reduced version of the waveform time
        - waveform: updated and reduced version of the waveform

    """
    if not isinstance(waveform, np.ndarray):
        print('waveform is not a numpy array, stopped function')
        return waveform, time
    if time is None:
        time = np.arange(0, len(waveform))
    if len(time) != len(waveform):
        print('time array was not the same as the waveform, used a arbitrary '
              'time instead')
        time = np.arange(0, len(waveform))
    subset = pd.DataFrame({'time': time,
                           'waveform': waveform})
    tuples = [tuple(x) for x in subset.values]
    tuples = rdp(tuples, tol=tol)
    df = pd.DataFrame(tuples, columns=['time', 'waveform'])
    return df.time, df.waveform


# The following parts are from stackoverflow:
# https://stackoverflow.com/questions/52818206/fitting-an-ellipse-to-a-set-of-2-d-points

def fitEllipse(x,y):
    x = x[:, np.newaxis]
    y = y[:, np.newaxis]
    D = np.hstack((x*x, x*y, y*y, x, y, np.ones_like(x)))
    S = np.dot(D.T, D)
    C = np.zeros([6,6])
    C[0,2] = C[2,0] = 2; C[1,1] = -1
    E, V = eig(np.dot(inv(S), C))
    n = np.argmax(np.abs(E))
    a = V[:,n]
    return a


def ellipse_center(a):
    b, c, d, f, g, a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    num = b*b-a*c
    x0 = (c*d-b*f)/num
    y0 = (a*f-b*d)/num
    return np.array([x0,y0])


def ellipse_axis_length(a):
    b, c, d, f, g, a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    up = 2*(a*f*f+c*d*d+g*b*b-2*b*d*f-a*c*g)
    down1 = (b*b-a*c)*((c-a)*np.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
    down2 = (b*b-a*c)*((a-c)*np.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
    res1 = np.sqrt(up/down1)
    res2 = np.sqrt(up/down2)
    return np.array([res1, res2])


def ellipse_angle_of_rotation(a):
    b, c, d, f, g, a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    if b == 0:
        if a > c:
            return 0
        else:
            return np.pi/2
    else:
        if a > c:
            return np.arctan(2*b/(a-c))/2
        else:
            return np.pi/2 + np.arctan(2*b/(a-c))/2

######
