"""This module contains all variables used as globals

For use in other modules first import:
from brainCellSuite import bcs_global

and refer in code as
bcs_global.path_db
"""

import configparser
import os

config = configparser.ConfigParser()
ini_file = os.path.join(os.path.dirname(__file__), 'braincellsuite.ini')
config.read(ini_file)

path_database = config.get('DATABASE_PATHS', 'path_database')
path_ephys_data = config.get('DATABASE_PATHS', 'path_ephys_data')
path_ephys_images = config.get('DATABASE_PATHS', 'path_ephys_images')
path_ephys_contours = config.get('DATABASE_PATHS', 'path_ephys_contours')
path_morph_data = config.get('DATABASE_PATHS', 'path_morph_data')
path_qc_ephys_data = config.get('DATABASE_PATHS', 'path_qc_ephys_data')
path_qc_morph_data = config.get('DATABASE_PATHS', 'path_qc_morph_data')
path_ephys_analysis = config.get('DATABASE_PATHS', 'path_ephys_analysis')
path_morph_analysis = config.get('DATABASE_PATHS', 'path_morph_analysis')
path_morph_ephys_analysis = config.get('DATABASE_PATHS', 'path_morph_ephys_analysis')
path_input_data = config.get('DATABASE_PATHS', 'path_input_data')
path_input_analysis = config.get('DATABASE_PATHS', 'path_input_analysis')
path_website = config.get('DATABASE_PATHS', 'path_website')

filename_cell_db = config.get('DATABASE_FILENAMES', 'filename_cell_db')
filename_slide_db = config.get('DATABASE_FILENAMES', 'filename_slide_db')

filebase_ephys_feature = config.get('DATABASE_FILEBASES',
                                    'filebase_ephys_feature')
filebase_ephys_feature_sweeps = config.get('DATABASE_FILEBASES',
                                           'filebase_ephys_feature_sweeps')
filebase_morph_qc = config.get('DATABASE_FILEBASES', 'filebase_morph_qc')
filebase_morph_feature = config.get('DATABASE_FILEBASES',
                                    'filebase_morph_feature')