# brainCellSuite-dev

Package for importing, wrangling, analyzing, and visualization of brain cell data (electrophysiology, morphology, imaging data). This version is used for the analysis of the insular cell atlas as submitted for publication: Jongbloets*, Chen*, Gingerich#, Muniak#, Mao.